set serveroutput on;
-- legger inn alle prodedyrer
@@oppgraderingsfunksjoner.sql
@@primus60.sql
@@primus61.sql

-- kj�rer prosedyre som skal h�ndtere oppgradering
declare
	resultat varchar2(1000 char);
Begin
	resultat := oppgraderPrimus(60,61);

	if resultat = 'OK' then
		-- oppgradering uten feil, s� da settes riktig Primus versjon og�s.
		UPDATE ENVIRONMENT SET VERDI = '6' WHERE VARIABEL = 'Major';
		UPDATE ENVIRONMENT SET VERDI = '1' WHERE VARIABEL = 'Minor';
		update environment set verdi = '0' where variabel = 'Release';
    dbms_output.put_line('-- OK --');
	else
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
	end if;
exception
	when others then
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
end;
/
