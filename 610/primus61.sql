create or replace function primus61(jobbnummer out number, jobnummer_in number) 
	return varchar2
	AUTHID CURRENT_USER
	is	
begin
	dbms_output.enable;

	begin
    jobbnummer := 1;
    
    jobbnummer := 7;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus61 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(61, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
