create or replace function primus16(jobbnummer out number, jobnummer_in number) 
	return varchar2
	AUTHID CURRENT_USER
	is	
begin
	dbms_output.enable;

	/*
	 13:30, 14:30, 15:30 of Historik dates 	
	*/

	begin
     jobbnummer:=1;
 	if jobnummer_in <= jobbnummer then 	 
		 EXECUTE IMMEDIATE (' create or replace 
		 FUNCTION CONVERTDATE(DATEINPUT DATE, sdato varchar2) RETURN VARCHAR2 AS DATEOUTPUT VARCHAR2(10);
			  TEMPHOUR  VARCHAR2(10);
			  TEMPMINT  VARCHAR2(10); 
			  DMYFORMAT VARCHAR2(10):=''DD.MM.YYYY'';
			  myFormat  VARCHAR2(10):=''MM.YYYY''; 
			 BEGIN
			IF INSTR(SDATO,''-'', 1, 1)>0 THEN   DATEOUTPUT := SDATO;
			 ELSE
			  begin     
				 temphour:=to_Char(dateinput,''HH24'');
				 Tempmint:=To_Char(Dateinput,''MI'');
				 Select Value Into Dmyformat From Nls_Session_Parameters Where Parameter=''NLS_DATE_FORMAT''; 
				
				 If Dmyformat =''DD.MM.RRRR'' Then Myformat := ''MM.RRRR'';
				 Else  Myformat := ''RRRR-MM'';
				 End If;    
			  
				   If (temphour=15 and tempmint=30)  then     dateoutput:=to_Char(dateinput, dmyformat);    
				   else if(temphour=14 and tempmint=30) then    dateoutput:=to_Char(dateinput, myformat);
				   else dateoutput:=to_Char(dateinput,''YYYY''); 
				   End If;
				   END IF;        
			 END;
		    End if;  
           RETURN DATEOUTPUT;
         END CONVERTDATE;');
      End if;
	 

	 jobbnummer:=2;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus16 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(16, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
