CREATE OR REPLACE FUNCTION primus27(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
    Prioritet
    */

    BEGIN
      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table superobjekt_prioritet_l
          ( prioritetid number(3,0) not null
          , prioritettekst varchar2(25 char) not null
          , superobjekttypeid number(3,0) not null
          , CHECKFORDELETE VARCHAR2(1 CHAR)
          , MERKE VARCHAR2(1 CHAR) default ''Y'' not null
          , UUID VARCHAR2(40 CHAR) not null
          , HID_OPPRETTET NUMBER(8,0)
          , HID_SLETTET NUMBER(8,0)
          , AUTORITET VARCHAR2(10 CHAR)
          , AUTORITET_STATUS VARCHAR2(1 CHAR)
          , AUTORITET_DATASET VARCHAR2(100 CHAR)
          , AUTORITET_KILDE VARCHAR2(100 CHAR)
          , constraint pk_superobjekt_prioritet_l primary key (prioritetid)
          , constraint fk_pri_sotypeid_sotype_l foreign key (superobjekttypeid) REFERENCES superobjekttype_l(superobjekttypeid)
        ) tablespace primusdt';

      END IF;
      jobbnummer := 2;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table superobjekt_prioritet_h
          ( objid number(8,0) not null
          , hid number(8,0) not null
          , prioritetid number(3,0) not null
          , kommentar varchar2(4000 char)
          , constraint pk_superobjekt_prioritet_h primary key (objid, hid)
          , constraint fk_so_prioritet_h_objid_so foreign key (objid) references superobjekt(objid)
          , constraint fk_so_prioritet_h_hid_hendelse foreign key (hid) references hendelse(hid)
          , constraint fk_so_prioritet_h_pid_prio_l foreign key (prioritetid) references superobjekt_prioritet_l(prioritetid)
        ) tablespace primusdt';
      END IF;
      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'alter table superobjekt add
          ( constraint fk_superobjekt_HID_prioritet foreign key (objid, hid_prioritet)
              references SUPEROBJEKT_prioritet_H(objid,hid))';
      END IF;
      jobbnummer := 4;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(1, ''Lav'', 3, getoracleuuid())');
      ELSE
        executeScript(jobbnummer,jobnummer_in,'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(1, ''Låg'', 3, getoracleuuid())');
      END IF;
      jobbnummer := 5;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(2, ''Medium'', 3, getoracleuuid())');
      ELSE
        executeScript(jobbnummer,jobnummer_in,'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(2, ''Medel'', 3, getoracleuuid())');
      END IF;
      jobbnummer := 6;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(3, ''Høy'', 3, getoracleuuid())');
      ELSE
        executeScript(jobbnummer,jobnummer_in,'insert into superobjekt_prioritet_l (prioritetid, prioritettekst, superobjekttypeid, uuid)
                        values(3, ''Hög'', 3, getoracleuuid())');
      END IF;
      jobbnummer := 7;
      COMMIT;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus27 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(27, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/