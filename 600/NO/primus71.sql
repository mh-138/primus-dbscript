create or replace function primus71(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table primus.BILDE_TEKST rename to bilde_tekst_h');
    end if;
    
    jobbnummer := 2;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 71, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then
        executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table primus.bilde_tekst_h add ( sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_BILDETEKST_H_SPRKKD not null )');
      else
        executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table primus.bilde_tekst_h add ( sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_BILDETEKST_H_SPRKKD not null )');
      end if;
    end if;
		
    jobbnummer := 3;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table bilde_tekst_h drop primary key cascade' );
    end if;

    jobbnummer := 4;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table bilde_tekst_h drop constraint CK_BILDETEKST_HID_NOT_NULL' );
    end if;

    jobbnummer := 5;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      execute immediate ( 'drop index PK_BILDETEKST' );
    end if;

    jobbnummer := 6;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bilde_tekst_h add 
                                ( constraint PK_BILDE_TEKST_H 
                                  primary key ( bildeid, sprakkode, hid ) )' );
    end if;
    
    jobbnummer := 7;
    if jobbnummer_inn <= jobbnummer then  
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bilde_tekst_h add 
                                ( constraint CK_BILDETEKST_HID_NOT_NULL 
                                       check ( hid is not null ) )');
    end if;
    
    jobbnummer := 8;
    if jobbnummer_inn <= jobbnummer then  
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'create table bilde_tekst ( bildeid number( 8 )
                         , sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_BILDETEKST_SPRKKD not null
                         , hid_bildetekst number( 9 ) 
                         , constraint PK_BILDETEKST primary key ( bildeid, sprakkode )
                         , constraint FK_BILDE_TEKST_HIDBLDTXT foreign key ( hid_bildetekst ) references hendelse ( hid )
                         , constraint FK_BILDE_TEKST_BILDE foreign key ( bildeid ) references bilde ( bildeid )
                         , constraint FK_BILDE_TEKST_BLDT_TXT_H foreign key ( bildeid, sprakkode, hid_bildetekst ) references bilde_tekst_h( bildeid, sprakkode, hid ))';
      else
        execute immediate 'create table bilde_tekst ( bildeid number( 8 )
                         , sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_BILDETEKST_SPRKKD not null
                         , hid_bildetekst number( 9 ) 
                         , constraint PK_BILDETEKST primary key ( bildeid, sprakkode )
                         , constraint FK_BILDE_TEKST_HIDBLDTXT foreign key ( hid_bildetekst ) references hendelse ( hid )
                         , constraint FK_BILDE_TEKST_BILDE foreign key ( bildeid ) references bilde ( bildeid )
                         , constraint FK_BILDE_TEKST_BLDT_TXT_H foreign key ( bildeid, sprakkode, hid_bildetekst ) references bilde_tekst_h( bildeid, sprakkode, hid ))';
      end if;
    end if;
    
    jobbnummer := 9;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bilde_tekst_h drop constraint FK_BILDETEKST_BILDE_BILDEID' );
    end if;

    jobbnummer := 10;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      execute immediate 'insert into bilde_tekst ( bildeid, hid_bildetekst ) select bildeid, Hid_Bildetekst from bilde';
    end if;
    
    jobbnummer := 11;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 71, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bilde_tekst_h add 
                                ( constraint FK_BILDETEKST_BILDE_BILDEID 
                                     foreign key ( bildeid, sprakkode ) 
                                  references bilde_tekst ( bildeid, sprakkode ) )');
    end if;
    
    commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus71 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(71, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      