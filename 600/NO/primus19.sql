create or replace function primus19(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is

begin
  dbms_output.enable;
  
  begin
	jobbnummer:=1;
	 executeScript(jobbnummer,jobnummer_in,'ALTER TABLE SUPEROBJEKT ADD (SUPEROBJEKTTYPEID number(3,0))');
    
	jobbnummer:=2;
	  executeScript(jobbnummer,jobnummer_in,'UPDATE SUPEROBJEKT S SET S.SUPEROBJEKTTYPEID = (SELECT SL.SUPEROBJEKTTYPEID FROM SUPEROBJEKTTYPE_L  SL WHERE SL.BESKRIVELSE = S.SO_TYPE)');
	  
	jobbnummer:=3;
	  executeScript(jobbnummer,jobnummer_in,'ALTER TABLE SUPEROBJEKT ADD (constraint FK_SUPEROBJEKT_TYPEID foreign key ( SUPEROBJEKTTYPEID )  references SUPEROBJEKTTYPE_L(SUPEROBJEKTTYPEID))');
    
    jobbnummer:= 4;
      executeScript(jobbnummer,jobnummer_in, 'ALTER TABLE PUBLISERBARTEKST_H drop constraint FK_PUBLISERBARTEKST_HOBJID');
	 
	jobbnummer:= 5;
      executeScript(jobbnummer,jobnummer_in, 'ALTER TABLE PUBLISERBARTEKST_H ADD constraint FK_PUBLISERBARTEKST_H_ADMHND foreign key ( OBJID)  references SUPEROBJEKT(OBJID)'); 
	 
  
	--innspill fra Vallby friluftsmuseum.
	jobbnummer:= 15;	
		  executeScript(jobbnummer,jobnummer_in, 'alter table beskrivelse_h modify beskrivelse varchar2(4000 char)');
    jobbnummer:= 16;	
		  executeScript(jobbnummer,jobnummer_in, 'alter table datering_h modify grunnlag varchar2(4000 char)');
	jobbnummer:= 17;
      commit;
	  
  
	jobbnummer := 20;
	  commit;
	jobbnummer := 21;  
         return 'OK';		   
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('primus19 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(19, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/
