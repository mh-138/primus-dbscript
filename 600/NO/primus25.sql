create or replace function primus25(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
    
	/*
    2013.09.27:
    Views for getting hierarchy information for concepts that supports hierarchies.

    ' � ' as a separator fails against ipimp database. Probably because some names there contains space.
    Use '�' instead. (avoid space)

	*/

	/*
	2013.09.27:
	Procedure for update of OBJEKT_FELTER_TABLE
	*/
	
	/*
     Etikett	
	 Oversettelse
	*/

	begin
	
	jobbnummer:=1;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace force view klassifikasjon_liste 
		  (id,  parent_id,  hid_slettet, m_path, uuid, autoritet, kode, kl_system, navn, f_path, lvl) as
          select kk.id, kk.parent_id, kk.hid_slettet, kk.m_path , kk.uuid, 
		  kk.autoritet, kk.kode, kk.kl_system,  kk.navn, sys_connect_by_path(kk.navn , ''�''), level
          from klassifikasjon_l kk
          where kk.hid_slettet is null
          connect by nocycle prior kk.id = kk.parent_id';        
      End if;
	  
	  jobbnummer:=2;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace force view materiale_liste 
		  (id,  parent_id,  hid_slettet, m_path, uuid, navn, beskrivelse, f_path, lvl, filterfelt) as
          select ml.id, ml.parent_id, ml.hid_slettet, ml.m_path , ml.uuid, 
		  ml.navn, ml.beskrivelse, sys_connect_by_path(ml.navn , ''�''), level, ml.filterfelt
          from materiale_l ml
          where ml.hid_slettet is null
          connect by nocycle prior ml.id = ml.parent_id';
       
      End if;
	  
	    jobbnummer:=3;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace force view teknikk_liste 
		  (id,  parent_id,  hid_slettet, m_path, uuid, navn, beskrivelse, f_path, lvl) as
          select tl.id, tl.parent_id, tl.hid_slettet, tl.m_path , 
		  tl.uuid, tl.navn, tl.beskrivelse, sys_connect_by_path(tl.navn , ''�''), level
          from teknikk_l tl
          where tl.hid_slettet is null
          connect by nocycle prior tl.id = tl.parent_id';
  
      End if;
	        jobbnummer:=4;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace force view dekor_teknikk_liste 
		  (id,  parent_id,  hid_slettet, m_path, uuid, navn, beskrivelse, f_path, lvl) as
          select dt.id, dt.parent_id, dt.hid_slettet, dt.m_path , 
		  dt.uuid, dt.navn, dt.beskrivelse, sys_connect_by_path(dt.navn , ''�''), level
          from dekorteknikk_l dt
          where dt.hid_slettet is null
          connect by nocycle prior dt.id = dt.parent_id';
        
      End if;
	  jobbnummer:=5;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace force view behandlingstype_liste 
		  (behandlingstypeid,  parent_id,  hid_slettet, m_path, uuid, navn, f_path, lvl, filterfeltid) as
          select bt.behandlingstypeid, bt.parent_id, bt.hid_slettet, 
		  bt.m_path , bt.uuid, bt.navn, sys_connect_by_path(bt.navn , ''�''), level, bt.filterfeltid
          from behandlingstype_l bt
          where bt.hid_slettet is null
          connect by nocycle prior bt.behandlingstypeid = bt.parent_id';
       
      End if;
	   jobbnummer:=6;
      if jobnummer_in <= jobbnummer then
        EXECUTE IMMEDIATE
          'create or replace
          PROCEDURE OBJEKT_FELTER_TABLE_update (pobjid IN NUMBER, ptext IN String, pfelt_type IN NUMBER) IS
          cursor OBFT(in_objid in Number, in_Felt_type in Number) is
            select OBJID, FELT_TYPE from OBJEKT_FELTER_TABLE 
			        where objid = in_objid and felt_type = in_Felt_type and slettet=0;
          ob Number;
          ft Number;
          BEGIN
            OPEN  OBFT(pobjid, pfelt_type);
            FETCH OBFT into ob, ft;
            IF OBFT%FOUND THEN
              UPDATE  OBJEKT_FELTER_TABLE
              SET slettet = 1  WHERE (OBJID = pobjid) AND (FELT_TYPE = pfelt_type) AND Slettet=0;
            End if;
            if ptext IS NOT NULL then
              INSERT INTO OBJEKT_FELTER_TABLE (OBJID, VERDI, felt_type, slettet) VALUES (pobjid, ptext, pfelt_type, 0);
            End if;

          END OBJEKT_FELTER_TABLE_update;';
      End if;
	
	
		jobbnummer:=10;
		if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE TABLE OVERSETTELSER
          (	
		    NOKKEL VARCHAR2(200 CHAR) ,
            SPRAK VARCHAR2(5 CHAR) ,
            VERDI VARCHAR2(4000 CHAR) ,
            ENDRET DATE ,
            CONSTRAINT OVERSETTELSER_PK PRIMARY KEY (NOKKEL, SPRAK)
          ) TABLESPACE PRIMUSDT ';

    End if;
	      jobbnummer:=11;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE TABLE OVERSETTELSER_HISTORIKK
          (	ID NUMBER(6,0) ,
            NOKKEL VARCHAR2(200 CHAR) ,
            SPRAK VARCHAR2(5 CHAR) ,
            ORIGINAL_VERDI VARCHAR2(4000 CHAR) ,
            NY_VERDI VARCHAR2(4000 CHAR) ,
            ENDRET DATE ,
            CONSTRAINT OVERSETTELSER_HISTORIKK_PK PRIMARY KEY (ID),
            CONSTRAINT OVERSETTELSER_HISTORIKK_O_FK1 FOREIGN KEY (NOKKEL, SPRAK) REFERENCES OVERSETTELSER (NOKKEL, SPRAK)
          ) TABLESPACE PRIMUSDT ';
  
    End if;
	    jobbnummer:=12;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE SEQUENCE  OVERSETTELSER_HISTORIKK_SEQ  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE';
    End if;
	
	jobbnummer:=100;
	 commit;
	return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus25 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(25, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/