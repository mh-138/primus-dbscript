create or replace function primus34(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Behandlingsforslag tabell
	*/

	begin
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'CREATE TABLE behandlingsforslag
        (	objid NUMBER(8,0)
        , materialbruk VARCHAR2(4000 char)
        , CONSTRAINT pk_behandlingsforslag PRIMARY KEY (objid)
        , CONSTRAINT fk_behandlingsf_admh_objid foreign key (objid)
        references adm_hendelse (objid) enable
        ) TABLESPACE primusdt';
    End if;
	
	if jobnummer_in <= jobbnummer then
		declare
			  nhid number;			 
		begin
				EXECUTE IMMEDIATE 'select hidseq.nextval from dual' INTO nhid;
				EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
				EXECUTE IMMEDIATE 'INSERT INTO ANALYSETYPE_L (TYPEID, NAVN, M_PATH, MERKE, HID_OPPRETTET, UUID) 	
                 VALUES (1, ''VISUELL ELLER OPTISK UNDERSØKELSE'', ''1/'', ''Y'', :a, GETORACLEUUID() )' using nhid;
				EXECUTE IMMEDIATE 'INSERT INTO ANALYSETYPE_L (TYPEID, NAVN, M_PATH, MERKE, HID_OPPRETTET, UUID) 
                 VALUES (2, ''Mekanisk undersøkelse'', ''2/'', ''Y'', :a, GETORACLEUUID())' using nhid;
				EXECUTE IMMEDIATE 'INSERT INTO ANALYSETYPE_L (TYPEID, NAVN, M_PATH, MERKE, HID_OPPRETTET, UUID) 	
                 VALUES(3, ''KJEMISK UNDERSØKELSE'',''3/'', ''Y'', :a, GETORACLEUUID())' using nhid;
				EXECUTE IMMEDIATE 'INSERT INTO ANALYSETYPE_L (TYPEID, NAVN, M_PATH, MERKE, HID_OPPRETTET, UUID)  
                 VALUES(4, ''Behandlingstest'', ''4/'', ''Y'', :a, GETORACLEUUID())' using nhid;
				

		  commit;		
		end;		
	End if;

	
		jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus34 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(34, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/