--- skade - legges inn på SUPEROBJEKT_nivå.  SUPEROBJEKTTYPE  'DAMAGE';
--- skal lagre stedet der den administrative hendelsen er utvørt. Knytter seg mot plasseringsmatrisa for museet
--- legger inn kode for å lagre anbefaling på en adm hendelse
--- da skade skal benytte obj_beskrivelse_h så må constraint fra obj_beskrivelse_h til objekt flyttes til superobjekt.



  -- tabell for Kunst historikk FELTER tilddligere OBJEKT_FELTER_TABLE
        --- obj_produksjonsdatering_h 
		--- obj_produksjonshistorikk_h
		--- obj_proveniensdatering_h 
		--- obj_provenienshistorikk_h	
		--- obj_opplagsopplysninger_h
		--- obj_stiltekst_h
		
		
-- kommentar - legges inn i adm_hendelse. For å få hierarki inn i adm_hendelse så benyttes parent_id og m_path

  -- Drop table tilstand_h   EXECUTE IMMEDIATE 'drop table primus.tilstand_h;';
       -- Drop superobjekt.so_type
  
    
      -- Drop '*_old' tables
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.AKS_ANDREOPPLEKST_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.AKS_ANDREOPPLINT_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.AKS_HISTORIKK_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.DOKUMENT_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.FRIMBILDER_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.OBJ_BESKRIVELSE_H_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.OBJ_HISTORIKK_H_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.OBJ_MOTIV_H_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.OPPL_H_OLD;';
      EXECUTE IMMEDIATE 'DROP TABLE PRIMUS.OPPLINTERN_H_OLD;';
	  
 --- ADM_HENDELSETYPE_L	  
   Typeid brukt for fastkoding 1-99
     TYPEID           Tekst    
	   9             Behandling
	   11            Tilstandsvurdering
	   18            Observasjon
	   19            Oppgave
	   20            Kommentar
	   21            Annotering
	   22            Analyse
	   23
	   
	   
alle andre typeid 100 og oppover
    
