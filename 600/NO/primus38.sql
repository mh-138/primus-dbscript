create or replace function primus38(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
     check if Column DIMUKODE
	*/

	begin

  
	jobbnummer:=1;	
	if jobnummer_in <= jobbnummer then
     BEGIN
      EXECUTE IMMEDIATE 'ALTER TABLE JURPERSON ADD (DIMUKODE VARCHAR2(50 CHAR))';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -1430 THEN
           NULL; -- suppresses ORA-01430 exception
        ELSE
         RAISE;
        END IF;
	 End;		
	END IF; 
	jobbnummer:=2;	
	if jobnummer_in <= jobbnummer then
     BEGIN
      EXECUTE IMMEDIATE 'ALTER TABLE OBJEKT ADD (DIMUKODE VARCHAR2(50 CHAR))';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -1430 THEN
           NULL; -- suppresses ORA-01430 exception
        ELSE
         RAISE;
        END IF;
	 End;		
	END IF; 
	
	jobbnummer:=3;	
	if jobnummer_in <= jobbnummer then
     BEGIN
      EXECUTE IMMEDIATE 'ALTER TABLE MAPPE ADD (DIMUKODE VARCHAR2(50 CHAR))';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -1430 THEN
           NULL; -- suppresses ORA-01430 exception
        ELSE
         RAISE;
        END IF;
	 End;		
	END IF; 
		
	jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus38 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(38, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	
