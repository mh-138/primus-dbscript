create or replace function primus30(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Annotering
	*/

	begin
		
     jobbnummer:=1; 
	  executeScript(jobbnummer,jobnummer_in,'alter table Bilde Add(Objid Number(8,0))');
	  
	 jobbnummer:=2;
       executeScript(jobbnummer,jobnummer_in,'alter table Bilde add Constraint FK_BILDE_OBJID foreign key(OBJID) References SUPEROBJEKT(OBJID)');
	   
     jobbnummer:=3;  
	   commit;
	   
     jobbnummer:=4;  
		if jobnummer_in <= jobbnummer then
		 EXECUTE IMMEDIATE ' 
		 DECLARE
			seq number := 0;       
		  BEGIN
				FOR i in (SELECT bildeid FROM bilde WHERE objid is null order by bildeid)
				LOOP          
				  select objidseq.nextval into seq from dual;
				  insert into superobjekt(objid, superobjekttypeid, SO_TYPE) values(seq, 5, ''BILDE'');
				  Update Bilde Set Objid = seq Where Bildeid = I.Bildeid;
				End Loop;
				commit;
		  END;';
		End if;  
		
   jobbnummer:=5;   
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table adm_hend_annotering
          ( objid NUMBER(8,0) NOT NULL ENABLE
          , hid NUMBER(8,0) NOT NULL ENABLE
          , nr NUMBER(6,0) NOT NULL ENABLE
          , objid_siste NUMBER(8,0)
          , rekkefolge NUMBER(6,0) NOT NULL ENABLE
          , uuid VARCHAR2(40 CHAR) NOT NULL ENABLE
          , x1 FLOAT(20), x2 FLOAT(20)
          , y1 FLOAT(20), y2 FLOAT(20)
          , a_type varchar2(20 Char)
          , beskrivelse varchar2(1000 char)
          , blob BLOB
          , farge VARCHAR2(10 BYTE) DEFAULT ''#FFFFFF''
          , CONSTRAINT PK_ADM_HEND_ANNOTRNG PRIMARY KEY (objid, hid, nr)
          , CONSTRAINT FK_ADM_HEND_ANNOTRNG_OBJID FOREIGN KEY (objid) REFERENCES ADM_HENDELSE (objid)
          , CONSTRAINT FK_ADM_HEND_ANNOTRNG_OBJIDS FOREIGN KEY (objid_siste) REFERENCES ADM_HENDELSE (objid)
          , CONSTRAINT FK_ADM_HEND_ANNOTRNG_HENDELSE FOREIGN KEY (hid) REFERENCES HENDELSE (hid) 
        ) tablespace primusdt';     
    End if;
	
	  jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus30 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(30, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/