create or replace function primus58(jobbnummer out number, jobbnummer_inn number)
	return varchar2
	AUTHID CURRENT_USER
	is
  newHID number;
begin
	dbms_output.enable;

  -- legger inn nye tabeller for lagring av felter i bygningsmodulen
  
  begin
    jobbnummer := 1;
    
    select hidseq.nextval into newhid from dual;
    
    insert into hendelse 
      (hid, dato, signid)
    values
      (newhid, sysdate, 1);
    
    SYS.DBMS_OUTPUT.PUT_LINE('--- Script 58 jobber med HID = ' || to_char( newHID ) );

    
    jobbnummer := 2;
    
    
    if jobbnummer_inn <= jobbnummer then 
      executeCreateTable(jobbnummer, jobbnummer_inn, 
        'create table bygning_bygningstype_l(
          ID NUMBER( 4 )
          , BESKRIVELSE VARCHAR2( 50 char )
          , HID_OPPRETTET NUMBER( 8 )          
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''   
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , constraint PK_BYGNING_BYGNINGSTYPE_L PRIMARY KEY ( id )
          , constraint UK_BYGNING_BYGNINGSTYPE_L_UUID UNIQUE ( UUID )
          , constraint UK_BYGNING_BYGNINGSTYPE_L_BESK UNIQUE ( BESKRIVELSE )
          , constraint FK_BYGNING_BTYPE_L_HENDELSE_1 FOREIGN KEY ( HID_OPPRETTET ) REFERENCES primus.hendelse( hid )
          , constraint FK_BYGNING_BTYPE_L_HENDELSE_2 FOREIGN KEY ( HID_SLETTET ) REFERENCES primus.hendelse( hid )
          , constraint CK_BYGNING_BTYPE_MERKE_NN CHECK ( MERKE IS NOT NULL ) enable 
          , constraint CK_BYGNING_BTYPE_HID_OPPR_NN CHECK ( hid_opprettet is not null ) enable 
          , constraint CK_BYGNING_BTYPE_BESK_NN CHECK ( BESKRIVELSE IS NOT NULL ) ENABLE
          , constraint CK_BYGNING_BTYPE_UUID_NN CHECK ( UUID IS NOT NULL ) ENABLE )
          tablespace primusdt' );
    end if;
    
    jobbnummer := 3;
    
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_bygningstype_h
            ( objid number( 9 )
            , hid number( 8 )
            , bygningtypeid number( 4 )
            , constraint PK_BYGNING_BYGNINGSTYPE_H PRIMARY KEY ( OBJID, HID )
            , constraint FK_BYG_BYGNINGSTYPE_HENDELSE FOREIGN KEY ( HID ) REFERENCES PRIMUS.HENDELSE( HID )
            , constraint FK_BYG_BYGNINGSTYPE_OBJID FOREIGN KEY ( OBJID ) REFERENCES primus.bygning( OBJID )
            , constraint FK_BYG_BYGNINGSTYPE_TYPEID FOREIGN KEY ( BYGNINGTYPEID ) REFERENCES PRIMUS.BYGNING_BYGNINGSTYPE_L( ID ) )
            TABLESPACE PRIMUSDT' );
    end if;
      
    jobbnummer := 4;  
    
    if jobbnummer_inn <= jobbnummer then
      executealtertable( jobbnummer, jobbnummer_inn, 
          'ALTER TABLE BYGNING ADD 
              ( HID_BYGNINGSTYPE NUMBER( 8 ) 
              , constraint FK_BYGNING_BYGNINGSTYPE_H FOREIGN KEY ( OBJID, HID_BYGNINGSTYPE ) 
                    REFERENCES PRIMUS.BYGNING_BYGNINGSTYPE_H( OBJID, HID ) 
              )');  
    end if;
    
    -- flytte listeverdier fra bygning_lister type1
    -- flytte alle verdier fra byggning_felter_table felt2

    jobbnummer := 5;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('
      insert into bygning_bygningstype_l 
        ( id
        , beskrivelse
        , hid_opprettet
        , uuid )
          ( select rownum
                 , tekst
                 , ' || newhid || '
                 , getoracleuuid() 
              from bygning_lister 
             where feltnr = 1)
      ');
      commit;
    end if;
    
    jobbnummer := 6;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('  
      insert into bygning_bygningstype_h
        ( objid
        , hid
        , bygningtypeid )
          ( select bf.objid
                 , bf.hid
                 , bbtl.id 
              from Bygning_Felter bf 
             inner join bygning_bygningstype_l bbtl 
                     on bf.tekst = bbtl.beskrivelse
             where bf.feltnr = 2)
      ');
      commit;
    end if;
      
    jobbnummer := 7;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('
      update bygning pb 
         set pb.hid_bygningstype = (select max(hid) 
                                      from bygning_bygningstype_h bbth 
                                     where bbth.objid = pb.objid)
      ');
      commit;
    end if;

    -- ferdig bygning_bygningstype --
    
    
    -- start bygning_funksjon -- 
    jobbnummer := 8;
    
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable(jobbnummer, jobbnummer_inn, 
        'create table bygning_funksjon_l
          ( ID NUMBER ( 4 )
          , BESKRIVELSE VARCHAR2( 50 CHAR )
          , HID_OPPRETTET NUMBER( 8 )         
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''   
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , CONSTRAINT PK_BYGNING_FUNKSJON_L PRIMARY KEY ( ID )
          , CONSTRAINT UK_BYGNING_FUNKSJON_L_UUID UNIQUE ( UUID )
          , CONSTRAINT UK_BYGNING_FUNKSJON_L_BESK UNIQUE ( BESKRIVELSE )
          , CONSTRAINT FK_BYGNING_FUNK_L_HENDELSE_1 FOREIGN KEY ( HID_OPPRETTET ) REFERENCES PRIMUS.HENDELSE( HID )
          , CONSTRAINT FK_BYGNING_FUNK_L_HENDELSE_2 FOREIGN KEY ( HID_SLETTET ) REFERENCES PRIMUS.HENDELSE( HID ) 
          , CONSTRAINT CK_BYGNING_FUNK_L_BESK_NN CHECK ( BESKRIVELSE IS NOT NULL ) ENABLE
          , CONSTRAINT CK_BYGNING_FUNK_L_HID_OPPR_NN CHECK ( HID_OPPRETTET IS NOT NULL ) ENABLE
          , CONSTRAINT CK_BYGNING_FUNK_L_UUID_NN CHECK ( UUID IS NOT NULL ) ENABLE
          , CONSTRAINT CK_BYGNING_FUNK_L_MERKE_NN CHECK ( MERKE IS NOT NULL ) ENABLE
          )
          TABLESPACE PRIMUSDT' );
    end if;

    jobbnummer := 9;
    
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_funksjon_h
          ( objid number( 9 )
          , hid number( 8 )
          , funksjonid number( 4 )
          , constraint PK_BYGNING_FUNKSJON_H PRIMARY KEY ( OBJID, HID )
          , constraint FK_BYG_FUNKSJON_HENDELSE FOREIGN KEY ( HID ) REFERENCES PRIMUS.HENDELSE( HID )
          , constraint FK_BYG_FUNKSJON_OBJID FOREIGN KEY ( objid ) references primus.bygning( objid )
          , constraint FK_BYG_FUNKSJON_FUNKID FOREIGN KEY ( FUNKSJONID ) REFERENCES PRIMUS.BYGNING_FUNKSJON_L( ID ) )
          TABLESPACE PRIMUSDT'
        );
    end if;

    
    jobbnummer := 10;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'alter table bygning add 
            ( HID_FUNKSJON number( 8 )
            , constraint fk_bygning_funksjon_h foreign key( objid, hid_funksjon ) 
                references primus.bygning_funksjon_h( objid, hid ) 
            )' );
    end if;
    -- flytte listeverdier fra bygning_lister type 2
    -- flytte verdier fra byning_felter_table, felt 6  
    jobbnummer := 11;     
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('
        insert into bygning_funksjon_l 
          ( id
          , beskrivelse
          , hid_opprettet
          , uuid )
          ( select rownum
                 , tekst
                 , ' || newhid || '
                 , getoracleuuid() 
              from bygning_lister 
             where feltnr = 2)'
      );
      commit;
    end if;
      
    jobbnummer := 12;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('
      insert into bygning_funksjon_h
        ( objid
        , hid
        , funksjonid )
        ( select bf.objid
               , bf.hid
               , bbtl.id 
            from Bygning_Felter bf 
           inner join bygning_funksjon_l bbtl 
                   on bf.tekst = bbtl.beskrivelse
           where bf.feltnr = 6 )
      ');
      commit;
    end if;

    jobbnummer := 13;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ( '
      update bygning pb 
         set pb.hid_funksjon = (select max(bfh.hid) 
                                  from bygning_funksjon_h bfh 
                                 where bfh.objid = pb.objid)
      ' );
      commit;
    end if;
    
    jobbnummer := 14;
    
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn,  
        'create table bygning_opprfunksjon_h
          ( objid number( 9 )
          , hid number( 8 )
          , opprfunksjonid number( 4 )
          , constraint PK_BYGNING_OPPRFUNKSJON_H PRIMARY KEY ( OBJID, HID )
          , constraint FK_BYG_OPPRFUNKSJON_HENDELSE FOREIGN KEY ( HID ) REFERENCES PRIMUS.HENDELSE( HID )
          , constraint FK_BYG_OPPRFUNKSJON_OBJID FOREIGN KEY ( objid ) references primus.bygning( objid )
          , constraint FK_BYG_OPPRFUNKSJON_FUNKID FOREIGN KEY ( OPPRFUNKSJONID ) REFERENCES PRIMUS.BYGNING_FUNKSJON_L( ID ) )
          TABLESPACE PRIMUSDT
        ' );
    end if;
    
    
    jobbnummer := 15;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer, jobbnummer_inn, 
        'alter table bygning add 
            ( HID_OPPRFUNKSJON number( 8 )
            , constraint fk_bygning_opprfunksjon_h foreign key( objid, hid_opprfunksjon ) 
                         references primus.bygning_opprfunksjon_h( objid, hid ) 
            )
        ' );
    end if;
    
    
    -- flytte verdier fra bygning_felter_table, felt 16
    jobbnummer := 16;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_opprfunksjon_h
        ( objid
        , hid
        , opprfunksjonid )
          (select bf.objid
                , bf.hid
                , bbtl.id 
             from Bygning_Felter bf 
            inner join bygning_funksjon_l bbtl 
               on bf.tekst = bbtl.beskrivelse
            where bf.feltnr = 16 )
        ' );
      commit;
    end if;  
    
    jobbnummer := 17;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        update bygning pb 
           set pb.hid_opprfunksjon = (select max(bfh.hid) 
                                        from bygning_opprfunksjon_h bfh
                                       where bfh.objid = pb.objid)
        ' );
    
      commit;
    end if;


    jobbnummer := 18;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_status_l
          ( ID NUMBER ( 4 )
          , BESKRIVELSE VARCHAR2( 50 CHAR )
          , HID_OPPRETTET NUMBER( 8 )         
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''   
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , constraint pk_bygning_status_l PRIMARY KEY ( ID )
          , constraint uk_bygning_status_l_uuid unique ( uuid )
          , constraint uk_bygning_status_l_besk unique ( beskrivelse )
          , constraint fk_bygning_status_l_hendelse_1 foreign key ( hid_opprettet ) references primus.hendelse( hid )
          , constraint fk_bygning_status_l_hendelse_2 foreign key ( hid_slettet ) references primus.hendelse( hid ) 
          , constraint ck_bygning_status_l_besk_NN check ( beskrivelse is not null ) enable
          , constraint ck_bygning_status_l_hid_opr_nn check ( hid_opprettet is not null) enable
          , constraint ck_bygning_status_l_uuid_nn check ( uuid is not null ) enable
          , constraint ck_bygning_status_l_merke_nn check ( merke is not null ) enable
          )
          tablespace primusdt
        ' );
    end if;
    

    jobbnummer := 19;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_status_h
          ( objid number( 9 )
          , hid number( 8 ) 
          , statusid number( 4 )
          , constraint pk_bygning_status_h primary key ( objid, hid )
          , constraint fk_byg_status_hendelse foreign key ( hid ) references primus.hendelse( hid )
          , constraint fk_byg_status_objid foreign key ( objid ) references primus.bygning( objid )
          , constraint fk_byg_status_statusid foreign key ( statusid ) references primus.bygning_status_l( id ) )
          tablespace primusdt
        ' );
    end if;
    
    jobbnummer := 20;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer, jobbnummer_inn, 
        'alter table bygning add 
             ( hid_status number( 8 )
             , constraint fk_bygning_status_h foreign key( objid, hid_status ) 
                          references primus.bygning_status_h( objid, hid ) )
        ');
    end if;
    -- flytte listeverdier fra bygning_lister type 4
    -- flytte verdier fra bygning_felter_table, felt 5
    jobbnummer := 21;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('
        insert into bygning_status_l 
             ( id
             , beskrivelse
             , hid_opprettet
             , uuid )
              ( select rownum
                    , tekst
                    , ' || newhid || '
                    , getoracleuuid() 
                 from bygning_lister 
                where feltnr = 4 )
        ' );
      commit;
    end if;
    
    jobbnummer := 22;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_status_h
             ( objid
             , hid
             , statusid )
               ( select bf.objid
                      , bf.hid
                      , bbtl.id 
                   from Bygning_Felter bf 
                  inner join bygning_status_l bbtl 
                          on bf.tekst = bbtl.beskrivelse
                  where bf.feltnr = 5 )
        ' );
      commit;
    end if;
    
    jobbnummer := 23;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        update bygning pb 
           set pb.hid_status = (select max(bfh.hid) 
                                  from bygning_status_h bfh 
                                 where bfh.objid = pb.objid)
        ' );
      commit;
    end if;
      
    jobbnummer := 24;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_vernetype_l
          ( ID NUMBER ( 4 )
          , BESKRIVELSE VARCHAR2( 100 CHAR )
          , HID_OPPRETTET NUMBER( 8 )          
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''  
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , constraint pk_bygning_vernetype_l primary key ( id )
          , constraint uk_bygning_vernetype_uuid unique ( uuid )
          , constraint uk_bygning_vernetype_besk unique ( beskrivelse )
          , constraint fk_bygning_vernetype_hend_1 foreign key ( hid_opprettet ) references primus.hendelse( hid )
          , constraint fk_bygning_vernetype_hend_2 foreign key ( hid_slettet ) references primus.hendelse( hid ) 
          , constraint ck_bygning_vernet_besk_nn check ( beskrivelse is not null ) enable
          , constraint ck_bygning_vernet_hid_opr_nn check ( hid_opprettet is not null ) enable
          , constraint ck_bygning_vernet_uuid_nn check ( uuid is not null ) enable
          , constraint ck_bygning_vernet_merke_nn check ( merke is not null ) enable
          )
          tablespace primusdt
        ' );
    end if;
    
    jobbnummer := 25;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_vernetype_h
          ( objid number( 9 )
          , hid number( 8 )
          , vernetypeid number( 4 )
          , constraint pk_bygning_vernetype_h primary key ( objid, hid )
          , constraint fk_byg_vernetype_hendelse foreign key ( hid ) references primus.hendelse( hid )
          , constraint fk_byg_vernetype_objid foreign key ( objid ) references primus.bygning( objid )
          , constraint fk_byg_vernetype_vernetypeid foreign key ( vernetypeid ) references primus.bygning_vernetype_l( id ) )
          tablespace primusdt
        ' );
    end if;
    
    jobbnummer := 26;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer, jobbnummer_inn,
        'alter table bygning add 
             ( hid_vernetype number( 8 )
             , constraint fk_bygning_vernetype_h foreign key( objid, hid_vernetype ) 
                          references primus.bygning_vernetype_h( objid, hid ) 
             )
        ' );
    end if;
    -- flytte listeverdier fra bygning_lister type 5
    -- flytte verdier fra bygning_felter_table, felt 10  
      
    jobbnummer := 27;
    if jobbnummer_inn <= jobbnummer then
      execute IMMEDIATE('
        insert into bygning_vernetype_l 
             ( id
             , beskrivelse
             , hid_opprettet
             , uuid )
              ( select rownum
                     , tekst
                     , ' || newhid || '
                     , getoracleuuid() 
                  from bygning_lister 
                 where feltnr = 5 )
        ' );
      commit;
    end if;
    
    jobbnummer := 28;
    if jobbnummer_inn <= jobbnummer then
      execute immediate('
        insert into bygning_vernetype_h
             ( objid
             , hid
             , vernetypeid )
              ( select bf.objid
                     , bf.hid
                     , bbtl.id 
                  from Bygning_Felter bf 
                 inner join bygning_vernetype_l bbtl 
                         on bf.tekst = bbtl.beskrivelse
                 where bf.feltnr = 10 )
        ' );
      commit;
    end if;
    
    jobbnummer := 29;
    if jobbnummer_inn <= jobbnummer_inn then
      execute immediate( '
        update bygning pb 
           set pb.hid_vernetype = (select max(bfh.hid) 
                                     from bygning_vernetype_h bfh 
                                    where bfh.objid = pb.objid)  
        ' );
      commit;
    end if;
      
      
    jobbnummer := 30;
    if jobbnummer_inn <= jobbnummer_inn then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_konstruksjon_l
            ( ID NUMBER ( 4 )
            , BESKRIVELSE VARCHAR2( 100 CHAR )
            , HID_OPPRETTET NUMBER( 8 )           
            , HID_SLETTET                NUMBER( 8 )          
            , UUID                       VARCHAR2( 40 CHAR )  
            , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
            , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''  
            , AUTORITET                  VARCHAR2( 10 CHAR )  
            , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
            , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
            , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
            , constraint PK_BYGNING_KONSTRUKSJON_L PRIMARY KEY ( ID )
            , constraint UK_bygning_konstruksjon_l_uuid unique ( uuid )
            , constraint UK_bygning_konstruksjon_l_besk unique ( beskrivelse )
            , constraint FK_byg_konst_l_hendelse_1 foreign key ( hid_opprettet ) references primus.hendelse( hid )
            , constraint fk_byg_konst_l_hendelse_2 foreign key ( hid_slettet ) references primus.hendelse( hid )
            , constraint ck_bygning_konst_l_beskrivelse check ( beskrivelse is not null) enable
            , constraint ck_bygning_konst_l_hid_oppr check ( hid_opprettet is not null) enable
            , constraint ck_bygning_konst_l_uuid check ( uuid is not null ) enable
            , constraint ck_bygning_konst_l_merke check ( merke is not null ) enable )
            tablespace primusdt
        ' );
    end if;  
    
    jobbnummer := 31;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        ' create table bygning_konstruksjon_h
              ( objid number( 9 ) 
              , hid number ( 8 )
              , konstruksjonid number( 4 )
              , konstruksjon_beskrivelse varchar2( 4000 char )
              , constraint pk_bygning_konstruksjon_h primary key ( objid, hid )
              , constraint fk_byg_konstruksjon_hendelse foreign key ( hid ) references primus.hendelse( hid )
              , constraint fk_byg_kosntruksjon_objid foreign key ( objid ) references primus.bygning( objid )
              , constraint fk_byg_konstruksjon_konsid foreign key (konstruksjonid) references primus.bygning_konstruksjon_l( id ) )
              tablespace primusdt
        ' );
    end if;
    
    
    jobbnummer := 32;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer, jobbnummer_inn, 
        ' alter table bygning add 
              ( hid_konstruksjon number( 8 )
              , constraint fk_bygning_konstruksjon_h foreign key( objid, hid_konstruksjon ) 
                           references primus.bygning_konstruksjon_h( objid, hid ) 
              )
        ' );
    end if;
    --flytte listeverider fra bygning_lister type 7
    -- flytte verdier fra byggning_felter_table, felt 7
    
    jobbnummer := 33;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_konstruksjon_l 
             ( id
             , beskrivelse
             , hid_opprettet
             , uuid )
              ( select rownum
                     , tekst
                     , ' || newhid || '
                     , getoracleuuid() 
                  from bygning_lister
                 where feltnr = 7
              )
        ' );
      commit;
    end if;

    jobbnummer := 34;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_konstruksjon_h
             ( objid
             , hid
             , konstruksjonid
             , konstruksjon_beskrivelse )
              ( select bf.objid
                     , bf.hid
                     , bfl.id
                     , bf.beskrivelse 
                  from Bygning_Felter bf 
                  left outer join bygning_konstruksjon_l bfl 
                              on bf.tekst = bfl.beskrivelse
                 where bf.feltnr = 7 )
        ' );
      commit;
    end if;
    
    jobbnummer := 35;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
         update bygning pb 
            set pb.hid_konstruksjon = (select max(bfh.hid) 
                                         from bygning_konstruksjon_h bfh 
                                        where bfh.objid = pb.objid)
         ' );
      commit;
    end if;  

    jobbnummer := 36;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_lovgrunnlag_l
          ( ID NUMBER ( 4 )
          , BESKRIVELSE VARCHAR2( 50 CHAR )
          , HID_OPPRETTET NUMBER( 8 )          
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , constraint pk_bygning_lovgrunnlag_l primary key ( id )
          , constraint uk_bygning_lovgrunnlag_l_uuid unique ( uuid )
          , constraint uk_bygning_lovgrunnlag_l_besk unique ( beskrivelse )
          , constraint fk_byg_lovgrlag_l_hendelse_1 foreign key ( hid_opprettet ) references primus.hendelse( hid )
          , constraint fk_byg_lovgrlag_l_hendelse_2 foreign key ( hid_slettet ) references primus.hendelse( hid ) 
          , constraint ck_byg_lovgrlag_l_hidoppr_nn check ( hid_opprettet is not null ) enable
          , constraint ck_byg_lovgrlag_l_besk_nn check ( beskrivelse is not null ) enable
          , constraint ck_byg_lovgrlag_l_uuid_nn check ( uuid is not null ) enable
          , constraint ck_byg_lovgrlag_l_merke_nn check ( merke is not null ) enable
          ) 
          tablespace primusdt
        ' );
    end if;
    
    jobbnummer := 37;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_lovgrunnlag_h
          ( objid number( 9 )
          , hid number( 8 ) 
          , lovgrunnlagid number( 4 )
          , constraint pk_bygning_lovgrunnlag_h primary key ( objid, hid )
          , constraint fk_byg_lovgrunnlag_hendelse foreign key ( hid ) references primus.hendelse( hid )
          , constraint fk_byg_lovgrunnlag_objid foreign key ( objid ) references primus.bygning( objid )
          , constraint fk_byg_lovgrunnlag_lovgrid foreign key ( lovgrunnlagid ) references primus.bygning_lovgrunnlag_l( id ) )
          tablespace primusdt
        ' );
    end if;

    jobbnummer := 38;
    if jobbnummer_inn <= Jobbnummer then
      executeAlterTable( jobbnummer, jobbnummer_inn, 
        'alter table bygning add 
             ( hid_lovgrunnlag number( 8 )
             , constraint fk_bygning_lovgrunnlag_h foreign key( objid, hid_lovgrunnlag ) 
                          references primus.bygning_lovgrunnlag_h( objid, hid ) 
             )
        ' );
    end if;  
    -- flytte listeverdier fra bygning_lister type 14
    -- flytte verdier fra byggning_felter_table, felt 11

    jobbnummer := 39;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_lovgrunnlag_l 
             ( id
             , beskrivelse
             , hid_opprettet
             , uuid )
              ( select rownum
                     , tekst
                     , ' || newhid || '
                     , getoracleuuid() 
                  from bygning_lister 
                 where feltnr = 14 )
        ' );
      commit;
    end if;
    
    jobbnummer := 40;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        insert into bygning_lovgrunnlag_h
             ( objid
             , hid
             , lovgrunnlagid )
              ( select bf.objid
                     , bf.hid
                     , blgl.id 
                  from Bygning_Felter bf 
                 inner join bygning_lovgrunnlag_l blgl 
                         on bf.tekst = blgl.beskrivelse
                 where bf.feltnr = 11 )
        ' );
      commit;
    end if;
    
    jobbnummer := 41;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        update bygning pb 
           set pb.hid_lovgrunnlag = (select max(bfh.hid) 
                                       from bygning_lovgrunnlag_h bfh 
                                      where bfh.objid = pb.objid
                                    )
        ' );
      commit;      
    end if;

     
    jobbnummer := 42;
    if jobbnummer_inn <= jobbnummer then    
      executeCreateTable( jobbnummer, jobbnummer_inn, 
        'create table bygning_kategori_l
          ( ID NUMBER ( 4 )
          , BESKRIVELSE VARCHAR2( 50 CHAR )
          , HID_OPPRETTET NUMBER( 8 )           
          , HID_SLETTET                NUMBER( 8 )          
          , UUID                       VARCHAR2( 40 CHAR )  
          , CHECKFORDELETE             VARCHAR2( 5 CHAR )   
          , MERKE                      VARCHAR2( 1 CHAR ) default ''Y''  
          , AUTORITET                  VARCHAR2( 10 CHAR )  
          , AUTORITET_STATUS           VARCHAR2( 1 CHAR )   
          , AUTORITET_DATASET          VARCHAR2( 100 CHAR ) 
          , AUTORITET_KILDE            VARCHAR2( 100 CHAR )
          , constraint pk_bygning_kategori_l primary key ( id )
          , constraint uk_bygning_kategori_l_uuid unique ( uuid )
          , constraint fk_byg_kategori_l_hendelse_1 foreign key ( hid_opprettet ) references primus.hendelse( hid )
          , constraint fk_byg_kategori_l_hendelse_2 foreign key ( hid_slettet ) references primus.hendelse( hid ) 
          , constraint ck_bygning_kategori_MERKE_NN check ( merke is not null ) enable
          , constraint ck_bygning_kategori_UUID_NN check ( UUID is not null ) enable
          , constraint ck_bygning_kategori_HIDOPPR_NN check ( HID_OPPRETTET IS NOT NULL) enable
          , constraint ck_bygning_kategori_BESK_NN check ( beskrivelse is not null ) enable )
          tablespace primusdt
        ' );  
    end if;
    
    jobbnummer := 43;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_kategori_h
                            ( objid number( 9 )
                            , hid number( 8 )
                            , kategoriid number( 4 )
                            , constraint pk_bygning_kategori_h primary key ( objid, hid )
                            , constraint fk_byg_kategori_hendelse foreign key ( hid ) 
                                         references primus.hendelse( hid )
                            , constraint fk_byg_kategori_objid foreign key ( objid ) 
                                         references primus.bygning( objid )
                            , constraint fk_byg_kategori_kategoriid foreign key( kategoriid ) 
                                         references primus.bygning_kategori_l( id ) 
                            )
                            tablespace primusdt
                         ' );
    end if;
    
    jobbnummer := 44;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bygning add 
                              ( hid_kategori number( 8 )
                              , constraint fk_bygning_kategori_h foreign key( objid, hid_kategori ) 
                                           references primus.bygning_kategori_h( objid, hid )
                              )
                         ' );
    end if;  
    -- flytte listeverdier fra bygning lister type 17
    -- flytte verdier fra byggning_felter_table, felt 17
    
    jobbnummer := 45;
    if jobbnummer_inn <= jobbnummer then
      execute immediate (
        ' insert into bygning_kategori_l 
               ( id
               , beskrivelse
               , hid_opprettet
               , uuid )
                ( select to_number(tekst)
                       , kommentar
                       , ' || newhid || '
                       , getoracleuuid() 
                    from bygning_lister 
                   where feltnr = 17 )
        ' );
      commit;
    end if;
    
    jobbnummer := 46;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ( 
        ' insert into bygning_kategori_h
               ( objid
               , hid
               , kategoriid )
                ( select bf.objid
                       , bf.hid
                       , bkl.id 
                    from Bygning_Felter bf 
                   inner join bygning_kategori_l bkl 
                           on bf.tekst = bkl.beskrivelse
                   where bf.feltnr = 17 )
        ' );
    end if;
    
    jobbnummer := 47;
    if jobbnummer_inn <= jobbnummer then
      execute immediate (
        ' update bygning pb 
             set pb.hid_kategori = (select max(bkh.hid) 
                                      from bygning_kategori_h bkh 
                                     where bkh.objid = pb.objid)
        ' );
      commit;
    end if;


    jobbnummer := 48;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_gabnr_h
                            ( objid number( 9 )
                            , hid number( 8 )
                            , gabnr varchar2( 100 char )
                            , constraint pk_bygning_gabnr_h primary key ( objid, hid )
                            , constraint fk_byg_gabnr_hendelse foreign key ( hid ) references primus.hendelse( hid )
                            , constraint fk_byg_gabnr_objid foreign key ( objid ) references primus.bygning( objid ) )
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 49;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bygning add 
                              ( hid_gabnr number( 8 )
                              , constraint fk_bygning_gabnr_h foreign key ( objid, hid_gabnr ) 
                                           references primus.bygning_gabnr_h( objid, hid ) 
                              )'
                       );    
    end if;                        

   
    -- flytte verdier fra bygning_felter_table, felt 1(tekst)
    jobbnummer := 50;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' insert into bygning_gabnr_h
               ( objid
               , hid
               , gabnr )
                ( select bf.objid
                       , bf.hid
                       , bf.tekst 
                    from Bygning_Felter bf 
                   where bf.feltnr = 1 )
        ' );
      commit;
    end if;
    
    jobbnummer := 51;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' update bygning pb 
             set pb.hid_gabnr = (select max(bgnh.hid) 
                                   from bygning_gabnr_h bgnh 
                                  where bgnh.objid = pb.objid )                                  
        ' );
      commit;
    end if;

    jobbnummer := 52;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_lokalitet_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , lokalitet varchar2( 100 char )
                              , constraint pk_bygning_lokalitet_h primary key ( objid, hid )
                              , constraint fk_byg_lokalitet_hendelse foreign key ( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_lokalitet_objid foreign key ( objid ) references primus.bygning( objid ) )
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 53;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , ' alter table bygning add 
                               ( hid_lokalitet number( 8 )
                               , constraint fk_bygning_lokalitet_h foreign key ( objid, hid_lokalitet ) 
                                            references primus.bygning_lokalitet_h( objid, hid ) )'
                       );
    end if;

    -- flytte verdier fra bygning_felter_table, felt 3 (tekst)                        
    jobbnummer := 54;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' insert into bygning_lokalitet_h
               ( objid
               , hid
               , lokalitet )
                ( select bf.objid
                       , bf.hid
                       , bf.tekst 
                    from Bygning_Felter bf 
                   where bf.feltnr = 3 )
        ' );       
      commit;
    end if;
    
    jobbnummer := 55;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        ' update bygning pb 
             set pb.hid_lokalitet = (select max(blh.hid) 
                                       from bygning_lokalitet_h blh 
                                      where blh.objid = pb.objid)
        ' );
      commit;
    end if;
      
    jobbnummer := 56;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_bruksomrade_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , bruksomrade varchar2( 200 char )
                              , constraint pk_bygning_bruksomrade_h primary key ( objid, hid )
                              , constraint fk_byg_bruksomrade_hendelse foreign key ( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_bruksomrade_objid foreign key ( objid ) references primus.bygning( objid ) )
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 57;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , ' alter table bygning add 
                               ( hid_bruksomrade number( 8 )
                               , constraint fk_bygning_bruksomrade_h foreign key ( objid, hid_bruksomrade ) 
                                            references primus.bygning_bruksomrade_h( objid, hid ) )'
                       );
    end if;

    -- flytte verdier fra bygning_felter_table, felt 4 ( tekst )
    jobbnummer := 58;
    
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' insert into bygning_bruksomrade_h
               ( objid
               , hid
               , bruksomrade )
                ( select bf.objid
                       , bf.hid
                       , bf.tekst 
                    from Bygning_Felter bf 
                   where bf.feltnr = 4 )
        ' );
      commit;
    end if;
    
    jobbnummer := 59;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' update bygning pb 
             set pb.hid_bruksomrade = (select max(bboh.hid) 
                                         from bygning_bruksomrade_h bboh 
                                        where bboh.objid = pb.objid)
        ' );
      commit;
    end if;
    
    jobbnummer := 60;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_etasje_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , antalletasjer varchar2( 10 char )
                              , etasjekommentar varchar2( 1000 char )
                              , constraint pk_bygning_etasje_h primary key ( objid, hid )
                              , constraint fk_byg_etasje_hendelse foreign key ( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_etasje_objid foreign key ( objid ) references primus.bygning( objid ) )
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 61;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bygning add 
                              ( hid_etasje number( 8 )
                              , constraint fk_bygning_etasje_h foreign key ( objid, hid_etasje ) 
                                           references primus.bygning_etasje_h( objid, hid ) )'
                       );
    end if;
    
    -- flytt verdier fra bygning_felter_tablee, felt 9 (antall etasjer - kode)(etasjekommentar - tekst)
    jobbnummer := 62;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        'insert into bygning_etasje_h
              ( objid
              , hid
              , antalletasjer
              , etasjekommentar )
               ( select bf.objid
                      , bf.hid
                      , bf.kode
                      , bf.tekst 
                   from Bygning_Felter bf 
                  where bf.feltnr = 9 )
        ' );
      commit;
    end if;
    
    jobbnummer := 63;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        ' update bygning pb 
             set pb.hid_etasje = (select max(beh.hid) 
                                    from bygning_etasje_h beh 
                                   where beh.objid = pb.objid)                                   
        ' );
      commit;
    end if;
    
    jobbnummer := 64;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_paragraf_h
                              ( objid number( 9 ) 
                              , hid number( 8 )
                              , paragraf varchar2( 200 char )
                              , constraint pk_bygning_paragraf_h primary key ( objid, hid )
                              , constraint fk_byg_paragraf_hendelse foreign key( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_paragraf_objid foreign key( objid ) references primus.bygning( objid ) )
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 65;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table bygning add 
                              ( hid_paragraf number( 8 )
                              , constraint fk_bygning_paragraf_h foreign key ( objid, hid_paragraf ) 
                                           references primus.bygning_paragraf_h( objid, hid ) )'
                       );
    end if;
    
    -- flytt verdier fra bygning_felter_table, felt 12 (tekst)

    jobbnummer := 66;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' insert into bygning_paragraf_h
               ( objid
               , hid
               , paragraf )
                ( select bf.objid
                       , bf.hid
                       , bf.tekst 
                    from Bygning_Felter bf 
                   where bf.feltnr = 12 )
        ' );
      commit;
    end if;
    
    jobbnummer := 67;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        ' update bygning pb 
             set pb.hid_paragraf = (select max(bph.hid) 
                                      from bygning_paragraf_h bph 
                                     where bph.objid = pb.objid)
        ' );
      commit;
    end if;
    
    jobbnummer := 68;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_vedtaksdato_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , vedtaksdato date
                              , constraint pk_bygning_vedtaksdato_h primary key ( objid, hid )
                              , constraint fk_byg_vedtaksdato_hendelse foreign key ( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_vedtaksdato_objid foreign key ( objid ) references primus.bygning( objid ) ) 
                            tablespace primusdt'
                        );  
    end if;
    
    jobbnummer := 69;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , ' alter table bygning add 
                               ( hid_vedtaksdato number( 8 ) 
                               , constraint fk_bygning_vedtaksdato_h foreign key( objid, hid_vedtaksdato ) 
                                            references primus.bygning_vedtaksdato_h( objid, hid ) )'
                       );
    end if;
    
                            
    -- flytt verdier fra bygning_felter_table, felt 13 (tekst - til dato konvertering !!)
    jobbnummer := 70;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        ' insert into bygning_vedtaksdato_h
               ( objid
               , hid
               , vedtaksdato )
                ( select bf.objid
                       , bf.hid
                       , bf.dato 
                    from Bygning_Felter bf 
                   where bf.feltnr = 13 )
        ' );
      commit;
    end if;
    
    jobbnummer := 71;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        ' update bygning pb 
             set pb.hid_vedtaksdato = (select max(bvdh.hid) 
                                         from bygning_vedtaksdato_h bvdh 
                                        where bvdh.objid = pb.objid)
        ' );
      commit;
    end if;

    
    jobbnummer := 72;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn
                        , 'create table bygning_vern_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , vern varchar2( 4000 char )
                              , constraint pk_bygning_vern_h primary key ( objid, hid )
                              , constraint fk_byg_vern_hendelse foreign key( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_vern_objekt foreign key( objid ) references primus.bygning( objid ))
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 73;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                       , jobbnummer_inn 
                       , ' alter table bygning add 
                               ( hid_vern number( 8 )
                               , constraint fk_bygning_vern_h foreign key( objid, hid_vern ) 
                                            references primus.bygning_vern_h( objid, hid ) )'
                       );
    end if;
    
    jobbnummer := 74;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        ' insert into bygning_vern_h
               ( objid
               , hid
               , vern )
                ( select bf.objid
                       , bf.hid
                       , bf.beskrivelse 
                    from Bygning_Felter bf 
                   where bf.feltnr = 14 )
        ' );
      commit;
    end if;
    
    jobbnummer := 75;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        'update bygning pb 
            set pb.hid_vern = (select max(bvh.hid) 
                                 from bygning_vern_h bvh 
                                where bvh.objid = pb.objid)
        ' );
      commit;
    end if;
    
    jobbnummer := 76;                        
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn 
                        , 'create table bygning_heftelser_h
                              ( objid number( 9 )
                              , hid number( 8 )
                              , heftelser varchar2( 4000 char )
                              , constraint pk_bygning_heftelser_h primary key ( objid, hid )
                              , constraint fk_byg_heftelser_hendelse foreign key( hid ) references primus.hendelse( hid )
                              , constraint fk_byg_heftelser_objekt foreign key( objid ) references primus.bygning( objid ))
                            tablespace primusdt'
                        );
    end if;
    
    jobbnummer := 77;
    if jobbnummer_inn <= jobbnummer then
      executeAlterTable( jobbnummer
                        , jobbnummer_inn
                        , 'alter table bygning add 
                               ( hid_heftelser number( 8 )
                               , constraint fk_bygning_heftelser_h foreign key( objid, hid_heftelser ) 
                                            references primus.bygning_heftelser_h( objid, hid ) )'
                        );
    end if;
    
    
    -- flytt verdier fra byggning_felter_table, felt 15 (tekst)
    
    jobbnummer := 78;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 
        'insert into bygning_heftelser_h
              ( objid
              , hid
              , heftelser )
               ( select bf.objid
                      , bf.hid
                      , bf.tekst 
                   from Bygning_Felter bf 
                  where bf.feltnr = 15 )
        ' );         
      commit;
    end if;
    
    jobbnummer := 79;
    if jobbnummer_inn <= jobbnummer then
      execute immediate(
        'update bygning pb 
            set pb.hid_heftelser = (select max(bhh.hid) 
                                      from bygning_heftelser_h bhh 
                                     where bhh.objid = pb.objid)
        ' );
      commit;                               
    end if;
    
    jobbnummer := 80;
    if jobbnummer_inn <= jobbnummer then
      execute immediate('grant insert, update on primus.bygning_heftelser_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_vern_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_vedtaksdato_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_paragraf_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_etasje_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_bruksomrade_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_lokalitet_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_gabnr_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_kategori_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_lovgrunnlag_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_konstruksjon_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_vernetype_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_status_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_opprfunksjon_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_funksjon_h to pri_reg');
      execute immediate('grant insert, update on primus.bygning_bygningstype_h to pri_reg');
      execute immediate('grant insert, update on primus.objekt_video to pri_reg');
      execute immediate('grant insert, update on primus.video_bfile to pri_reg');
      execute immediate('grant insert, update on primus.video to pri_reg');
      execute immediate('grant insert, update on primus.analyse_metode_beskrivelse to pri_reg');
      execute immediate('grant insert, update on primus.analyse_metode to pri_reg');
      execute immediate('grant insert, update on primus.analyse to pri_reg');
      execute immediate('grant insert, update on primus.adm_hend_intervall to pri_reg');
      execute immediate('grant insert, update on primus.intervall to pri_reg');
      execute immediate('grant insert, update on primus.adm_hend_annotering to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode_beskrivelse to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode_kostnad to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode_ressursbruk to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode_datering to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_prioritet_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_stiltekst_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_opplagsopplysninger_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_provenienshistorikk_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_proveniensdatering_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_produksjonshistorikk_h to pri_reg');
      execute immediate('grant insert, update on primus.obj_produksjonsdatering_h to pri_reg');
      execute immediate('grant insert, update on primus.signatur_brukerrolle to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode_materiale to pri_reg');
      execute immediate('grant insert, update on primus.behandling_metode to pri_reg');
      execute immediate('grant insert, update on primus.behandling to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_anbefaling_h to pri_reg');
      execute immediate('grant insert, update on primus.skadetype_h to pri_reg');
      execute immediate('grant insert, update on primus.skade to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_status_h to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_formaal_h to pri_reg');
      
      execute immediate('grant insert, update on primus.adm_hendelse to pri_reg');
      execute immediate('grant insert, update on primus.adm_hendelse_person to pri_reg');
      execute immediate('grant insert, update on primus.adm_hendelse_undertype to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_anbefaling_h to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_formaal_h to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_kostnad to pri_reg');
      execute immediate('grant insert, update on primus.adm_h_tidsbruk to pri_reg');
      execute immediate('grant insert, update on primus.adm_hend_annotering to pri_reg');
      execute immediate('grant insert, update on primus.adm_hend_intervall to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_datering_h to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_idnr_h to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_prioritet_h to pri_reg');
      execute immediate('grant insert, update on primus.superobjekt_status_h to pri_reg');
      execute immediate('grant insert, update on primus.objekt_adm_hendelse to pri_reg');

      execute immediate('grant insert, update on primus.plass to pri_reg');

      execute immediate('grant insert, update on primus.publiserbaringress_h to pri_reg');
      execute immediate('grant insert, update on primus.publiserbartekst_h to pri_reg');      
      
    end if;

    jobbnummer := 81;
    if jobbnummer_inn <= jobbnummer then
      executeCreateTable( jobbnummer
                        , jobbnummer_inn 
                        , 'create table publiserbaroverskrift_h 
                            ( objid number( 8 ) 
                            , hid number( 9 )
                            , overskrift varchar2( 200 char )
                            , constraint pk_publiserbaroverskrift primary key ( objid, hid )
                            , constraint fk_pub_overskrift_hid foreign key( hid ) references primus.hendelse( hid )
                            , constraint fk_pub_overskrift_objid foreign key( objid) references primus.superobjekt( objid )
                            ) tablespace primusdt
                          ' );
      execute immediate('grant insert, update on primus.publiserbaroverskrift_h to pri_reg');
    end if;
    
    jobbnummer := 82;
    if jobbnummer_inn <= jobbnummer then 
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table superobjekt add   
                              ( hid_publiserbartekst number( 9 ) 
                              , hid_publiserbaringress number( 9 )
                              , hid_publiserbaroverskrift number( 9 )
                              , constraint fk_hid_publtekst_publtekst_h foreign key ( objid, hid_publiserbartekst ) references primus.publiserbartekst_h( objid, hid )
                              , constraint fk_hid_publingr_publingress_h foreign key ( objid, hid_publiserbaringress ) references primus.publiserbaringress_h( objid, hid )
                              , constraint fk_hid_publover_publover_h foreign key ( objid, hid_publiserbaroverskrift ) references primus.publiserbaroverskrift_h( objid, hid ) )' );
    end if;
    
    jobbnummer := 83;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('update superobjekt so
                             set ( so.hid_publiserbaringress, so.hid_publiserbartekst )
                                  = (select ah.hid_publiserbaringress
                                          , ah.hid_publiserbartekst 
                                       from primus.adm_hendelse ah
                                      where so.objid = ah.objid)');
      commit;
    end if;
    
    jobbnummer := 84;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ( 'alter table publiserbartekst_h add ( beskrivelsehtml clob )' );
    end if;
    
    jobbnummer := 85;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('alter table adm_hendelse 
                                 drop constraint FK_ADM_HENDELSE_HID_PINGRESS
                                 drop constraint FK_ADM_HENDELSE_HID_PTEKST 
                        ');
    end if;
    
    jobbnummer := 86;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('alter table adm_hendelse drop ( hid_publiserbartekst, hid_publiserbaringress )');
    end if;
    
    jobbnummer := 87;
    if jobbnummer_inn <= jobbnummer then
      execute immediate ('alter table publiserbaringress_h 
                                drop constraint FK_PUBLISERBARINGRESS_HHID
                        ');
    end if;
    
    jobbnummer := 88;
    if jobbnummer_inn <= jobbnummer then
      execute immediate('alter table publiserbaringress_h  
                           add constraint FK_PUBINGRESS_SO_OBJID 
                                  foreign key ( objid ) 
                               references primus.superobjekt ( objid )');
    end if;
    
    
    
    jobbnummer := 100;
      
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE(chr(13) || chr(10)||'****************************');
			SYS.DBMS_OUTPUT.PUT_LINE('Primus58 feilet! Jobbnummer: ' || jobbnummer);
			SYS.DBMS_OUTPUT.PUT_LINE('****************************'||chr(13)||chr(10));
			PRIMUSOPPGRADERINGSTOPP(58, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	                           
                          