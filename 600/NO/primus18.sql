create or replace function primus18(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
	problemkoder number;
	ex_rollekodeproblem exception;
begin
  dbms_output.enable;
  
  begin
	jobbnummer := 1;
	
	problemkoder := 0;
	if jobnummer_in <= jobbnummer then
    select count(*)
    into problemkoder
    from primus.rollejp_l
    where rollekode in ('120', '121', '122', '123', '240');
    
    if problemkoder > 0 then
      raise ex_rollekodeproblem;
    end if;
  end if;


	jobbnummer:=2;
	if svenskEllerNorskDatabase = 'NOR' then
		executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (120,''Utlåner, institusjon'', ''120'', ''X'', ''Y'')');
	else
	  executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (120,''Utlånare, institution'', ''120'', ''X'', ''Y'')');
	end if;
  
  jobbnummer:=3;
  if svenskEllerNorskDatabase = 'NOR' then
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (121,''Låntaker, institusjon'', ''121'', ''X'', ''Y'')');
	else
	  executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (121,''Låntagare, institution'', ''121'', ''X'', ''Y'')');
	end if;
	
  jobbnummer:=4;
	if svenskEllerNorskDatabase = 'NOR' then
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (122,''Utlåner, kontaktperson'',''122'', ''X'', ''Y'')');
	else
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (122,''Utlånare, kontaktperson'',''122'', ''X'', ''Y'')');
	end if;
	
  jobbnummer:=5;
	if svenskEllerNorskDatabase = 'NOR' then
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (123,''Låntaker, kontaktperson'', ''123'', ''X'', ''Y'')');
	else
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (123,''Låntagare, kontaktperson'', ''123'', ''X'', ''Y'')');
	end if;
	
  jobbnummer:=6;
	if svenskEllerNorskDatabase = 'NOR' then
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (240,''Ansvarlig for oppgave'',  240, ''X'', ''Y'')');
	else
	   executeScript(jobbnummer, jobnummer_in, 'Insert into ROLLEJP_L (ROLLEKODE, BESKRIVELSE, ROLLEID, CHECKFORDELETE, MERKE) VALUES (240,''Ansvarig för uppgift'',  240, ''X'', ''Y'')');
	end if;
	
  jobbnummer:=7;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(1,''Objekt'')');	
  
  jobbnummer:=8;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(2,''ACTOR'')');	
	
  jobbnummer:=9;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(3,''ADM_HEND'')');	
  
  jobbnummer:=10;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(4,''DAMAGE'')');		   
	
  jobbnummer:=11;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(5,''BILDE'')');		      
	
  jobbnummer:=12;
	executeScript(jobbnummer, jobnummer_in, 'INSERT INTO SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID, BESKRIVELSE) values(6,''KOLLI'')');		      
  
  jobbnummer:=13;
  if jobnummer_in <= jobbnummer then
	  DECLARE
      maxTid number;
	  begin
	    maxTid  := 99;
		  
      For TH in (select DISTINCT KODE, VERDI from LISTER where LISTE_TYPE=10 order by KODE)
			Loop			
        maxTid := maxTid+1;
			  execute immediate 'Insert INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) values (:a, :b)' using maxTid, TH.verdi;			  
			End Loop;
      
      commit;
	  end;	 
	End if;

	jobbnummer:=14;
	if svenskEllerNorskDatabase = 'NOR' then
	  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Fast'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLASSERING''))');
  else
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Fast'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLACERING''))');
  end if;
  
  jobbnummer:=15;
  if svenskEllerNorskDatabase = 'NOR' then
	  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select max(TYPEID)+1 from ADM_HENDELSETYPE_L), ''Midlertidig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLASSERING''))');
  else
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select max(TYPEID)+1 from ADM_HENDELSETYPE_L), ''Tillfällig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLACERING''))');
  end if;
  
  jobbnummer:=16;
  if svenskEllerNorskDatabase = 'NOR' then
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Filvedlegg'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
	else
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Filbilaga'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
  end if;
	
  jobbnummer:=17;
	if svenskEllerNorskDatabase = 'NOR' then
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Media'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
  else
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT MAX(TYPEID)+1 FROM ADM_HENDELSETYPE_L), ''Media'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
  end if;
  
  jobbnummer:=18;
  if svenskEllerNorskDatabase = 'NOR' then
	  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select max(TYPEID)+1 from ADM_HENDELSETYPE_L), ''Objekt'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
  else
    executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select max(TYPEID)+1 from ADM_HENDELSETYPE_L), ''Objekt'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
  end if;

  jobbnummer:=19;
	executeScript(jobbnummer,jobnummer_in,'ALTER TABLE ADM_HENDELSE_PERSON add (OBJID NUMBER(8,0), OBJID_SISTE NUMBER(8,0), NR NUMBER(3,0), FRADATO DATE, TILDATO  DATE)');


  jobbnummer:=21;
	executeScript(jobbnummer,jobnummer_in,'alter table adm_hendelse add (CONSTRAINT FK_ADM_HENDELSE_HID_TILSTAND FOREIGN KEY (OBJID,HID_TILSTAND) REFERENCES TILSTAND (OBJID, HID),
	                                                                              constraint FK_ADM_HENDELSE_HID_FORMAAL foreign key (objid, hid_formaal) references adm_h_formaal_h(objid, hid),
																				  constraint FK_ADM_Hendelse_HID_status_h foreign key (objid, hid_status) references superobjekt_status_h (objid, hid),
																				  constraint FK_ADM_Hendelse_PLASSKODE foreign key (plassid) references plasskode(plassid),
																				  constraint fk_ADM_anbefaling_h foreign key (objid, hid_anbefaling) references adm_h_anbefaling_h (objid, hid))');
  jobbnummer:=22;
	Commit;

	jobbnummer:=23;
	if jobnummer_in <= jobbnummer then	  
		declare
      nhid number;
		begin
			select hidseq.nextval into nhid from dual;
			
      execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;

      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(1, ''Annet'', ''Y'', ''61f5c4a5-93fc-4bd8-93fb-5561b9829503'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(2, ''Innlån'', ''Y'', ''fcb65c3b-e0c0-441a-b873-7cef3136ecc3'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(3, ''Innmeldt hendelse'', ''Y'', ''0daeffe3-bf12-4e93-adea-b5f961a6538e'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(4, ''Innmeldt skade'', ''Y'', ''7b9414fd-5e8d-4d0c-a9bd-8713300aa72b'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(5, ''Oppdrag'', ''Y'', ''ccc85249-15e1-442b-b959-12d0106f7588'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(6, ''Revisjonsprosjekt'', ''Y'', ''5d1cc471-8ef1-4309-abe5-181d369a24e9'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(7, ''Utlån'', ''Y'', ''90ee69fc-a82c-41cc-9d64-4d93b49b8507'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(8, ''Utstilling'', ''Y'', ''34938589-edf7-4611-ae12-42dc206476ed'', :a)' using nhid;
				EXECUTE IMMEDIATE	'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, uuid, hid_opprettet, filterfeltid)
			      values(9,''FDV-runde'', getoracleuuid(), :a, 3)' using nhid;     -- Objekttypeid for BYGNING er 3
			  EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, uuid, hid_opprettet, filterfeltid)
            values(10,''Innmeldt avvik'', getoracleuuid(), :a, 3)' using nhid; -- Objekttypeid for BYGNING er 3
      else
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(1, ''Annat'', ''Y'', ''61f5c4a5-93fc-4bd8-93fb-5561b9829503'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(2, ''Inlån'', ''Y'', ''fcb65c3b-e0c0-441a-b873-7cef3136ecc3'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(3, ''Anmäld händelse'', ''Y'', ''0daeffe3-bf12-4e93-adea-b5f961a6538e'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(4, ''Anmäld skada'', ''Y'', ''7b9414fd-5e8d-4d0c-a9bd-8713300aa72b'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(5, ''Uppdrag'', ''Y'', ''ccc85249-15e1-442b-b959-12d0106f7588'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(6, ''Revisionsprojekt'', ''Y'', ''5d1cc471-8ef1-4309-abe5-181d369a24e9'', :a)' using nhid;
        execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(7, ''Utlån'', ''Y'', ''90ee69fc-a82c-41cc-9d64-4d93b49b8507'', :a)' using nhid;
				execute immediate 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
				    values(8, ''Utställning'', ''Y'', ''34938589-edf7-4611-ae12-42dc206476ed'', :a)' using nhid;
				EXECUTE IMMEDIATE	'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, uuid, hid_opprettet, filterfeltid)
			      values(9,''FDV-runde'', getoracleuuid(), :a, 3)' using nhid;     -- Objekttypeid for BYGNING er 3
				EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, uuid, hid_opprettet, filterfeltid)
            values(10,''Anmäld avvikelse'', getoracleuuid(), :a, 3)' using nhid; -- Objekttypeid for BYGNING er 3
      end if;
			
      commit;
    exception
			when others then
				DBMS_OUTPUT.put_line('Feil ved insert av adm_h_formaaltype_l');
				rollback;
		end;
	end if;		

   
  jobbnummer:=24;
    
  if jobnummer_in <= jobbnummer then	
    declare
		  nhid number;
		  skadeid number;
		  skadeidParent number;
		begin
		  EXECUTE IMMEDIATE 'select hidseq.nextval from dual' INTO nhid;
		  EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
		
      if svenskEllerNorskDatabase = 'NOR' then
	      EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
        values (:a, ''Konservering'', NULL, ''/''||:b, :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;
        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
        values (:a, ''Daglig drift'', NULL, ''/''||:b, :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;
		    EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke, FILTERFELTID)
        values (:a, ''Antikvarisk vedlikehold'', NULL, ''/''||:b, :c, sys_guid(), ''Y'', 3)' using skadeid, skadeid, nhid;
        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke, FILTERFELTID)
        values (:a, ''Teknisk drift'', NULL, ''/''||:b, :c, sys_guid(), ''Y'', 3)' using skadeid, skadeid, nhid;
      else
        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
        values (:a, ''Konservering'', NULL, ''/''||:b, :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;
        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
        values (:a, ''Daglig drift'', NULL, ''/''||:b, :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;
		    EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke, FILTERFELTID)
        values (:a, ''Antikvariskt underhåll'', NULL, ''/''||:b, :c, sys_guid(), ''Y'', 3)' using skadeid, skadeid, nhid;
        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke, FILTERFELTID)
        values (:a, ''Teknisk drift'', NULL, ''/''||:b, :c, sys_guid(), ''Y'', 3)' using skadeid, skadeid, nhid;
      end if;
		  commit;
		exception
		  when others then
			DBMS_OUTPUT.put_line('Feil ved insert av skade_l');
			rollback;
		end;		
  End if;

	-- Dropping constraint in order to add damage descriptions to obj_beskrivelse_h.
  -- Probably a temporary solution
  jobbnummer:=28;
	executeScript(jobbnummer,jobnummer_in, 'alter table obj_beskrivelse_h drop constraint OBJEKT_BESKRIVELSE');
  
  jobbnummer:=29;
	executeScript(jobbnummer,jobnummer_in, 'alter table obj_beskrivelse_h add (constraint fk_obj_beskrivelse_h_superobj foreign key (objid) references superobjekt(objid))');

  jobbnummer:=30;
	executeScript(jobbnummer,jobnummer_in, 'alter table skade add (constraint fk_skade_skadetypeh_hid foreign key (objid, hid_skadetype) references skadetype_h (objid, hid),
	                                                                 constraint fk_skade_skade_pos_h foreign key (objid, hid_skadeposisjon) references skade_posisjon_h(objid, hid),
																	 constraint FK_HID_DATERING foreign key (objid, hid_datering) references SUPEROBJEKT_DATERING_H(OBJID, HID))');
  
  -- Dropping constraint in order to be able to add images to administrative events
  -- Possibly a temporary solution.   
  jobbnummer:=31;
  executeScript(jobbnummer,jobnummer_in, 'alter table objekt_bilde drop constraint OBJEKT_OBJ_BILDE');

  jobbnummer:=32;
  executeScript(jobbnummer,jobnummer_in, 'alter table objekt_bilde add (
          CONSTRAINT SUPEROBJEKT_OBJ_BILDE FOREIGN KEY (OBJID)
          REFERENCES SUPEROBJEKT (OBJID) ON DELETE CASCADE ENABLE)');

  -- PrimusWeb requires the CLOB field "INNHOLD" in table DOKUMENT in order to
  -- be able to do SOLR indexing of attachments.
  jobbnummer:=33;
  executeScript(jobbnummer,jobnummer_in,'alter table dokument add (innhold clob)');

  jobbnummer:=34;
	if jobnummer_in <= jobbnummer then	
    EXECUTE IMMEDIATE ' create or replace PROCEDURE adm_hendelse_person_update (pobjid IN NUMBER,
					  pnr in NUMBER,
					  poldhid IN NUMBER,
					  pnewhid IN NUMBER,
					  prolleid in number,
					  PROLLEKODE in varchar2,
					  PSTATUSID in number,
					  pfradato in date,
					  ptildato in date,
					  pjpnr IN NUMBER
					  ) IS
				BEGIN
					IF poldhid = pnewhid THEN
						UPDATE adm_hendelse_person SET rolleid = prolleid, rollekode = prollekode, 
							   ROLLEJPSTATUSID = PSTATUSID, fradato = pfradato, tildato = ptildato, jpnr = pjpnr
							   WHERE objid = pobjid  AND nr = pnr  AND hid = poldhid;
					ELSE
						UPDATE adm_hendelse_person	SET objid_siste = NULL
						WHERE objid = pobjid  AND nr = pnr  AND hid = poldhid;
						INSERT INTO adm_hendelse_person (OBJID, HID, NR, OBJID_SISTE, ROLLEID, ROLLEKODE, ROLLEJPSTATUSID, FRADATO, TILDATO, JPNR)
						VALUES	(pobjid, pnewhid, pnr, pobjid, prolleid, prollekode, PSTATUSID, pfradato, ptildato, pjpnr);
				 End if;	
				end ADM_HENDELSE_PERSON_UPDATE;';
	End if;			

	
  jobbnummer:=35;
  if svenskEllerNorskDatabase = 'NOR' then
	  executeScript(jobbnummer,jobnummer_in,'Update ADM_HENDELSETYPE_L Set TYPEID = 9 Where TEKST =''Behandling'' ');
	else
    executeScript(jobbnummer,jobnummer_in,'Update ADM_HENDELSETYPE_L Set TYPEID = 9 Where TEKST =''Behandling'' ');
  end if;
	jobbnummer:=36;
	if svenskEllerNorskDatabase = 'NOR' then
	  executeScript(jobbnummer,jobnummer_in,'Update ADM_HENDELSETYPE_L Set TYPEID = 11 Where TEKST =''Tilstandsvurdering'' ');
	else
    executeScript(jobbnummer,jobnummer_in,'Update ADM_HENDELSETYPE_L Set TYPEID = 11 Where TEKST =''Tillståndsvärdering'' ');
  end if;
  
  jobbnummer:= 37;
  commit;

  jobbnummer:= 38;	 	 
  return 'OK';		   
	exception
		when ex_rollekodeproblem then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus18 feilet! Problemer med rollejp_l - kontakt Anders!!!');
			PRIMUSOPPGRADERINGSTOPP(18, 'FEIL', jobbnummer, 'Primus18 feilet! Problemer med rolljp_l - kontakt Anders!!!');
			return 'FEIL';
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus18 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(18, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/
