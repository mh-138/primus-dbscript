create or replace function primus61(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  constraintName varchar2(30 char);
begin
	dbms_output.enable;

	begin
    jobbnummer := 10;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         inner join user_cons_columns ucc1
                 on ucc.constraint_name = ucc1.constraint_name
                and ucc1.column_name = 'HID_PUBLISERBARINGRESS'
                and ucc1.position = 2
              where uc.table_name like 'SUPEROBJEKT'
                and constraint_type = 'R';
        execute immediate 'alter table superobjekt drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 10' );
      end;
    
    end if;
    
    jobbnummer := 11;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         inner join user_cons_columns ucc1
                 on ucc.constraint_name = ucc1.constraint_name
                and ucc1.column_name = 'HID_PUBLISERBAROVERSKRIFT'
                and ucc1.position = 2
              where uc.table_name like 'SUPEROBJEKT'
                and constraint_type = 'R';
        execute immediate 'alter table superobjekt drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 11' );
      end;
    
    end if;
        
    jobbnummer := 12;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         inner join user_cons_columns ucc1
                 on ucc.constraint_name = ucc1.constraint_name
                and ucc1.column_name = 'HID_PUBLISERBARTEKST'
                and ucc1.position = 2
              where uc.table_name like 'SUPEROBJEKT'
                and constraint_type = 'R';
        execute immediate 'alter table superobjekt drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 12' );
      end;
      --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table superobjekt drop constraint FK_HID_PUBLTEKST_PUBLTEKST_H' );
    end if;
    
    jobbnummer := 13;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         inner join user_cons_columns ucc1
                 on ucc.constraint_name = ucc1.constraint_name
                and ucc1.column_name = 'HID_PUBLISERBARINGRESS'
                and ucc1.position = 2
              where uc.table_name like 'UTSTILLING'
                and constraint_type = 'R';
        execute immediate 'alter table UTSTILLING drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 13' );
      end;
    --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table utstilling drop constraint FK_UT_PUBLISERBARINGRESS' );
    end if;
    
    jobbnummer := 14;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         inner join user_cons_columns ucc1
                 on ucc.constraint_name = ucc1.constraint_name
                and ucc1.column_name = 'HID_PUBLISERBARTEKST'
                and ucc1.position = 2
              where uc.table_name like 'UTSTILLING'
                and constraint_type = 'R';
        execute immediate 'alter table UTSTILLING drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 14' );
      end;
    end if;
    
    jobbnummer := 15;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    if svenskEllerNorskDatabase = 'NOR' then 
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h add ( sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_PUBLISERBARINGR_H_SPRAKKODE not null )' );
    else  
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h add ( sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_PUBLISERBARINGR_H_SPRAKKODE not null )' );
    end if;
    jobbnummer := 16;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h drop constraint PK_PUBLISERBARINGRESS_H cascade' );

    jobbnummer := 17;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        execute Immediate 'drop index PK_PUBLISERBARINGRESS_H';
      exception
        when others then
          if (SQLCODE = -1418) then
            null;
          else
            raise;
          end if;
      end;
    end if;
    
    -- sjekker om publiserbaringress_h har FK mot superobjekt - denne skal flyttes ett steg ned til 
    -- SUPEROBJEKT_PUBLISERINGSINFO og utvides til å ha med sprakkode.
    jobbnummer := 18;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         where uc.table_name like 'PUBLISERBARINGRESS_H'
           and constraint_type = 'R'
           and r_constraint_name = 'I01_SUPEROBJEKT';
        execute immediate 'alter table PUBLISERBARINGRESS_H drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 18' );
      end;
    end if;    
    
    
    --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h drop constraint FK_PUBINGRESS_SO_OBJID' );
    
    jobbnummer := 19;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         where uc.table_name like 'PUBLISERBARINGRESS_H'
           and constraint_type = 'R'
           and r_constraint_name = 'PK_OBJID_U';
        execute immediate 'alter table PUBLISERBARINGRESS_H drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 19' );
      end;
    end if;    
   
    
    jobbnummer := 20;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h add ( constraint PK_PUBLISERBARINGRESS_H primary key (objid, hid, sprakkode ) )' );

    jobbnummer := 21;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    if svenskEllerNorskDatabase = 'NOR' then 
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaroverskrift_h add( sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_PUBLISERBAROVER_H_SPRAKKODE not null ) ');
    else
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaroverskrift_h add( sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_PUBLISERBAROVER_H_SPRAKKODE not null ) ');
    end if;
    
    jobbnummer := 22;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select constraint_name 
          into constraintName
          from user_constraints
         where table_name =  'PUBLISERBAROVERSKRIFT_H'
           and Constraint_Type = 'P'; 
              execute immediate 'alter table PUBLISERBAROVERSKRIFT_H drop constraint ' || constraintName || ' cascade';
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 22' );
      end;

      --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaroverskrift_h drop constraint PK_PUBLISERBAROVERSKRIFT cascade' );
    end if;
    
    
    jobbnummer := 23;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaroverskrift_h add ( constraint PK_PUBLISERBAROVERSKRIFT_H primary key (objid, hid, sprakkode ) ) ');

    jobbnummer := 24;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    if svenskEllerNorskDatabase = 'NOR' then 
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h add ( sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_PUBLISERBARTEKST_H_SPRAKKOD not null  ) ');
    else
      executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h add ( sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_PUBLISERBARTEKST_H_SPRAKKOD not null  ) ');
    end if;
    jobbnummer := 25;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h add ( beskrivelseRTF clob ) ');

    jobbnummer := 26;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select constraint_name 
          into constraintName
          from user_constraints
         where table_name =  'PUBLISERBARTEKST_H'
           and Constraint_Type = 'P'; 
              execute immediate 'alter table PUBLISERBARTEKST_H drop constraint ' || constraintName || ' cascade';
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 26' );
      end;
    end if;
    --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h drop constraint PK_PUBLISERBARTEKST_H cascade ');

    jobbnummer := 27;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        execute immediate 'drop index PK_PUBLISERBARTEKST_H';
      exception
        when others then
          if (SQLCODE = -1418) then
            null;
          else
            raise;
          end if;
      end;
    end if;

    jobbnummer := 28;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select uc.constraint_name
          into constraintName
          from user_constraints uc
         inner join user_cons_columns ucc 
                 on uc.constraint_name = ucc.constraint_name
                and ucc.column_name = 'OBJID'
                and ucc.position = 1
         where uc.table_name like 'PUBLISERBARTEKST_H'
           and constraint_type = 'R'
           and r_constraint_name = 'I01_SUPEROBJEKT';
        execute immediate 'alter table PUBLISERBARTEKST_H drop constraint ' || constraintName;
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 28' );
      end;
    end if;    
    --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h drop constraint FK_PUBLISERBARTEKST_H_ADMHND' );

    jobbnummer := 29;    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      
            PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      begin
        select constraint_name 
          into constraintName
          from user_constraints
         where table_name =  'PUBLISERBARTEKST_H'
           and Constraint_Type = 'P'; 
              execute immediate 'alter table PUBLISERBARTEKST_H drop constraint ' || constraintName || ' cascade';
      exception
        when no_data_found then
          dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 26' );
      end;
      --executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h drop constraint PK_PUBLISERBARTEKST_H cascade ');
    end if;
    
    jobbnummer := 30;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h add ( constraint PK_PUBLISERBARTEKST_H primary key ( objid, hid, sprakkode ) ) ');

    jobbnummer := 31;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      execute immediate 'comment on column primus.publiserbartekst_h.beskrivelse is ''Inneholder verdien uten tekstformatering'' ';
      execute immediate 'comment on column primus.publiserbartekst_h.beskrivelseHTML is ''Inneholder verdien med html fomatering. Dette er master verdien. Tekstverdien er generert ut fra denne'' ';
    end if;

    jobbnummer := 32;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then 

        execute immediate 
         ' create table superobjekt_publiseringsinfo
            ( objid number( 8 )
            , sprakkode varchar2( 3 char ) default ''NOR'' constraint CK_SUPEROBJEKT_PUBINFO_SPRKKDE not null
            , hid_publiserbaroverskrift number( 9 )
            , hid_publiserbaringress number( 9 )
            , hid_publiserbartekst number( 9 )
            , rekkefolge number( 3 )
            , constraint pk_superobjekt_pubinfo primary key ( objid, sprakkode )
            , constraint fk_superobjekt_pubinfo_so foreign key ( objid ) references primus.superobjekt( objid )
            , constraint fk_superobjekt_pbinf_oversk foreign key ( objid, sprakkode, hid_publiserbaroverskrift ) references primus.publiserbaroverskrift_h ( objid, sprakkode, hid )
            , constraint fk_superobjekt_pbinf_ingress foreign key ( objid, sprakkode, hid_publiserbaringress ) references primus.publiserbaringress_h ( objid, sprakkode, hid )
            , constraint fk_superobjekt_pbinf_tekst foreign key ( objid, sprakkode, hid_publiserbartekst ) references primus.publiserbartekst_h ( objid, sprakkode, hid )
            ) tablespace primusdt';
      else
        execute immediate 
         ' create table superobjekt_publiseringsinfo
            ( objid number( 8 )
            , sprakkode varchar2( 3 char ) default ''SWE'' constraint CK_SUPEROBJEKT_PUBINFO_SPRKKDE not null
            , hid_publiserbaroverskrift number( 9 )
            , hid_publiserbaringress number( 9 )
            , hid_publiserbartekst number( 9 )
            , rekkefolge number( 3 )
            , constraint pk_superobjekt_pubinfo primary key ( objid, sprakkode )
            , constraint fk_superobjekt_pubinfo_so foreign key ( objid ) references primus.superobjekt( objid )
            , constraint fk_superobjekt_pbinf_oversk foreign key ( objid, sprakkode, hid_publiserbaroverskrift ) references primus.publiserbaroverskrift_h ( objid, sprakkode, hid )
            , constraint fk_superobjekt_pbinf_ingress foreign key ( objid, sprakkode, hid_publiserbaringress ) references primus.publiserbaringress_h ( objid, sprakkode, hid )
            , constraint fk_superobjekt_pbinf_tekst foreign key ( objid, sprakkode, hid_publiserbartekst ) references primus.publiserbartekst_h ( objid, sprakkode, hid )
            ) tablespace primusdt';
      end if;
    end if;
    
    jobbnummer := 33;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      execute immediate 'grant insert, update on superobjekt_publiseringsinfo to pri_reg'; 
    end if;
    
    jobbnummer := 34;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 61, jobbnummer );
      execute immediate 
          ' insert into superobjekt_publiseringsinfo 
                 ( objid
                 , hid_publiserbaroverskrift
                 , hid_publiserbaringress
                 , hid_publiserbartekst)
                  select objid
                       , hid_publiserbaroverskrift
                       , hid_publiserbaringress
                       , hid_publiserbartekst 
                    from primus.superobjekt';
                    
      commit;              
    end if;

    jobbnummer := 35;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaringress_h add ( constraint FK_PUBINGRESS_SO_PUB foreign key (objid, sprakkode) references superobjekt_publiseringsinfo( objid, sprakkode ) )');

    jobbnummer := 36;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbaroverskrift_h add ( constraint FK_PUBOVERSK_SO_PUB foreign key ( objid, sprakkode ) references superobjekt_publiseringsinfo( objid, sprakkode ) )');

    jobbnummer := 37;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table publiserbartekst_h add ( constraint FK_PUBTEKST_SO_PUB foreign key ( objid, sprakkode ) references superobjekt_publiseringsinfo( objid, sprakkode ) ) ');

    jobbnummer := 38;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table superobjekt drop column hid_publiserbaroverskrift' );

    jobbnummer := 39;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table superobjekt drop column hid_publiserbaringress' );

    jobbnummer := 40;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    executeAlterTable( jobbnummer,jobbnummer_inn, 'alter table superobjekt drop column hid_publiserbartekst' );
    
    jobbnummer := 41;
    PrimusOppdJobbNrIVersjon( 61, jobbnummer );
    execute immediate 'create or replace function XML_ELEMENT_TILSTAND(objid_in number) return xmltype as objectxml xmltype;
begin
  SELECT XMLAGG(XMLELEMENT(tilstand, XMLFOREST( pt.tilstandskode as tilstandskode
       , tl.beskrivelse as tilstand
       , pt.beskrivelse as tilstandbeskrivelse )))
    INTO objectxml
       from (
          select oah.objid
               , ah.objid as admobjid
               , ah.hid_tilstand as hidTilst
               , sod.fradato
               , sod.tildato
          from objekt_adm_hendelse oah
          inner join adm_hendelse ah
                  on ah.objid = oah.admh_objid
                 and ah.hovedtype = 11
                 and ah.hid_slettet is null
          inner join superobjekt_datering_h sod
                  on sod.objid = ah.objid
                 and sod.hid = ah.hid_datering
          where oah.objid_siste = objid_in
            and oah.hid_slettet is null
          order by sod.fradato desc) gyldig
  left outer join Tilstand pt
               on pt.objid = gyldig.admobjid
              and pt.hid = Gyldig.hidTilst
  left outer join tilstand_l tl
               on tl.kode = pt.tilstandskode
  where rownum = 1;
  
  return Objectxml;
end;';

    jobbnummer := 100;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus61 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(61, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
