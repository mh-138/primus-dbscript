create or replace function primus48(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;	/*		Data overføring fra MUSHENDELSE til ADM_HENDELSE.	*/
	begin
	
		 jobbnummer:=1;
 		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table MUSHENDELSE add (ADMH_OBJID NUMBER(8,0), NEWHOVEDTYPE NUMBER(3,0), NEWUNDERTYPE NUMBER(3,0), MERKE VARCHAR2(1 CHAR))');      		 
		 jobbnummer:= jobbnummer+1; 
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table UTLAAN add (ADMH_OBJID NUMBER(8,0), MERKE varchar2(1 CHAR))');   				 
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table TILSTAND_H add (ADMH_OBJID NUMBER(8,0), MERKE varchar2(1 CHAR))');			 
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table PLASSKODE add (ADMH_OBJID NUMBER(8,0), MERKE VARCHAR2(1 CHAR))');	
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table MIDLPLASS add (ADMH_OBJID NUMBER(8,0), MERKE VARCHAR2(1 CHAR))'); 
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table OBJEKT_ADM_HENDELSE add (HID_SLETTET NUMBER(8,0), MERKE VARCHAR2(1 CHAR),constraint FK_OADM_HENDELSE_HID_SLETTET foreign key(HID_SLETTET) references HENDELSE(HID))');  
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table OBJEKT_ADM_HENDELSE MODIFY (FRADATO TIMESTAMP(6))');
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table OBJEKT_ADM_HENDELSE MODIFY (KOMMENTAR VARCHAR2(4000 CHAR))');		 
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table OBJEKT_UTLAAN add (ADMH_OBJID NUMBER(8,0), MERKE varchar2(1 CHAR))');   
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table OBJEKT_UTSTILLING add (ADMH_OBJID NUMBER(8,0), MERKE varchar2(1 CHAR))');   
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table UTSTILLING add (ADMH_OBJID NUMBER(8,0), MERKE varchar2(1 CHAR), NR NUMBER(3,0))');	
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table SUPEROBJEKT_HENDELSE add ( MERKE varchar2(1 CHAR))');			 
		 jobbnummer:= jobbnummer+1;
		 executeAlterTable(jobbnummer,jobnummer_in, 'alter table PLASS add (BESKRIVELSE VARCHAR2(200 CHAR))');	
		 jobbnummer:= jobbnummer+1;
         executeAlterTable(jobbnummer,jobnummer_in, 'alter table SUPEROBJEKT_HENDELSE_FELTER add (VALUTAID NUMBER(8,0))');    
         jobbnummer:= jobbnummer+1;
	      executeAlterTable(jobbnummer,jobnummer_in, 'ALTER TABLE  ADM_HENDELSE ADD (DIMUKODE VARCHAR2(50 CHAR))');
				 

         jobbnummer:= jobbnummer+1;
		 if jobnummer_in <= jobbnummer then	
          commit;
		 End if; 
		
		 if jobnummer_in <= jobbnummer then	
		  execute immediate ('CREATE TABLE ADM_HENDELSE_UNDERTYPE_L (TYPEID NUMBER (3,0),
							TEKST VARCHAR2 (50 CHAR) NOT NULL, PARENT_ID NUMBER(3,0),
							CONSTRAINT PK_ADM_HENDELSE_UNDERTYPE_L PRIMARY KEY (TYPEID),
							CONSTRAINT UN_ADM_HENDELSE_UT_L_TKT_PID UNIQUE(TEKST, PARENT_ID),
							CONSTRAINT FK_ADM_HENDELSE_UT_L_PID FOREIGN KEY (PARENT_ID) REFERENCES ADM_HENDELSETYPE_L (TYPEID)								
							) tablespace primusdt');
           End if;
        jobbnummer:= jobbnummer+1;
        if jobnummer_in <= jobbnummer then	
		 execute immediate ('CREATE TABLE ADM_HENDELSE_UNDERTYPE (OBJID NUMBER (8,0), OBJID_SISTE NUMBER (8,0), 
		                     HID NUMBER (8,0), NR NUMBER (3,0), TYPEID NUMBER(3,0),
							CONSTRAINT PK_ADM_HENDELSE_UNDERTYPE PRIMARY KEY (OBJID, HID, NR),
							CONSTRAINT FK_ADM_HENDELSE_UT_OBJID FOREIGN KEY (OBJID) REFERENCES ADM_HENDELSE (OBJID),
							CONSTRAINT FK_ADM_HENDELSE_UT_OBJIDS FOREIGN KEY (OBJID_SISTE) REFERENCES ADM_HENDELSE (OBJID),
							CONSTRAINT FK_ADM_HENDELSE_UT_HID FOREIGN KEY (HID) REFERENCES HENDELSE (HID),
                            CONSTRAINT FK_ADM_HENDELSE_UT_TYPEID FOREIGN KEY (TYPEID) REFERENCES ADM_HENDELSE_UNDERTYPE_L (TYPEID)															
							) tablespace primusdt');
        End if;

		jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in, 'alter table OBJEKT_ADM_HENDELSE drop COLUMN HID_AVSLUTTET_OR_INNLEVERT');
		
        jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in, 'Update ADM_HENDELSE_PERSON AHP set AHP.OBJID_SISTE=(SELECT ut.objid from UTSTILLING ut   WHERE ut.hid=AHP.HID) Where AHP.OBJID is NULL');  
        jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in, 'Update ADM_HENDELSE_PERSON AHP set AHP.OBJID=AHP.OBJID_SISTE Where OBJID is NULL');           
		 
        jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in, 'alter table OBJEKT_ADM_HENDELSE add(INNLEVERT date)');
		
        		
		jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in,'alter table ADM_HENDELSE ADD (HID_AVSLUTTET NUMBER(8,0), CONSTRAINT FK_ADM_HENDELSE_HID_AVSLUTTET FOREIGN KEY (HID_AVSLUTTET) REFERENCES HENDELSE(HID) )');		
		
		  
		jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in,'DELETE From ADM_HENDELSETYPE_L WHERE PARENT_ID is NOT NULL');
		  
		jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in,'alter table PRIMUS.OPPL_H drop constraint OBJEKT_OPPL '); 
		
		jobbnummer:= jobbnummer+1;	
		  executeScript(jobbnummer,jobnummer_in,'alter table PRIMUS.OPPL_H add constraint FK_OPPL_H_OBJID foreign key(OBJID) references PRIMUS.SUPEROBJEKT(OBJID)');
		  
		
    jobbnummer:= jobbnummer+1; 
		if jobnummer_in <= jobbnummer then	
			  DECLARE		  
				iTId Integer;				
			  BEGIN  
				  SELECT TYPEID INTO iTId FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) IN ('UTSTILLING', 'UTSTÄLLNING');				  
				  For TH in (select DISTINCT TYPEID, BESKRIVELSE FROM UTSTILLINGSTYPE_L UL)
					Loop						   		  
					   execute immediate 'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES (:a, :b, :c)' using TH.TYPEID, TH.BESKRIVELSE, iTId;
					END LOOP;		 
				  commit;		  
			  exception 
          when NO_DATA_FOUND then
            dbms_output.put_line( ' Undertype for Utstilling ikke oppdatert. ' );
        end;

		End if;    
  

		
		jobbnummer:= jobbnummer+1;	
		if svenskEllerNorskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Fast'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) = ''PLASSERING''))');
    else
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Fast'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) = ''PLACERING''))');
    end if;
    
		jobbnummer:= jobbnummer+1;	 
		if svenskEllerNorskDatabase = 'NOR' then
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Midlertidig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLASSERING''))');
    else
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Midlertidig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PLACERING''))');
    end if;
    
		jobbnummer:= jobbnummer+1;	
		if svenskEllerNorskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Filvedlegg'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
    else
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Filvedlegg'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
    end if;
    
		jobbnummer:= jobbnummer+1;	
    if svenskEllerNOrskDatabase = 'NOR' then
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Media'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
		else
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Media'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
    end if;
    
		jobbnummer:= jobbnummer+1;	
    if svenskEllerNorskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Objekt'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
		else
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Objekt'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
    end if;
    
    jobbnummer:= jobbnummer+1;	
    if svenskEllerNorskDatabase = 'NOR' then
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Midlertidig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLISERING''))');
    else
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Midlertidig'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) =''PUBLICERING''))');
    end if;
    jobbnummer:= jobbnummer+1;		    
    if svenskEllerNorskDatabase = 'NOR' then
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Utlån'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) like ''UTL%N''))');
    else
		  executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((SELECT nvl(MAX(TYPEID), 0) + 1 FROM ADM_HENDELSE_UNDERTYPE_L), ''Utlån'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) like ''UTL%N''))');
    end if;
    
    jobbnummer:= jobbnummer+1;
    if svenskEllerNorskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Depositum'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) like ''UTL%N''))');
    else
      executeScript(jobbnummer,jobnummer_in,'INSERT INTO ADM_HENDELSE_UNDERTYPE_L (TYPEID, TEKST, PARENT_ID) VALUES ((select nvl(MAX(TYPEID), 0) + 1 from ADM_HENDELSE_UNDERTYPE_L), ''Deposition'', (SELECT TYPEID FROM ADM_HENDELSETYPE_L WHERE UPPER(Tekst) like ''UTL%N''))');
    end if;
    
    jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
		  execute immediate 'update MUSHENDELSE M set M.UNDERTYPE=''Utlån'' Where M.utlaanid in(select utlaanid from UTLAAN WHERE UTLTYPE=''U'') ';	   
		  commit;
		End if;
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'update MUSHENDELSE M set M.UNDERTYPE=''Depositum'' Where M.utlaanid in(select utlaanid from UTLAAN WHERE UTLTYPE=''D'') ';	   
		  else
        execute immediate 'update MUSHENDELSE M set M.UNDERTYPE=''Deposition'' Where M.utlaanid in(select utlaanid from UTLAAN WHERE UTLTYPE=''D'') ';	   
      end if;
      commit;
		End if;		 
	   
		
		
		jobbnummer:= jobbnummer+1;		
    if svenskEllerNOrskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in, 'MERGE INTO ADM_HENDELSETYPE_L  USING DUAL ON (TEKST=''Innlevert'' ) 
		                                          WHEN NOT MATCHED THEN  INSERT (TYPEID, TEKST) VALUES (200, ''Innlevert'')');
    else
      executeScript(jobbnummer,jobnummer_in, 'MERGE INTO ADM_HENDELSETYPE_L  USING DUAL ON (TEKST=''Återlämnat'' ) 
		                                          WHEN NOT MATCHED THEN  INSERT (TYPEID, TEKST) VALUES (200, ''Återlämnat'')');
    end if;
		
    jobbnummer:= jobbnummer+1;		
    if svenskEllerNOrskDatabase = 'NOR' then
      executeScript(jobbnummer,jobnummer_in, 'MERGE INTO ADM_HENDELSETYPE_L  USING DUAL ON (TEKST=''Avsluttet'' ) 
		                                          WHEN NOT MATCHED THEN  INSERT (TYPEID, TEKST) VALUES (201, ''Avsluttet'')');			
		else
      executeScript(jobbnummer,jobnummer_in, 'MERGE INTO ADM_HENDELSETYPE_L  USING DUAL ON (TEKST=''Avsluttet'' ) 
		                                          WHEN NOT MATCHED THEN  INSERT (TYPEID, TEKST) VALUES (201, ''Avslutad'')');			
    end if;
    

		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then	
		   execute immediate 'update MUSHENDELSE M set M.NEWHOVEDTYPE=(select TYPEID from ADM_HENDELSETYPE_L AL WHERE AL.TEKST=M.HOVEDTYPE)';	
		   commit   ;
		End if;

		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
		  execute immediate 'update MUSHENDELSE M set M.NEWUNDERTYPE=(select TYPEID from ADM_HENDELSE_UNDERTYPE_L AL WHERE AL.TEKST=M.UNDERTYPE and AL.PARENT_ID=M.NEWHOVEDTYPE)';	   
		  commit;
		End if;

		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
		  execute immediate 'UPDATE MUSHENDELSE M 
                            SET M.NEWUNDERTYPE=(select TYPEID from ADM_HENDELSE_UNDERTYPE_L AL WHERE AL.TEKST like ''Utl%n'' and AL.PARENT_ID=M.NEWHOVEDTYPE) 
                          WHERE M.UTLAANID is not NULL
                            AND m.utlaanid in (select utlaanid from utlaan where utltype = ''U'')';	   
		  commit;
		End if;

		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'UPDATE MUSHENDELSE M 
                              SET M.NEWUNDERTYPE=(select TYPEID from ADM_HENDELSE_UNDERTYPE_L AL WHERE AL.TEKST like ''Depositum'' and AL.PARENT_ID=M.NEWHOVEDTYPE)  
                            WHERE M.UTLAANID is not NULL 
                              AND utlaanid in (select utlaanid from utlaan where utltype = ''D'')';	   
		  else
        execute immediate 'UPDATE MUSHENDELSE M 
                              SET M.NEWUNDERTYPE=(select TYPEID from ADM_HENDELSE_UNDERTYPE_L AL WHERE AL.TEKST like ''Deposition'' and AL.PARENT_ID=M.NEWHOVEDTYPE)  
                            WHERE M.UTLAANID is not NULL
                              AND utlaanid in (select utlaanid from utlaan where utltype = ''D'')';	   
      end if;
      commit;
		End if;		
	
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE GETNEW_OBJID(nobjid OUT NUMBER, nhid IN number) 
        AUTHID CURRENT_USER IS     			
			  BEGIN
				 Select OBJIDSEQ.NEXTVAL into nobjid from dual;  
				 Insert into SUPEROBJEKT (OBJID, SO_TYPE, SUPEROBJEKTTYPEID) values (nobjid, ''ADM_HEND'', 3);
				 Insert into SUPEROBJEKT_HENDELSE (OBJID, HID, MERKE) values (nobjid, nhid, ''X'');			 			
			  END GETNEW_OBJID;';
		End if;	
		
    jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate 'create or replace PROCEDURE GETHIDFROM_GIVENDATE(nHid OUT NUMBER, nobjid number, dDato date) 
      AUTHID CURRENT_USER IS			
			BEGIN
			   Select HIDSEQ.NEXTVAL into nHid from dual;
			   INSERT into PRIMUS.HENDELSE (HID, DATO, SIGNID) VALUES (nhid, dDato, 1);
			   INSERT into PRIMUS.SUPEROBJEKT_HENDELSE (OBJID, HID) values (NOBJID, NHID);			 			
			END GETHIDFROM_GIVENDATE;';
		End if;	
	
		jobbnummer:= jobbnummer+1;	--26
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_DATERING(nAHobjid NUMBER, nhid number) 
        AUTHID CURRENT_USER IS     			
			  BEGIN			 
				 Insert Into SUPEROBJEKT_DATERING_H(OBJID, HID, FRADATO, TILDATO)(select DISTINCT nAHobjid, nhid, DATO, TIL_DATO From MUSHENDELSE WHERE hid=nhid); 
				 UPDATE ADM_HENDELSE set HID_DATERING = nHid Where objid = nAHobjid;			 
			  END SAVE_AH_DATERING;';
		End if;	
		
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AHUT_DATERING(nAHobjid NUMBER, nhid number, dfradato date, dtildato date) 
        AUTHID CURRENT_USER IS     			
			  BEGIN	
                		  
				 Insert Into SUPEROBJEKT_DATERING_H(OBJID, HID, FRADATO, TILDATO) values(nAHobjid, nhid, dFRADATO, dTILDATO ); 
				 UPDATE ADM_HENDELSE set HID_DATERING = nHid Where objid = nAHobjid;			 
				
			  END SAVE_AHUT_DATERING;';
		End if;	
	
	
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_BESKRIVELSE(nAHobjid NUMBER, nhid number) 
        AUTHID CURRENT_USER IS     			
			  BEGIN			 			 
				 Insert Into OBJ_BESKRIVELSE_H(OBJID, HID, BESKRIVELSE)(select nAHobjid, nhid, tekst From MUSHENDELSE WHERE hid=nhid and TEKST is not NULL); 
				 UPDATE ADM_HENDELSE set HID_BESKRIVELSE = nHid where objid = nAHobjid;			 
			  END SAVE_AH_BESKRIVELSE;';
		End if;	
		
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_FORMAAL(nAHobjid NUMBER, nhid number, frmlType number, formaal varchar2)
        AUTHID CURRENT_USER IS     			
			  BEGIN			 			 
				 Insert Into ADM_H_FORMAAL_H(OBJID, HID, FORMAAL, FORMAALTYPEID) values (nAHobjid, nhid, formaal, frmlType); 
				 UPDATE ADM_HENDELSE set HID_FORMAAL = nHid where objid = nAHobjid;			 
			  END SAVE_AH_FORMAAL;';
		End if;	
		
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_ANDREOPPL(nAHobjid NUMBER, nhid number,  andreoppl varchar2) 
        AUTHID CURRENT_USER IS     			
			  BEGIN			 			 
				 Insert Into OPPL_H(OBJID, HID, OPPL2) values (nAHobjid, nhid, andreoppl); 
				 UPDATE ADM_HENDELSE set HID_OPPL = nHid where objid = nAHobjid;			 
			  END SAVE_AH_ANDREOPPL;';
		End if;	
		
	jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
		   execute immediate 'create or replace PROCEDURE SAVE_AH_IDNR(nAHobjid NUMBER, nhid number, vidnr varchar2) 
       AUTHID CURRENT_USER IS     			
			  BEGIN		
		   	   Insert Into SUPEROBJEKT_IDNR_H(OBJID, HID, IDENTIFIKASJONSNR) values (nAHobjid, nhid, vidnr); 
			   UPDATE ADM_HENDELSE set HID_IDNR =nhid where objid= nAHobjid;         			 
		    END SAVE_AH_IDNR;';
		End if;
		
	jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AHINN_DATERING(nAHobjid NUMBER, nhid number, dinnlevert date) 
        AUTHID CURRENT_USER IS     			
			  BEGIN			 
				 Insert Into SUPEROBJEKT_DATERING_H(OBJID, HID, FRADATO, TILDATO) values(nAHobjid, nhid, dinnlevert, dinnlevert ); 
				 UPDATE ADM_HENDELSE set HID_DATERING = nHid Where objid = nAHobjid;			 
			  END SAVE_AHINN_DATERING;';
		End if;	
		
		jobbnummer:= jobbnummer+1;	
		executeScript(jobbnummer,jobnummer_in,'alter table PRIMUS.DOKUMENT add (FILNAVN varchar2(100 CHAR), DOKUMENTTYPEID NUMBER(3,0))'); 
		
		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate ' CREATE TABLE UTLAAN_DOK_TEMP (UTLAANID NUMBER (8,0), FILNAVN VARCHAR2(100 CHAR), DOKUMENT BLOB, DOKUMENTTYPEID NUMBER(3,0), DOKID NUMBER(8,0) )';
		end if;
		
		jobbnummer:= jobbnummer+1;
		if jobnummer_in <= jobbnummer then		
			execute immediate 'INSERT INTO UTLAAN_DOK_TEMP SELECT UTLAANID, FILNAVN, TO_LOB(DOKUMENT),DOKUMENTTYPEID,  DOKID  from UTLAAN_DOK Where FILNAVN is not NULL and DOKUMENT is not NULL';
		End if;	

		jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_UTLAAN_DOK(nUTDOKID NUMBER, nAHobjid NUMBER) 
        AUTHID CURRENT_USER IS     			
			  ndokid number;
			  BEGIN	
			     Select DOKIDSEQ.NEXTVAL into ndokid from dual;  		          	
                 execute immediate ''Insert Into DOKUMENT(DOKID, FILNAVN, BESKRIVELSE, FILTYPE,BYTES, BLOB, DOKUMENTTYPEID)(select :a, FILNAVN, FILNAVN, SUBSTR(FILNAVN, length(FILNAVN)-2), length(DOKUMENT),  DOKUMENT, DOKUMENTTYPEID  From UTLAAN_DOK_TEMP WHERE DOKID=:b)'' using ndokid, nUTDOKID; 				 				 
				 execute immediate ''Insert Into DOKNOTAT_SUPEROBJEKT(OBJID, DOKID) values (:a, :b) '' using nAHobjid, ndokid; 
				 
			  END SAVE_AH_UTLAAN_DOK;';
		End if;			
	
	jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AHUT_OBJEKT(nAHobjid NUMBER, nhid number, nutlaanid number) 
        AUTHID CURRENT_USER IS  
          iHid_Innlevert NUMBER;
			  BEGIN			 
			     For TH in (select DISTINCT MP.OBJID, MP.UTLAANID, UT.FRADATO, UT.TILDATO, MP.HID_SLETTET, MP.KOMMENTAR, MP.TILBAKELEVERT 			                             
						    FROM OBJEKT_UTLAAN MP, UTLAAN UT  Where MP.UTLAANID = UT.UTLAANID 						 
						    AND MP.MERKE is NULL AND MP.UTLAANID=nutlaanid)
			     Loop
                     Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO, HID_SLETTET, KOMMENTAR, INNLEVERT) 
								   values (nAHobjid, TH.OBJID, TH.OBJID, nHid, 1,  TH.FRADATO, TH.TILDATO, TH.HID_SLETTET, TH.KOMMENTAR, TH.TILBAKELEVERT); 				   
				    
				   update OBJEKT_UTLAAN set MERKE=''X'' Where UTLAANID = TH.UTLAANID AND OBJID=TH.OBJID;                   
				 END LOOP;                 				 
			  END SAVE_AHUT_OBJEKT;';
		End if;	

	
    jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AHUTS_OBJEKT(nAHobjid NUMBER, nhid number) 
        AUTHID CURRENT_USER IS     			
			  BEGIN			 
			     For TH in (select DISTINCT MP.OBJID,  MP.FRADATO, MP.TILDATO, MP.KATALOGNR, MP.KOMMENTAR, MP.NR, MP.OBJID_SISTE			                             
								FROM OBJEKT_UTSTILLING MP Where MP.BESKRIVENDE_OBJID = nAHobjid	 AND MP.MERKE is NULL)
			     Loop
                   Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO, KATALOGNR, KOMMENTAR) 
								   values (nAHobjid, TH.OBJID, TH.OBJID_SISTE, nHid, TH.nr,  TH.FRADATO, TH.TILDATO, TH.KATALOGNR, TH.KOMMENTAR); 
				   update OBJEKT_UTSTILLING set MERKE=''X'' Where BESKRIVENDE_OBJID = nAHobjid AND OBJID=TH.OBJID;                   
				 END LOOP;                 				 
			  END SAVE_AHUTS_OBJEKT;';
		End if;	
		
		
---	jobbnummer:= jobbnummer+1;
---	   executeScript(jobbnummer, jobnummer_in, 'Update ROLLEJP_L Set  ROLLEKODE=''241'' , ROLLEID =241 WHERE BESKRIVELSE like ''Utl%ner. Inst.'' ');	
---    jobbnummer:= jobbnummer+1;
---	   executeScript(jobbnummer, jobnummer_in, 'Update ROLLEJP_L Set  ROLLEKODE=''242'' , ROLLEID =242 WHERE BESKRIVELSE like ''Utl%ner. Kontaktperson'' ');	
---    jobbnummer:= jobbnummer+1;
---	   executeScript(jobbnummer, jobnummer_in, 'Update ROLLEJP_L Set  ROLLEKODE=''243'' , ROLLEID =243 WHERE BESKRIVELSE like ''L%ntaker. Inst.'' ');	
---    jobbnummer:= jobbnummer+1;
---	   executeScript(jobbnummer, jobnummer_in, 'Update ROLLEJP_L Set  ROLLEKODE=''244'' , ROLLEID =244 WHERE BESKRIVELSE like ''L%ntaker. Kontaktperson'' ');	
    
	---- forsøker en recompile av primus49 til 56...
  
  jobbnummer := 60;
	execute immediate ( 'alter function primus49 compile' );
  jobbnummer := 61;
	execute immediate ( 'alter function primus50 compile' );
  jobbnummer := 62;
	execute immediate ( 'alter function primus51 compile' );
  jobbnummer := 63;
	execute immediate ( 'alter function primus52 compile' );
  jobbnummer := 64;
	execute immediate ( 'alter function primus53 compile' );
  jobbnummer := 65;
	execute immediate ( 'alter function primus54 compile' );
  jobbnummer := 66;
	execute immediate ( 'alter function primus55 compile' );
  jobbnummer := 67;
	execute immediate ( 'alter function primus56 compile' );
	
	
	
    jobbnummer:= jobbnummer+ 1+100;		
    return 'OK';
	exception
		when others then	
            ROLLBACK;		
			SYS.DBMS_OUTPUT.PUT_LINE('Primus48 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(48, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
