CREATE OR REPLACE FUNCTION primus42(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
      Materialbruk er flyttet fra oppgave til kommentar.
    */

    BEGIN

      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table oppgave drop column materialbruk';
      END IF;
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'MERGE INTO adm_hendelsetype_l USING DUAL ON (typeid=23)
                         WHEN NOT MATCHED THEN INSERT (typeid, tekst)
                         values(23, ''Behandlingsforslag'')';
        ELSE
          EXECUTE IMMEDIATE 'MERGE INTO adm_hendelsetype_l USING DUAL ON (typeid=23)
		 									 WHEN NOT MATCHED THEN INSERT (typeid, tekst)
		 									 values(23, ''Behandlingsförslag'')';
        END IF;
      END IF;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus42 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(42, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/
