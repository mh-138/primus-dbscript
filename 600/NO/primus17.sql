set serveroutput on format wraped;

create or replace function primus17(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is

begin
  dbms_output.enable;
  
  begin
	
	jobbnummer:=1;
		  if jobnummer_in <= jobbnummer then
	
	        execute immediate ('CREATE TABLE SUPEROBJEKTTYPE_L (SUPEROBJEKTTYPEID NUMBER (3,0), BESKRIVELSE VARCHAR2 (15 CHAR), 
						        CONSTRAINT PK_SUPEROBJEKTTYPE_L PRIMARY KEY (SUPEROBJEKTTYPEID)) tablespace primusdt');
	
	      End if;
	jobbnummer:=2;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE SUPEROBJEKT_DATERING_H (OBJID NUMBER (8,0), HID NUMBER (8,0),
							    FRADATO DATE , TILDATO DATE, GRUNNLAG VARCHAR2 (500 CHAR),
								CONSTRAINT PK_SUPEROBJEKT_DATRG_H PRIMARY KEY (OBJID, HID),
								CONSTRAINT FK_SUPEROBJEKT_DATRG_H_OBJID FOREIGN KEY (OBJID) REFERENCES SUPEROBJEKT (OBJID),
								CONSTRAINT FK_SUPEROBJEKT_DATRG_H_HID FOREIGN KEY (HID) REFERENCES HENDELSE (HID)) tablespace primusdt');
           End if;		  
	jobbnummer:=3;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE SUPEROBJEKT_IDNR_H (OBJID NUMBER (8,0), HID NUMBER (8,0),
							    IDENTIFIKASJONSNR VARCHAR2 (25 CHAR), IDENTIFIKASJONSTALL NUMBER(8,0),
							    CONSTRAINT FK_SUPEROBJEKT_IDNR_OBJID FOREIGN KEY (OBJID) REFERENCES SUPEROBJEKT (OBJID),
							    CONSTRAINT FK_SUPEROBJEKT_IDNR_HID FOREIGN KEY (HID) REFERENCES HENDELSE (HID),
							    CONSTRAINT PK_SUPEROBJEKT_IDNR_H PRIMARY KEY (OBJID, HID)
							    ) tablespace primusdt');
           End if;	

	jobbnummer:=4;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE ADM_HENDELSETYPE_L (TYPEID NUMBER (3,0),
								TEKST VARCHAR2 (50 CHAR) NOT NULL, PARENT_ID NUMBER(3,0),
								CONSTRAINT PK_ADM_HENDELSETYPE_L PRIMARY KEY (TYPEID),
								CONSTRAINT UN_ADM_HENDELSETYPE_L_TEKST UNIQUE(TEKST, PARENT_ID)
								) tablespace primusdt');
           End if;
	jobbnummer:=5;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE ADM_HENDELSE ( OBJID NUMBER (8,0), HID_OPPRETTET NUMBER (8,0),
              HID_SLETTET NUMBER (8,0), HOVEDTYPE NUMBER(3,0), UNDERTYPE NUMBER(3,0),
			  HID_DATERING NUMBER (8,0), HID_BESKRIVELSE NUMBER (8,0),
			  SAKSNUMMER VARCHAR2 (25 CHAR) , PUBLISERES VARCHAR2 (1 CHAR),
			  HID_PUBLISERINGSNIVAA NUMBER (8,0), HID_PUBLISERBARINGRESS NUMBER (8,0),
              HID_PUBLISERBARTEKST NUMBER (8,0),  AUTORITET VARCHAR2 (10 CHAR),
			  AUTORITET_STATUS VARCHAR2 (10 CHAR) , UUID VARCHAR2 (40 CHAR),
			  HID_IDNR NUMBER (8,0), HID_OPPL NUMBER(8,0),  HID_TILSTAND NUMBER(8,0), 
			  hid_formaal number(8,0), hid_anbefaling  number(8,0), 
			  hid_status  number(8,0), plassid number(8,0), 
			  PARENT_ID NUMBER(8,0) default -1, M_PATH varchar2(200 char),
              CONSTRAINT FK_ADM_HENDELSE_OBJID FOREIGN KEY (OBJID) REFERENCES SUPEROBJEKT (OBJID),
			  CONSTRAINT PK_ADM_HENDELSE PRIMARY KEY (OBJID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_OPPRETET FOREIGN KEY (HID_OPPRETTET) REFERENCES HENDELSE (HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_SLETTET FOREIGN KEY (HID_SLETTET) REFERENCES HENDELSE (HID),
			  CONSTRAINT FK_ADM_HENDELSE_HOVEDTYPE FOREIGN KEY (HOVEDTYPE) REFERENCES ADM_HENDELSETYPE_L (TYPEID),
			  CONSTRAINT FK_ADM_HENDELSE_UNDERTYPE FOREIGN KEY (UNDERTYPE) REFERENCES ADM_HENDELSETYPE_L (TYPEID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_BESK FOREIGN KEY (OBJID, HID_BESKRIVELSE) REFERENCES OBJ_BESKRIVELSE_H (OBJID, HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_DATERING FOREIGN KEY (OBJID, HID_DATERING) REFERENCES SUPEROBJEKT_DATERING_H (OBJID, HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_PNIVAA FOREIGN KEY (OBJID, HID_PUBLISERINGSNIVAA) REFERENCES PUBLISERINGSNIVAAOBJEKT_H (OBJID,HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_PINGRESS FOREIGN KEY (OBJID, HID_PUBLISERBARINGRESS) REFERENCES PUBLISERBARINGRESS_H (OBJID,HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_PTEKST FOREIGN KEY (OBJID, HID_PUBLISERBARTEKST) REFERENCES PUBLISERBARTEKST_H (OBJID, HID),
			  CONSTRAINT FK_ADM_HENDELSE_HID_IDNR FOREIGN KEY (OBJID,HID_IDNR) REFERENCES SUPEROBJEKT_IDNR_H (OBJID, HID),
			  CONSTRAINT UK_ADM_HENDELSE_UUID UNIQUE(UUID)
			  ) tablespace primusdt');
           End if;
	jobbnummer:=6;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE OBJEKT_ADM_HENDELSE 
                                 ( ADMH_OBJID NUMBER (8,0)
                                 , OBJID NUMBER (8,0)
                                 , OBJID_SISTE NUMBER (8,0)
                                 , HID NUMBER (8,0)
                                 , NR NUMBER(3,0)
                                 , FRADATO DATE
                                 , TILDATO DATE
                                 , KATALOGNR VARCHAR2(25 CHAR)
                                 , KOMMENTAR VARCHAR2(200 CHAR)
                                 , HID_AVSLUTTET_OR_INNLEVERT NUMBER (8,0)
                                 , CONSTRAINT FK_OBJ_ADMHND_SO_OBJID FOREIGN KEY (OBJID) REFERENCES SUPEROBJEKT (OBJID)
                                 , CONSTRAINT FK_OBJ_ADMHND_SO_OBJIDS FOREIGN KEY (OBJID_SISTE) REFERENCES SUPEROBJEKT (OBJID)
                                 , CONSTRAINT FK_OBJ_ADMHND_ADMH_OBJID FOREIGN KEY (ADMH_OBJID) REFERENCES ADM_HENDELSE (OBJID)
                                 , CONSTRAINT FK_OBJ_ADMHND_HID FOREIGN KEY (HID) REFERENCES HENDELSE (HID)
                                 , CONSTRAINT PK_OBJ_ADMHND PRIMARY KEY (OBJID, ADMH_OBJID, HID, NR)
                                 , CONSTRAINT FK_OBJ_ADMHND_HID_INNLEVERT FOREIGN KEY (HID_AVSLUTTET_OR_INNLEVERT) REFERENCES HENDELSE (HID)
                                 ) tablespace primusdt');
           End if;
	
  jobbnummer:=7;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE PLASS 
                                 ( OBJID NUMBER (8,0)  NOT NULL 
                                 , PLASSID NUMBER (8,0)
                                 , ANSVARLIG VARCHAR2 (30 CHAR)
                                 , JPNR NUMBER(8,0)
                                 , CONSTRAINT PK_PLASS_OBJID PRIMARY KEY (OBJID)
                                 , CONSTRAINT FK_PLASS_OBJID FOREIGN KEY (OBJID) REFERENCES ADM_HENDELSE (OBJID)
                                 , CONSTRAINT FK_PLASS_JPNR FOREIGN KEY (JPNR) REFERENCES JURPERSON (JPNR)
                                 ) tablespace primusdt');
           End if;
	jobbnummer:=8;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE TILSTAND
				( OBJID NUMBER (8,0)  NOT NULL
				, HID NUMBER(8,0) NOT NULL
				, TILSTANDSKODE VARCHAR2 (5 CHAR)
				, CONSTRAINT PK_TILSTAND_OBJID PRIMARY KEY (OBJID, HID)
				, CONSTRAINT FK_TILSTAND_OBJID FOREIGN KEY (OBJID) REFERENCES ADM_HENDELSE (OBJID)
				, CONSTRAINT FK_TILSTAND_HID FOREIGN KEY (HID) REFERENCES HENDELSE (HID)
              ) tablespace primusdt');
           End if;		   


	jobbnummer:=11;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('create table adm_h_formaaltype_l
			 (formaaltypeid number(3,0) not null
			 , formaaltekst varchar2(25 char)
			 , merke varchar2(1 char) default (''Y'')
			 , uuid varchar2(40 char) not null
			 , hid_opprettet number(8,0)
			 , hid_slettet number(8,0)
			 , autoritet	VARCHAR2(40 CHAR)
			 , autoritet_status	VARCHAR2(40 CHAR)
			 , autoritet_dataset	VARCHAR2(100 CHAR)
			 , autoritet_kilde	VARCHAR2(100 CHAR)
			 , filterfeltid NUMBER(3,0)
			 , CONSTRAINT pk_ah_formaaltype_l PRIMARY KEY (formaaltypeid)
			 , constraint fk_adh_formaal_filtertypeid FOREIGN KEY (filterfeltid) references OBJEKTTYPER (TYPEID)
			 , CONSTRAINT uk_ah_formaaltype_UUID UNIQUE (UUID)) TABLESPACE PRIMUSDT');
           End if;		
	
	jobbnummer:=12;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('create table adm_h_formaal_h
			  (objid number(8,0) not null,
			   hid number(8,0) not null,
			   formaal varchar2(4000 char),
			   formaaltypeid number(3),
			   constraint PK_adm_h_formaal_h PRIMARY KEY (objid,hid),
			   constraint FK_ahformaal_admhendelse FOReign key (objid) references ADM_HENDELSE (objid),
			   constraint FK_ahformaal_formaalid foreign key (formaaltypeid) references adm_h_formaaltype_l (formaaltypeid)
			   ) TABLESPACE PRIMUSDT');
           End if;
		   
	jobbnummer:=13;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE SKADE_L
							   (ID NUMBER(6,0),
								NAVN VARCHAR2(250 CHAR),
								PARENT_ID NUMBER(6,0),
								BESKRIVELSE VARCHAR2(200 CHAR),
								M_PATH VARCHAR2(100 CHAR),
								HID_OPPRETTET NUMBER(8,0),
								HID_SLETTET NUMBER(8,0),
								UUID VARCHAR2(40 BYTE),
								MERKE CHAR(1 CHAR) default (''Y''),
								AUTORITET VARCHAR2(10 CHAR),
								AUTORITET_STATUS VARCHAR2(1 CHAR),
								AUTORITET_DATASET VARCHAR2(100 CHAR),
								AUTORITET_KILDE VARCHAR2(100 CHAR),
								FILTERFELTID NUMBER(3,0),
								CONSTRAINT PK_SKADE_L PRIMARY KEY (ID),
								 CONSTRAINT UK_SKADE_L_UUID UNIQUE (UUID),
								 CONSTRAINT FK_SKADE_L_HID FOREIGN KEY (HID_OPPRETTET) REFERENCES HENDELSE (HID),
								 CONSTRAINT FK_SKADE_L_HID_S FOREIGN KEY (HID_SLETTET) REFERENCES HENDELSE (HID),
								 constraint fk_skade_l_filterfeltid FOREIGN KEY (filterfeltid) references OBJEKTTYPER (TYPEID)
								 ) TABLESPACE PRIMUSDT');
           End if;
		--- de forskjellige statuser en adm_hendelse kan ha lagres i tabellen adm_hendelse_status_
		jobbnummer:=14;  
		  if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE 'create table superobjekt_status_l
					  ( statusid number(3,0) not null
					  , statustekst varchar2(25 char) not null
					  , superobjekttypeid number(3,0) not null
					  , CHECKFORDELETE VARCHAR2(1 CHAR)
					  , MERKE VARCHAR2(1 CHAR) default ''Y'' not null
					  , UUID VARCHAR2(40 CHAR) not null
					  , HID_OPPRETTET NUMBER(8,0)
					  , HID_SLETTET NUMBER(8,0)
					  , AUTORITET VARCHAR2(10 CHAR)
					  , AUTORITET_STATUS VARCHAR2(1 CHAR)
					  , AUTORITET_DATASET VARCHAR2(100 CHAR)
					  , AUTORITET_KILDE VARCHAR2(100 CHAR)
					  , constraint pk_superobjekt_status_l primary key (statusid)
					  , constraint fk_soid_superobjekttype_l foreign key (superobjekttypeid) references superobjekttype_l(superobjekttypeid)
					) tablespace primusdt';				  
		   
           End if;		   
	
	
	jobbnummer:=15;
	--- legge til en tabell som fikser status til en adm_hendelse
		  if jobnummer_in <= jobbnummer then
			 EXECUTE IMMEDIATE
					'create table superobjekt_status_h
					  ( objid number(8,0) not null
					  , hid number(8,0) not null
					  , statusid number(3,0) not null
					  , kommentar varchar2(4000 CHAR)
					  , constraint pk_superobjekt_status_h primary key (objid, hid)
					  , constraint fk_so_status_h_objid_so foreign key (objid) references superobjekt(objid)
					  , constraint fk_so_status_h_hid_hendelse foreign key (hid) references hendelse(hid)
					  , constraint fk_so_status_h_sid_status_l foreign key (statusid) references superobjekt_status_l(statusid)
					) tablespace primusdt';

           End if;
	jobbnummer:=16; --- oppretter en skade tabell
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('create table skade
					(objid number(8,0) NOT NULL
					, admobjid number(8,0)
					, objobjid number(8,0) NOT NULL
					, UUID varchar2(40 char) NOT NULL
					, skadenummer varchar2(25 char) NOT NULL
					, hid_opprettet number(8,0) NOT NULL
					, hid_slettet number(8,0)
					, hid_beskrivelse number(8,0)
					, hid_skadetype number(8,0)
					, hid_skadeposisjon number(8,0)
					, hid_datering number(8,0)					
					, constraint PK_SKADE primary key (objid)
					, constraint FK_SKADE_SUPEROBJEKT foreign key (objid) references superobjekt (objid)
					, constraint FK_SKADE_ADM_HENDELSE foreign key (admobjid) references adm_hendelse (objid)
					, constraint FK_SKADE_OBJEKT foreign key (objobjid) references objekt(objid)
					, CONSTRAINT FK_SKADE_OPPRETTET_HID FOREIGN KEY (HID_OPPRETTET) REFERENCES HENDELSE (HID)
					, constraint FK_SKADE_SLETTET_HID FOREIGN KEY (HID_SLETTET) REFERENCES HENDELSE (HID)
					, constraint FK_SKADE_BESKRIVELSE_HID FOREIGN KEY (objid, hid_beskrivelse) REFERENCES obj_beskrivelse_h (objid, hid)
					) tablespace primusdt');
           End if;
		   
		   
		  
        
			
    jobbnummer:=17;--- tabell for å lagre skadetyper.
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('create table skadetype_h
				( objid number(8,0) not null
				, hid number(8,0) not null
				, skadetypeid number(6,0)
				, constraint PK_SKADE_TYPE primary key (objid, hid)
				, constraint FK_SKADE_TYPE_ID_SKADEL foreign key(skadetypeid) references SKADE_L (ID))
				tablespace primusdt');
           End if;
    jobbnummer:=18; --- oppretter tabell for adminstrative hendelser sitt felt anbefaling.
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('create table adm_h_anbefaling_h
				( objid number(8,0) not null
				, hid number(8,0) not null
				, anbefaling varchar2(4000 char)
				, constraint pk_adm_h_anbefaling_h primary key (objid, hid)
				, constraint fk_adm_hend_anbefaling_objid foreign key (objid) references adm_hendelse(objid))
				tablespace primusdt');
           End if;
		   
    jobbnummer:= 19;
	    if jobnummer_in <= jobbnummer then	
	      EXECUTE IMMEDIATE('Create table skade_posisjon_h 
            ( objid number(8,0) not null
            , hid number(8,0) not null
            , posisjon varchar2(1000 char)
            , constraint pk_skade_posisjon primary key (objid, hid)
            , constraint fk_skade foreign key (objid) references skade(objid))
            tablespace primusdt');
		End if;	
		
	jobbnummer:= 20;      
    jobbnummer:= 21;
	       if jobnummer_in <= jobbnummer then
		     execute immediate (' CREATE SEQUENCE SKADE_LIDSEQ INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE ');
           End if;
    jobbnummer:= 22;    
 	     commit;

    jobbnummer:= 23;	 	 
         return 'OK';		   
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus17 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(17, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/
