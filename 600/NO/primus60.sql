create or replace function primus60(jobbnummer out number, jobnummer_in number) 
	return varchar2
	AUTHID CURRENT_USER
	is	
begin
	dbms_output.enable;

	begin
    jobbnummer := 1;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add ( jpobjid number( 8 ) )' );
    
    jobbnummer := 2;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add ( rolleid number( 8 ) )' );
    
    jobbnummer := 3;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add ( rollestatusid number( 4 ) )' );
    
    jobbnummer := 4;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add constraint fk_jpobjid_jurperson foreign key ( jpobjid ) references jurperson( objid ) ' );
    
    jobbnummer := 5;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add constraint fk_rolleid_rollejp foreign key ( rolleid ) references rollejp_l( rolleid ) ' );
    
    jobbnummer := 6;
    PrimusOppdJobbNrIVersjon( 60, jobbnummer );
    executeAlterTable( jobbnummer,jobnummer_in, 'alter table varemerke_h add constraint fk_rollestatusid_rollejp_sta foreign key ( rollestatusid ) references rollejpstatus_l( statusid ) ' );

    jobbnummer := 7;

    
    
    
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus60 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(60, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
