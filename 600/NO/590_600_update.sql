set serveroutput on;
-- legger inn alle prodedyrer
@@oppgraderingsfunksjoner.sql
--@@primus16.sql
--@@primus17.sql
--@@primus18.sql
--@@primus19.sql
--@@primus20.sql
--@@primus21.sql
--@@primus22.sql
--@@primus23.sql
--@@primus24.sql
--@@primus25.sql
--@@primus26.sql
--@@primus27.sql
--@@primus28.sql
--@@primus29.sql
--@@primus30.sql
--@@primus31.sql
--@@primus32.sql
--@@primus33.sql
--@@primus34.sql
--@@primus35.sql
--@@primus36.sql
--@@primus37.sql
--@@primus38.sql
--@@primus39.sql
--@@primus40.sql
--@@primus41.sql
--@@primus42.sql
--@@primus43.sql
--@@primus44.sql
--@@primus45.sql
--@@primus46.sql
--@@primus47.sql
--@@primus48.sql
--@@primus49.sql
--@@primus50.sql
--@@primus51.sql
--@@primus52.sql
--@@primus53.sql
--@@primus54.sql
--@@primus55.sql
--@@primus56.sql
--@@primus57.sql
--@@primus58.sql
--@@primus59.sql
@@primus60.sql
@@primus61.sql
@@primus62.sql
@@primus63.sql
@@primus64.sql
@@primus65.sql
@@primus66.sql
@@primus67.sql

-- kj�rer prosedyre som skal h�ndtere oppgradering
declare
	resultat varchar2(1000 char);
Begin
	resultat := oppgraderPrimus(59,67);

	if resultat = 'OK' then
		-- oppgradering uten feil, s� da settes riktig Primus versjon og�s.
		/*UPDATE ENVIRONMENT SET VERDI = '0' WHERE VARIABEL = 'Major';
		UPDATE ENVIRONMENT SET VERDI = '0' WHERE VARIABEL = 'Minor';
		update environment set verdi = '0' where variabel = 'Release';*/
     dbms_output.put_line('-- OK --');
	else
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
	end if;
exception
	when others then
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
end;
/
