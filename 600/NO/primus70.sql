create or replace function primus70(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 70, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table primus.historikk add ( uuid varchar2( 40 char ) ) ');
    end if;
    
    jobbnummer := 2;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 70, jobbnummer );
      execute immediate '
            create table plassering_egnethet_l
                         ( EGNETID      NUMBER(3)          
                         , EGNETKODE  VARCHAR2(5 CHAR)
                         , EGNETTEKST VARCHAR2(25 CHAR)  
                         , CHECKFORDELETE VARCHAR2(1 CHAR)   
                         , MERKE VARCHAR2(1 CHAR) default ''Y''  
                         , UUID VARCHAR2(40 CHAR)    
                         , HID_OPPRETTET NUMBER(8)          
                         , HID_SLETTET NUMBER(8)          
                         , AUTORITET VARCHAR2(10 CHAR)  
                         , AUTORITET_STATUS VARCHAR2(1 CHAR)   
                         , AUTORITET_DATASET VARCHAR2(100 CHAR) 
                         , AUTORITET_KILDE VARCHAR2(100 CHAR)
                         , constraint pk_plassering_egnethet_l primary key ( egnetid )
                         , constraint fk_plassegnethet_hidopprettet foreign key ( hid_opprettet ) references primus.hendelse ( hid )
                         , constraint fk_plassegnethet_hidslettet foreign key ( hid_slettet ) references primus.hendelse ( hid )
                         , constraint uk_plassegnethet_UUID unique ( uuid ) 
                         , constraint ck_uuidnotnull check ( uuid is not null )
                         , constraint ck_egnettekst check ( egnettekst is not null )
                         )';
    end if;

    jobbnummer := 3;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 70, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table plasskode add ( egnetid number( 3 ) )');
    end if;
    
    jobbnummer := 4;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 70, jobbnummer );
      executeAlterTable( jobbnummer, jobbnummer_inn, 'alter table plasskode add ( constraint fk_plasskodeegnetid foreign key ( egnetid ) references primus.plassering_egnethet_l ( egnetid ) )');
    end if;

    jobbnummer := 5;
    if  jobnummer_in <= jobbnummer then		  
      EXECUTE IMMEDIATE 'create or replace PROCEDURE TILSTAND_UPDATE (pobjid NUMBER, phid NUMBER, pTKODE VARCHAR2, pTGRAD NUMBER, pBESK VARCHAR2) IS				  
				BEGIN
				  MERGE INTO TILSTAND  
               USING DUAL 
                  ON (    OBJID = pobjid 
                      and HID = phid ) 				  
				  WHEN MATCHED THEN 
               UPDATE 
                  Set BESKRIVELSE = pBESK
                    , KGRADID = pTGRAD
                    , TILSTANDSKODE = pTKODE
				  WHEN NOT MATCHED THEN  
               INSERT (OBJID, HID, TILSTANDSKODE, BESKRIVELSE, KGRADID) 
                  VALUES (pobjid, phid, pTKODE, pBESK, pTGRAD);				  
				  
          UPDATE ADM_HENDELSE SET HID_TILSTAND = phid WHERE (OBJID = pobjid);
				END TILSTAND_UPDATE;';
		END if;	

    jobbnummer := 6;
    if  jobnummer_in <= jobbnummer then		  
      EXECUTE IMMEDIATE 'create or replace PROCEDURE OAH_INNLEVERT_UPDATE (pahobjid NUMBER, pobjid NUMBER, pOldhid NUMBER, pNewhid NUMBER, pNr NUMBER, pInnlevert DATE, pfradato DATE, ptildato DATE, pktlog varchar2, pkomm varchar2) IS				  								  
                          BEGIN
                            IF pOldhid = pNewhid then					
                              UPDATE OBJEKT_ADM_HENDELSE 
                                 Set INNLEVERT=pInnlevert
                                   , katalognr = pktlog
                                   , kommentar = pkomm
                                   , fradato = pfradato
                                   , tildato = ptildato
                               WHERE OBJID_SISTE = pobjid 
                                 and HID = pNewHid 
                                 and NR = pnr 
                                 and ADMH_OBJID = pahobjid 
                                 and HID_SLETTET is NULL;
                            ELSE
                              UPDATE OBJEKT_ADM_HENDELSE 
                                 Set OBJID_SISTE = NULL 
                               WHERE OBJID_SISTE = pobjid 
                                 and HID = pOldhid 
                                 and NR = pnr 
                                 and ADMH_OBJID = pahobjid 
                                 and HID_SLETTET is NULL;
                              INSERT INTO OBJEKT_ADM_HENDELSE 
                                (ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, INNLEVERT, KATALOGNR, KOMMENTAR, FRADATO, TILDATO) 
                              VALUES 
                                (pahobjid, pobjid, pobjid, pNewHID, pNr, pInnlevert, pktlog, pkomm, pfradato, ptildato);				  				  									
                            END IF;
                          END OAH_INNLEVERT_UPDATE;';
		END if;	
    jobbnummer := 100;
    commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus70 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(70, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      