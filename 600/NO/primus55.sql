create or replace function primus55(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	begin	        
		
		
		
		jobbnummer:= 1;             executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(004, ''AFA'', ''Afghani'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(020, ''ADP'', ''Andorran Peseta'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(024, ''AON'', ''New Kwanza'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(031, ''AZM'', ''Azerbaijanian Manat'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(040, ''ATS'', ''Schilling'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(056, ''BEF'', ''Belgian Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(070, ''BAD'', ''Dinar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(076, ''BRN'', ''New Cruzado'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(100, ''BGL'', ''Lev'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(112, ''BYB'', ''Belarussian Ruble'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(180, ''ZRZ'', ''Zaire'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(196, ''CYP'', ''Cyprus Pound'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(200, ''CSK'', ''Koruna'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(218, ''ECS'', ''Sucre'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(226, ''GQE'', ''Ekwele'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(233, ''EEK'', ''Kroon'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(246, ''FIM'', ''Finnish markka'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(250, ''FRF'', ''French Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(268, ''GEK'', ''Georgian Coupon'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(276, ''DEM'', ''Deutsche Mark'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(278, ''DDM'', ''Mark der DDR'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(288, ''GHC'', ''Cedi'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(300, ''GRD'', ''Drachma'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(372, ''IEP'', ''Irish Pound'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(380, ''ITL'', ''Italian Lira'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(428, ''LVR'', ''Latvian Ruble'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(440, ''LTT'', ''Talonas'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(442, ''LUF'', ''Luxembourg Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(450, ''MGF'', ''Malagasy Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(466, ''MLF'', ''Mali Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(470, ''MTL'', ''Maltese Lira'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(508, ''MZM'', ''Mozambique Metical'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(528, ''NLG'', ''Netherlands Guilder'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(616, ''PLZ'', ''Zloty'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(620, ''PTE'', ''Portuguese Escudo'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(624, ''GWP'', ''Guinea-Bissau Peso'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(626, ''TPE'', ''Timor Escudo'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(642, ''ROL'', ''Old Leu'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(703, ''SKK'', ''Slovak Koruna'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(705, ''SIT'', ''Tolar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(716, ''ZWD'', ''Zimbabwe Dollar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(720, ''YDD'', ''Yemeni Dinar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(724, ''ESP'', ''Spanish Peseta'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(736, ''SDD'', ''Sudanese Dinar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(740, ''SRG'', ''Surinam Guilder'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(762, ''TJR'', ''Tajik Ruble'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(792, ''TRL'', ''Old Turkish Lira'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(795, ''TMM'', ''Turkmenistan Manat'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(804, ''UAK'', ''Karbovanet'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(810, ''RUR'', ''Russian Ruble'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(862, ''VEB'', ''Bolivar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(890, ''YUN'', ''Yugoslavian Dinar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(891, ''YUM'', ''New Dinar'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(894, ''ZMK'', ''Zambian Kwacha'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(935, ''ZWR'', ''Zimbabwe Dollar'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(939, ''GHP'', ''Ghana Cedi'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(942, ''ZWN'', ''Zimbabwe Dollar (new)'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(945, ''AYM'', ''Azerbaijan Manat'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(954, ''XEU'', ''European Currency Unit (E.C.U)'')');		
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(982, ''AOR'', ''Kwanza Reajustado'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(983, ''ECV'', ''Unidad de Valor Constante (UVC)'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(987, ''BRR'', ''Cruzeiro Real'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(988, ''LUL'', ''Luxembourg Financial Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(989, ''LUC'', ''Luxembourg Convertible Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(991, ''ZAL'', ''Financial Rand'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(992, ''BEL'', ''Financial Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(993, ''BEC'', ''Convertible Franc'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(995, ''ESB'', ''"A" Account (convertible Peseta Account)'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(996, ''ESA'', ''Spanish Peseta'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'insert into primus.valuta_l(valuta_id, alpha_kode, valuta) values(998, ''USS'', ''US Dollar (Same day)'')');
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.verdi=''NOK'' Where shf.felt_type=8 and Upper(shf.verdi)=''NKR'' '); 
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.verdi=''DEM'' Where shf.felt_type=8 and Upper(shf.verdi)=''DM'' '); 
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.verdi=''FRF'' Where shf.felt_type=8 and Upper(shf.verdi)=''FF'' '); 
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.verdi=''ISK'' Where shf.felt_type=8 and Upper(shf.verdi)=''IGK'' '); 
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.valutaid =(select vl.valuta_id from valuta_l vl Where (vl.ALPHA_KODE=Upper(shf.verdi) OR upper(vl.VALUTA)=Upper(shf.verdi))) Where shf.felt_type=8 '); 
        jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'update SUPEROBJEKT_HENDELSE_FELTER shf set shf.valutaid =(select vl.valutaid from SUPEROBJEKT_HENDELSE_FELTER vl Where vl.objid=shf.objid and vl.hid=shf.hid and vl.felt_type=8) Where shf.felt_type=7'); 
		jobbnummer:= jobbnummer+1;  executeScript(jobbnummer,jobnummer_in,'alter table ADM_H_KOSTNAD add (KOMMENTAR varchar2(500 CHAR))');        
			
		
    jobbnummer:= jobbnummer+1;	
		if jobnummer_in <= jobbnummer then	
		  DECLARE		  
        iId Integer;
        lastBeskId Integer;
        newNR Integer;
		  BEGIN  
        For HTK in (select DISTINCT hovedtypekode 
                               FROM MUSHENDELSE 
        
                              WHERE hovedtypekode in (10006, 10007))
			  Loop	
          lastBeskId := -1;
          For TH in (select DISTINCT MP.objid
                                   , MP.HID
                                   , MH.DATO
                                   , MH.TIL_DATO
                                   , MH.NEWHOVEDTYPE
                                   , MH.beskrivende_objekt
                                   , length(MH.tekst) ltekst
                                   , MP.VERDI
                                   , MP.VALUTAID
                                FROM Superobjekt_hendelse_FELTER MP
                                   , MUSHENDELSE MH 
                               WHERE MP.HID = MH.HID
                                 AND MP.FELT_TYPE = 7
                                 AND MH.hovedtypekode = HTK.hovedtypekode
                                 AND MH.MERKE is NULL 
                            ORDER BY MH.beskrivende_objekt )
          Loop	
                
            iId := 0;    
            if th.beskrivende_objekt is null then
              execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
            else
              iId := th.beskrivende_objekt;
            end if;
            if lastBeskId <> iId then
              execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, TH.NEWHOVEDTYPE;								   
              
              if th.dato is not null then	
                execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato, th.til_dato;
              End if;				
              
              if TH.VERDI is not NULL then
                execute immediate 'Insert Into ADM_H_KOSTNAD(OBJID, HID, KOMMENTAR, VALUTA_ID) values (:a, :b, :c, :d)' using  iId, TH.hid, TH.VERDI, TH.VALUTAID; 
                execute immediate 'UPDATE ADM_HENDELSE set HID_KOSTNAD = :a Where objid = :b ' using TH.hid, iId;			 
              End if; 
            
              if TH.ltekst > 0 then
                execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
              End if;					   				
            end if;
            
            lastBeskId := iId;
            select ( nvl( max( nr ), 0 ) + 1 ) into newNr
                              from objekt_adm_hendelse 
                             where objid = th.objid
                               and admh_objid = iId ;

            execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO)
                     values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, newNR,  TH.DATO, TH.TIL_DATO;
          
            execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using TH.HID;									
          END LOOP;		 
          commit; 
        END LOOP; 
		  END;	
    			
		END IF;	
		
    -- 79 - hvis noen lurer....
    jobbnummer:= jobbnummer+1; --------- Alle Hendelses type unntat Plassering, utlån, utstilling, og Tilstandsvurdering går her  -------------
		if jobnummer_in <= jobbnummer then	
		  DECLARE		  
        iId Integer;
        lastBeskId Integer;
        newNr Integer;
		  BEGIN  
        For HTK in (select DISTINCT hovedtypekode 
                              FROM MUSHENDELSE 
                             WHERE hovedtypekode not in (10001, 10002, 10003, 10004,  10006, 10007) 
						                   AND not ( ( hovedtype = 'Tilstandsvurdering' ) OR (hovedtype = 'Tillståndsvärdering')))
        LOOP	
          lastBeskId := -1;
			 
          For TH in (select DISTINCT MP.objid
                                   , MP.HID
                                   , MH.DATO
                                   , MH.TIL_DATO
                                   , MH.NEWHOVEDTYPE
                                   , MH.NEWUNDERTYPE
                                   , MH.beskrivende_objekt
                                   , length(MH.tekst) ltekst
                                FROM Superobjekt_hendelse MP
                                   , MUSHENDELSE MH 
                               WHERE MP.HID = MH.HID
                                 AND MH.hovedtypekode = HTK.hovedtypekode
                                 AND MH.MERKE is NULL
                               ORDER BY mh.beskrivende_objekt)
          LOOP	
                
            iId := 0;
            if th.beskrivende_objekt is null then
              execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
            else
              iId := th.beskrivende_objekt;
            end if;
            if lastBeskId <> iId then  
              execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) 
                       values (:a, :b, :c)' using iId, TH.HID, TH.NEWHOVEDTYPE;
              
              if TH.NEWUNDERTYPE > 0 then
                execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) 
                               values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;
              End if;				   
                       
              execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato, th.til_dato;
                            
              if TH.ltekst > 0 then
                execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
              End if;						
            end if;            
            
            lastBeskId := iId;
            select ( nvl( max( nr ), 0 ) + 1 ) into newNr
                                          from objekt_adm_hendelse 
                                         where objid = th.objid
                                           and admh_objid = iId ;
            
            execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO) 
                     values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, newNr,  TH.DATO, TH.TIL_DATO;

            execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using TH.HID;									
          END LOOP;		 
          commit; 
        
        END LOOP; 
		  END;	
    			
		End if;		
		
		
	
	    
	jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus55 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(55, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/