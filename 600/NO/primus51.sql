create or replace function primus51(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
  newHID number;
begin
	dbms_output.enable;

	/*
		Data overføring fra MUSHENDELSE til ADM_HENDELSE.
	*/

	begin
    jobbnummer := 1;
    PrimusOppdJobbNrIVersjon( 51, jobbnummer );
    -- skaffe en hid....
    SELECT hidseq.NEXTVAL into newHID from dual;
    insert into hendelse (hid, signid, dato) values (newHID, 1, sysdate);
    --sjekk om de riktige rollekodene er på plass - 
    if svenskEllerNorskDatabase = 'NOR' then  --'2944c1d8-9127-4cf6-9bcc-5f94719d57fb'
      execute immediate( 'merge into rollejp_l r 
                          using ( select 120 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set rollekode = ''120''
                                  , uuid = ''2944c1d8-9127-4cf6-9bcc-5f94719d57fb''
                                  , checkfordelete = ''X''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 120
                                , ''120''
                                , ''Utlåner, institusjon''
                                , ''Y''
                                , ''X''
                                , ''2944c1d8-9127-4cf6-9bcc-5f94719d57fb''
                                , ' || newHID || ' )' ) ;
    else
      execute immediate( 'merge into rollejp_l r 
                          using ( select 120 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set rollekode = ''120''
                                  , uuid = ''2944c1d8-9127-4cf6-9bcc-5f94719d57fb''
                                  , checkfordelete = ''X''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 120
                                , ''120''
                                , ''Utlånare, institution''
                                , ''Y''
                                , ''X''
                                , ''2944c1d8-9127-4cf6-9bcc-5f94719d57fb''
                                , ' || newHID || ' )' ) ;
    end if;
  
  jobbnummer := 3;
  PrimusOppdJobbNrIVersjon( 51, jobbnummer );
  if svenskEllerNorskDatabase = 'NOR' then --'7df70090-9e9c-4c5f-a08a-ede266816d31'
      execute immediate( 'merge into rollejp_l r 
                          using ( select 121 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''121''
                                  , r.uuid = ''7df70090-9e9c-4c5f-a08a-ede266816d31''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Låntaker, institusjon''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 121
                                , ''121''
                                , ''Låntaker, institusjon''
                                , ''Y''
                                , ''X''
                                , ''7df70090-9e9c-4c5f-a08a-ede266816d31''
                                , ' || newHID || ' )' ) ;
	else
      execute immediate( 'merge into rollejp_l r 
                          using ( select 121 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''121''
                                  , r.uuid = ''7df70090-9e9c-4c5f-a08a-ede266816d31''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Låntagare, institution''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 121
                                , ''121''
                                , ''Låntagare, institution''
                                , ''Y''
                                , ''X''
                                , ''7df70090-9e9c-4c5f-a08a-ede266816d31''
                                , ' || newHID || ' )' ) ;
	end if;
	
  jobbnummer:=4;
  PrimusOppdJobbNrIVersjon( 51, jobbnummer );
	if svenskEllerNorskDatabase = 'NOR' then --'f77ea0cb-66ff-487b-9d90-b7052bc82376'
      execute immediate( 'merge into rollejp_l r 
                          using ( select 122 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''122''
                                  , r.uuid = ''f77ea0cb-66ff-487b-9d90-b7052bc82376''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Utlåner, kontaktperson''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 122
                                , ''122''
                                , ''Utlåner, kontaktperson''
                                , ''Y''
                                , ''X''
                                , ''f77ea0cb-66ff-487b-9d90-b7052bc82376''
                                , ' || newHID || ' )' ) ;
	else
      execute immediate( 'merge into rollejp_l r 
                          using ( select 122 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''122''
                                  , r.uuid = ''f77ea0cb-66ff-487b-9d90-b7052bc82376''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Utlånare, kontaktperson''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 122
                                , ''122''
                                , ''Utlånare, kontaktperson''
                                , ''Y''
                                , ''X''
                                , ''f77ea0cb-66ff-487b-9d90-b7052bc82376''
                                , ' || newHID || ' )' ) ;
	end if;
	
  jobbnummer:=5;
  PrimusOppdJobbNrIVersjon( 51, jobbnummer );
	if svenskEllerNorskDatabase = 'NOR' then --'675cb91f-fd46-4153-8aa2-2b265ec25a5f'
      execute immediate( 'merge into rollejp_l r 
                          using ( select 123 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''123''
                                  , r.uuid = ''675cb91f-fd46-4153-8aa2-2b265ec25a5f''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Låntaker, kontaktperson''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 123
                                , ''123''
                                , ''Låntaker, kontaktperson''
                                , ''Y''
                                , ''X''
                                , ''675cb91f-fd46-4153-8aa2-2b265ec25a5f''
                                , ' || newHID || ' )' ) ;
	else
      execute immediate( 'merge into rollejp_l r 
                          using ( select 123 rolleid from dual ) tu
                             on ( r.rolleid = tu.rolleid )
                           when matched then update
                                set r.rollekode = ''123''
                                  , r.uuid = ''675cb91f-fd46-4153-8aa2-2b265ec25a5f''
                                  , r.checkfordelete = ''X''
                                  , r.beskrivelse = ''Låntagare, kontaktperson''
                           when not matched then insert
                                ( r.rolleid
                                , r.rollekode
                                , r.beskrivelse
                                , r.merke
                                , r.checkfordelete
                                , r.uuid
                                , r.hid_opprettet )
                                values
                                ( 123
                                , ''123''
                                , ''Låntagare, kontaktperson''
                                , ''Y''
                                , ''X''
                                , ''675cb91f-fd46-4153-8aa2-2b265ec25a5f''
                                , ' || newHID || ' )' ) ;
	end if;
  
		jobbnummer := 7;  ------------------- Utlaan ----------------	  
    PrimusOppdJobbNrIVersjon( 51, jobbnummer );
		--UTLAAN with a row value in MUSHENDELSE
		if jobnummer_in <= jobbnummer then	
    DECLARE		  
			iId Integer;		  
			rlaaner integer;			
			rlaanerI Integer;			
			rutlaaner Integer;			
			rutlaanerI Integer;
      iHid_Avsluttet Integer;			 
    BEGIN 				  

			  rlaaner    := 123;
			  rlaanerI   := 121;
			  rutlaaner  := 122;
			  rutlaanerI := 120;
		  
			  For TH in (select DISTINCT MH.HID
                                 , MP.UTLAANID
                                 , MP.FRADATO
                                 , MP.TILDATO
                                 , MP.UTLAANSNR
                                 , MP.LAANER_NAVN
                                 , MP.LAANER_INST
                                 , MP.UTLAANER
                                 , MP.UTLAANSINST
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MP.SAKSNUMMER
                                 , length(MH.tekst) ltekst
                                 , MP.FORMAAL
                                 , MP.ANDREOPPL
                                 , MP.AVSLUTTET
                                 , MH.beskrivende_objekt
                              FROM UTLAAN MP
                                 , MUSHENDELSE MH 
                             Where MP.UTLAANID = MH.UTLAANID 
                               AND MH.hovedtypekode = 10002						 
                               AND MH.MERKE is NULL )
			  Loop	
	            
          iId := 0;    
          if th.beskrivende_objekt is null then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
          else
            iId := th.beskrivende_objekt;
          end if;				
				
          execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE,  SAKSNUMMER) 
								   values (:a, :b, :c, :d)' using iId, TH.HID, TH.NEWHOVEDTYPE, TH.SAKSNUMMER;
			
          if TH.AVSLUTTET is not NULL then
            begin
              execute immediate 'BEGIN GETHIDFROM_GIVENDATE(:a, :b, :c); END;' using OUT iHid_Avsluttet, iId, TH.AVSLUTTET ;	
              execute immediate 'UPDATE ADM_HENDELSE  SET HID_AVSLUTTET =:a WHERE OBJID=:b ' using iHid_Avsluttet, iId;						
            END;
          END if;
          
          if TH.NEWUNDERTYPE > 0 then				   
            execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) 
				                   values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;						   				   
          End if;	
          
          if TH.FORMAAL is not NULL then
            execute immediate 'BEGIN SAVE_AH_FORMAAL(:a, :b, :c, :d); END;' using iId, TH.hid, 1, TH.FORMAAL;			
          End if;	
          
          if TH.ANDREOPPL is not NULL then
            execute immediate 'BEGIN SAVE_AH_ANDREOPPL(:a, :b, :c); END;' using iId, TH.hid, TH.ANDREOPPL;			
          End if;				

          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.fradato, th.tildato;
          execute immediate 'BEGIN SAVE_AHUT_OBJEKT(:a, :b, :C); END;' using iId, TH.hid, TH.utlaanid;				
          execute immediate 'BEGIN SAVE_AH_IDNR(:a, :b, :C); END;' using iId, TH.hid, TH.UTLAANSNR;
				
          if TH.LAANER_NAVN is not null then
              execute immediate 'Insert into ADM_HENDELSE_PERSON(OBJID, HID, NR, OBJID_SISTE, JPNR, ROLLEKODE, ROLLEID) 
                         values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.HID, 1, iId,TH.LAANER_NAVN, rlaaner, rlaaner;
          End if;				   

          if TH.LAANER_INST is not null then
				    execute immediate 'Insert into ADM_HENDELSE_PERSON(OBJID, HID, NR, OBJID_SISTE, JPNR, ROLLEKODE, ROLLEID) 
					     			   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.HID, 1, iId,TH.LAANER_INST, rlaanerI, rlaanerI;
          End if;	
	        
          if TH.UTLAANER is not null then
				    execute immediate 'Insert into ADM_HENDELSE_PERSON(OBJID, HID, NR, OBJID_SISTE, JPNR, ROLLEKODE, ROLLEID) 
					     			   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.HID, 1, iId,TH.UTLAANER, rutlaaner, rutlaaner;
          End if;				   
				
          if TH.UTLAANSINST is not null then
				    execute immediate 'Insert into ADM_HENDELSE_PERSON(OBJID, HID, NR, OBJID_SISTE, JPNR, ROLLEKODE, ROLLEID) 
					     			   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.HID, 1, iId,TH.UTLAANSINST, rutlaanerI, rutlaanerI;
          End if;	
				
				
          if TH.ltekst > 0 then
            execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
          End if;	
                
          For UD in (select DISTINCT DOKID  FROM UTLAAN_DOK_TEMP WHERE utlaanid=TH.UTLAANID order by DOKID )
			    Loop
            execute immediate 'BEGIN SAVE_AH_UTLAAN_DOK(:a, :b); END;' using  UD.DOKID, iId;			 
          END LOOP;  
				
				execute immediate 'update MUSHENDELSE set MERKE=''X'' Where UTLAANID = :b ' using TH.utlaanid;		
				execute immediate 'update UTLAAN set ADMH_OBJID = :a, MERKE=''X'' Where UTLAANID = :b ' using iId, TH.UTLAANID;
		
			  END LOOP;		 
			 commit; 
		  END;	   			
			
		End if;	
	
	jobbnummer:= 8;	
  PrimusOppdJobbNrIVersjon( 51, jobbnummer );
		executeScript(jobbnummer,jobnummer_in,'Update REFERANSE R set R.OBJID=(Select U.ADMH_OBJID from UTLAAN U WHERE U.UTLAANID=R.UTLAANID) Where UTLAANID is not NULL'); 

----------------------------- END of UTLAAN ----------------------------------		
		
	
    jobbnummer := 100;		
    return 'OK';
	exception
		when others then
		    ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus51 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(51, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/