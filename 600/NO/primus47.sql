create or replace function primus47(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*

	*/

	begin
    DBMS_OUTPUT.put_line('jobnummer_in');
    DBMS_OUTPUT.put_line(jobnummer_in);
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-9'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_ARTWORK''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-24'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_PHOTOGRAPH''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-4'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_THING''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-34'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_ARCHITECTURE''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-39'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_DESIGN''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_0-19'', gruppe = ''WEB'' where variabel = ''DEF_REG_LEVEL_ID_BUILDING''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_18-1'', gruppe = ''WEB'' where variabel = ''DEF_PUBLISH_LEVEL_ID''';
			EXECUTE IMMEDIATE 'update environment set verdi = ''ct_32-1'', gruppe = ''WEB'' where variabel = ''DEF_PUBLISH_LEVEL_ACTOR_ID''';
      commit;
		End if;
		jobbnummer:=2;
		if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE
			'CREATE TABLE billedkunstdel_l
				( typeid NUMBER(6,0)
				, navn VARCHAR2(250 CHAR)
        , beskrivelse VARCHAR2(2000 CHAR)
        , hid_opprettet NUMBER(8,0) NOT NULL ENABLE
        , hid_slettet NUMBER(8,0)
        , uuid VARCHAR2(40 BYTE) NOT NULL ENABLE
        , merke	CHAR(1 CHAR)
        , autoritet VARCHAR2(10 CHAR)
        , autoritet_status VARCHAR2(1 CHAR)
        , autoritet_dataset VARCHAR2(100 CHAR)
        , autoritet_kilde VARCHAR2(100 CHAR)
        , CONSTRAINT pk_billedkunstdel_l PRIMARY KEY (typeid)
        , CONSTRAINT uk_billedkunstdel_l UNIQUE (uuid)
        , CONSTRAINT fk_billedkunstdel_l_hid FOREIGN KEY (hid_opprettet)
	          REFERENCES primus.hendelse (hid) ENABLE
	      , CONSTRAINT fk_billedkunstdel_l_hid_s FOREIGN KEY (hid_slettet)
	          REFERENCES primus.hendelse (hid) ENABLE
				) TABLESPACE primusdt';
      commit;
		End if;
		jobbnummer:=3;
		if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(1, ''Ramme'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(2, ''Blindramme'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(3, ''Lerret'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(4, ''Grundering'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(5, ''Malingslag'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(6, ''Ferniss'', :a, sys_guid(), ''Y'')' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(1, ''Ram'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(2, ''Spännram'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(3, ''Duk'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(4, ''Grundering'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(5, ''Färglager'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into billedkunstdel_l(typeid, navn, hid_opprettet, uuid, merke) values(6, ''Fernissa'', :a, sys_guid(), ''Y'')' using nhid;
        END IF;
        commit;
        exception
        when others then
          DBMS_OUTPUT.put_line('Feil ved insert av billedkunstdel_l');
        rollback;
      end;
		End if;
		jobbnummer:=4;
		if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE
			'CREATE TABLE konservering_prioritet_l
			  ( typeid NUMBER(6,0)
			  , navn VARCHAR2(250 CHAR)
			  , beskrivelse VARCHAR2(2000 CHAR)
        , hid_opprettet NUMBER(8,0) NOT NULL ENABLE
        , hid_slettet NUMBER(8,0)
        , uuid VARCHAR2(40 BYTE) NOT NULL ENABLE
        , merke	CHAR(1 CHAR)
        , autoritet VARCHAR2(10 CHAR)
        , autoritet_status VARCHAR2(1 CHAR)
        , autoritet_dataset VARCHAR2(100 CHAR)
        , autoritet_kilde VARCHAR2(100 CHAR)
        , CONSTRAINT pk_konservering_prioritet_l PRIMARY KEY (typeid)
        , CONSTRAINT uk_konservering_prioritet_l UNIQUE (uuid)
        , CONSTRAINT fk_konservering_pri_l_hid FOREIGN KEY (hid_opprettet)
	          REFERENCES primus.hendelse (hid) ENABLE
	      , CONSTRAINT fk_konservering_pri_l_hid_s FOREIGN KEY (hid_slettet)
	          REFERENCES primus.hendelse (hid) ENABLE
				) TABLESPACE primusdt';
      commit;
		End if;
    jobbnummer:=5;
		if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(1, ''Lav'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(2, ''Medium'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(3, ''Høy'', :a, sys_guid(), ''Y'')' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(1, ''Låg'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(2, ''Medel'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into konservering_prioritet_l(typeid, navn, hid_opprettet, uuid, merke) values(3, ''Hög'', :a, sys_guid(), ''Y'')' using nhid;
        END IF;
        commit;
        exception
        when others then
          DBMS_OUTPUT.put_line('Feil ved insert av konservering_prioritet_l');
        rollback;
      end;
		End if;
    jobbnummer:=6;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'CREATE TABLE tiltaksklasse_l
        (typeid NUMBER(6,0)
        , kode VARCHAR2(5 CHAR) NOT NULL ENABLE
        , navn VARCHAR2(250 CHAR)
        , beskrivelse VARCHAR2(2000 CHAR)
        , hid_opprettet NUMBER(8,0) NOT NULL ENABLE
        , hid_slettet NUMBER(8,0)
        , uuid VARCHAR2(40 BYTE) NOT NULL ENABLE
        , merke	CHAR(1 CHAR)
        , autoritet VARCHAR2(10 CHAR)
        , autoritet_status VARCHAR2(1 CHAR)
        , autoritet_dataset VARCHAR2(100 CHAR)
        , autoritet_kilde VARCHAR2(100 CHAR)
        , CONSTRAINT pk_tiltaksklasse_l PRIMARY KEY (typeid)
        , CONSTRAINT uk_tiltaksklasse_l UNIQUE (uuid)
        , CONSTRAINT fk_tiltaksklasse_l_hid FOREIGN KEY (hid_opprettet)
            REFERENCES primus.hendelse (hid) ENABLE
        , CONSTRAINT fk_tiltaksklasse_l_hid_s FOREIGN KEY (hid_slettet)
            REFERENCES primus.hendelse (hid) ENABLE
        ) TABLESPACE primusdt';
      commit;
    End if;
    jobbnummer:=7;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                             values(1, ''TK0'', ''Ingen tiltak'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                             values(2, ''TK1'', ''Vedlikehold/forebyggende konservering'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                             values(3, ''TK2'', ''Moderate reparasjoner og/eller ytterligere undersøkelse'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                             values(4, ''TK3'', ''Store inngrep basert på diagnose'', :a, sys_guid(), ''Y'')' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                           values(1, ''TK0'', ''Ingen åtgärd'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                           values(2, ''TK1'', ''Underhåll/förebyggande konservering'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                           values(3, ''TK2'', ''Ringa reparationer och/eller ytterligare undersökning'', :a, sys_guid(), ''Y'')' using nhid;
          EXECUTE IMMEDIATE 'insert into tiltaksklasse_l(typeid, kode, navn, hid_opprettet, uuid, merke)
                           values(4, ''TK3'', ''Stora ingrepp baserade på diagnos'', :a, sys_guid(), ''Y'')' using nhid;
        END IF;
        commit;
        exception
        when others then
          DBMS_OUTPUT.put_line('Feil ved insert av tiltaksklasse_l');
        rollback;
      end;
    End if;
    jobbnummer:=8;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'ALTER TABLE tilstand add
        ( metode VARCHAR2(2000 CHAR)
        , billedkunstdelid NUMBER(6,0)
        , tiltaksklasseid NUMBER(6,0)
        , konserveringprioritetid NUMBER(6,0)
        , dato_neste_tv date
        , miljoanbefalinger VARCHAR2(2000 CHAR)
        , magasinbehov VARCHAR2(2000 CHAR)
        , framvisningsanbefalinger VARCHAR2(2000 CHAR)
        , handteringsanbefalinger VARCHAR2(2000 CHAR)
        , forpakningsanbefalinger VARCHAR2(2000 CHAR)
        , spesialkrav VARCHAR2(2000 CHAR)
        , risiko VARCHAR2(2000 CHAR)
        , miljohistorikk VARCHAR2(2000 CHAR)
        , fra_dato_miljohistorikk date
        , til_dato_miljohistorikk date
        , CONSTRAINT fk_tilstand_bkdel_l foreign key (billedkunstdelid)
            references billedkunstdel_l (typeid) enable
        , CONSTRAINT fk_tilstand_tiltaksklasse_l foreign key (tiltaksklasseid)
            references tiltaksklasse_l (typeid) enable)';
      commit;
    End if;
    jobbnummer:=9;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Annet'' where formaaltypeid = 1';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Innlån'' where formaaltypeid = 2';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Inntak'' where formaaltypeid = 3';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Skade og tap av objekt'' where formaaltypeid = 4';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Utlevering av objekt'' where formaaltypeid = 5';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Kontroll av plassering'' where formaaltypeid = 6';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Utlån'' where formaaltypeid = 7';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Bruk av samlinger'' where formaaltypeid = 8';
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                             values (11, ''Konservering'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                             values (12, ''Risikohåndtering'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                             values (13, ''Aksesjon'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                             values (14, ''Avhending'', ''Y'', sys_guid(), :a)' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Annat'' where formaaltypeid = 1';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Inlån'' where formaaltypeid = 2';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Intag'' where formaaltypeid = 3';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Skada och tapp av objekt'' where formaaltypeid = 4';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Utlämning av objekt'' where formaaltypeid = 5';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Kontroll av placering'' where formaaltypeid = 6';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Utlån'' where formaaltypeid = 7';
          EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set formaaltekst = ''Användning av samlingar'' where formaaltypeid = 8';
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                           values (11, ''Konservering'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                           values (12, ''Riskhantering'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                           values (13, ''Accession'', ''Y'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into adm_h_formaaltype_l(formaaltypeid, formaaltekst, merke, UUID, hid_opprettet)
                           values (14, ''Avhändande'', ''Y'', sys_guid(), :a)' using nhid;
        END IF;
        commit;
				exception
				  when others then
					DBMS_OUTPUT.put_line('Feil ved insert av adm_h_formaaltype_l');
					rollback;
				end;
    End if;
    jobbnummer:=10;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'ALTER TABLE environment add
        ( blob BLOB
        , verdi_type VARCHAR2(15 CHAR))';
      commit;
    end if;
    jobbnummer:=11;
    if jobnummer_in <= jobbnummer then
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        EXECUTE IMMEDIATE
          'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
           values(100, ''Svært god'', ''Svært god'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(101, ''God'', ''God'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(102, ''Tilfredsstillende'', ''Tilfredsstillende'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(103, ''Dårlig'', ''Dårlig'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(104, ''Kritisk'', ''Kritisk'')';
      ELSE
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(100, ''Mycket gott'', ''Mycket gott'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(101, ''Gott'', ''Gott'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(102, ''Tillfredsställande'', ''Tillfredsställande'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(103, ''Dåligt'', ''Dåligt'')';
        EXECUTE IMMEDIATE
        'insert into tilstand_l(kode, kort_beskrivelse, beskr_foto)
         values(104, ''Kritiskt'', ''Kritiskt'')';
      END IF;
      commit;
    end if;
    jobbnummer:=12;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table bygningsdelstype_l
          ( typeid number(6,0) not null enable
          , kode VARCHAR2(5 CHAR) not null enable
          , type varchar2(250 CHAR)
          , beskrivelse varchar2(2000 char)
          , parent_id number(6,0)
          , m_path varchar2(100 char)
          , merke varchar2(1 CHAR)
          , uuid varchar2(40 char)
          , hid_opprettet number(8,0)
          , hid_slettet number(8,0)
          , autoritet varchar2(10 CHAR)
          , autoritet_status varchar2(1 char)
          , autoritet_dataset varchar2(100 char)
          , autoritet_kilde varchar(100 char)
          , constraint pk_bygningsdelstype_l primary key (typeid)
          , constraint uk_bygningsdelstype_l_kode unique (kode)
          , constraint uk_bygningsdelstype_l_uuid unique (uuid)
          , constraint fk_bygningsdelstype_l_hid foreign key (hid_opprettet) references hendelse (hid) enable
          , constraint fk_bygningsdelstype_l_hid_s foreign key (hid_slettet) references hendelse (hid) enable
        ) tablespace primusdt';
      commit;
    end if;
    jobbnummer:=13;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(1, ''2'', ''Bygning'', -1, ''1/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(2, ''3'', ''VVS-installasjoner'', -1, ''2/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(3, ''4'', ''Elkraft'', -1, ''3/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(4, ''5'', ''Tele og automatisering'', -1, ''4/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(5, ''6'', ''Andre installasjoner'', -1, ''5/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(6, ''7'', ''Utendørs'', -1, ''6/'', sys_guid(), :a)' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(1, ''2'', ''Byggnad'', -1, ''1/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(2, ''3'', ''VVS-installationer'', -1, ''2/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(3, ''4'', ''Elkraft'', -1, ''3/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(4, ''5'', ''Tele och automatisering'', -1, ''4/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(5, ''6'', ''Andra installationer'', -1, ''5/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(6, ''7'', ''Utomhus'', -1, ''6/'', sys_guid(), :a)' using nhid;
        END IF;
        commit;
      end;
    end if;
    jobbnummer:=14;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(7, ''20'', ''Bygning, generelt'', 1, ''1/7/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(8, ''21'', ''Grunn og fundamenter'', 1, ''1/8/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(9, ''22'', ''Bæresystemer'', 1, ''1/9/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(10, ''23'', ''Yttervegger'', 1, ''1/10/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(11, ''24'', ''Innervegger'', 1, ''1/11/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(12, ''25'', ''Dekker'', 1, ''1/12/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(13, ''26'', ''Yttertak'', 1, ''1/13/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(14, ''27'', ''Fast inventar'', 1, ''1/14/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(15, ''28'', ''Trapper, balkonger, m.m.'', 1, ''1/15/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(16, ''29'', ''Andre bygningsmessige deler'', 1, ''1/16/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(17, ''30'', ''VVS-installasjoner, generelt'', 2, ''2/17/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(18, ''31'', ''Sanitær'', 2, ''2/18/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(19, ''32'', ''Varme'', 2, ''2/19/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(20, ''33'', ''Brannslokking'', 2, ''2/20/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(21, ''34'', ''Gass og trykkluft'', 2, ''2/21/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(22, ''35'', ''Prosesskjøling'', 2, ''2/22/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(23, ''36'', ''Luftbehandling'', 2, ''2/23/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(24, ''37'', ''Komfortkjøling'', 2, ''2/24/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(25, ''38'', ''Vannbehandling'', 2, ''2/25/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(26, ''39'', ''Andre VVS-installasjoner'', 2, ''2/26/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(27, ''40'', ''Elkraft, generelt'', 3, ''3/27/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(28, ''41'', ''Basisinstallasjon for elkraft'', 3, ''3/28/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(29, ''42'', ''Høyspent forsyning'', 3, ''3/29/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(30, ''43'', ''Lavspent forsyning'', 3, ''3/30/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(31, ''44'', ''Lys'', 3, ''3/31/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(32, ''45'', ''Elvarme'', 3, ''3/32/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(33, ''46'', ''Reservekraft'', 3, ''3/33/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(34, ''49'', ''Andre kraftinstallasjoner'', 3, ''3/34/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(35, ''50'', ''Tele og automatisering, generelt'', 4, ''4/35/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(36, ''51'', ''Basisintallasjon for tele og automatisering'', 4, ''4/36/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(37, ''52'', ''Integrert kommunikasjon'', 4, ''4/37/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(38, ''53'', ''Telefoni og personsøkning'', 4, ''4/38/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(39, ''54'', ''Alarm og signal'', 4, ''4/39/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(40, ''55'', ''Lyd og bilde'', 4, ''4/40/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(41, ''56'', ''Automatisering'', 4, ''4/41/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(42, ''57'', ''Instrumentering'', 4, ''4/42/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(43, ''59'', ''Andre installasjoner for tele og automatiseringer'', 4, ''4/43/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(44, ''60'', ''Andre installasjoner, generelt'', 5, ''5/44/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(45, ''61'', ''Prefabrikert rom'', 5, ''5/45/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(46, ''62'', ''Person- og varetransport'', 5, ''5/46/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(47, ''63'', ''Transportanlegg for småvarer m.v.'', 5, ''5/47/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(48, ''64'', ''Sceneteknisk utstyr'', 5, ''5/48/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(49, ''65'', ''Avfall og støvsuging'', 5, ''5/49/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(50, ''66'', ''Fasmontert spesialutrustning for virksomhet'', 5, ''5/50/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(51, ''67'', ''Løs spesialutrustning for virksomhet'', 5, ''5/51/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(52, ''69'', ''Andre tekniske installasjoner'', 5, ''5/52/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(53, ''70'', ''Utendørs, generelt'', 6, ''6/53/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(54, ''71'', ''Bearbeidet terreng'', 6, ''6/54/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(55, ''72'', ''Utendørs konstruksjoner'', 6, ''6/55/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(56, ''73'', ''Utendørs VVS'', 6, ''6/56/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(57, ''74'', ''Utendørs elkraft'', 6, ''6/57/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(58, ''75'', ''Utendørs tele og automatisering'', 6, ''6/58/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(59, ''76'', ''Veger og plasser'', 6, ''6/59/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(60, ''77'', ''Park og hage'', 6, ''6/60/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(61, ''78'', ''Utendørs infrastruktur'', 6, ''6/61/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(62, ''79'', ''Andre utendørs anlegg'', 6, ''6/62/'', sys_guid(), :a)' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(7, ''20'', ''Byggnad, generellt'', 1, ''1/7/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(8, ''21'', ''Grund och fundament'', 1, ''1/8/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(9, ''22'', ''Bärsystem'', 1, ''1/9/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(10, ''23'', ''Ytterväggar'', 1, ''1/10/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(11, ''24'', ''Innerväggar'', 1, ''1/11/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(12, ''25'', ''Dekker'', 1, ''1/12/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(13, ''26'', ''Yttertak'', 1, ''1/13/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(14, ''27'', ''Fast inventarie'', 1, ''1/14/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(15, ''28'', ''Trappor, balkonger, m.m.'', 1, ''1/15/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(16, ''29'', ''Andra byggnadsrelaterade delar'', 1, ''1/16/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(17, ''30'', ''VVS-installationer, generellt'', 2, ''2/17/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(18, ''31'', ''Sanitet'', 2, ''2/18/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(19, ''32'', ''Värme'', 2, ''2/19/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(20, ''33'', ''Brandskydd'', 2, ''2/20/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(21, ''34'', ''Gas och tryckluft'', 2, ''2/21/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(22, ''35'', ''Processkylning'', 2, ''2/22/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(23, ''36'', ''Luftbehandling'', 2, ''2/23/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(24, ''37'', ''Komfortkylning'', 2, ''2/24/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(25, ''38'', ''Vattenbehandling'', 2, ''2/25/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(26, ''39'', ''Andra VVS-installationer'', 2, ''2/26/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(27, ''40'', ''Elkraft, generellt'', 3, ''3/27/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(28, ''41'', ''Basinstallation för elkraft'', 3, ''3/28/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(29, ''42'', ''Högspänningskabel'', 3, ''3/29/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(30, ''43'', ''Lågspänningskabel'', 3, ''3/30/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(31, ''44'', ''Belysning'', 3, ''3/31/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(32, ''45'', ''Elvärme'', 3, ''3/32/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(33, ''46'', ''Reservkraft'', 3, ''3/33/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(34, ''49'', ''Andra kraftinstallationer'', 3, ''3/34/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(35, ''50'', ''Tele och automatisering, generellt'', 4, ''4/35/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(36, ''51'', ''Basintallation för tele och automatisering'', 4, ''4/36/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(37, ''52'', ''Integrerad kommunikation'', 4, ''4/37/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(38, ''53'', ''Telefoni och personsökning'', 4, ''4/38/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(39, ''54'', ''Alarm och signal'', 4, ''4/39/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(40, ''55'', ''Ljud och bild'', 4, ''4/40/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(41, ''56'', ''Automatisering'', 4, ''4/41/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(42, ''57'', ''Instrumentering'', 4, ''4/42/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(43, ''59'', ''Andra installationer för tele och automatiseringar'', 4, ''4/43/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(44, ''60'', ''Andra installationer, generellt'', 5, ''5/44/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(45, ''61'', ''Prefabricerat rum'', 5, ''5/45/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(46, ''62'', ''Person- och varutransport'', 5, ''5/46/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(47, ''63'', ''Transportanlegg for småvarer m.v.'', 5, ''5/47/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(48, ''64'', ''Scenteknisk utrustning'', 5, ''5/48/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(49, ''65'', ''Avfall och dammsugning'', 5, ''5/49/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(50, ''66'', ''Fasmontert spesialutrustning for virksomhet'', 5, ''5/50/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(51, ''67'', ''Lös specialutrustning för verksamhet'', 5, ''5/51/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(52, ''69'', ''Andra tekniska installationer'', 5, ''5/52/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(53, ''70'', ''Utomhus, generellt'', 6, ''6/53/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(54, ''71'', ''Bearbetad terräng'', 6, ''6/54/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(55, ''72'', ''Utomhus konstruktioner'', 6, ''6/55/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(56, ''73'', ''Utomhus VVS'', 6, ''6/56/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(57, ''74'', ''Utomhus elkraft'', 6, ''6/57/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(58, ''75'', ''Utomhus tele och automatisering'', 6, ''6/58/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(59, ''76'', ''Vägar och platser'', 6, ''6/59/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(60, ''77'', ''Park och trädgård'', 6, ''6/60/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(61, ''78'', ''Utomhus infrastruktur'', 6, ''6/61/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(62, ''79'', ''Andra utomhusanläggningar'', 6, ''6/62/'', sys_guid(), :a)' using nhid;
        END IF;
        commit;
      end;
    end if;
    jobbnummer:=15;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        select hidseq.nextval into nhid from dual;
        execute immediate 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(63, ''211'', ''Klargjøring av tomt'', 8, ''1/8/63/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(64, ''212'', ''Byggegrop'', 8, ''1/8/64/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(65, ''213'', ''Grunnforsterkning'', 8, ''1/8/65/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(66, ''214'', ''Støttekonstruksjon'', 8, ''1/8/66/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(67, ''215'', ''Pelefundamentering'', 8, ''1/8/67/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(68, ''216'', ''Direkte fundamentering'', 8, ''1/8/68/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(69, ''217'', ''Drenering'', 8, ''1/8/69/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(70, ''218'', ''Utstyr og komplettering'', 8, ''1/8/70/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(71, ''219'', ''Andre deler av grunn og fundamenter'', 8, ''1/8/71/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(72, ''221'', ''Rammer'', 9, ''1/9/72/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(73, ''222'', ''Søyler'', 9, ''1/9/73/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(74, ''223'', ''Bjelker'', 9, ''1/9/74/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(75, ''224'', ''Avstivende konstruksjoner'', 9, ''1/9/75/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(76, ''225'', ''Brannbeskyttelse av bærende konstruksjoner'', 9, ''1/9/76/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(77, ''226'', ''Kledning og overflate'', 9, ''1/9/77/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(78, ''228'', ''Utstyr og komplettering'', 9, ''1/9/78/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(79, ''229'', ''Andre deler av bæresystem'', 9, ''1/9/79/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(80, ''231'', ''Bærende yttervegger'', 10, ''1/10/80/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(81, ''232'', ''Ikke-bærende yttervegger'', 10, ''1/10/81/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(82, ''233'', ''Glassfasader'', 10, ''1/10/82/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(83, ''234'', ''Vinduer, dører, porter'', 10, ''1/10/83/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(84, ''235'', ''Utvendig klending og overflate'', 10, ''1/10/84/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(85, ''236'', ''Invendig overflate'', 10, ''1/10/85/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(86, ''237'', ''Solavskjerming'', 10, ''1/10/86/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(87, ''238'', ''Utstyr og komplettering'', 10, ''1/10/87/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(88, ''239'', ''Andre deler av yttervegg'', 10, ''1/10/88/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(89, ''241'', ''Bærenede innervegger'', 11, ''1/11/89/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(90, ''242'', ''Ikke-bærende innervegger'', 11, ''1/11/90/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(91, ''243'', ''Systemvegger, glassfelt'', 11, ''1/11/91/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(92, ''244'', ''Vinduer, dører, foldevegger'', 11, ''1/11/92/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(93, ''245'', ''Skjørt'', 11, ''1/11/93/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(94, ''246'', ''Kledning og overflate'', 11, ''1/11/94/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(95, ''248'', ''Utstyr og komplettering'', 11, ''1/11/95/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(96, ''249'', ''Andre deler av innervegg'', 11, ''1/11/96/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(97, ''251'', ''Frittbærende dekker'', 12, ''1/12/97/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(98, ''252'', ''Gulv på grunn'', 12, ''1/12/98/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(99, ''253'', ''Oppfõret gulv, påstøp'', 12, ''1/12/99/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(100, ''254'', ''Gulvsystemer'', 12, ''1/12/100/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(101, ''255'', ''Guloverflate'', 12, ''1/12/101/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(102, ''256'', ''Faste himlinger og overflatebehandling'', 12, ''1/12/102/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(103, ''257'', ''Systemhimlinger'', 12, ''1/12/103/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(104, ''258'', ''Utstyr og komplettering'', 12, ''1/12/104/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(105, ''259'', ''Andre deler og dekker'', 12, ''1/12/105/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(106, ''261'', ''Primærkonstruksjoner'', 13, ''1/13/106/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(107, ''262'', ''Taktekning'', 13, ''1/13/107/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(108, ''263'', ''Glasstak, overlys, takluker'', 13, ''1/13/108/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(109, ''264'', ''Takoppbygg'', 13, ''1/13/109/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(110, ''265'', ''Gesimser, takrenner og nedløp'', 13, ''1/13/110/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(111, ''266'', ''Himling og innvendig overflate'', 13, ''1/13/111/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(112, ''267'', ''Prefabrikerte takelementer'', 13, ''1/13/112/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(113, ''268'', ''Utstyr og komplettering'', 13, ''1/13/113/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(114, ''269'', ''Andre deler av yttertak'', 13, ''1/13/114/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(115, ''271'', ''Murte piper og ildsteder'', 14, ''1/14/115/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(116, ''272'', ''Monteringsferdige ildsteder'', 14, ''1/14/116/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(117, ''273'', ''Kjøkkeninnredning'', 14, ''1/14/117/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(118, ''274'', ''Innredning og garnityr for våtrom'', 14, ''1/14/118/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(119, ''275'', ''Skap og reoler'', 14, ''1/14/119/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(120, ''276'', ''Sittebenker, stolrader, bord'', 14, ''1/14/120/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(121, ''277'', ''Skilt og tavler'', 14, ''1/14/121/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(122, ''278'', ''Utstyr og komplettering'', 14, ''1/14/122/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(123, ''279'', ''Annet fast inventar'', 14, ''1/14/123/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(124, ''281'', ''Innvendige trapper'', 15, ''1/15/124/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(125, ''282'', ''Utvendige trapper'', 15, ''1/15/125/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(126, ''283'', ''Ramper'', 15, ''1/15/126/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(127, ''284'', ''Balkonger og verandaer'', 15, ''1/15/127/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(128, ''285'', ''Tribuner og amfier'', 15, ''1/15/128/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(129, ''286'', ''Baldakiner og skjermtak'', 15, ''1/15/129/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(130, ''287'', ''Andre rekkverk, håndlister og fendere'', 15, ''1/15/130/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(131, ''288'', ''Utstyr og kompletteringer'', 15, ''1/15/131/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(132, ''289'', ''Andre trapper, balkonger m.m.'', 15, ''1/15/132/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(133, ''311'', ''Bunnledninger for sanitærinstallasjoner'', 18, ''2/18/133/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(134, ''312'', ''Ledningsnett for sanitærinstallasjoner'', 18, ''2/18/134/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(135, ''314'', ''Armatur for sanitærinstallasjoner'', 18, ''2/18/135/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(136, ''315'', ''Utstyr for sanitærinstallasjoner'', 18, ''2/18/136/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(137, ''316'', ''Isolasjon for sanitærinstallasjoner'', 18, ''2/18/137/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(138, ''319'', ''Andre deler av sanitærinstallasjoner'', 18, ''2/18/138/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(139, ''321'', ''Bunnledning for varmeinstallasjoner'', 19, ''2/19/139/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(140, ''322'', ''Ledningsnett for varmeinstallasjoner'', 19, ''2/19/140/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(141, ''324'', ''Armatur for varmeinstallasjoner'', 19, ''2/19/141/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(142, ''325'', ''Utstyr for varmeinstallasjoner'', 19, ''2/19/142/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(143, ''326'', ''Isolasjon for varmeinstallasjoner'', 19, ''2/19/143/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(144, ''329'', ''Andre deler av varmeinstallasjoner'', 19, ''2/19/144/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(145, ''331'', ''Installasjon for manuell brannslokking med vann'', 20, ''2/20/145/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(146, ''332'', ''Installasjon for brannslokking med sprinkler'', 20, ''2/20/146/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(147, ''333'', ''Installasjon for brannslokking med vanntåke'', 20, ''2/20/147/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(148, ''334'', ''Installasjon for brannslokking med pulver'', 20, ''2/20/148/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(149, ''335'', ''Installasjon for brannslokking med inertgass'', 20, ''2/20/149/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(150, ''339'', ''Andre deler av installasjoner for brannslokking'', 20, ''2/20/150/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(151, ''341'', ''Installasjon til gass for bygningsdrift'', 21, ''2/21/151/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(152, ''342'', ''Installasjon til gass for virksomhet i ferdig bygg'', 21, ''2/21/152/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(153, ''343'', ''Installasjon til medisinske gasser'', 21, ''2/21/153/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(154, ''345'', ''Installasjon til trykkluft for virksomhet i ferdig bygg'', 21, ''2/21/154/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(155, ''346'', ''Installasjon til medisinsk trykkluft'', 21, ''2/21/155/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(156, ''347'', ''Vakuumsystemer'', 21, ''2/21/156/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(157, ''349'', ''Andre deler av installasjoner til gass- og trykkluft'', 21, ''2/21/157/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(158, ''351'', ''Kjøleromsystemer'', 22, ''2/22/158/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(159, ''352'', ''Fryseromsystemer'', 22, ''2/22/159/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(160, ''353'', ''Kjølesystemer for virksomhet'', 22, ''2/22/160/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(161, ''354'', ''Kjølesystemer for produksjon'', 22, ''2/22/161/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(162, ''355'', ''Kuldesystemer for innendørs idrettsbaner'', 22, ''2/22/162/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(163, ''359'', ''Andre deler av installasjoner for kulde- og kjølesystemer'', 22, ''2/22/163/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(164, ''361'', ''Kanalnett i grunnen for luftbehandling'', 23, ''2/23/164/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(165, ''362'', ''Kanalnett for luftbehandling'', 23, ''2/23/165/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(166, ''364'', ''Utstyr for luftfordeling'', 23, ''2/23/166/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(167, ''365'', ''Utstyr for luftbehandling'', 23, ''2/23/167/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(168, ''366'', ''Isolasjon av installasjon for luftbehandling'', 23, ''2/23/168/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(169, ''369'', ''Annet utstyr for luftbehandling'', 23, ''2/23/169/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(170, ''371'', ''Ledningsnett i grunnen for komfortkjøling'', 24, ''2/24/170/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(171, ''372'', ''Ledningsnett for komfortkjøling'', 24, ''2/24/171/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(172, ''374'', ''Armaturer for komfortkjøling'', 24, ''2/24/172/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(173, ''375'', ''Utstyr for komfortkjøling'', 24, ''2/24/173/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(174, ''376'', ''Isolasjon av installasjon for komfortkjøling'', 24, ''2/24/174/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(175, ''379'', ''Andre deler for komfortkjøling'', 24, ''2/24/175/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(176, ''381'', ''Systemer for rensing av forbruksvann'', 25, ''2/25/176/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(177, ''382'', ''Systemer for rensing av avløpsvann'', 25, ''2/25/177/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(178, ''383'', ''Systemer for rensing av vann til svømmebasseng'', 25, ''2/25/178/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(179, ''386'', ''Innendørs fontener og springvann'', 25, ''2/25/179/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(180, ''389'', ''Andre deler for vannbehandling'', 25, ''2/25/180/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(181, ''411'', ''Systemer for kabelføring'', 28, ''3/28/181/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(182, ''412'', ''Systemer for jording'', 28, ''3/28/182/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(183, ''413'', ''Systemer for lynvern'', 28, ''3/28/183/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(184, ''414'', ''Systemer for elkraftuttak'', 28, ''3/28/184/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(185, ''419'', ''Andre basisinstallasjoner for elkraft'', 28, ''3/28/185/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(186, ''421'', ''Fordelingssystemer'', 29, ''3/29/186/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(187, ''422'', ''Nettstasjoner'', 29, ''3/29/187/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(188, ''429'', ''Andre deler for høyspent forsyning'', 29, ''3/29/188/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(189, ''431'', ''System for elkraftinntak'', 30, ''3/30/189/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(190, ''432'', ''Systemer for hovedfordeling'', 30, ''3/30/190/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(191, ''433'', ''Elkraftfordeling til alminnelig forbruk'', 30, ''3/30/191/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(192, ''434'', ''Elkraftfordeling til driftstekniske installasjoner'', 30, ''3/30/192/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(193, ''435'', ''Elkraftfordeling til virksomhet'', 30, ''3/30/193/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(194, ''439'', ''Andre deler for lavspent forsyning'', 30, ''3/30/194/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(195, ''442'', ''Belysningsutstyr'', 31, ''3/31/195/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(196, ''443'', ''Nødlysutstyr'', 31, ''3/31/196/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(197, ''449'', ''Andre deler for installasjoner til lys'', 31, ''3/31/197/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(198, ''452'', ''Varmeovner'', 32, ''3/32/198/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(199, ''453'', ''Varmeelementer for innebygging'', 32, ''3/32/199/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(200, ''454'', ''Vannvarme og elektrokjeler'', 32, ''3/32/200/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(201, ''459'', ''Annen elvarme'', 32, ''3/32/201/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(202, ''461'', ''Elkraftaggregater'', 33, ''3/33/202/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(203, ''462'', ''Avbruddsfri kraftforsyning'', 33, ''3/33/203/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(204, ''463'', ''Akkumulatoranlegg'', 33, ''3/33/204/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(205, ''469'', ''Andre deler for reservekraftforsyning'', 33, ''3/33/205/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(206, ''511'', ''Systemer for kabelføring'', 36, ''4/36/206/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(207, ''512'', ''Jording'', 36, ''4/36/207/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(208, ''514'', ''Intakskabler for teleanlegg'', 36, ''4/36/208/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(209, ''515'', ''Telefordelinger'', 36, ''4/36/209/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(210, ''519'', ''Andre basisinstallasjoner for tele og automatisering'', 36, ''4/36/210/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(211, ''521'', ''Kabling for IKT'', 37, ''4/37/211/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(212, ''522'', ''Nettutstyr'', 37, ''4/37/212/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(213, ''523'', ''Sentralutstyr'', 37, ''4/37/213/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(214, ''524'', ''Terminalutstyr'', 37, ''4/37/214/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(215, ''529'', ''Andre deler for integrert kommunikasjon'', 37, ''4/37/215/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(216, ''532'', ''Systemer for telefoni'', 38, ''4/38/216/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(217, ''534'', ''Systemer for porttelefoner'', 38, ''4/38/217/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(218, ''535'', ''Systemer for høyttalende hustelefoner'', 38, ''4/38/218/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(219, ''536'', ''Systemer for personsøkning'', 38, ''4/38/219/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(220, ''539'', ''Andre deler for telefoni og personsøkning'', 38, ''4/38/220/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(221, ''542'', ''Brannalarm'', 39, ''4/39/221/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(222, ''543'', ''Adgangskontroll, innbrudds- og overfallsalarm'', 39, ''4/39/222/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(223, ''544'', ''Pasientsignal'', 39, ''4/39/223/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(224, ''545'', ''Uranlegg og tidsregistrering'', 39, ''4/39/224/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(225, ''549'', ''Andre deler for alarm og signal'', 39, ''4/39/225/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(226, ''552'', ''Fellesantenne'', 40, ''4/40/226/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(227, ''553'', ''Internfjernsyn'', 40, ''4/40/227/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(228, ''554'', ''Lyddistribusjonsanlegg'', 40, ''4/40/228/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(229, ''555'', ''Lydanlegg'', 40, ''4/40/229/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(230, ''556'', ''Bilde og AV-systemer'', 40, ''4/40/230/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(231, ''559'', ''Andre deler for lyd- og bildesystemer'', 40, ''4/40/231/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(232, ''562'', ''Sentral driftskontroll og automatisering'', 41, ''4/41/232/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(233, ''563'', ''Lokal automatisering'', 41, ''4/41/233/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(234, ''564'', ''Buss-systemer'', 41, ''4/41/234/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(235, ''565'', ''FDVUS:Administrative systemer'', 41, ''4/41/235/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(236, ''569'', ''Andre deler for automatisering'', 41, ''4/41/236/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(237, ''571'', ''Kabling for instrumentering'', 42, ''4/42/237/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(238, ''572'', ''Instrumentering for måling av mengde'', 42, ''4/42/238/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(239, ''573'', ''Instrumentering for måling av trykk'', 42, ''4/42/239/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(240, ''574'', ''Instrumentering for måling av temperatur'', 42, ''4/42/240/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(241, ''575'', ''Instrumentering for måling av lengde'', 42, ''4/42/241/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(242, ''576'', ''Instrumentering for måling av vekt'', 42, ''4/42/242/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(243, ''577'', ''Instrumentering for måling av elektriske størrelser'', 42, ''4/42/243/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(244, ''578'', ''Instrumentering for analyse'', 42, ''4/42/244/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(245, ''579'', ''Annen instrumentering'', 42, ''4/42/245/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(246, ''611'', ''Prefabrikerte kjølerom'', 45, ''5/45/246/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(247, ''612'', ''Prefabrikerte fryserom'', 45, ''5/45/247/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(248, ''613'', ''Prefabrikerte baderom'', 45, ''5/45/248/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(249, ''614'', ''Prefabrikerte skjermrom'', 45, ''5/45/249/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(250, ''615'', ''Prefabrikerte sjakter'', 45, ''5/45/250/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(251, ''619'', ''Andre prefabrikerte rom'', 45, ''5/45/251/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(252, ''621'', ''Heiser'', 46, ''5/46/252/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(253, ''622'', ''Rulletrapper'', 46, ''5/46/253/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(254, ''623'', ''Rullebånd'', 46, ''5/46/254/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(255, ''624'', ''Løftebord'', 46, ''5/46/255/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(256, ''625'', ''Trappeheiser'', 46, ''5/46/256/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(257, ''626'', ''Kraner'', 46, ''5/46/257/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(258, ''627'', ''Fasade- og takvask'', 46, ''5/46/258/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(259, ''629'', ''Annen person- og varetransport'', 46, ''5/46/259/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(260, ''631'', ''Dokument- og småvaretransportører'', 47, ''5/47/260/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(261, ''632'', ''Transportanlegg for tørr og løs masse'', 47, ''5/47/261/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(262, ''639'', ''Andre transportanlegg for småvarer mv.'', 47, ''5/47/262/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(263, ''651'', ''Utstyr for oppsamling og behandling av avfall'', 49, ''5/49/263/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(264, ''652'', ''Sentralstøvsuger'', 49, ''5/49/264/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(265, ''653'', ''Pneumatisk søppeltransport'', 49, ''5/49/265/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(266, ''659'', ''Andre installasjoner for avfall og støvsuging'', 49, ''5/49/266/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(267, ''711'', ''Grovplanert terreng'', 54, ''6/54/267/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(268, ''712'', ''Drenering'', 54, ''6/54/268/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(269, ''713'', ''Forsterket grunn'', 54, ''6/54/269/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(270, ''714'', ''Grøfter og groper for tekniske installasjoner'', 54, ''6/54/270/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(271, ''719'', ''Annen terrengbearbeiding'', 54, ''6/54/271/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(272, ''721'', ''Støttemurer og andre murer'', 55, ''6/55/272/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(273, ''722'', ''Trapper og ramper i terreng'', 55, ''6/55/273/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(274, ''723'', ''Frittstående skjermtak, leskur mv.'', 55, ''6/55/274/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(275, ''724'', ''Svømmebassenger mv.'', 55, ''6/55/275/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(276, ''725'', ''Gjerder, porter og bommer'', 55, ''6/55/276/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(277, ''726'', ''Kanaler og kulverter for tekniske installasjoner'', 55, ''6/55/277/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(278, ''727'', ''Kummer og tanker for tekniske installasjoner'', 55, ''6/55/278/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(279, ''729'', ''Andre utendørs konstruksjoner'', 55, ''6/55/279/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(280, ''731'', ''Utendørs VA'', 56, ''6/56/280/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(281, ''732'', ''Utendørs varme'', 56, ''6/56/281/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(282, ''733'', ''Utendørs brannslokking'', 56, ''6/56/282/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(283, ''734'', ''Utendørs gassinstallasjoner'', 56, ''6/56/283/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(284, ''735'', ''Utendørs kjøling for idrettsbaner'', 56, ''6/56/284/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(285, ''736'', ''Utendørs luftbehandlingsanlegg'', 56, ''6/56/285/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(286, ''737'', ''Utendørs forsyningsanlegg for termisk energi'', 56, ''6/56/286/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(287, ''738'', ''Utendørs fontener og springvann'', 56, ''6/56/287/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(288, ''739'', ''Andre utendørs røranlegg'', 56, ''6/56/288/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(289, ''742'', ''Utendørs høyspent forsyning'', 57, ''6/57/289/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(290, ''743'', ''Utendørs lavspent forsyning'', 57, ''6/57/290/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(291, ''744'', ''Utendørs lys'', 57, ''6/57/291/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(292, ''745'', ''Utendørs elvarme'', 57, ''6/57/292/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(293, ''746'', ''Utendørs reservekraft'', 57, ''6/57/293/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(294, ''749'', ''Andre installasjoner for utendørs elkraft'', 57, ''6/57/294/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(295, ''752'', ''Utendørs integrert kommunikasjon'', 58, ''6/58/295/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(296, ''753'', ''Utendørs telefoni og personsøkning'', 58, ''6/58/296/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(297, ''754'', ''Utendørs alarm og signal'', 58, ''6/58/297/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(298, ''755'', ''Utendørs lyd og bilde'', 58, ''6/58/298/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(299, ''756'', ''Utendørs automatisering'', 58, ''6/58/299/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(300, ''759'', ''Andre installasjoner for utendørs tele og automatisering'', 58, ''6/58/300/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(301, ''761'', ''Veger'', 59, ''6/59/301/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(302, ''762'', ''Plasser'', 59, ''6/59/302/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(303, ''763'', ''Skilter'', 59, ''6/59/303/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(304, ''764'', ''Sikkerhetsrekkverk, avvisere mv.'', 59, ''6/59/304/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(305, ''769'', ''Andre deler for veger og plasser'', 59, ''6/59/305/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(306, ''771'', ''Gressarealer'', 60, ''6/60/306/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(307, ''772'', ''Beplantning'', 60, ''6/60/307/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(308, ''773'', ''Utstyr'', 60, ''6/60/308/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(309, ''779'', ''Andre deler for parker og hager'', 60, ''6/60/309/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(310, ''783'', ''Tilknytning til eksterne nett for vannforsyning, avløp og fjernvarme'', 61, ''6/61/310/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(311, ''784'', ''Tilknytning til eksternt elkraftnett'', 61, ''6/61/311/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(312, ''785'', ''Tilknytning til eksternt telenett'', 61, ''6/61/312/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(313, ''789'', ''Andre deler for utendørs infrastuktur'', 61, ''6/61/313/'', sys_guid(), :a)' using nhid;
        ELSE
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(63, ''211'', ''Färdigställande av tomt'', 8, ''1/8/63/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(64, ''212'', ''Byggrop'', 8, ''1/8/64/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(65, ''213'', ''Grundförstärkning'', 8, ''1/8/65/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(66, ''214'', ''Stödkonstruktion'', 8, ''1/8/66/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(67, ''215'', ''Pelefundamentering'', 8, ''1/8/67/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(68, ''216'', ''Direkte fundamentering'', 8, ''1/8/68/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(69, ''217'', ''Dränering'', 8, ''1/8/69/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(70, ''218'', ''Tillägg och komplettering'', 8, ''1/8/70/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(71, ''219'', ''Andra delar av grund och fundament'', 8, ''1/8/71/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(72, ''221'', ''Rammer'', 9, ''1/9/72/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(73, ''222'', ''Søyler'', 9, ''1/9/73/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(74, ''223'', ''Bjälkar'', 9, ''1/9/74/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(75, ''224'', ''Förstyvande konstruktioner'', 9, ''1/9/75/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(76, ''225'', ''Brandskydd av bärande konstruktioner'', 9, ''1/9/76/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(77, ''226'', ''Beklädnad och yta'', 9, ''1/9/77/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(78, ''228'', ''Tillägg och komplettering'', 9, ''1/9/78/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(79, ''229'', ''Andra delar av bärsystem'', 9, ''1/9/79/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(80, ''231'', ''Bärande ytterväggar'', 10, ''1/10/80/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(81, ''232'', ''Icke bärande ytterväggar'', 10, ''1/10/81/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(82, ''233'', ''Glasfasader'', 10, ''1/10/82/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(83, ''234'', ''Fönster, dörrar, portar'', 10, ''1/10/83/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(84, ''235'', ''Utvändig beklädnad och yta'', 10, ''1/10/84/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(85, ''236'', ''Invändig yta'', 10, ''1/10/85/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(86, ''237'', ''Solavskärmning'', 10, ''1/10/86/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(87, ''238'', ''Tillägg och komplettering'', 10, ''1/10/87/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(88, ''239'', ''Andra delar av yttervägg'', 10, ''1/10/88/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(89, ''241'', ''Bärande innerväggar'', 11, ''1/11/89/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(90, ''242'', ''Icke bärande innerväggar'', 11, ''1/11/90/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(91, ''243'', ''Systemväggar, glasfält'', 11, ''1/11/91/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(92, ''244'', ''Fönster, dörrar, vikväggar'', 11, ''1/11/92/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(93, ''245'', ''Skjørt'', 11, ''1/11/93/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(94, ''246'', ''Beklädnad och yta'', 11, ''1/11/94/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(95, ''248'', ''Tillägg och komplettering'', 11, ''1/11/95/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(96, ''249'', ''Andra delar av innervägg'', 11, ''1/11/96/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(97, ''251'', ''Frittbærende dekker'', 12, ''1/12/97/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(98, ''252'', ''Golv på grund'', 12, ''1/12/98/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(99, ''253'', ''Oppfôret gulv, påstøp'', 12, ''1/12/99/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(100, ''254'', ''Golvsystem'', 12, ''1/12/100/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(101, ''255'', ''Golvyta'', 12, ''1/12/101/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(102, ''256'', ''Fasta innertak och ytbehandling'', 12, ''1/12/102/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(103, ''257'', ''Systeminnertak'', 12, ''1/12/103/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(104, ''258'', ''Tillägg och komplettering'', 12, ''1/12/104/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(105, ''259'', ''Andre deler og dekker'', 12, ''1/12/105/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(106, ''261'', ''Primärkonstruktioner'', 13, ''1/13/106/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(107, ''262'', ''Taktäckning'', 13, ''1/13/107/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(108, ''263'', ''Glastak, överljus, takluckor'', 13, ''1/13/108/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(109, ''264'', ''Takuppbyggnad'', 13, ''1/13/109/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(110, ''265'', ''Gesimser, takrenner og nedløp'', 13, ''1/13/110/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(111, ''266'', ''Innertak och invändig yta'', 13, ''1/13/111/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(112, ''267'', ''Prefabricerade takelement'', 13, ''1/13/112/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(113, ''268'', ''Tillägg och komplettering'', 13, ''1/13/113/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(114, ''269'', ''Andra delar av yttertak'', 13, ''1/13/114/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(115, ''271'', ''Murade rökgångar och eldstäder'', 14, ''1/14/115/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(116, ''272'', ''Monteringsfärdiga eldstäder'', 14, ''1/14/116/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(117, ''273'', ''Köksinredning'', 14, ''1/14/117/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(118, ''274'', ''Inredning och garnityr för våtrum'', 14, ''1/14/118/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(119, ''275'', ''Skåp och hyllor'', 14, ''1/14/119/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(120, ''276'', ''Sittbänkar, stolsrader, bord'', 14, ''1/14/120/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(121, ''277'', ''Skyltar och tavlor'', 14, ''1/14/121/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(122, ''278'', ''Tillägg och komplettering'', 14, ''1/14/122/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(123, ''279'', ''Annan fast inredning'', 14, ''1/14/123/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(124, ''281'', ''Invändiga trappor'', 15, ''1/15/124/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(125, ''282'', ''Utvändiga trappor'', 15, ''1/15/125/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(126, ''283'', ''Ramper'', 15, ''1/15/126/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(127, ''284'', ''Balkonger och verandor'', 15, ''1/15/127/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(128, ''285'', ''Tribuner og amfier'', 15, ''1/15/128/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(129, ''286'', ''Baldakiner och skärmtak'', 15, ''1/15/129/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(130, ''287'', ''Andre rekkverk, håndlister og fendere'', 15, ''1/15/130/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(131, ''288'', ''Tillägg och kompletteringer'', 15, ''1/15/131/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(132, ''289'', ''Andra trappor, balkonger m.m.'', 15, ''1/15/132/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(133, ''311'', ''Bottenledningar för sanitärinstallationer'', 18, ''2/18/133/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(134, ''312'', ''Ledningsnät för sanitärinstallationer'', 18, ''2/18/134/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(135, ''314'', ''Armatur för sanitärinstallationer'', 18, ''2/18/135/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(136, ''315'', ''Utrustning för sanitärinstallationer'', 18, ''2/18/136/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(137, ''316'', ''Isolering för sanitärinstallationer'', 18, ''2/18/137/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(138, ''319'', ''Andra delar av sanitärinstallationer'', 18, ''2/18/138/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(139, ''321'', ''Bottenledning för värmeinstallationer'', 19, ''2/19/139/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(140, ''322'', ''Ledningsnät för värmeinstallationer'', 19, ''2/19/140/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(141, ''324'', ''Armatur för värmeinstallationer'', 19, ''2/19/141/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(142, ''325'', ''Utrustning för värmeinstallationer'', 19, ''2/19/142/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(143, ''326'', ''Isolering för värmeinstallationer'', 19, ''2/19/143/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(144, ''329'', ''Andra delar av värmeinstallationer'', 19, ''2/19/144/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(145, ''331'', ''Installation för manuell brandsläckning med vatten'', 20, ''2/20/145/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(146, ''332'', ''Installation för brandsläckning med sprinkler'', 20, ''2/20/146/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(147, ''333'', ''Installation för brandsläckning med vattendimma'', 20, ''2/20/147/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(148, ''334'', ''Installation för brandsläckning med pulver'', 20, ''2/20/148/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(149, ''335'', ''Installation för brandsläckning med inert gas'', 20, ''2/20/149/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(150, ''339'', ''Andra delar av installationer för brandsläckning'', 20, ''2/20/150/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(151, ''341'', ''Installasjon til gass for bygningsdrift'', 21, ''2/21/151/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(152, ''342'', ''Installation till gas för verksamhet i färdig byggnad'', 21, ''2/21/152/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(153, ''343'', ''Installation till medicinska gaser'', 21, ''2/21/153/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(154, ''345'', ''Installation till tryckluft för verksamhet i färdig byggnad'', 21, ''2/21/154/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(155, ''346'', ''Installation till medicinsk tryckluft'', 21, ''2/21/155/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(156, ''347'', ''Vakuumsystem'', 21, ''2/21/156/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(157, ''349'', ''Andra delar av installationer till gas- och tryckluft'', 21, ''2/21/157/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(158, ''351'', ''Kylrumssystem'', 22, ''2/22/158/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(159, ''352'', ''Frysrumssystem'', 22, ''2/22/159/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(160, ''353'', ''Kylsystem för verksamhet'', 22, ''2/22/160/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(161, ''354'', ''Kylsystem för produktion'', 22, ''2/22/161/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(162, ''355'', ''Köldsystem för idrottsbanor inomhus'', 22, ''2/22/162/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(163, ''359'', ''Andra delar av installationer för köld- och kylsystem'', 22, ''2/22/163/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(164, ''361'', ''Kanalsystem i grunden för luftbehandling'', 23, ''2/23/164/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(165, ''362'', ''Kanalsystem för luftbehandling'', 23, ''2/23/165/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(166, ''364'', ''Utrustning för luftfördelning'', 23, ''2/23/166/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(167, ''365'', ''Utrustning för luftbehandling'', 23, ''2/23/167/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(168, ''366'', ''Isolering av installation för luftbehandling'', 23, ''2/23/168/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(169, ''369'', ''Annan utrustning för luftbehandling'', 23, ''2/23/169/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(170, ''371'', ''Ledningssystem i grunden för komfortkyla'', 24, ''2/24/170/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(171, ''372'', ''Ledningssystem för komfortkyla'', 24, ''2/24/171/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(172, ''374'', ''Armaturer för komfortkyla'', 24, ''2/24/172/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(173, ''375'', ''Utrustning för komfortkyla'', 24, ''2/24/173/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(174, ''376'', ''Isolering av installation för komfortkyla'', 24, ''2/24/174/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(175, ''379'', ''Andra delar för komfortkyla'', 24, ''2/24/175/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(176, ''381'', ''System för rening av dricksvatten'', 25, ''2/25/176/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(177, ''382'', ''System för rening av avloppsvatten'', 25, ''2/25/177/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(178, ''383'', ''System för rening av vatten till simbassäng'', 25, ''2/25/178/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(179, ''386'', ''Fontäner och springvatten inomhus'', 25, ''2/25/179/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(180, ''389'', ''Andra delar för vattenbehandling'', 25, ''2/25/180/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(181, ''411'', ''System för kabelföring'', 28, ''3/28/181/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(182, ''412'', ''System för jordning'', 28, ''3/28/182/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(183, ''413'', ''System för åskskydd'', 28, ''3/28/183/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(184, ''414'', ''Systemer for elkraftuttak'', 28, ''3/28/184/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(185, ''419'', ''Andra basinstallationer för elkraft'', 28, ''3/28/185/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(186, ''421'', ''Fördelningssystem'', 29, ''3/29/186/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(187, ''422'', ''Nätstationer'', 29, ''3/29/187/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(188, ''429'', ''Andra delar för högspänningsleverans'', 29, ''3/29/188/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(189, ''431'', ''System for elkraftinntak'', 30, ''3/30/189/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(190, ''432'', ''System för huvudfördelning'', 30, ''3/30/190/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(191, ''433'', ''Elkraftfördelning till allmän förbrukning'', 30, ''3/30/191/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(192, ''434'', ''Elkraftfördelning till drifttekniska installationer'', 30, ''3/30/192/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(193, ''435'', ''Elkraftfördelning till verksamhet'', 30, ''3/30/193/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(194, ''439'', ''Andra delar för lågspänningsleverans'', 30, ''3/30/194/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(195, ''442'', ''Belysningsutrustning'', 31, ''3/31/195/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(196, ''443'', ''Nödbelysningsutrustning'', 31, ''3/31/196/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(197, ''449'', ''Andra delar för installationer till belysning'', 31, ''3/31/197/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(198, ''452'', ''Varmeovner'', 32, ''3/32/198/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(199, ''453'', ''Varmeelementer for innebygging'', 32, ''3/32/199/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(200, ''454'', ''Vannvarme og elektrokjeler'', 32, ''3/32/200/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(201, ''459'', ''Annan elvärme'', 32, ''3/32/201/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(202, ''461'', ''Elkraftsaggregat'', 33, ''3/33/202/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(203, ''462'', ''Avbrottsfri kraftleverans'', 33, ''3/33/203/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(204, ''463'', ''Ackumulatoranläggning'', 33, ''3/33/204/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(205, ''469'', ''Andra delar för reservkraftsleverans'', 33, ''3/33/205/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(206, ''511'', ''System för kabelföring'', 36, ''4/36/206/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(207, ''512'', ''Jordning'', 36, ''4/36/207/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(208, ''514'', ''Intakskabler for teleanlegg'', 36, ''4/36/208/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(209, ''515'', ''Telefordelinger'', 36, ''4/36/209/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(210, ''519'', ''Andra basinstallationer för tele och automatisering'', 36, ''4/36/210/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(211, ''521'', ''Kablage för IKT'', 37, ''4/37/211/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(212, ''522'', ''Nätutrustning'', 37, ''4/37/212/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(213, ''523'', ''Centralutrustning'', 37, ''4/37/213/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(214, ''524'', ''Terminalutrusting'', 37, ''4/37/214/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(215, ''529'', ''Andra delar för integrerad kommunikation'', 37, ''4/37/215/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(216, ''532'', ''System för telefoni'', 38, ''4/38/216/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(217, ''534'', ''System för porttelefoner'', 38, ''4/38/217/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(218, ''535'', ''System för högtalartelefoner'', 38, ''4/38/218/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(219, ''536'', ''System för personsökning'', 38, ''4/38/219/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(220, ''539'', ''Andra delar för telefoni och personsökning'', 38, ''4/38/220/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(221, ''542'', ''Brandlarm'', 39, ''4/39/221/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(222, ''543'', ''Åtkomstkontroll, inbrotts- och överfallslarm'', 39, ''4/39/222/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(223, ''544'', ''Patientlarm'', 39, ''4/39/223/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(224, ''545'', ''Uranlegg og tidsregistrering'', 39, ''4/39/224/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(225, ''549'', ''Andra delar för larm och signal'', 39, ''4/39/225/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(226, ''552'', ''Fellesantenne'', 40, ''4/40/226/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(227, ''553'', ''Intern-TV'', 40, ''4/40/227/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(228, ''554'', ''Ljuddistributionsanläggning'', 40, ''4/40/228/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(229, ''555'', ''Ljudanläggning'', 40, ''4/40/229/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(230, ''556'', ''Bild- och AV-system'', 40, ''4/40/230/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(231, ''559'', ''Andra delar för ljud- och bildsystem'', 40, ''4/40/231/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(232, ''562'', ''Central driftkontroll och automatisering'', 41, ''4/41/232/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(233, ''563'', ''Lokal automatisering'', 41, ''4/41/233/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(234, ''564'', ''Buss-systemer'', 41, ''4/41/234/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(235, ''565'', ''FDVUS:Administrative systemer'', 41, ''4/41/235/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(236, ''569'', ''Andra delar för automatisering'', 41, ''4/41/236/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(237, ''571'', ''Kablage för instrumentering'', 42, ''4/42/237/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(238, ''572'', ''Instrumentering för mätning av mängd'', 42, ''4/42/238/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(239, ''573'', ''Instrumentering för mätning av tryck'', 42, ''4/42/239/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(240, ''574'', ''Instrumentering för mätning av temperatur'', 42, ''4/42/240/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(241, ''575'', ''Instrumentering för mätning av längd'', 42, ''4/42/241/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(242, ''576'', ''Instrumentering för mätning av vikt'', 42, ''4/42/242/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(243, ''577'', ''Instrumentering för mätning av elektriska storheter'', 42, ''4/42/243/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(244, ''578'', ''Instrumentering för analys'', 42, ''4/42/244/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(245, ''579'', ''Annan instrumentering'', 42, ''4/42/245/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(246, ''611'', ''Prefabricerade kylrum'', 45, ''5/45/246/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(247, ''612'', ''Prefabricerade frysrum'', 45, ''5/45/247/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(248, ''613'', ''Prefabricerade badrum'', 45, ''5/45/248/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(249, ''614'', ''Prefabricerade skärmrum'', 45, ''5/45/249/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(250, ''615'', ''Prefabricerade schakt'', 45, ''5/45/250/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(251, ''619'', ''Andra prefabricerade rum'', 45, ''5/45/251/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(252, ''621'', ''Hissar'', 46, ''5/46/252/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(253, ''622'', ''Rulltrappor'', 46, ''5/46/253/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(254, ''623'', ''Rullband'', 46, ''5/46/254/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(255, ''624'', ''Lyftbord'', 46, ''5/46/255/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(256, ''625'', ''Trapphissar'', 46, ''5/46/256/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(257, ''626'', ''Kranar'', 46, ''5/46/257/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(258, ''627'', ''Fasad- och taktvätt'', 46, ''5/46/258/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(259, ''629'', ''Annan person- och varutransport'', 46, ''5/46/259/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(260, ''631'', ''Dokument- og småvaretransportører'', 47, ''5/47/260/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(261, ''632'', ''Transportanläggning för torr och lös massa'', 47, ''5/47/261/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(262, ''639'', ''Andre transportanlegg for småvarer mv.'', 47, ''5/47/262/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(263, ''651'', ''Utrustning för uppsamling och behandling av avfall'', 49, ''5/49/263/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(264, ''652'', ''Centraldammsugare'', 49, ''5/49/264/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(265, ''653'', ''Pneumatisk soptransport'', 49, ''5/49/265/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(266, ''659'', ''Andra installationer för avfall och dammsugning'', 49, ''5/49/266/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(267, ''711'', ''Grovplanerad terräng'', 54, ''6/54/267/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(268, ''712'', ''Dränering'', 54, ''6/54/268/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(269, ''713'', ''Förstärkt grund'', 54, ''6/54/269/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(270, ''714'', ''Diken och gropar för tekniska installationer'', 54, ''6/54/270/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(271, ''719'', ''Annan terrängbearbetning'', 54, ''6/54/271/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(272, ''721'', ''Stödmurar och andra murar'', 55, ''6/55/272/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(273, ''722'', ''Trappor och ramper i terräng'', 55, ''6/55/273/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(274, ''723'', ''Fristående skärmtak, läskydd m.m.'', 55, ''6/55/274/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(275, ''724'', ''Simbassänger m.m.'', 55, ''6/55/275/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(276, ''725'', ''Stängsel, portar och bommar'', 55, ''6/55/276/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(277, ''726'', ''Kanaler och kulvertar för tekniska installationer'', 55, ''6/55/277/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(278, ''727'', ''Kammare och tankar för tekniska installationer'', 55, ''6/55/278/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(279, ''729'', ''Andra konstruktioner utomhus'', 55, ''6/55/279/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(280, ''731'', ''VA utomhus'', 56, ''6/56/280/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(281, ''732'', ''Värme utomhus'', 56, ''6/56/281/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(282, ''733'', ''Brandsläckning utomhus'', 56, ''6/56/282/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(283, ''734'', ''Gasinstallationer utomhus'', 56, ''6/56/283/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(284, ''735'', ''Kylning för idrottsbanor utomhus'', 56, ''6/56/284/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(285, ''736'', ''Luftbehandlingsanläggning utomhus'', 56, ''6/56/285/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(286, ''737'', ''Leveransanläggning utomhus för termisk energi'', 56, ''6/56/286/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(287, ''738'', ''Fontäner och springvatten utomhus'', 56, ''6/56/287/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(288, ''739'', ''Andra röranläggningar utomhus'', 56, ''6/56/288/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(289, ''742'', ''Högspänningsleverans utomhus'', 57, ''6/57/289/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(290, ''743'', ''Lågspänningsleverans utomhus'', 57, ''6/57/290/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(291, ''744'', ''Belysning utomhus'', 57, ''6/57/291/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(292, ''745'', ''Elvärme utomhus'', 57, ''6/57/292/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(293, ''746'', ''Reservkraft utomhus'', 57, ''6/57/293/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(294, ''749'', ''Andra installationer utomhus för elkraft'', 57, ''6/57/294/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(295, ''752'', ''Integrerad kommunikation utomhus'', 58, ''6/58/295/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(296, ''753'', ''Telefoni och personsökning utomhus'', 58, ''6/58/296/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(297, ''754'', ''Larm och signaler utomhus'', 58, ''6/58/297/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(298, ''755'', ''Ljud och bild utomhus'', 58, ''6/58/298/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(299, ''756'', ''Automatisering utomhus'', 58, ''6/58/299/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(300, ''759'', ''Andra installationer utomhus för tele och automatisering'', 58, ''6/58/300/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(301, ''761'', ''Vägar'', 59, ''6/59/301/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(302, ''762'', ''Platser'', 59, ''6/59/302/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(303, ''763'', ''Skyltar'', 59, ''6/59/303/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(304, ''764'', ''Skyddsräcken, avvisare m.m.'', 59, ''6/59/304/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(305, ''769'', ''Andra delar för vägar och platser'', 59, ''6/59/305/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(306, ''771'', ''Gräsytor'', 60, ''6/60/306/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(307, ''772'', ''Planteringar'', 60, ''6/60/307/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(308, ''773'', ''Utrustning'', 60, ''6/60/308/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(309, ''779'', ''Andra delar för parker och trädgårdar'', 60, ''6/60/309/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(310, ''783'', ''Anslutning till externa nät för vattenförsörjning, avlopp och fjärrvärme'', 61, ''6/61/310/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(311, ''784'', ''Anslutning till externt elkraftsnät'', 61, ''6/61/311/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(312, ''785'', ''Anslutning till externt telenät'', 61, ''6/61/312/'', sys_guid(), :a)' using nhid;
          EXECUTE IMMEDIATE 'insert into bygningsdelstype_l(typeid, kode, type, parent_id, m_path, uuid, hid_opprettet) values(313, ''789'', ''Andra delar för infrastuktur utomhus'', 61, ''6/61/313/'', sys_guid(), :a)' using nhid;
        END IF;
        commit;
      end;
    end if;
    jobbnummer:=16;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'alter table oppgave add
        ( bygningsdelstypeid number(6,0)
        , constraint fk_oppgave_bd_type_id foreign key (bygningsdelstypeid) references bygningsdelstype_l (typeid) enable)';
      commit;
    end if;
    jobbnummer:=17;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'alter table skade add
      ( bygningsdelstypeid number(6,0)
      , constraint fk_skade_bd_type_id foreign key (bygningsdelstypeid) references bygningsdelstype_l (typeid) enable)';
      commit;
    end if;
    jobbnummer:=18;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
      'alter table tilstand add
      ( bygningsdelstypeid number(6,0)
      , constraint fk_tilstand_bd_type_id foreign key (bygningsdelstypeid) references bygningsdelstype_l (typeid) enable)';
      commit;
    end if;
    jobbnummer:=19;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE 'alter table analyse_metode add rekkefolge number(3,0)';
      commit;
    End if;
    jobbnummer:=20;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE 'alter table bilde add exif clob';
      commit;
    End if;
    jobbnummer:=21;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
        skadeid number;
      begin
        EXECUTE IMMEDIATE 'select hidseq.nextval from dual' INTO nhid;
        EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;

        EXECUTE IMMEDIATE 'update skade_l set merke=''X'' where (navn=''Konservering'' or navn=''Daglig drift'') and hid_slettet is null';

        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
                        values (:a, ''Biologisk'', NULL, :b||''/'', :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;

        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
                        values (:a, ''Kjemisk'', NULL, :b||''/'', :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;

        EXECUTE IMMEDIATE 'select skade_lidseq.nextval from dual' INTO skadeid;
        EXECUTE IMMEDIATE 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid, merke)
                        values (:a, ''Mekanisk'', NULL, :b||''/'', :c, sys_guid(), ''Y'')' using skadeid, skadeid, nhid;
        commit;
      end;
    End if;
    jobbnummer:=22;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        EXECUTE IMMEDIATE 'select hidseq.nextval from dual' INTO nhid;
        EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        EXECUTE IMMEDIATE 'update behandlingstype_l set merke = ''X'' where filterfeltid is null';
        commit;
      end;
    End if;
    jobbnummer:=23;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE 'alter table oppgave add frist date';
    End if;
    jobbnummer:=24;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'Insert into ENVIRONMENT (VARIABEL,VERDI,SYNLIG,GRUPPE,VERDI_TYPE) values (''EDITION_ADM_EVENTS'',''False'',null,''EDITION'',''bool'')';
      EXECUTE IMMEDIATE 'Insert into ENVIRONMENT (VARIABEL,VERDI,SYNLIG,GRUPPE,VERDI_TYPE) values (''EDITION_FOLDER'',''False'',null,''EDITION'',''bool'')';
      EXECUTE IMMEDIATE 'Insert into ENVIRONMENT (VARIABEL,VERDI,SYNLIG,GRUPPE,VERDI_TYPE) values (''EDITION_REPORT_EXCEL'',''False'',null,''EDITION'',''bool'')';
      EXECUTE IMMEDIATE 'Insert into ENVIRONMENT (VARIABEL,VERDI,SYNLIG,GRUPPE,VERDI_TYPE) values (''EDITION_ANNOTATION'',''False'',null,''EDITION'',''bool'')';
      EXECUTE IMMEDIATE 'Insert into ENVIRONMENT (VARIABEL,VERDI,SYNLIG,GRUPPE,VERDI_TYPE) values (''EDITION'',''Small'',null,''EDITION'',''str'')';
    END IF;
    jobbnummer:=25;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'update environment set gruppe = ''DEF_CONCEPT'' where variabel like ''DEF_%'' and gruppe = ''WEB''';
    END IF;
    jobbnummer:=26;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'update environment set VERDI = ''7.0.0'' where variabel = ''VERSION'' and gruppe = ''WEB''';
    END IF;
    jobbnummer:=27;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'ALTER TABLE TILSTAND MODIFY BESKRIVELSE VARCHAR2(4000 CHAR)';
    END IF;
    jobbnummer:=28;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'update tilstand t1 set t1.tiltaksklasseid = 1
                         where exists (
                          select null
                          from tilstand t2 join objekt_adm_hendelse oah on t2.objid = oah.admh_objid
                            join objekt o on oah.objid_siste = o.objid
                          where o.objtypeid = 3 and t2.tilstandskode = ''TG0'' and t2.objid = t1.objid and t2.hid = t1.hid)';
      EXECUTE IMMEDIATE 'update tilstand t1 set t1.tiltaksklasseid = 2
                         where exists (
                          select null
                          from tilstand t2 join objekt_adm_hendelse oah on t2.objid = oah.admh_objid
                            join objekt o on oah.objid_siste = o.objid
                          where o.objtypeid = 3 and t2.tilstandskode = ''TG1'' and t2.objid = t1.objid and t2.hid = t1.hid)';
      EXECUTE IMMEDIATE 'update tilstand t1 set t1.tiltaksklasseid = 3
                         where exists (
                          select null
                          from tilstand t2 join objekt_adm_hendelse oah on t2.objid = oah.admh_objid
                            join objekt o on oah.objid_siste = o.objid
                          where o.objtypeid = 3 and t2.tilstandskode = ''TG2'' and t2.objid = t1.objid and t2.hid = t1.hid)';
      EXECUTE IMMEDIATE 'update tilstand t1 set t1.tiltaksklasseid = 3
                         where exists (
                          select null
                          from tilstand t2 join objekt_adm_hendelse oah on t2.objid = oah.admh_objid
                            join objekt o on oah.objid_siste = o.objid
                          where o.objtypeid = 3 and t2.tilstandskode = ''TG3'' and t2.objid = t1.objid and t2.hid = t1.hid)';
    END IF;
    jobbnummer:=29;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'alter table tilstand_l add
                        ( NAVN VARCHAR2(250 CHAR)
	                      , BESKRIVELSE VARCHAR2(2000 CHAR)
	                      , HID_OPPRETTET NUMBER(8,0)
	                      , HID_SLETTET NUMBER(8,0)
	                      , UUID VARCHAR2(40 BYTE)
	                      , MERKE CHAR(1 CHAR) default ''Y''
	                      , AUTORITET VARCHAR2(10 CHAR)
	                      , AUTORITET_STATUS VARCHAR2(1 CHAR)
	                      , AUTORITET_DATASET VARCHAR2(100 CHAR)
	                      , AUTORITET_KILDE VARCHAR2(100 CHAR)
	                      , CONSTRAINT FK_TILSTAND_L_HID FOREIGN KEY (HID_OPPRETTET)
	                        REFERENCES PRIMUS.HENDELSE (HID) ENABLE
	                      , CONSTRAINT FK_TILSTAND_L_HID_S FOREIGN KEY (HID_SLETTET)
	                        REFERENCES PRIMUS.HENDELSE (HID) ENABLE)';
      commit;
    END IF;
    jobbnummer:=30;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'update tilstand_l set navn = beskr_foto, beskrivelse = kort_beskrivelse';
      commit;
    END IF;
    jobbnummer:=31;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE '	CREATE OR REPLACE TRIGGER SUPEROBJEKTTYPE_SO_TYPE
			                     BEFORE INSERT ON SUPEROBJEKT   FOR EACH ROW
                            DECLARE
                               BEGIN
                                if (:NEW.SO_TYPE is NULL) and (:NEW.SUPEROBJEKTTYPEID is not NULL) then
                                 SELECT CASE WHEN :NEW.SUPEROBJEKTTYPEID = 1 THEN ''Objekt''
                                                     WHEN :NEW.SUPEROBJEKTTYPEID = 2 THEN ''Actor''
                                           WHEN :NEW.SUPEROBJEKTTYPEID = 3 THEN ''ADM_HEND''
                                       WHEN :NEW.SUPEROBJEKTTYPEID = 4 THEN ''DAMAGE''
                                       WHEN :NEW.SUPEROBJEKTTYPEID = 5 THEN ''BILDE''
                                       WHEN :NEW.SUPEROBJEKTTYPEID = 6 THEN ''KOLLI''
                                       WHEN :NEW.SUPEROBJEKTTYPEID = 7 THEN ''VIDEO''
                                       END INTO :NEW.SO_TYPE FROM DUAL;
                                Elsif (:NEW.SO_TYPE is not NULL) and (:NEW.SUPEROBJEKTTYPEID is NULL) then
                                 SELECT CASE WHEN :NEW.SO_TYPE =''Objekt''   THEN 1
                                                     WHEN :NEW.SO_TYPE =''Actor''    THEN 2
                                           WHEN :NEW.SO_TYPE =''ADM_HEND'' THEN 3
                                       WHEN :NEW.SO_TYPE =''DAMAGE''   THEN 4
                                       WHEN :NEW.SO_TYPE =''BILDE''    THEN 5
                                       WHEN :NEW.SO_TYPE =''KOLLI''    THEN 6
                                       WHEN :NEW.SO_TYPE =''VIDEO''    THEN 7
                                       END INTO :NEW.SUPEROBJEKTTYPEID FROM DUAL;
                                END IF;
		                        END;';
    END IF;
    jobbnummer:=32;
    IF jobnummer_in <= jobbnummer THEN
      EXECUTE IMMEDIATE 'ALTER TRIGGER SUPEROBJEKTTYPE_SO_TYPE ENABLE';
    END IF;
    jobbnummer:=33;
    if jobnummer_in <= jobbnummer then
      declare
        nhid number;
      begin
        EXECUTE IMMEDIATE 'select hidseq.nextval from dual' INTO nhid;
        EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' using nhid;
        commit;
        EXECUTE IMMEDIATE 'update behandlingstype_l set merke = ''X'' where autoritet is null';
        EXECUTE IMMEDIATE 'update analysetype_l set merke = ''X'' where autoritet is null';
        EXECUTE IMMEDIATE 'update skade_l set merke = ''X'' where autoritet is null';
        EXECUTE IMMEDIATE 'update materiale_l set merke = ''X'' where autoritet is null and filterfelt = ''BEHAN''';
        EXECUTE IMMEDIATE 'update adm_h_formaaltype_l set merke = ''X'' where autoritet is null and filterfeltid = 3';
        commit;
      end;
    End if;
    jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus47 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(47, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/




