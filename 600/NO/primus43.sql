create or replace function primus43(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Settings
	*/

	begin
	jobbnummer:=1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table environment modify VARIABEL VARCHAR2(50 CHAR)';
		EXECUTE IMMEDIATE 'alter table environment modify VERDI VARCHAR2(100 CHAR)';
		EXECUTE IMMEDIATE 'alter table environment add GRUPPE VARCHAR2(30 CHAR)';		
	End if;	
	
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX'', ''TS'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_NUMBER_SIZE'', ''4'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_FORMAT'', ''<prefix>.<yyyy>-<n>'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_PHOTOGRAPH'', ''PT'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_NUMBER_SIZE_PHOTOGRAPH'', ''5'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_FORMAT_PHOTOGRAPH'', ''<prefix>.<yyyy>-<n>'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_THING'', ''TT'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_ACTOR'', ''AT'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_PLACE'', ''PL'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_CONSERVATION_EVENT'', ''CE'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_DAMAGE'', ''AE_18'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_TREATMENT_EVENT'', ''AE_9'', ''ID'')';
	End if;
    jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then			
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_CONDITION_ASSESSMENT_EVENT'', ''AE_11'', ''ID'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_PREFIX_OBSERVATION_EVENT'', ''AE_18'', ''ID'')';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''EMAIL_HOST'', ''postman.kulturit.no'', ''EMAIL'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''EMAIL_PORT'', ''25'', ''EMAIL'')';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
    
    IF primus.SVENSKELLERNORSKDATABASE = 'NOR' THEN
      EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''CLIENT_LANGUAGE'', ''no'', ''WEB'')';
    ELSE
      EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''CLIENT_LANGUAGE'', ''sv'', ''WEB'')';
    END IF;

		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''VERSION'', ''6.0.0'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''MODULE_NAME'', ''Konservering'', ''WEB'')';
		
    /* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''CLIENT_LANGUAGE'', ''en'', 'WEB')'; */
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_VIEW_TYPE'', ''thumbs'', ''WEB'')';
		/* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_VIEW_TYPE'', ''list'', 'WEB')'; */
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_EDIT_MODE'', ''ro'', ''WEB'')';
		/* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_EDIT_MODE'', ''ed'', 'WEB')'; */
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_NAV_TYPE'', ''tabs'', ''WEB'')';
		/* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_NAV_TYPE'', ''accordion'', 'WEB')'; */
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_TITLE_STATUS_ID'', ''ct_35-1'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_TITLE_LANGUAGE_ID'', ''ct_36-300306'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_ARTWORK'', ''ct_43-9'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_PHOTOGRAPH'', ''ct_42-24'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_THING'', ''ct_44-4'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_ARCHITECTURE'', ''ct_45-34'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_DESIGN'', ''ct_46-39'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_REG_LEVEL_ID_BUILDING'', ''ct_67-19'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_PUBLISH_LEVEL_ID'', ''ct_18-1'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_PUBLISH_LEVEL_ACTOR_ID'', ''ct_32-1'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_GENDER'', ''unknown'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_ROLE_STATUS_ID'', ''ct_2-6'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_PLACE_ROLE_STATUS_ID'', ''ct_4-6'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_EVENT_PHOTOGRAPH_ID '', ''ct_19-101'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_EVENT_PRODUCTION_ID'', ''ct_19-100'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_PHOTOGRAPHER_ID '', ''ct_1-1'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_PICTURED_ID'', ''ct_1-70'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_ARTIST_ID'', ''ct_1-101'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_PERSON_ID'', ''ct_23-1'', ''WEB'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_ARCHITECT_ID'', ''ct_1-108'', ''WEB'')';
		/* Svensk */
		/* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_DESIGNER_ID'', ''ct_1-223'', 'WEB')'; */ /* rollekode 11D - Designer/Formgiver */
		/* EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_DESIGNER_ID'', ''ct_1-233'', 'WEB')'; */ /* rollekode: 14D - Designer/Formgiver */
		/* Norsk */
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DEF_ACTOR_DESIGNER_ID'', ''ct_1-101'', ''WEB'')'; /* rollekode: 10K - Kunstner */
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''ID_URL'', ''http://museum.phpdev.kit.no/plugins/primus/pages/json.php'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''DATA_URL'', ''http://museum.phpdev.kit.no/plugins/primus/pages/json.php?id={}'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''RESPONSE_URL'', ''http://museum.phpdev.kit.no/plugins/primus/pages/oppdaterdato.php?id={}'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''COLLECTION_ID'', ''ct_31-1'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''SET_RIGHTS'', ''True'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''RIGHTS_TYPE_ID'', ''ct_34-23'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''REFERENCE_URL'', ''http://museum.phpdev.kit.no/pages/view.php?ref={}'', ''RESOURCESPACE'')';
		EXECUTE IMMEDIATE 'insert into environment(variabel, verdi, gruppe) values(''REFERENCE_TYPE_ID'', ''9'', ''RESOURCESPACE'')';
	End if;
	jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus43 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(43, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
