CREATE OR REPLACE FUNCTION primus26(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
    Status
    */

    BEGIN
      jobbnummer := 1;
      executeScript(jobbnummer, jobnummer_in,
                    'alter table superobjekt add( hid_status number(8,0), hid_prioritet number(8,0))');

      jobbnummer := 4;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'alter table superobjekt add
          ( constraint fk_superobjekt_hid_status foreign key (objid, hid_status)
              references SUPEROBJEKT_STATUS_H(objid,hid))';
      END IF;

      jobbnummer := 5;
      executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
                        values(1, ''Ny'', 3, getoracleuuid())');

      jobbnummer := 6;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
                      values(2, ''Under arbeid'', 3, getoracleuuid())');
      ELSE
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
                      values(2, ''Under arbete'', 3, getoracleuuid())');
      END IF;
      jobbnummer := 7;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
                      values(3, ''Avsluttet'', 3, getoracleuuid())');
      ELSE
        executeScript(jobbnummer, jobnummer_in, 'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
                      values(3, ''Avslutad'', 3, getoracleuuid())');
      END IF;
      jobbnummer := 8;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create or replace PROCEDURE SUPEROBJEKT_STATUS_UPDATE (
	        pobjid IN NUMBER, poldhid IN NUMBER, 
            pnewhid IN NUMBER, pstatusid IN NUMBER, 
			pkommentar varchar2) IS  
			BEGIN
			  if poldHid =pnewhid then
				 UPDATE SUPEROBJEKT_STATUS_H  SET STATUSID = pstatusid WHERE (objid = pobjid AND hid = pnewhid);
			  ELSE
				 INSERT INTO SUPEROBJEKT_STATUS_H (OBJID, HID, STATUSID, KOMMENTAR)
				   VALUES (pobjid, pnewhid, pstatusid, pkommentar);
				 UPDATE SUPEROBJEKT SET hid_STATUS = pnewhid WHERE (objid = pobjid);
			 End if; 
			END SUPEROBJEKT_STATUS_UPDATE;';
      END IF;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus26 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(26, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/