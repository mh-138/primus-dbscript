create or replace function primus65(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 65, jobbnummer );
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table objekt_navn add ( jpnr number( 8 ) ) ' );
    end if;

    jobbnummer := 2;
    
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 65, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table objekt_navn add ( constraint fk_objektnavn_jurperson foreign key ( jpnr ) references jurperson ( jpnr ) )' );
    end if;
    
    jobbnummer := 3;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus65 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(65, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      