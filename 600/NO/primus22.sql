CREATE OR REPLACE FUNCTION primus22(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS

  BEGIN
    dbms_output.enable;

    BEGIN


      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_produksjonsdatering_h(objid NUMBER(8,0)
						               , hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)						   
                           , CONSTRAINT FK_produksjonsdtg_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_produksjonsdtg_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_produksjonsdtg_h PRIMARY KEY (objid, hid))  tablespace primusdt ';
      END IF;

      jobbnummer := 2;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_produksjonshistorikk_h
                           ( objid NUMBER(8,0)
						               , hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)
                           , CONSTRAINT FK_produksjonshist_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_produksjonshist_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_produksjonshist_h PRIMARY KEY (objid, hid))  tablespace primusdt';
      END IF;

      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_proveniensdatering_h
                           ( objid NUMBER(8,0)
						               , hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)
                           , CONSTRAINT FK_proveniensdtg_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_proveniensdtg_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_proveniensdtg_h PRIMARY KEY (objid, hid)) tablespace primusdt';
      END IF;

      jobbnummer := 4;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_provenienshistorikk_h
                           ( objid NUMBER(8,0)
						               , hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)
                           , CONSTRAINT FK_provenienshist_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_provenienshist_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_provenienshist_h PRIMARY KEY (objid, hid)) tablespace primusdt';
      END IF;
      jobbnummer := 5;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_opplagsopplysninger_h
                           ( objid NUMBER(8,0), hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), kommentar VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)
                           , CONSTRAINT FK_opplgsopplysnr_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_opplgsopplysnr_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_opplgsopplysnr_h PRIMARY KEY (objid, hid)) tablespace primusdt';
      END IF;
      jobbnummer := 6;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create table obj_stiltekst_h
                           ( objid NUMBER(8,0), hid NUMBER(8,0)
                           , verdi  VARCHAR2(1000 CHAR), TABLE_KEY NUMBER(8,0)
                           , CONSTRAINT FK_stiltekst_h_objid FOREIGN KEY(objid) REFERENCES objekt(objid)
                           , CONSTRAINT FK_stiltekst_h_hid FOREIGN KEY(HID) REFERENCES HENDELSE(HID)
                           , CONSTRAINT PK_stiltekst_h PRIMARY KEY (objid, hid)) tablespace primusdt';
      END IF;


      jobbnummer := 7;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table objekt add
						   ( hid_produksjonsdtrng number(8,0)
						    , hid_produksjonshist number(8,0)
							, hid_proveniensdtrng number(8,0)
							, hid_provenienshist number(8,0)
							, hid_opplag number(8,0)
							, hid_stiltekst number(8,0)
							, constraint fk_objekt_hid_prdksdtg foreign key (objid, hid_produksjonsdtrng) references obj_produksjonsdatering_h(objid, hid)
							, constraint fk_objekt_hid_prdkshst foreign key (objid, hid_produksjonshist) references obj_produksjonshistorikk_h(objid, hid)
							, constraint fk_objekt_hid_provsdtg foreign key (objid, hid_proveniensdtrng) references obj_proveniensdatering_h(objid, hid)
							, constraint fk_objekt_hid_provshst foreign key (objid, hid_provenienshist) references obj_provenienshistorikk_h(objid, hid)
							, constraint fk_objekt_hid_opplag foreign key (objid, hid_opplag) references obj_opplagsopplysninger_h(objid, hid)			
							, constraint fk_objekt_hid_stiltkst foreign key (objid, hid_stiltekst) references obj_stiltekst_h(objid, hid)			
							)';
      END IF;

      jobbnummer := 8;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create or replace
          FUNCTION GETNEWHID(nobjid number) RETURN NUMBER as  HID NUMBER;
          nHid Number;
          BEGIN
             Select HIDSEQ.NEXTVAL into nHid from dual;
             INSERT into HENDELSE (HID, DATO, SIGNID) VALUES (nhid, Sysdate, 1);
             INSERT into SUPEROBJEKT_HENDELSE (OBJID, HID) values (NOBJID, NHID);
             HID:=nhid;
          RETURN HID;
          END GETNEWHID;';
      END IF;


      jobbnummer := 15;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create or replace procedure update_Objekt(
     felttype number, NOBJID number, nhid number, nslettet number) as
          COLUMNNAME varchar2(100);
    begin
            if NSLETTET = 0 then
       if felttype=1 then
        COLUMNNAME := ''hid_produksjonshist'';
       end if;
       if felttype=2 then
        COLUMNNAME := ''hid_provenienshist'';
       end if;
       if felttype=3 then
        COLUMNNAME := ''hid_produksjonsdtrng'';
       end if;
       if felttype=4 then
        COLUMNNAME := ''hid_proveniensdtrng'';
       end if;
       if felttype=5 then
        COLUMNNAME := ''hid_stiltekst'';
       end if;
       if felttype=6 then
        COLUMNNAME := ''HID_OPPLAG'';
       End if;

         EXECUTSQLSTR(''update OBJEKT set ''||COLUMNNAME||'' = ''||NHID ||'' where OBJID= ''||NOBJID);
       end if;
    end update_Objekt;';
      END IF;

      jobbnummer := 16;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        'create or replace procedure POPULATE_HISTORIKK_TABLE as
         cursor MYCURSOR(FT number) is  select OBJID, VERDI, SLETTET, TABLE_KEY  from OBJEKT_FELTER_TABLE where (FELT_TYPE =FT) ORDER by OBJID, TABLE_KEY;
      nobjid NUMBER;
      NHID number;
      nslettet number;
      nTABLE_KEY number;
      VERDI varchar2(1000);
    begin
       for i in 1..5 Loop
        open MYCURSOR(I);
         LOOP
          FETCH MyCursor INTO nobjid, verdi, nslettet, nTABLE_KEY;
          EXIT when MYCURSOR%NOTFOUND;
          nhid := GETNEWHID(nobjid);
          if i = 1 then
           insert into obj_produksjonshistorikk_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);
          end if;
          if i = 2 then
           insert into obj_provenienshistorikk_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);
          end if;
          if i = 3 then
            insert into obj_produksjonsdatering_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);
          end if;
          if i = 4 then
           insert into obj_proveniensdatering_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);
          end if;
          if i = 5 then
           insert into obj_stiltekst_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);
          end if;
                    update_Objekt( i, nobjid, nHid, nslettet);
         END LOOP;
        CLOSE MyCursor;
        commit;
       end LOOP;
    end POPULATE_HISTORIKK_TABLE;';
      END IF;

      jobbnummer := 17;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        ' create or replace procedure POPULATE_OPPLAG_TABLE as
          cursor MY2CURSOR is  select distinct OB1.OBJID, OB1.VERDI, OB1.SLETTET, OB2.VERDI, ob1.TABLE_KEY
                     from OBJEKT_FELTER_TABLE OB1 full outer join OBJEKT_FELTER_TABLE OB2 on OB1.OBJID = OB2.OBJID
                     and OB2.FELT_TYPE =9 and  ob2.slettet =0 where OB1.FELT_TYPE =6 order by ob1.objid;

    cursor MYCURSOR is	select distinct OBJID, NULL, SLETTET, VERDI, TABLE_KEY
         from OBJEKT_FELTER_TABLE  where (FELT_TYPE in (9))
         and OBJID not in( select OB1.OBJID	from OBJEKT_FELTER_TABLE OB1
         left outer join OBJEKT_FELTER_TABLE OB2 on OB1.OBJID = OB2.OBJID
         and OB2.FELT_TYPE =9 and OB2.SLETTET =0
                   where (OB1.FELT_TYPE =6) ) order by objid;

      nobjid NUMBER;
      NHID number;
      nslettet number;
      nTABLE_KEY number;
      VERDI varchar2(1000);
      komm varchar2(1000);
    begin
        open MY2CURSOR;
         LOOP
        FETCH MY2CURSOR into NOBJID, VERDI, NSLETTET, KOMM, nTABLE_KEY;
        EXIT when MY2CURSOR%NOTFOUND;
         NHID := GETNEWHID(NOBJID);
         insert into OBJ_OPPLAGSOPPLYSNINGER_H (OBJID, HID, VERDI, KOMMENTAR, TABLE_KEY) values (NOBJID, NHID, VERDI, KOMM, nTABLE_KEY);
         update_Objekt( 6, nobjid, nHid, nslettet);
         end LOOP;
        CLOSE My2Cursor;
        -- for opplagkommentar som har ikke opplag
        open MYCURSOR;
         LOOP
        FETCH MYCURSOR into NOBJID, VERDI, NSLETTET, KOMM, nTABLE_KEY;
        EXIT when MYCURSOR%NOTFOUND;
         NHID := GETNEWHID(NOBJID);
         insert into OBJ_OPPLAGSOPPLYSNINGER_H (OBJID, HID, VERDI, KOMMENTAR, TABLE_KEY) values (NOBJID, NHID, VERDI, KOMM, nTABLE_KEY);
         update_Objekt( 6, nobjid, nHid, nslettet);
         end LOOP;
        CLOSE MyCursor;
    end POPULATE_OPPLAG_TABLE;';
      END IF;

      jobbnummer := 18;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        'create or replace
     PROCEDURE obj_produksjonsdtng_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING) AS
    CURSOR myCurser IS SELECT objid FROM obj_produksjonsdatering_h WHERE (objid = pobjid AND hid = phid);
    dummy NUMBER;
     BEGIN
      OPEN myCurser;
      FETCH myCurser INTO dummy;
      IF myCurser%FOUND then
      UPDATE obj_produksjonsdatering_h SET VERDI = pverdi  WHERE (OBJID = pobjid) AND (HID = phid);
      ELSE
      INSERT INTO obj_produksjonsdatering_h (OBJID, HID, VERDI) VALUES (pobjid, phid, pverdi);
      UPDATE OBJEKT SET  hid_produksjonsdtrng= phid WHERE (OBJID = pobjid);
      END IF;
     END obj_produksjonsdtng_h_update;';
      END IF;

      jobbnummer := 19;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        'create or replace
     PROCEDURE obj_produksjonshist_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING) AS
    CURSOR myCurser IS SELECT objid FROM obj_produksjonshistorikk_h WHERE (objid = pobjid AND hid = phid);
    dummy NUMBER;
     BEGIN
     OPEN myCurser;
     FETCH myCurser INTO dummy;
     IF myCurser%FOUND then
       UPDATE obj_produksjonshistorikk_h SET VERDI = pverdi  WHERE (OBJID = pobjid) AND (HID = phid);
     ELSE
       INSERT INTO obj_produksjonshistorikk_h (OBJID, HID, VERDI) VALUES (pobjid, phid, pverdi);
       UPDATE OBJEKT SET  hid_produksjonshist= phid WHERE (OBJID = pobjid);
     END IF;
     END obj_produksjonshist_h_update;';
      END IF;

      jobbnummer := 20;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create or replace
    PROCEDURE obj_proveniensdtng_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING) AS
      CURSOR myCurser IS SELECT objid FROM obj_proveniensdatering_h WHERE (objid = pobjid AND hid = phid);
      dummy NUMBER;
    BEGIN
      OPEN myCurser;
      FETCH myCurser INTO dummy;
      IF myCurser%FOUND then
      UPDATE obj_proveniensdatering_h SET VERDI = pverdi  WHERE (OBJID = pobjid) AND (HID = phid);
      ELSE
      INSERT INTO obj_proveniensdatering_h (OBJID, HID, VERDI) VALUES (pobjid, phid, pverdi);
      UPDATE OBJEKT SET  hid_proveniensdtrng= phid WHERE (OBJID = pobjid);
      END IF;
    END obj_proveniensdtng_h_update;';
      END IF;

      jobbnummer := 21;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        'create or replace
     PROCEDURE obj_provenienshist_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING) AS
     CURSOR myCurser IS SELECT objid FROM obj_provenienshistorikk_h WHERE (objid = pobjid AND hid = phid);
     dummy NUMBER;
    BEGIN
      OPEN myCurser;
      FETCH myCurser INTO dummy;
      IF myCurser%FOUND then
      UPDATE obj_provenienshistorikk_h SET VERDI = pverdi  WHERE (OBJID = pobjid) AND (HID = phid);
      ELSE
      INSERT INTO obj_provenienshistorikk_h (OBJID, HID, VERDI) VALUES (pobjid, phid, pverdi);
      UPDATE OBJEKT SET  hid_provenienshist= phid WHERE (OBJID = pobjid);
      END IF;
    END obj_provenienshist_h_update;';
      END IF;

      jobbnummer := 22;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create or replace
    PROCEDURE obj_opplagsopplysng_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING, pKomm IN STRING) AS
      CURSOR myCurser IS SELECT objid FROM obj_opplagsopplysninger_h WHERE (objid = pobjid AND hid = phid);
      dummy NUMBER;
    BEGIN
      OPEN myCurser;
      FETCH myCurser INTO dummy;
      IF myCurser%FOUND then
      UPDATE obj_opplagsopplysninger_h SET VERDI = pverdi, KOMMENTAR = pKomm  WHERE (OBJID = pobjid) AND (HID = phid);
      ELSE
      INSERT INTO obj_opplagsopplysninger_h (OBJID, HID, VERDI, KOMMENTAR) VALUES (pobjid, phid, pverdi, pKomm);
      UPDATE OBJEKT SET  hid_opplag= phid WHERE (OBJID = pobjid);
      END IF;
    END obj_opplagsopplysng_h_update;';
      END IF;

      jobbnummer := 23;
      IF jobnummer_in <= jobbnummer
      THEN

        EXECUTE IMMEDIATE
        'create or replace
    PROCEDURE obj_stiltekst_h_update (pobjid IN NUMBER, phid IN NUMBER, pverdi IN STRING) AS
      CURSOR myCurser IS SELECT objid FROM obj_stiltekst_h WHERE (objid = pobjid AND hid = phid);
      dummy NUMBER;
    BEGIN
      OPEN myCurser;
      FETCH myCurser INTO dummy;
      IF myCurser%FOUND then
      UPDATE obj_stiltekst_h SET VERDI = pverdi  WHERE (OBJID = pobjid) AND (HID = phid);
      ELSE
      INSERT INTO obj_stiltekst_h (OBJID, HID, VERDI) VALUES (pobjid, phid, pverdi);
      UPDATE OBJEKT SET  hid_stiltekst= phid WHERE (OBJID = pobjid);
      END IF;
    END obj_stiltekst_h_update;';


      END IF;

      jobbnummer := 24;
      IF jobnummer_in <= jobbnummer
      THEN
        DECLARE
          nnewhid        NUMBER;
          versjonsnummer NUMBER;
        BEGIN --start dbjobb 606
          SELECT hidseq.nextval
          INTO nnewhid
          FROM dual;
          EXECUTE IMMEDIATE ' insert into hendelse (hid, signid, dato) values (:a, 1, sysdate)' USING nnewhid;

          IF svenskEllerNorskDatabase = 'NOR'
          THEN
            EXECUTE IMMEDIATE 'insert into brukerrolle_l(brukerrolleid, brukerrolle, hid_opprettet) values(1, ''Håndtverker'', :a  )' USING nnewhid;
            EXECUTE IMMEDIATE 'insert into brukerrolle_l(brukerrolleid, brukerrolle, hid_opprettet) values(2, ''Konservator'',:a)' USING nnewhid;
          ELSE
            EXECUTE IMMEDIATE 'insert into brukerrolle_l(brukerrolleid, brukerrolle, hid_opprettet) values(1, ''Hantverkare'', :a  )' USING nnewhid;
            EXECUTE IMMEDIATE 'insert into brukerrolle_l(brukerrolleid, brukerrolle, hid_opprettet) values(2, ''Antikvarie'',:a)' USING nnewhid;
          END IF;
        END;
      END IF;


      jobbnummer := 25;
      COMMIT;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus22 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(22, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;
  END;
/
