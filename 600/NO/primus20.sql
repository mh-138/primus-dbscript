create or replace function primus20(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is

begin
  dbms_output.enable;
  
  begin
	
	jobbnummer:=1;
	 if jobnummer_in <= jobbnummer then
	  execute immediate 'create sequence behmetodeseq
              MINVALUE 1 
              MAXVALUE 999999999999999999999999999 
              INCREMENT BY 1 
              START WITH 1 
              CACHE 20 
              NOORDER  
              NOCYCLE';
	End if;		  
			  
	 jobbnummer:=2;
	 if jobnummer_in <= jobbnummer then
	  execute immediate 'create sequence behandlingtypeidseq
            MINVALUE 1 
            MAXVALUE 999999999999999999999999999 
            INCREMENT BY 1 
            START WITH 1 
            CACHE 20 
            NOORDER  
            NOCYCLE';		  
	End if;		
			  
	jobbnummer := 3;
     if jobnummer_in <= jobbnummer then
	  execute immediate 'create table behandlingstype_l
                          ( BEHANDLINGSTYPEID NUMBER(6,0)
                          , NAVN VARCHAR2(250 CHAR)
                          , PARENT_ID NUMBER(6,0)
                          , BESKRIVELSE  VARCHAR2(2000 CHAR)
                          , M_PATH VARCHAR2(100 CHAR)
                          , HID_OPPRETTET NUMBER(8,0) NOT NULL
                          , HID_SLETTET NUMBER(8,0)
                          , UUID  VARCHAR2(40 BYTE) NOT NULL
                          , MERKE CHAR(1 CHAR) DEFAULT ''Y'' NOT NULL
                          , AUTORITET VARCHAR2(10 CHAR)
                          , AUTORITET_STATUS VARCHAR2(1 CHAR)
                          , AUTORITET_DATASET VARCHAR2(100 CHAR)
                          , AUTORITET_KILDE VARCHAR2(100 CHAR)
                          , FILTERFELTID NUMBER(3,0)
                          , CONSTRAINT FK_BEHANDLINGSTYPE_L_HID FOREIGN KEY(HID_OPPRETTET) REFERENCES HENDELSE(HID)
                          , CONSTRAINT FK_BEHANDLINGSTYPE_L_HID_S FOREIGN KEY(HID_SLETTET) REFERENCES HENDELSE(HID)
                          , CONSTRAINT PK_BEHANDLINGSTYPE_L PRIMARY KEY (BEHANDLINGSTYPEID)
                          , CONSTRAINT UK_BEHANDLINGSTYPE_L_UUID UNIQUE(UUID))';
		End if;				  
  
        jobbnummer := 4;
        if jobnummer_in <= jobbnummer then
	     execute immediate 'create table behandling
                           ( objid number(8,0)
                           , constraint PK_BEHANDLING primary key (objid)
                           , constraint FK_BEH_ADMH_OBJID foreign key (objid) references adm_hendelse(objid))';
	  End if;					   
   
        jobbnummer := 5;
        if jobnummer_in <= jobbnummer then
	      execute immediate 'create table behandling_metode
                          ( objid number(8,0)
                          , hid number(9,0)
                          , nr number(3,0)
                          , objid_siste number(8,0)
                          , behmetodeid number(8,0) -- unik - bruke sekvens...
                          , behandlingstypeid number(5,0)
                          , ressursbruk varchar2(100 char)
                          , kostnad number(10, 2)
                          , constraint PK_BEHANDLING_METODE primary key (objid, hid, nr)
                          , constraint FK_BEH_METODE_BEH foreign key (objid) references behandling (objid)
                          , constraint FK_BEH_METODE_HENDELSE foreign key (hid) references hendelse (hid)
                          , constraint FK_BEH_METODE_BEHTYPE foreign key (behandlingstypeid) references behandlingstype_l (behandlingstypeid)
                          , constraint UK_BEH_METODE_BEHMETODEID unique (behmetodeid))';
	  End if;					  
					  
  
        jobbnummer := 6;
        if jobnummer_in <= jobbnummer then
	       execute immediate 'create table behandling_metode_materiale
                          ( behmetodeid number(8,0)
                          , hid number(9,0)
                          , nr number(3,0)
                          , behmetodeid_siste number(8,0)
                          , materialeid number(8,0)
                          , kommentar varchar2(1000 char)
                          , constraint PK_BEHANDLING_METODE_MATERIALE primary key (behmetodeid, hid, nr)
                          , constraint FK_BEHMETMAT foreign key (behmetodeid) references behandling_metode (behmetodeid)
                          , constraint FK_BEHMETMAT_HENDELSE foreign key (hid) references hendelse(hid)
                          , constraint FK_BEHMETMAT_MATERIALE foreign key (materialeid) references materiale_l(id))';
						  
         End if;
        jobbnummer :=7;
        if jobnummer_in <= jobbnummer then
	      execute immediate 'create table brukerrolle_l
								(brukerrolleid number(4)
								, brukerrolle varchar2(20 char) not null
								, hid_opprettet number(9) not null
								, hid_slettet number(9)
								, merke varchar(1 char) default ''Y'' not null
								, constraint PK_BRUKERROLLE primary key (brukerrolleid))
								tablespace primusdt
								';
		End if;						
			-- opprette tabell for koblingen mellom signatur og roller
		jobbnummer :=8;
         if jobnummer_in <= jobbnummer then
	      execute immediate 'create table signatur_brukerrolle
								(signid number(6) not null
								, brukerrolleid number(4) not null
								, constraint PK_SIGNATURBRUKERROLLE primary key (signid, brukerrolleid)
								, constraint FK_SIGNBRUKERROLLE_SIGN foreign key (signid) references signatur(signid)
								, constraint FK_SIGNNBRUKERROLLE_L foreign key (brukerrolleid) references brukerrolle_l(brukerrolleid))
								tablespace primusdt
								';
		End if;
	
	jobbnummer:= 9;
      commit;
	jobbnummer := 10;
         return 'OK';		   
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus20 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(20, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/
