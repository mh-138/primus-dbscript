create or replace function primus57(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	-- Legger inn ny trigger for digitaltmuseum tabellen
  -- Legger til merke kolonne for hylle, fag, reol og vegg tabellene

	begin
        jobbnummer := 1;	
		-- legger til nye kolonner i digitaltmuseum tabellen, brukes av gateway.
		if  jobnummer_in <= jobbnummer then		
			executeAlterTable(jobbnummer, jobnummer_in, 'alter table primus.digitaltmuseum add (publisert_status char(10), publisert_dato timestamp(9), publisert_melding varchar2(2000 char))');
		end if;
		jobbnummer := 2;
	 
		-- endrer trigger på tabellen digitaltmuseum slik at gateway kan oppdatere "sine" kolonner uten at DATO feltet oppdateres.
		if jobnummer_in <= jobbnummer then
			execute immediate 'create or replace trigger DIGITALTMUSEUM_TIMESTAMP
									before insert or update on PRIMUS.DIGITALTMUSEUM
									FOR EACH ROW
									DECLARE
									begin
									if updating then
										IF ( ( :NEW.PUBLISERT_DATO IS NULL or ( :new.publisert_dato = :OLD.PUBLISERT_DATO ) ) 
											AND ( :NEW.PUBLISERT_MELDING IS NULL OR ( :NEW.PUBLISERT_MELDING = :OLD.PUBLISERT_MELDING ) )
											AND ( :NEW.PUBLISERT_STATUS IS NULL OR ( :NEW.PUBLISERT_STATUS = :OLD.PUBLISERT_STATUS ) ) ) THEN
												:NEW.DATO  := current_timestamp;
										END IF;
									END IF;
									IF inserting then
										IF :NEW.PUBLISERT_STATUS IS NULL and :NEW.PUBLISERT_DATO IS NULL
												and :NEW.PUBLISERT_MELDING IS NULL THEN
											:NEW.DATO  := current_timestamp;
										END IF;
									end if;
								end;';
		end if;

		jobbnummer := 3;
		if jobnummer_in <= jobbnummer then
			executeAlterTable(jobbnummer, jobnummer_in, 'alter table adm_hendelse add (hid_slettet_kommentar varchar2 (100 char))');
		end if;
		
    --- legger til merke for plassering_xx_tabeller - PRIWIN-1160
    jobbnummer := 4;
    if jobnummer_in <= jobbnummer then
      executeAlterTable(jobbnummer, jobnummer_in, 'alter table plassering_reol_l add ( merke varchar2( 1 char ) default ''Y'' ) ');
    end if;

    jobbnummer := 5;
    if jobnummer_in <= jobbnummer then
      executeAlterTable(jobbnummer, jobnummer_in, 'alter table plassering_fag_l add ( merke varchar2( 1 char ) default ''Y'' )');
    end if;
    
    jobbnummer := 6;
    if jobnummer_in <= jobbnummer then
      executeAlterTable(jobbnummer, jobnummer_in, 'alter table plassering_hylle_l add ( merke varchar2( 1 char ) default ''Y'' )');
    end if; 

    jobbnummer := 7;
    if jobnummer_in <= jobbnummer then
      executeAlterTable(jobbnummer, jobnummer_in, 'alter table plassering_vegg_l add ( merke varchar2( 1 char ) default ''Y'' )');
    end if; 

    jobbnummer := 100;
		
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus57 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(57, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	 