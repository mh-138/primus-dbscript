CREATE OR REPLACE FUNCTION primus29(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
    Konsekvensgrad
    */

    BEGIN
      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table konsekvensgrad_l
          ( kgradid NUMBER(3,0)
          , kode VARCHAR2(5 CHAR) NOT NULL ENABLE
          , kort_beskrivelse VARCHAR2(100 CHAR)
          , constraint pk_konsekvensgrad_l PRIMARY KEY (kgradid)
          , constraint uk_konsekvensgrad_kode UNIQUE (KODE)
        ) tablespace primusdt';

      END IF;
      jobbnummer := 2;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(1, ''KG0'', ''På lang sikt'')');
      ELSE
        executeScript(jobbnummer,jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(1, ''KG0'', ''På lång sikt'')');
      END IF;
      jobbnummer := 3;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(2, ''KG1'', ''På middels lang sikt'')');
      ELSE
        executeScript(jobbnummer,jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(2, ''KG1'', ''På medellång sikt'')');
      END IF;
      jobbnummer := 4;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(3, ''KG2'', ''På kort sikt'')');
      ELSE
        executeScript(jobbnummer,jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(3, ''KG2'', ''På kort sikt'')');
      END IF;
      jobbnummer := 5;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(4, ''KG3'', ''Strakstiltak'')');
      ELSE
        executeScript(jobbnummer,jobnummer_in,
                      'insert into konsekvensgrad_l (kgradid, kode, kort_beskrivelse) values(4, ''KG3'', ''Omedelbar åtgärd'')');
      END IF;
      jobbnummer := 6;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'alter table tilstand add
          ( kgradid NUMBER(3,0)
          , constraint fk_kons_kgradid foreign key (kgradid) references konsekvensgrad_l(kgradid))';
      END IF;
      jobbnummer := 4;
      COMMIT;

      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus29 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(29, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/