create or replace function primus52(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Data overføring fra MUSHENDELSE til ADM_HENDELSE.
	*/
	begin
	    jobbnummer:=1; ------ utstilling---------------
	    --UTSTILLING with a row value in MUSHENDELSE
		if jobnummer_in <= jobbnummer then	
		  PrimusOppdJobbNrIVersjon( 52, jobbnummer );
      DECLARE		  
			iId Integer;		  
		  BEGIN  
			  For TH in (select DISTINCT MP.objid
                                 , MP.HID
                                 , MP.FRADATO
                                 , MP.TILDATO
                                 , MP.UTSTILLINGSID
                                 , MP.PUBLISERES
                                 , MP.HID_PUBLISERBARINGRESS
                                 , MP.HID_PUBLISERBARTEKST
                                 , MP.HID_PUBLISERINGSNIVAA
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MP.SAKSNR
                                 , MP.BESKRIVELSE
                                 , MP.UUID
                                 , MP.DIMUKODE
						                  FROM UTSTILLING MP
                                 , MUSHENDELSE MH 
                             WHERE MP.HID = MH.HID
						                   AND MH.hovedtypekode = 10003						 
						                   AND MH.MERKE is NULL)
			  Loop	
	            
          iId := TH.OBJID;    
			
          execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE,  SAKSNUMMER, 
				                   PUBLISERES, HID_PUBLISERINGSNIVAA, HID_PUBLISERBARINGRESS, HID_PUBLISERBARTEKST, UUID, DIMUKODE) 
								   values (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)' 
								   using iId, TH.HID, TH.NEWHOVEDTYPE,  TH.SAKSNR, TH.PUBLISERES, TH.HID_PUBLISERINGSNIVAA, 
								   TH.HID_PUBLISERBARINGRESS, TH.HID_PUBLISERBARTEKST, TH.uuid, TH.DIMUKODE;
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.fradato, th.tildato;
          execute immediate 'BEGIN SAVE_AHUTS_OBJEKT(:a, :b); END;' using iId, TH.hid;
          execute immediate 'BEGIN SAVE_AH_IDNR(:a, :b, :C); END;' using iId, TH.hid, TH.UTSTILLINGSID;
          
          if TH.BESKRIVELSE is not NULL then
            execute immediate 'Insert Into OBJ_BESKRIVELSE_H(OBJID, HID, BESKRIVELSE) values (:a, :b, :c)' USING iId, TH.hid, th.BESKRIVELSE; 
            execute immediate 'UPDATE ADM_HENDELSE set HID_BESKRIVELSE = :a where objid = :b' using TH.hid, iId;						 
          End if;					   				
          
          execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using TH.HID;		
          execute immediate 'update UTSTILLING set  MERKE=''X'' Where OBJID = :a ' using TH.OBJID;
	
			  END LOOP;		 
			  commit;
		  END;	
    			
		End if;		

    jobbnummer:= 2;	
    PrimusOppdJobbNrIVersjon( 52, jobbnummer );    
    executeScript(jobbnummer,jobnummer_in, 'alter table UTSTILLINGSTYPE_H add (NR NUMBER(3,0)) ');
	  
    jobbnummer:= 3;		
    PrimusOppdJobbNrIVersjon( 52, jobbnummer );
	  executeScript(jobbnummer,jobnummer_in, 'Update UTSTILLINGSTYPE_H Set NR = 1');	
	  
    jobbnummer:= 4;	  
    PrimusOppdJobbNrIVersjon( 52, jobbnummer );
    if jobnummer_in <= jobbnummer then		
      DECLARE		  
        iId Integer;				
			BEGIN  			
			  
			  For TL in (select Distinct u.objid
                                 , u.hid_UTSTILLINGSTYPE 
                              FROM utstilling u
                                 , utstillingstype_h uh 
                             WHERE u.hid_UTSTILLINGSTYPE = UH.HId)
			  Loop
				iId :=1;       
				  For TH in (select Distinct hid from UTSTILLINGSTYPE_H uh Where objid= TL.objid)
				  Loop	
					 
					  if TH.hid <> TL.hid_UTSTILLINGSTYPE then
						iId := Iid +1;		
						execute immediate 'Update UTSTILLINGSTYPE_H Set NR = :a Where objid=:b and HID=:C' using Iid, TL.objid, TH.hid;
					    commit;
					  End if;
					END LOOP;	 
			  END LOOP;		 	  
			exception
        when no_data_found then
          dbms_output.put_line( ' 52 -- ingen data funnet, oppdatering av historisk utstillingstype_h tabellen ' );
      end;
	End if;		
	
	jobbnummer:= 5;
	  if jobnummer_in <= jobbnummer then		
	    PrimusOppdJobbNrIVersjon( 52, jobbnummer );
      execute immediate '  DECLARE		  
			  iId Integer;				
			BEGIN  			
			  
			  For TL in (select Distinct UH.* , 
			             case When UH.NR=1 then UH.OBJID  
			                  When UH.NR<>1 then NULL End as OBJID_SISTE   
			             from UTSTILLINGSTYPE_H UH)
			  Loop
				execute immediate ''Insert INTO ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) values (:a, :b, :c, :d, :e)'' 
				using TL.OBJID, TL.OBJID_SISTE, Tl.HID, TL.NR, Tl.TYPEID;
				commit;				
			  END LOOP;		 	  
			end;';

	End if;	    
		
    ----------------- END of Utstilling  ----------------
    jobbnummer:= 100;		
    return 'OK';
	exception
		when others then
		    ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus52 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(52, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/