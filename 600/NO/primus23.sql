
create or replace function primus23(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is

begin
  dbms_output.enable;
  
  begin
  
  
      JOBBNUMMER := 1;
        EXECUTE IMMEDIATE ' begin POPULATE_HISTORIKK_TABLE; end;';
	  
	  JOBBNUMMER := 2;
	     EXECUTE IMMEDIATE 'begin POPULATE_OPPLAG_TABLE; end;';

	 
	 
	 	jobbnummer:= 13;
      commit;
	jobbnummer := 4;
         return 'OK';		   
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus23 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(23, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/
