create or replace function primus56(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	

	begin
			jobbnummer := 1;
			if jobnummer_in <= jobbnummer then
				executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_produksjonshistorikk_h add (table_key number(8))');
				executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_provenienshistorikk_h add (table_key number(8))');
				executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_produksjonsdatering_h add (table_key number(8))');
				executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_proveniensdatering_h add (table_key number(8))');
        executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_stiltekst_h add (table_key number(8))');
        executeAlterTable(jobbnummer, jobnummer_in, 'alter table obj_opplagsopplysninger_h add (table_key number(8))');
			end if;
    jobbnummer := 2;
    if  jobnummer_in <= jobbnummer then		
          
		  EXECUTE IMMEDIATE
          'create or replace procedure POPULATE_HISTORIKK_TABLE as
           cursor MYCURSOR(FT number) is  select OBJID, VERDI, SLETTET, TABLE_KEY  from OBJEKT_FELTER_TABLE where (FELT_TYPE =FT) ORDER by OBJID, TABLE_KEY;  
			  nobjid NUMBER;
			  NHID number;
			  nslettet number;
			  nTABLE_KEY number;
			  VERDI varchar2(1000);  
			begin  
				 for i in 1..5 Loop 
					open MYCURSOR(I);
					 LOOP
					  FETCH MyCursor INTO nobjid, verdi, nslettet, nTable_Key;
					  EXIT when MYCURSOR%NOTFOUND;   					  
						nhid := GETNEWHID(nobjid);
						if i = 1 then						
						 insert into obj_produksjonshistorikk_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY); 						 
						end if;
						if i = 2 then						 
						 insert into obj_provenienshistorikk_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY);                         
						end if;
						if i = 3 then
						  insert into obj_produksjonsdatering_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY); 						  
						end if;
						if i = 4 then						 
						 insert into obj_proveniensdatering_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY); 						
						end if;	
						if i = 5 then						 
						 insert into obj_stiltekst_h (OBJID, HID, verdi, TABLE_KEY) values (nobjid, nHid, verdi, nTABLE_KEY); 						
					  end if;	
                      update_Objekt( i, nobjid, nHid, nslettet);
					 END LOOP;
					CLOSE MyCursor;
				  commit;
				 end LOOP;  
			end POPULATE_HISTORIKK_TABLE;';
    End if;
		jobbnummer:= 3;
        if  jobnummer_in <= jobbnummer then	
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_produksjonshist=null Where objid In (select objid from obj_produksjonshistorikk_h Where TABLE_KEY is not null)';
         EXECUTE IMMEDIATE  'DELETE from obj_produksjonshistorikk_h Where TABLE_KEY is not null';
		End if;		
		
    jobbnummer:= 4;
        if  jobnummer_in <= jobbnummer then	
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_provenienshist=null Where objid In (select objid from obj_provenienshistorikk_h Where TABLE_KEY is not null)';
		 EXECUTE IMMEDIATE  'DELETE from obj_provenienshistorikk_h Where TABLE_KEY is not null';
		End if;		
		jobbnummer:= 5;
        if  jobnummer_in <= jobbnummer then		 		
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_produksjonsdtrng=null Where objid In (select objid from obj_produksjonsdatering_h Where TABLE_KEY is not null)';
		 EXECUTE IMMEDIATE  'DELETE from obj_produksjonsdatering_h Where TABLE_KEY is not null';
		End if;		
		jobbnummer:= 6;
        if  jobnummer_in <= jobbnummer then		  
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_proveniensdtrng=null Where objid In (select objid from obj_proveniensdatering_h Where TABLE_KEY is not null)';
		 EXECUTE IMMEDIATE  'DELETE from obj_proveniensdatering_h Where TABLE_KEY is not null';
		End if;		
		jobbnummer:= 7;
        if  jobnummer_in <= jobbnummer then		  
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_stiltekst=null Where objid In (select objid from obj_stiltekst_h Where TABLE_KEY is not null)';
		 EXECUTE IMMEDIATE  'DELETE from obj_stiltekst_h Where TABLE_KEY is not null';
		End if;		
		jobbnummer:= 8;
        if  jobnummer_in <= jobbnummer then		  
		 EXECUTE IMMEDIATE  'UPDATE Objekt set hid_opplag=null Where objid In (select objid from OBJ_OPPLAGSOPPLYSNINGER_H Where TABLE_KEY is not null)';
		 EXECUTE IMMEDIATE  'DELETE from OBJ_OPPLAGSOPPLYSNINGER_H Where TABLE_KEY is not null';
		End if;	
		
	    jobbnummer:= 9;
		if  jobnummer_in <= jobbnummer then		  
         EXECUTE IMMEDIATE ' begin POPULATE_HISTORIKK_TABLE; end;';
		End if;
	  
	    jobbnummer:= 10;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'begin POPULATE_OPPLAG_TABLE; end;';
		End if; 
            
			
		jobbnummer:= 11;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE SUPEROBJEKT_DATERING_UPDATE (pobjid NUMBER, phid NUMBER,
					 pfradato DATE, ptildato DATE, pgrunnlag STRING) IS				  
				BEGIN
				  MERGE INTO SUPEROBJEKT_DATERING_H  USING DUAL ON (OBJID=pobjid and HID=phid ) 
				  WHEN MATCHED THEN   UPDATE  SET FRADATO = pfradato, TILDATO = ptildato, GRUNNLAG = pgrunnlag					   
				  WHEN NOT MATCHED THEN  INSERT (OBJID,HID,FRADATO, TILDATO, GRUNNLAG) VALUES (pobjid, phid , pfradato, ptildato,  pgrunnlag);

				  UPDATE ADM_HENDELSE SET HID_DATERING = phid WHERE (OBJID = pobjid);			  
				  
				END SUPEROBJEKT_DATERING_UPDATE;';
		End if;	
		
		jobbnummer:= 12;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE OBJ_BESKRIVELSE_H_UPDATE (pobjid NUMBER, phid NUMBER) IS				  
				BEGIN
				  MERGE INTO OBJ_BESKRIVELSE_H  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN NOT MATCHED THEN  INSERT (OBJID,HID) VALUES (pobjid, phid);
				  UPDATE ADM_HENDELSE SET HID_BESKRIVELSE = phid WHERE (OBJID = pobjid);
				END OBJ_BESKRIVELSE_H_UPDATE;';
		End if;	
		
		jobbnummer:= 13;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE OPPL_H_UPDATE (pobjid NUMBER, phid NUMBER) IS				  
				BEGIN
				  MERGE INTO OPPL_H  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN NOT MATCHED THEN  INSERT (OBJID,HID) VALUES (pobjid, phid);
				  UPDATE ADM_HENDELSE SET HID_OPPL = phid WHERE (OBJID = pobjid);
				END OPPL_H_UPDATE;';
		End if;	
		
				
		jobbnummer:= 14;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE ADM_H_FORMAAL_H_UPDATE (pobjid NUMBER, phid NUMBER, pFormaalTypeid NUMBER, pFormaal VARCHAR2) IS				  
				BEGIN
				  MERGE INTO ADM_H_FORMAAL_H  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN MATCHED THEN  UPDATE Set FORMAAL=pFormaal ,  FORMAALTYPEID=pFormaalTypeid
				  WHEN NOT MATCHED THEN  INSERT (OBJID,HID, FORMAAL, FORMAALTYPEID) VALUES (pobjid, phid, pFormaal, pFormaalTypeid);				  
				  UPDATE ADM_HENDELSE SET HID_FORMAAL = phid WHERE (OBJID = pobjid);
				END ADM_H_FORMAAL_H_UPDATE;';
		End if;	
		
		
		jobbnummer:= 15;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE SUPEROBJEKT_IDNR_H_UPDATE (pobjid NUMBER, phid NUMBER, pIdNrTall NUMBER, pIdNrValue VARCHAR2) IS				  
				BEGIN
				  MERGE INTO SUPEROBJEKT_IDNR_H  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN MATCHED THEN  UPDATE Set IDENTIFIKASJONSNR=pIdNrValue ,  IDENTIFIKASJONSTALL=pIdNrTall
				  WHEN NOT MATCHED THEN  INSERT (OBJID, HID, IDENTIFIKASJONSNR, IDENTIFIKASJONSTALL) VALUES (pobjid, phid, pIdNrValue, pIdNrTall);				  
				  UPDATE ADM_HENDELSE SET HID_IDNR = phid WHERE (OBJID = pobjid);
				END SUPEROBJEKT_IDNR_H_UPDATE;';
		END if;		
		
		jobbnummer:= 16;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE ADM_H_KOSTNAD_UPDATE (pobjid NUMBER, phid NUMBER, pKOSTNAD NUMBER, pVALUTA_ID NUMBER, pKOMMENTAR VARCHAR2) IS				  
				BEGIN
				  MERGE INTO ADM_H_KOSTNAD  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN MATCHED THEN  UPDATE Set KOSTNAD=pKOSTNAD ,  KOMMENTAR=pKOMMENTAR, VALUTA_ID=pVALUTA_ID
				  WHEN NOT MATCHED THEN  INSERT (OBJID, HID, KOSTNAD, KOMMENTAR, VALUTA_ID) VALUES (pobjid, phid, pKOSTNAD, pKOMMENTAR, pVALUTA_ID);				  
				  UPDATE ADM_HENDELSE SET HID_KOSTNAD = phid WHERE (OBJID = pobjid);
				END ADM_H_KOSTNAD_UPDATE;';
		END if;	
		
		jobbnummer:= 17;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE TILSTAND_UPDATE (pobjid NUMBER, phid NUMBER, pTKODE VARCHAR2, pTGRAD NUMBER, pBESK VARCHAR2) IS				  
				BEGIN
				  MERGE INTO TILSTAND  USING DUAL ON (OBJID=pobjid and HID=phid ) 				  
				  WHEN MATCHED THEN  UPDATE Set   BESKRIVELSE=pBESK, KGRADID=pTGRAD
				  WHEN NOT MATCHED THEN  INSERT (OBJID, HID, TILSTANDSKODE, BESKRIVELSE, KGRADID) VALUES (pobjid, phid, pTKODE, pBESK, pTGRAD);				  
				  UPDATE ADM_HENDELSE SET HID_TILSTAND = phid WHERE (OBJID = pobjid);
				END TILSTAND_UPDATE;';
		END if;	
		
		
		jobbnummer:= 18;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE OAH_INNLEVERT_UPDATE (pahobjid NUMBER, pobjid NUMBER, pOldhid NUMBER, pNewhid NUMBER, pNr NUMBER, pInnlevert DATE, pfradato DATE, ptildato DATE, pktlog varchar2, pkomm varchar2) IS                                                                                                                                                                                          
BEGIN
  IF pOldhid = pNewhid then                                                                     
    UPDATE OBJEKT_ADM_HENDELSE 
       SET INNLEVERT=pInnlevert
         , KOMMENTAR=pkomm 
     WHERE OBJID_SISTE = pobjid 
       AND HID = pNewHid 
       AND NR = pnr 
       AND ADMH_OBJID = pahobjid 
       AND HID_SLETTET is NULL;
  ELSE
    UPDATE OBJEKT_ADM_HENDELSE 
       SET OBJID_SISTE = NULL 
     WHERE OBJID_SISTE = pobjid 
       AND HID = pOldhid 
       AND NR = pnr 
       AND ADMH_OBJID = pahobjid 
       AND HID_SLETTET is NULL;
    INSERT INTO OBJEKT_ADM_HENDELSE 
      ( ADMH_OBJID
      , OBJID
      , OBJID_SISTE
      , HID
      , NR
      , INNLEVERT
      , KATALOGNR
      , KOMMENTAR
      , FRADATO
      , TILDATO ) 
      VALUES 
      ( pahobjid
      , pobjid
      , pobjid
      , pNewHID
      , pNr
      , pInnlevert
      , pktlog
      , pkomm
      , pfradato
      , ptildato );                                                                                                                                                                                                                                                                 
  END IF;
END OAH_INNLEVERT_UPDATE;
';
		End if;	
		
		jobbnummer:= 19;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE OBJEKT_ADMH_UPDATE (pahobjid NUMBER, pobjid NUMBER, pOldhid NUMBER, pNewhid NUMBER, pNr NUMBER, pInnlevert DATE, pfradato DATE, ptildato DATE, pktlog varchar2, pkomm varchar2) IS				  								  
				BEGIN
				  IF pOldhid = pNewhid then					
					UPDATE OBJEKT_ADM_HENDELSE Set INNLEVERT=pInnlevert, fradato=pfradato, tildato=ptildato, KATALOGNR=pktlog , KOMMENTAR=pkomm
					 WHERE OBJID_SISTE=pobjid and HID=pNewHid and NR=pnr and ADMH_OBJID=pahobjid and HID_SLETTET is NULL;
				  ELSE
				    UPDATE OBJEKT_ADM_HENDELSE Set OBJID_SISTE=NULL WHERE OBJID_SISTE=pobjid and HID=pOldhid and NR=pnr and ADMH_OBJID=pahobjid and HID_SLETTET is NULL;
				    INSERT INTO OBJEKT_ADM_HENDELSE (ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, INNLEVERT, KATALOGNR, KOMMENTAR, FRADATO, TILDATO) 
					   VALUES (pahobjid, pobjid, pobjid, pNewHID, pNr, pInnlevert, pktlog, pkomm, pfradato, ptildato);				  				  									
				  END IF;
				  
				END OBJEKT_ADMH_UPDATE;';
		End if;	
		
		
		jobbnummer:= 20;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'create or replace PROCEDURE PUBLISERINGSNIVAAOB_H_update (pobjid IN NUMBER,
				pnewhid IN NUMBER,  ppid IN NUMBER, pisObjekt IN NUMBER) IS
				BEGIN
				 begin
					 INSERT INTO PUBLISERINGSNIVAAOBJEKT_H  (OBJID, HID, PUBLISERINGSNIVAALISTEID) VALUES (pobjid, pnewhid, ppid);
					 if pisObjekt = 1 then
					  UPDATE OBJEKT SET HID_PUBLISERINGSNIVAA = pnewhid WHERE (OBJID = pobjid);
					 else
					  UPDATE ADM_HENDELSE SET HID_PUBLISERINGSNIVAA = pnewhid WHERE (OBJID = pobjid);
					 end if;
				  Exception
				   when others then
					UPDATE PRIMUS.PUBLISERINGSNIVAAOBJEKT_H  SET PUBLISERINGSNIVAALISTEID = ppid WHERE (OBJID = pobjid AND hid = pnewhid);
				  end;
				End Publiseringsnivaaob_H_Update;';
		End if;	
		

	
		
		
		jobbnummer:= 21;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE '
		  create or replace FUNCTION  XML_UTLAAN(ADMH_OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
		   BEGIN
			  SELECT  XMLELEMENT(OBJEKT
			   ,XMLFOREST(''UL'' AS OBJTYPEID)
			   ,XMLFOREST(AH.SAKSNUMMER AS SAKSNUMMER)	
			   ,XMLFOREST(SIDN.IDENTIFIKASJONSNR as UTLAANSNR)
			   ,XMLFOREST(AHFM.FORMAAL AS FORMAAL)
			  ,XMLELEMENT(DATO, 
			    XMLFOREST(SD.FRADATO as FRADATO), 
				XMLFOREST(SD.TILDATO AS TILDATO))
			  ,XMLELEMENT( LAANTAKER, 
			    XMLFOREST(JP4.JPNAVN AS LAANTAKERP), 
				XMLFOREST(JP3.JPNAVN AS LAANTAKERI))
			  ,XMLELEMENT( LAANGIVER, 
			    XMLFOREST(JP2.JPNAVN AS LAANGIVERP), 
				XMLFOREST(JP1.JPNAVN AS LAANGIVERI))
			  ,
			  ( 
			     SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS ANTALL)
				 FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU  
				 INNER JOIN PRIMUS.OBJEKT OB  ON OBU.OBJID_SISTE = OB.OBJID AND OB.HID_SLETTET IS NULL
			     where OBU.ADMH_OBJID=AH.OBJID AND OBU.HID_SLETTET is NULL 
				)
			 ,
			  (
				SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS UNRETURNED)FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU 
				INNER JOIN PRIMUS.OBJEKT OB  ON OBU.OBJID_SISTE = OB.OBJID AND OB.HID_SLETTET IS NULL                
				Where OBU.ADMH_OBJID=AH.OBJID  AND OBU.HID_SLETTET is NULL AND OBU.INNLEVERT is NULL  
				AND ((SD.TILDATO <Sysdate) OR (SD.TILDATO is NULL))
			 )		  

				)
				INTO OBJECTXML FROM PRIMUS.ADM_HENDELSE AH 
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_IDNR_H SIDN     On AH.objid = SIDN.OBJID and AH.HID_IDNR = SIDN.HID
			LEFT OUTER JOIN PRIMUS.ADM_H_FORMAAL_H AHFM  On AH.objid = AHFM.OBJID and AH.HID_FORMAAL = AHFM.HID    
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_DATERING_H SD   On AH.objid = SD.OBJID and AH.HID_DATERING = SD.HID
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPLNR  On AH.objid = AHPLNR.OBJID_SISTE and AHPLNR.JPNR is not null and AHPLNR.ROLLEID=244
			LEFT OUTER JOIN PRIMUS.JURPERSON JP4  On AHPLNR.JPNR=JP4.JPNR and AHPLNR.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPLNI  On AH.objid = AHPLNI.OBJID_SISTE and AHPLNI.JPNR is not null and AHPLNI.ROLLEID=243
			LEFT OUTER JOIN PRIMUS.JURPERSON JP3  On AHPLNI.JPNR=JP3.JPNR and AHPLNI.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPUTNR  On AH.objid = AHPUTNR.OBJID_SISTE and AHPUTNR.JPNR is not null and AHPUTNR.ROLLEID=242
			LEFT OUTER JOIN PRIMUS.JURPERSON JP2  On AHPUTNR.JPNR=JP2.JPNR and AHPUTNR.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPUTNI  On AH.objid = AHPUTNI.OBJID_SISTE and AHPUTNI.JPNR is not null and AHPUTNI.ROLLEID=241
			LEFT OUTER JOIN PRIMUS.JURPERSON JP1  On AHPUTNI.JPNR=JP1.JPNR and AHPUTNI.OBJID_SISTE is not NULL			
			WHERE AH.OBJID=ADMH_OBJID_IN AND AH.HID_SLETTET is NULL;
				RETURN OBJECTXML;
          END XML_UTLAAN;';
		End if;
		
		jobbnummer:= 22;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE '
		create or replace FUNCTION  XML_UTSTILLING(ADMH_OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
		   BEGIN
			  SELECT  XMLELEMENT(OBJEKT
			   ,XMLFOREST(''UL'' AS OBJTYPEID)
			   ,XMLFOREST(AH.SAKSNUMMER AS SAKSNUMMER)	
			   ,XMLFOREST(SIDN.IDENTIFIKASJONSNR as MUSEUMSNUMMER)
			   ,XMLFOREST(AHFM.FORMAAL AS FORMAAL)
			   ,XMLELEMENT(DATO, 
			    XMLFOREST(SD.FRADATO as FRADATO), 
				XMLFOREST(SD.TILDATO AS TILDATO))
			  ,
			  	( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(ADMH_OBJID_IN) as nxml from Dual)  TT  )
			  ,
			   XMLELEMENT(JP_NAVN, XMLFOREST(JP4.JPNAVN AS NAVN))
	          ,			 			  
			  ( 
			     SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS ANTALLOBJEKTER)
				 FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU  
				 INNER JOIN PRIMUS.OBJEKT OB  ON OBU.OBJID_SISTE = OB.OBJID AND OB.HID_SLETTET IS NULL
			     where OBU.ADMH_OBJID=AH.OBJID AND OBU.HID_SLETTET is NULL 
			 )
			)
				INTO OBJECTXML FROM PRIMUS.ADM_HENDELSE AH 
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_IDNR_H SIDN     On AH.objid = SIDN.OBJID and AH.HID_IDNR = SIDN.HID
			LEFT OUTER JOIN PRIMUS.ADM_H_FORMAAL_H AHFM  On AH.objid = AHFM.OBJID and AH.HID_FORMAAL = AHFM.HID    
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_DATERING_H SD   On AH.objid = SD.OBJID and AH.HID_DATERING = SD.HID
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPLNR  On AH.objid = AHPLNR.OBJID_SISTE and AHPLNR.JPNR is not null and AHPLNR.rolleid in (100, 101, 106)
			LEFT OUTER JOIN PRIMUS.JURPERSON JP4  On AHPLNR.JPNR=JP4.JPNR and AHPLNR.OBJID_SISTE is not NULL			
			WHERE AH.OBJID=ADMH_OBJID_IN AND AH.HID_SLETTET is NULL;
				RETURN OBJECTXML;
          END XML_UTSTILLING;';
		End if;
		  
		
		
		jobbnummer:= 23;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'CREATE OR REPLACE FORCE VIEW PRIMUS.TMP_UTLAAN_XML (OBJID, XML_DATA) AS 
                           SELECT ADMH_OBJID, XML_UTLAAN(ADMH_OBJID) XML_DATA FROM UTLAAN';
        End if;		


        jobbnummer:= 24;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE '
       		 create or replace PROCEDURE adm_hendelse_person_update (pobjid NUMBER,
					  pnr  NUMBER,
					  poldhid NUMBER,
					  pnewhid  NUMBER,
					  prolleid number,
					  PROLLEKODE varchar2,
					  PSTATUSID number,
					  pfradato date,
					  ptildato date,
					  pjpnr NUMBER,
					  iID NUMBER
					  ) IS
				BEGIN
					IF poldhid = pnewhid THEN
						UPDATE primus.adm_hendelse_person SET rolleid = prolleid, rollekode = prollekode,
							   ROLLEJPSTATUSID = PSTATUSID, fradato = pfradato, tildato = ptildato, jpnr = pjpnr
							   WHERE ID=iID;
					ELSE
						UPDATE primus.adm_hendelse_person	SET objid_siste = NULL
						WHERE ID=iID;
						INSERT INTO primus.adm_hendelse_person (OBJID, HID, NR, OBJID_SISTE, ROLLEID, ROLLEKODE, ROLLEJPSTATUSID, FRADATO, TILDATO, JPNR)
						VALUES	(pobjid, pnewhid, pnr, pobjid, prolleid, prollekode, PSTATUSID, pfradato, ptildato, pjpnr);
				 End if;
				end ADM_HENDELSE_PERSON_UPDATE;';
		End if;		
	



        jobbnummer:= 25;
		if  jobnummer_in <= jobbnummer then		  
	     EXECUTE IMMEDIATE '
       		 create or replace PROCEDURE ADM_HENDELSE_UNDERTYPE_UPDATE (pobjid NUMBER,
					  pnr NUMBER,
					  poldhid NUMBER,
					  pnewhid NUMBER,					  
					  ptypeid NUMBER
					  ) IS
				BEGIN
					IF poldhid = pnewhid THEN
						UPDATE primus.ADM_HENDELSE_UNDERTYPE SET TYPEID = ptypeid  WHERE OBJID=pObjid and HID=poldhid and NR=pnr;
					ELSE
						UPDATE primus.ADM_HENDELSE_UNDERTYPE SET objid_siste = NULL
						WHERE OBJID=pObjid and HID=poldhid and NR=pnr;
						INSERT INTO primus.ADM_HENDELSE_UNDERTYPE (OBJID, HID, NR, OBJID_SISTE, TYPEID)	VALUES(pobjid, pnewhid, pnr, pobjid, ptypeid);
				 End if;
				end ADM_HENDELSE_UNDERTYPE_UPDATE;';
		End if;	
	    
		jobbnummer:= 26;
	    executeAlterTable(jobbnummer,jobnummer_in, 'ALTER TABLE  UTSTILLING MODIFY FRADATO DATE null');
		
		jobbnummer:= 27;
	    executeAlterTable(jobbnummer,jobnummer_in, 'ALTER TABLE  UTLAAN MODIFY UTLTYPE varchar2(5 Char) null');
	
		jobbnummer:= 28;
	    executeAlterTable(jobbnummer,jobnummer_in, 'ALTER TABLE  UTLAAN MODIFY FRADATO DATE null');
		
		jobbnummer:= 29;
    declare
      consName varchar2(30 char);
    begin
      select uc.constraint_name
        into consName
        from user_constraints uc
       inner join user_cons_columns ucc 
               on ucc.constraint_name = uc.r_constraint_name
              and ucc.table_name = 'MUSHENDELSE'
              and ucc.column_name = 'HID'
              and ucc.position = 1
       where uc.table_name = 'UTSTILLING';
      
      executeAlterTable(jobbnummer,jobnummer_in, 'alter table PRIMUS.UTSTILLING drop constraint ' || consName );
		
    exception
      when no_data_found then
        dbms_output.put_line(' 56 ---- constraint fra utstilling til mushendelse eksisterer ikke ' );
		end;
		jobbnummer:= jobbnummer+1;
	    executeAlterTable(jobbnummer,jobnummer_in, 'ALTER TABLE  ADM_H_KOSTNAD MODIFY KOSTNAD NUMBER(10,2)');
	
       jobbnummer:= jobbnummer+1;
	    executeScript(jobbnummer,jobnummer_in,'UPDATE SUPEROBJEKT S SET S.SUPEROBJEKTTYPEID = (SELECT SL.SUPEROBJEKTTYPEID FROM SUPEROBJEKTTYPE_L  SL WHERE UPPER(SL.BESKRIVELSE) = UPPER(S.SO_TYPE))');
		
	   jobbnummer:= jobbnummer+1;
	    executeScript(jobbnummer,jobnummer_in,'grant insert, update on primus.OBJEKT_MEDIASERVER to pri_reg'); 
	jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus56 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(56, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
