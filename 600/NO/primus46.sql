CREATE OR REPLACE FUNCTION primus46(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*

    */

    BEGIN

      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table ADM_HENDELSE drop column PLASSID';
      END IF;
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table SKADETYPE_H add( constraint FK_SKADETYPE_H_OBJID foreign key(OBJID) references SKADE(OBJID))';
      END IF;

      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'update ADM_H_FORMAALTYPE_L set filterfeltid = 3 where formaaltypeid in(9, 10)';
      END IF;
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'UPDATE tilstand_l set filterfeltid = NULL ';
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG0'' )
                         WHEN MATCHED THEN	 Update Set filterfeltid=3
               WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
               VALUES (''TG0'', ''Ingen symptomer'', ''Ingen symptomer'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG1'' )
                         WHEN MATCHED THEN	 Update Set filterfeltid=3
               WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
               VALUES (''TG1'', ''Svake symptomer'', ''Svake symptomer'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG2'' )
                         WHEN MATCHED THEN	 Update Set filterfeltid=3
               WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
               VALUES (''TG2'', ''Vesentlige symptomer'', ''Vesentlige symptomer'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG3'' )
                         WHEN MATCHED THEN	 Update Set filterfeltid=3
               WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
               VALUES (''TG3'', ''Kraftige eller alvorlige symptomer'', ''Kraftige eller alvorlige symptomer'', 3)';
        ELSE
          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG0'' )
	                     WHEN MATCHED THEN	 Update Set filterfeltid=3
						 WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
						 VALUES (''TG0'', ''Inga symptom'', ''Inga symptom'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG1'' )
	                     WHEN MATCHED THEN	 Update Set filterfeltid=3
						 WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
						 VALUES (''TG1'', ''Svaga symptom'', ''Svaga symptom'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG2'' )
	                     WHEN MATCHED THEN	 Update Set filterfeltid=3
						 WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
						 VALUES (''TG2'', ''Väsentliga symptom'', ''Väsentliga symptom'', 3)';

          EXECUTE IMMEDIATE 'MERGE INTO tilstand_l  USING DUAL ON (KODE=''TG3'' )
	                     WHEN MATCHED THEN	 Update Set filterfeltid=3
						 WHEN NOT MATCHED THEN  INSERT (kode, kort_beskrivelse, beskr_foto, filterfeltid)
						 VALUES (''TG3'', ''Kraftiga eller allvarliga symptom'', ''Kraftiga eller allvarliga symptom'', 3)';
        END IF;
      END IF;
      /* Annotering - endre kontekst for annotering fra bilde til objekt */
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'alter table annotering add
          (BILDEID NUMBER(8,0),
           CONSTRAINT FK_ANNOTERING_BILDEID FOREIGN KEY(BILDEID) REFERENCES BILDE(BILDEID))';
      END IF;
      /* Rekkefølge på rettighet */
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table rettighet add rekkefolge number(3,0)';
      END IF;
      jobbnummer := jobbnummer + 1;
      /* Rekkefølge på behandling_metode */
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table behandling_metode add rekkefolge number(3,0)';
      END IF;
      jobbnummer := jobbnummer + 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'alter table video add mediaid varchar2(50 char)';
      END IF;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus46 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(46, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/
