create or replace function primus33(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Analyse tabeller
	*/

	begin
	jobbnummer:=1;    
		if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table analyse
          (	objid number(8,0)
          , constraint pk_analyse primary key (objid) enable
          , constraint fk_analyse_admh_objid foreign key (objid)
              references adm_hendelse (objid) enable
        ) tablespace primusdt';
      jobbnummer:=3;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table analysetype_l
          (	typeid number(6,0),
            navn varchar2(250 char),
            parent_id number(6,0),
            beskrivelse varchar2(2000 char),
            m_path varchar2(100 char),
            hid_opprettet number(8,0) not null enable,
            hid_slettet number(8,0),
            uuid varchar2(40 byte) not null enable,
            merke char(1 char) default ''y'' not null enable,
            autoritet varchar2(10 char),
            autoritet_status varchar2(1 char),
            autoritet_dataset varchar2(100 char),
            autoritet_kilde varchar2(100 char),
            filterfelt varchar2(5 char),
            constraint pk_analysetype_l primary key (typeid) enable,
            constraint uk_analysetype_l_uuid unique (uuid) enable,
            constraint fk_analysetype_l_hid foreign key (hid_opprettet)
              references hendelse (hid) enable,
            constraint fk_analysetype_l_hid_s foreign key (hid_slettet)
              references hendelse (hid) enable
        ) tablespace primusdt';
      jobbnummer:=4;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table analyse_metode
          (	objid number(8,0),
             hid number(8,0),
             nr number(3,0),
             objid_siste number(8,0),
             analysemetodeid number(8,0),
             analysetypeid number(5,0),
             hid_slettet number(8,0),
             hid_beskrivelse number(8,0),
            constraint pk_analyse_metode primary key (objid, hid, nr)
            , constraint uk_a_metode_analysemetodeid unique (analysemetodeid) enable
            , constraint fk_a_metode_analyse foreign key (objid)
                references analyse (objid) enable
            , constraint fk_a_metode_hendelse foreign key (hid)
                references hendelse (hid) enable
            , constraint fk_a_metode_analysetypeid foreign key (analysetypeid)
                references analysetype_l (typeid) enable
            , constraint fk_am_hid_slettet foreign key (hid_slettet)
                references hendelse (hid) enable
        ) tablespace primusdt';
      jobbnummer:=5;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table analyse_metode_beskrivelse
          (	analysemetodeid number(8,0) not null enable
            , hid number(8,0) not null enable
            , beskrivelse varchar2(1000 char)
            , constraint pk_analyse_metode_besk primary key (analysemetodeid, hid) enable
            , constraint fk_analyse_metode_besk_amid foreign key (analysemetodeid)
                references analyse_metode (analysemetodeid) enable
            , constraint fk_analyse_metode_besk_hid foreign key (hid)
                references hendelse (hid) enable
        ) tablespace primusdt';
      jobbnummer:=6;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'alter table analyse_metode add
          ( constraint fk_am_hid_beskrivelse foreign key (analysemetodeid, hid_beskrivelse)
              references analyse_metode_beskrivelse (analysemetodeid, hid) enable)';
      jobbnummer:=7;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create sequence analysemetodeidseq  INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE ';
    End if;
		jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus33 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(33, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/