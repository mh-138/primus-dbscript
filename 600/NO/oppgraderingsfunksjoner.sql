create or replace procedure executeScript( jobbnummer number, jobnummer_in number, sScript varchar2) is
begin

  if jobnummer_in <= jobbnummer then	
	  execute immediate (sScript);
	End if;    

end;
/

create or replace procedure executeCreateTable( jobbnummer number, jobnummer_in number, sScript varchar2) 
AUTHID CURRENT_USER
is
begin

    if jobnummer_in <= jobbnummer then
     begin	
		execute immediate (sScript);
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;			
	 End if;    

end;
/




create or replace procedure executeAlterTable( jobbnummer number, jobnummer_in number, sScript varchar2) is
begin

  if jobnummer_in <= jobbnummer then
    begin	
      execute immediate (sScript);
    Exception
      WHEN OTHERS THEN
        IF (SQLCODE = -1430) THEN --suppresses ORA-01430,  column being added already exists in table
          NULL;
        elsif (sqlcode = -2275) then -- lik constraint ligger allerede i tabellen.
          null;
        ELSE 
          RAISE;
        END IF;
    End;			
	End if;    
end;
/

create or replace procedure PrimusOppdJobbNrIVersjon(in_versjonsnr in number, in_jobbnr in number ) is
begin
  update PRIMUS.PRIMUSVERSJON
    set jobbnr = in_jobbnr
  where versjonsnr = in_versjonsnr;

  commit;
end;
/

Create Or Replace 
function SjekkOmOppgraderingSkalKjores(sversjonnr in number, jobbnummer out Number) return number is
  sisteVersjon PRIMUSVERSJON.versjonsnr%type;
  sisteStatus PRIMUSVERSJON.status%type;
  sistejobbnr PRIMUSVERSJON.jobbnr%type;  
begin
  begin
    jobbnummer := 0;
    Select Versjonsnr, Status, Jobbnr Into Sisteversjon, Sistestatus, Sistejobbnr 
	From Primusversjon Where Versjonsnr = (Select Max(Versjonsnr) From Primusversjon);    
  
  if ((sisteVersjon=sversjonnr) and (sisteStatus = 'FEIL')) then 
      Jobbnummer := Sistejobbnr;
	   Return 1;	
  else if ((sisteVersjon=sversjonnr) and (sisteStatus = 'Start')) then      
	  return 1;
  else if (((sisteVersjon + 1)=sversjonnr) and (sisteStatus = 'OK')) then      
	  return 1;	  
  else	  
      return 0;
  end if;
  end if;
  end if;  
  exception
    When No_Data_Found Then
      if sversjonnr = 1 then      
	    return 1;
      end if;
    when others then
      return 0;
  End;
End;
/



create or replace function svenskEllerNorskDatabase 
	return varchar2 
	AUTHID CURRENT_USER
	IS
	primusuuid varchar2(100 char);
begin
	begin
		
    execute immediate 'select pe.verdi 
                         from ENVIRONMENT pe
                        where upper( pe.variabel ) = ''UID'' ' into primusuuid;
	
		if substr(lower(primusuuid), 0, 2) = 'se' then
			return 'SWE';
		else
			return 'NOR';
		end if;
	exception 	
		when no_data_found then
			return 'NOR';
	end;
end;
/

