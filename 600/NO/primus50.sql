create or replace function primus50(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/* Data overfÃ¸ring fra MUSHENDELSE til ADM_HENDELSE.	*/
  /*   ------------ Fast plassering ----------------    */
	begin

		jobbnummer := 1; 
    --  rett opp alle plasskoder der samme plassid er brukt pÃ¥ mange objekter.
    if jobnummer_in <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      
      declare
      
        cursor curAlleObjekter is
              select objid, plassid 
                from objekt 
               where plassid in ( select plassid from (
                      select plassid, count(plassid) as teller from objekt group by plassid ) pk
                       where pk.teller > 1 ) order by plassid;
        nobjid number;
        nplassobjid number;
        nplassid number;
        nlastPlassid number;
        nnewPlassid number;
      BEGIN
        nlastPlassid := -1;
        open curAlleObjekter;
        loop
          fetch curAlleObjekter into nobjid, nplassid;
          exit when curAlleObjekter%NOTFOUND;
          
          if nplassid <> nlastPlassid then
            select objid into nplassobjid from plasskode where plassid = nplassid;
            IF nobjid <> nplassobjid then
              update plasskode set objid = nobjid where plassid = nplassid;
            end if;
          else
          --
          insert into plasskode ( PLASSID 
                                , SPID
                                , BYGNID
                                , FAG
                                , HYLLE2
                                , REOL2
                                , ROM2
                                , AKSESJONID
                                , HID
                                , BESKRIVELSE
                                , OBJID
                                , XTRA1
                                , XTRA2
                                , XTRA3
                                , XTRA4
                                , KOLLI
                                , VEGG )
                                 SELECT plassidseq.nextval 
                                     , SPID
                                     , BYGNID
                                     , FAG
                                     , HYLLE2
                                     , REOL2
                                     , ROM2
                                     , AKSESJONID
                                     , HID
                                     , BESKRIVELSE
                                     , nobjid
                                     , xtra1
                                     , xtra2
                                     , xtra3
                                     , xtra4
                                     , kolli
                                     , vegg
                                  from plasskode 
                                 where plassid = nplassid;
            select plassidseq.currval into nnewplassid from dual;
            update objekt set plassid = nnewplassid where objid = nobjid;

          END IF;    
          nlastPlassid := nplassid;
        end loop;
        close curAlleObjekter;
        commit;
      end;
    end if;
    

    jobbnummer := 2;

    
		---------Fast plass with a row value in MUSHENDELSE -----------
		if jobnummer_in <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 50, jobbnummer );

      DECLARE
        iId Integer;
        lastBeskId Integer;
        lastPlassid integer;
      BEGIN
        lastBeskId := -1;

			  For TH in (select DISTINCT PK.objid
                                 , PK.HID
                                 , PK.plassid
                                 , MH.DATO
                                 , MH.TIL_DATO
                                 , PK.BESKRIVELSE
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.SAKSNUMMER
                                 , MH.beskrivende_objekt
                                 , length(MH.tekst) ltekst
                              FROM PLASSKODE PK
                                 , MUSHENDELSE MH
                             WHERE PK.hid = MH.HID
                               AND MH.hovedtypekode = 10001
                               AND MH.UNDERTYPE like 'Fast'
                               AND MH.MERKE is NULL
                               AND PK.OBJID is not NULL
                             ORDER BY MH.BESKRIVENDE_OBJEKT )
			  Loop

          iId := 0;


          if ( TH.beskrivende_objekt is null ) OR ( lastPlassid <> th.plassid AND th.beskrivende_objekt = lastBeskId ) then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
          else
            iId := TH.beskrivende_objekt;
          end if;

          lastBeskid := th.beskrivende_objekt;
          lastPlassid := th.plassid;

--        if lastBeskId <> iId then
          execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE,  SAKSNUMMER)
                     values (:a, :b, :c, :d)' using iId, TH.HID, TH.NEWHOVEDTYPE, TH.SAKSNUMMER;
          execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID)
                     values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :c, :d); END;' using iId, TH.hid, TH.DATO, TH.TIL_DATO;

          if TH.ltekst > 0 then
            execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
          End if;

          execute immediate 'Insert Into PLASS(OBJID, PLASSID, BESKRIVELSE) values (:a, :b, :c)' using iId, TH.plassid, TH.BESKRIVELSE;

--        end if;

				if TH.OBJID is not NULL then
          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO)
                     values (:a, :b, :c, :d, :e, :f)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.DATO;
        End if;


        execute immediate 'update MUSHENDELSE set MERKE=''X'' Where Hid = :b ' using TH.hid;
				execute immediate 'update PLASSKODE set ADMH_OBJID = :a, MERKE=''X'' Where PLASSID = :b ' using iId, TH.PLASSID;

			  END LOOP;
			  commit;
      exception
        when others then
          dbms_output.put_line( ' 50 -- ingen data funnet pÃ¥ fast plass objid is not null --- ' );
		  END;

		End if;


	jobbnummer:= 3;	 --Fast plass with objid is not null
  if jobnummer_in <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 50, jobbnummer );

    DECLARE
      iId Integer;
      iHVT INTEGER;
      iUT INTEGER;
    BEGIN
      if svenskellernorskdatabase = 'NOR' then
        Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLASSERING';
      else
        Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLACERING';
      end if;

      Select TYPEID into iUT  from ADM_HENDELSE_UNDERTYPE_L Where UPPER(tekst) ='FAST' and Parent_id= iHVT;

      For TH in ( select DISTINCT objid
                                , HID
                                , plassid
                                , BESKRIVELSE 
                             FROM PLASSKODE
                            Where merke is null 
                              AND hid is not NULL  
                              AND OBJID is not null )
      Loop

        execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
        execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, iHVT;
        execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, iUT;
        execute immediate 'Insert Into PLASS(OBJID, PLASSID, BESKRIVELSE) values (:a, :b, :c)' using iId, TH.plassid, TH.BESKRIVELSE;
        execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR) values (:a, :b, :c, :d, :e)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1;
        execute immediate 'update PLASSKODE set ADMH_OBJID = :a, MERKE=''X'' Where PLASSID = :b ' using iId, TH.plassid;

      END LOOP;
      commit;
		exception
      when no_data_found then
        dbms_output.put_line( ' 50 -- ingen data funnet pÃ¥ fast plass objid is not null --- ' );
    END;
  End if;


    jobbnummer:= 4;	--Fast plass with a row value in OBJEKT table
		if jobnummer_in <= jobbnummer then
		  PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      DECLARE
			  iId Integer;
			  iHVT INTEGER;
			  iUT INTEGER;
			BEGIN
			  if svenskellernorskdatabase = 'NOR' then
          Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLASSERING';
        else
          Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLACERING';
        end if;

			  Select TYPEID into iUT  from ADM_HENDELSE_UNDERTYPE_L Where UPPER(tekst) ='FAST' and Parent_id= iHVT;

        For TH in (select DISTINCT OB.objid
                                 , PK.HID
                                 , PK.plassid
                                 , PK.BESKRIVELSE
			                        FROM PLASSKODE PK
                                 , OBJEKT OB
                             WHERE PK.merke is null
						                   AND PK.hid is not NULL
                               AND OB.plassid = PK.Plassid)
			  Loop
				  execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
				  execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, iHVT;
				  execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, iUT;
				  execute immediate 'Insert Into PLASS(OBJID, PLASSID, BESKRIVELSE) values (:a, :b, :c)' using iId, TH.plassid, TH.BESKRIVELSE;
				  execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR) values (:a, :b, :c, :d, :e)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1;
				  execute immediate 'update PLASSKODE set ADMH_OBJID = :a, MERKE=''X'' Where PLASSID = :b ' using iId, TH.plassid;

			  END LOOP;
			 commit;
			exception
        when no_data_found then
          dbms_output.put_line( ' 50 -- ingen data funnet fast plass with a row value in object table ' );

      END;

	    End if;

		jobbnummer:= 5;	--FAST plass with a row value in MUSHENDELSE but not in PLASSKODE
		if jobnummer_in <= jobbnummer then
		  PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      DECLARE
			  iId Integer;
			BEGIN
			  For TH in (select DISTINCT SH.objid
                                 , SH.HID
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.SAKSNUMMER
                                 , MH.DATO
                                 , MH.TIL_DATO
                                 , MH.beskrivende_objekt
                                 , length(MH.tekst) ltekst
						 FROM Superobjekt_hendelse SH, MUSHENDELSE MH
						 Where SH.hid = MH.HID
						 AND MH.hovedtypekode=10001
						 AND MH.UNDERTYPE like 'Fast'
						 AND MH.MERKE is NULL
						 AND SH.MERKE is null)
			  Loop

          iId := 0;
          if TH.beskrivende_objekt is null then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
          else
            iId := TH.beskrivende_objekt;
          end if;

          execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE, SAKSNUMMER)
								   values (:a, :b, :c, :d)' using iId, TH.HID, TH.NEWHOVEDTYPE,  TH.SAKSNUMMER;
          execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :c, :d); END;' using iId, TH.hid, TH.DATO, TH.TIL_DATO;

          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO)
								   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.DATO, TH.TIL_DATO;
          if TH.ltekst > 0 then
            execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
          End if;

          execute immediate 'update MUSHENDELSE set MERKE=''X'' Where Hid = :b ' using TH.hid;

			  END LOOP;
			 commit;
			exception
        when no_data_found then
          dbms_output.put_line( ' 50 -- ingen data funnet; Fast plass with a row value in mushendelse but not in klasskode. ' );

			END;
		End if;

    jobbnummer := 6;
    if jobnummer_in <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      declare
        nobjid number;
        nnewadmobjid number;
        
        fortsett boolean;
        dfradato date;
        dtildato date;
        vnewhovedtype varchar2( 50 char );
        vnewundertype varchar2( 50 char );
        
      begin
        for plakod in ( select plassid
                             , hid
                             , nvl( objid, -1 ) as objid
                          from plasskode
                         where merke is null
                         order by plassid )
        loop
          -- forsÃ¸ker Ã¥ finne ut hvilket objekt denne plasseringen tilhÃ¸rer 
          -- hvis objid i plasstabellen ikke har verdi.
          fortsett := true;
          
          if plakod.objid = -1 then
            begin
              select objid
                into nobjid
                from objekt
               where plassid = plakod.plassid;
              
              if svenskEllerNorskDatabase = 'SWE' then
                vnewundertype := 'Fast';
              else
                vnewundertype := 'Fast';
              end if;
            exception
              when no_data_found then
                nobjid := -1;
                fortsett := false;  -- nobjid er ikke funnet - da stoppes videre kjÃƒÂ¸ring.
                --dbms_output.put_line(' feil med plassid: ' || nplassid );
                --update plasskode set merke = null where plassid = nplassid;
            end;
          end if;
          
          if fortsett then 
            begin
        
              select hovedtype, undertype, dato, til_dato
                into vnewhovedtype, vnewundertype, dfradato, dtildato
                from mushendelse where hid = plakod.hid;
        
            exception
              when no_data_found then            
                dfradato := null;
                vnewundertype := 'Fast';
            end;

            if vnewundertype = 'Fast' then
          
              -- lag en ny adm_hendelse - for dette objektet
              getnew_objid( nnewadmobjid, plakod.hid );
                  
              insert into adm_hendelse
                ( objid, hid_opprettet, hovedtype, uuid)
              values
                ( nnewadmobjid, plakod.hid, 100, GETORACLEUUID);
                  
      
              --- hvis ikke bruk hid sin dato
              if dfradato is null then
                select dato into dfradato from hendelse where hid = plakod.hid;
              end if;
            
              insert into superobjekt_datering_h
                ( objid
                , hid
                , fradato
                , tildato )
              values
                ( nnewadmobjid
                , plakod.hid
                , dfradato
                , dtildato );
          
              update adm_hendelse set hid_datering = plakod.Hid where objid = nnewadmobjid;
          
              -- ny undertype - fast/midlertidig
              insert into adm_hendelse_undertype
                ( objid
                , objid_siste
                , nr
                , hid
                , typeid )
              values
                ( nnewadmobjid
                , nnewadmobjid
                , 1
                , plakod.hid
                , (select typeid from adm_hendelse_undertype_l where parent_id = 100 and lower( tekst )  = lower( vnewundertype ) ) );
          
              -- bruk gammel plasskode...
              insert into plass
                ( objid, plassid )
              values
                ( nnewadmobjid, plakod.plassid );
      
              --- koble sammen objid og adm_hendelse
              insert into objekt_adm_hendelse
                ( admh_objid
                , objid
                , objid_siste
                , hid
                , nr
                , fradato)
              values
                ( nnewadmobjid
                , nobjid
                , nobjid
                , plakod.hid
                , 1
                , dfradato );
            end if;

            update plasskode
               set merke = 'X'
                 , admh_objid = nnewadmobjid         
             where plassid = plakod.plassid;
          else
            update plasskode
               set merke = 'E'
             where plassid = plakod.plassid;
          
          end if;
          
        end loop;
       
      end;
    end if;

    jobbnummer := 7;	--  FAST plass get Fradato if Fradto is null
		if jobnummer_in <= jobbnummer then
			PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      DECLARE
        dt date;
			begin
        For TH in(  Select Distinct OAH.OBJID
                                  , OAH.hid
                                  , oah.Admh_objid
                               FROM OBJEKT_ADM_HENDELSE OAH
                              INNER JOIN adm_hendelse ah 
                                      ON ah.objid = oah.admh_objid
                                     AND ah.hovedtype = 100
                              INNER JOIN adm_hendelse_undertype ahut 
                                      ON ahut.objid = ah.objid
                                     AND ahut.objid_siste = ah.objid
                                     AND ahut.typeid = (select typeid from Adm_Hendelse_Undertype_L where parent_id = 100 and lower( tekst ) = 'fast' )
                              WHERE oah.fradato is null )
        Loop

          UPDATE OBJEKT_ADM_HENDELSE oah
             SET oah.FRADATO = (Select h.Dato FROM Hendelse h where h.hid = oah.hid)
           WHERE oah.objid = TH.objid 
             AND oah.Admh_objid = th.admh_objid;

        End loop;
        commit;

			exception
        when no_data_found then
          dbms_output.put_line( ' 50 -- ingen data funnet: fast plass get fradato if fradato is null' );
      End;
		End if;

	  jobbnummer:= 8;
		if jobnummer_in <= jobbnummer then
		  PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      DECLARE
			BEGIN
			  for TH in (Select O.OBJID
                        , P.objid ADMH_OBJID
			               FROM OBJEKT O
                        , PLASS P
						        WHERE O.plassid IS NOT NULL
                      AND P.plassid = O.plassid)
			  loop
			    Update OBJEKT_ADM_HENDELSE oah
             set oah.MERKE = 'X'
           WHERE oah.objid = TH.objid
             AND oah.Admh_objid = TH.ADMH_OBJID;
			  end loop;

        commit;
			exception
        when no_data_found then
          dbms_output.put_line( ' 50 -- ingen data funnet; sett merke pÃ¥ alle som har objid og admid satt' );
      end;
		End if;



		jobbnummer:= 9;	--  FAST plass get Tildato
		if jobnummer_in <= jobbnummer then
			PrimusOppdJobbNrIVersjon( 50, jobbnummer );
      DECLARE
				lastobjid Integer;
				lastadmO Integer;
				lastmerke String(5);
			BEGIN

				for TH in ( Select Distinct OAH.OBJID
                               FROM OBJEKT_ADM_HENDELSE OAH
                         INNER JOIN adm_hendelse ah 
                                 ON ah.objid = oah.admh_objid
                                AND ah.hovedtype = 100
                         INNER JOIN adm_hendelse_undertype ahut 
                                 ON ahut.objid = ah.objid
                                AND ahut.objid_siste = ah.objid
                                AND ahut.typeid = (select typeid from Adm_Hendelse_Undertype_L where parent_id = 100 and lower( tekst ) = 'fast' )
                         ORDER BY OAH.objid)
				loop
				  lastobjid := -1;
					lastadmO  := -1;
					lastmerke := Null;

					for TL in (Select DISTINCT OAH.ADMH_OBJID
                                  , OAH.OBJID
                                  , OAH.HID
                                  , PH.dato
                                  , OAH.MERKE
                               FROM OBJEKT_ADM_HENDELSE OAH
                              inner join adm_hendelse ah 
                                      on ah.objid = oah.admh_objid
                                     and ah.hovedtype = 100
                              inner join hendelse ph
                                      on oah.hid = ph.hid
                              inner join adm_hendelse_undertype ahut
                                      on ahut.objid = ah.objid
                                     and ahut.objid_siste = ah.objid
                                     and ahut.typeid = (select typeid from adm_hendelse_undertype_l where parent_id = 100 and lower(tekst) = 'fast' )
                 )
					  loop
              if (lastobjid = TL.objid) then
                if  (lastobjid = TL.objid) and (lastmerke is null)  then
                  UPDATE OBJEKT_ADM_HENDELSE oah 
                     SET oah.TILDATO = TL.dato 
                   WHERE oah.objid=lastobjid 
                     AND oah.Admh_objid=lastadmO;
                End if;
              End if;
              
              lastobjid := TL.objid;
              lastadmO := TL.Admh_objid;
              lastmerke := TL.MERKE;
					  end loop;

				end loop;
				commit;
			exception
        when no_data_found then
          dbms_output.put_line( ' 50 -- ingen data funnet, fast plass, get tildato ' );
      end;
		End if;


------------------------------------- End of Fast plassering ----------------


    jobbnummer:= jobbnummer+ 1+100;
    return 'OK';
	exception
		when others then
            ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus50 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(50, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/