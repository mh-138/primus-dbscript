create or replace function primus49(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Data overføring fra MUSHENDELSE til ADM_HENDELSE.
	*/

	begin
	
		jobbnummer:=1;	--------------  Midlertidig Plassering -----------			
		------Midlertidig plass with a row value in MUSHENDELSE ---------
		if jobnummer_in <= jobbnummer then	
      DECLARE		  
			  iId Integer; 
			  iHid_Innlevert Integer;	
        lastBeskId Integer;
			BEGIN  
        lastBeskId := -1;
			  For TH in (select DISTINCT MP.objid
                                 , MP.HID
                                 , MP.plassid
                                 , MP.FRADATO
                                 , MP.TILDATO
                                 , MP.FORMAAL
                                 , MP.ANSVARLIG
                                 , MP.BESKRIVELSE
                                 , MP.INNLEVERT
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.SAKSNUMMER
                                 , MH.beskrivende_objekt
                                 , length( MH.tekst ) ltekst
						                  FROM MIDLPLASS MP
                                 , MUSHENDELSE MH 
                             WHERE MP.hid = MH.HID 
                               AND MH.hovedtypekode = 10001 
                               AND MH.UNDERTYPE like 'Midlertidig'
                               AND MH.MERKE is NULL 
                             ORDER BY MH.BESKRIVENDE_OBJEKT)
			  Loop	
	            
          iId := 0;    
          if th.beskrivende_objekt is null then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
          else
            iId := th.beskrivende_objekt;
          end if;
          
          if lastBeskId <> iId then
            execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE, SAKSNUMMER) 
                     values (:a, :b, :c, :d)' using iId, TH.HID, TH.NEWHOVEDTYPE,  TH.SAKSNUMMER;
                  
            execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) 
                     values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;
          
                             
            execute immediate 'BEGIN SAVE_AH_DATERING(:a, :b); END;' using iId, TH.hid;
          
            if TH.FORMAAL is not NULL then
              execute immediate 'BEGIN SAVE_AH_FORMAAL(:a, :b, :c, :d); END;' using iId, TH.hid, 1, TH.FORMAAL;			
            End if;	

            if TH.ltekst > 0 then
              execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
            End if;

            execute immediate 'Insert Into PLASS
                                          ( OBJID
                                          , PLASSID
                                          , ANSVARLIG
                                          , BESKRIVELSE ) 
                                          values 
                                          ( :a
                                          , :b
                                          , :c
                                          , :D)' 
                                    using iId
                                        , TH.plassid
                                        , TH.ANSVARLIG
                                        , TH.BESKRIVELSE;
          end if;
          
          lastBeskId := iId;
          
          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE
                                  ( ADMH_OBJID
                                  , OBJID
                                  , OBJID_SISTE
                                  , HID
                                  , NR
                                  , FRADATO
                                  , TILDATO
                                  , INNLEVERT) 
								             values 
                                  ( :a
                                  , :b
                                  , :c
                                  , :d
                                  , :e
                                  , :f
                                  , :g
                                  , :h)' 
                              using iId
                                  , TH.OBJID
                                  , TH.OBJID
                                  , TH.Hid
                                  , 1
                                  , TH.FRADATO
                                  , TH.TILDATO
                                  , TH.INNLEVERT;
								   
				execute immediate 'update MUSHENDELSE set MERKE=''X'' Where Hid = :b ' using TH.hid;		
				execute immediate 'update MIDLPLASS set ADMH_OBJID = :a, MERKE=''X'' Where objid = :b And hid =:C ' using iId, TH.objid, TH.hid;
				execute immediate 'update PLASSKODE set ADMH_OBJID = :a, MERKE=''X'' Where PLASSID = :b ' using iId, TH.plassid;								
			  END LOOP;		 
			 commit; 
			END;			
		End if;	
		
		
		jobbnummer:= 2;	--Midlertidig plass with out row value in MUSHENDELSE
		if jobnummer_in <= jobbnummer then			   
      DECLARE		  
			  iId Integer;		  
			  iHVT INTEGER;
			  iUT INTEGER;	
        iHid_Innlevert Integer;			  
			BEGIN 
        if svenskellernorskdatabase = 'NOR' then
          Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLASSERING';
        else
          Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where UPPER(tekst) ='PLACERING';
        end if;
			  
        Select TYPEID into iUT  from ADM_HENDELSE_UNDERTYPE_L Where UPPER(tekst) ='MIDLERTIDIG' and Parent_id= iHVT;			 					  
			  
        For TH in (select DISTINCT MP.objid, MP.HID, MP.plassid, MP.FRADATO, MP.TILDATO, MP.INNLEVERT, 
			                             MP.FORMAAL, MP.ANSVARLIG, MP.BESKRIVELSE                                        			 
						 FROM MIDLPLASS MP 
						 Where merke is null 
						 AND hid is not NULL)
			  Loop	
          iId := 0;    
          execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;			 
          execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) 
                     values (:a, :b, :c)' using iId, TH.HID, iHVT;	
          execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) 
                     values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, iUT;

                     
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :c, :d); END;' using iId, TH.hid,  TH.FRADATO, TH.TILDATO;
          
          if TH.FORMAAL is not NULL then
            execute immediate 'BEGIN SAVE_AH_FORMAAL(:a, :b, :c, :d); END;' using iId, TH.hid, 1, TH.FORMAAL;									
          End if;
          
          execute immediate 'Insert Into PLASS(OBJID, PLASSID, ANSVARLIG, BESKRIVELSE) 
                     values (:a, :b, :c, :D)' using iId, TH.plassid, TH.ANSVARLIG, TH.BESKRIVELSE;			
          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO, INNLEVERT) 
                     values (:a, :b, :c, :d, :e, :f, :g, :h)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.FRADATO, TH.TILDATO, TH.INNLEVERT;	



          
          execute immediate 'update MIDLPLASS set ADMH_OBJID = :a, MERKE=''X'' Where objid = :b And hid =:C ' using iId, TH.objid, TH.hid;
          execute immediate 'update PLASSKODE set ADMH_OBJID = :a, MERKE=''X'' Where PLASSID = :b ' using iId, TH.plassid;						

			  END LOOP;		 
        commit; 
			exception
        when NO_DATA_FOUND then
          dbms_output.put_line(' Ingen midlertidige plasseringer uten rad i mushendelse tabellen' );
      END;			
		End if;	
		
		
		
		jobbnummer:= jobbnummer+1;	--Midlertidig plass with a row value in MUSHENDELSE
		if jobnummer_in <= jobbnummer then	
		   DECLARE		  
			  iId Integer;		  
			BEGIN  
			  For TH in (select DISTINCT MP.objid
                                 , MP.HID
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.SAKSNUMMER
                                 , MH.BESKRIVENDE_OBJEKT
                                 , MH.DATO
                                 , MH.TIL_DATO
                                 , length(MH.tekst) ltekst
                              FROM Superobjekt_hendelse MP
                                 , MUSHENDELSE MH 
                             WHERE MP.hid = MH.HID 
						                   AND MH.hovedtypekode=10001 
						                   AND MH.UNDERTYPE like 'Midlertidig' 
                  						 AND MH.MERKE is NULL 
						                   AND MP.MERKE is NULL)
			  Loop	
	            
          iId := 0;    
            if th.beskrivende_objekt is null then
              execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;
            else
              iId := th.beskrivende_objekt;
            end if;
        
          execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;			                 
          execute immediate 'Insert into ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE, SAKSNUMMER) 
								   values (:a, :b, :c, :d)' using iId, TH.HID, TH.NEWHOVEDTYPE, TH.SAKSNUMMER;
          execute immediate 'Insert Into ADM_HENDELSE_UNDERTYPE(OBJID, OBJID_SISTE, HID, NR, TYPEID) 
								   values (:a, :b, :c, :d, :e)' using iId, iId, TH.Hid, 1, TH.NEWUNDERTYPE;				   
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :c, :d); END;' using iId, TH.hid, TH.DATO, TH.TIL_DATO;
				
          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO) 
								   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.DATO, TH.TIL_DATO;
          if TH.ltekst > 0 then
            execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
          End if;								   
          execute immediate 'update MUSHENDELSE set MERKE=''X'' Where Hid = :b ' using TH.hid;					
			  END LOOP;		 
        commit; 
			END;			
		End if;	
	
	
    ----------------------------- End of Midlertidig plassering   ---------------------------------------------	
	
    jobbnummer:= jobbnummer+ 1+100;		
    return 'OK';
	exception
		when others then
		    ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus49 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(49, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/