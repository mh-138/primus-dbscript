create or replace function primus36(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
   
	*/

	begin

    jobbnummer:=1;  
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE '	CREATE OR REPLACE TRIGGER SUPEROBJEKTTYPE_SO_TYPE 
			   BEFORE INSERT ON SUPEROBJEKT   FOR EACH ROW
			DECLARE  
			   BEGIN		
				  if (:NEW.SO_TYPE is NULL) and (:NEW.SUPEROBJEKTTYPEID is not NULL) then
				   SELECT CASE WHEN :NEW.SUPEROBJEKTTYPEID = 1 THEN ''Objekt''
                               WHEN :NEW.SUPEROBJEKTTYPEID = 2 THEN ''Actor''
					           WHEN :NEW.SUPEROBJEKTTYPEID = 3 THEN ''ADM_HEND''
							   WHEN :NEW.SUPEROBJEKTTYPEID = 4 THEN ''DAMAGE''
							   WHEN :NEW.SUPEROBJEKTTYPEID = 5 THEN ''BILDE''
							   WHEN :NEW.SUPEROBJEKTTYPEID = 6 THEN ''KOLLI''    
							   END INTO :NEW.SO_TYPE FROM DUAL;
				  Elsif (:NEW.SO_TYPE is not NULL) and (:NEW.SUPEROBJEKTTYPEID is NULL) then
				   SELECT CASE WHEN :NEW.SO_TYPE =''Objekt''   THEN 1
                               WHEN :NEW.SO_TYPE =''Actor''    THEN 2
					           WHEN :NEW.SO_TYPE =''ADM_HEND'' THEN 3
							   WHEN :NEW.SO_TYPE =''DAMAGE''   THEN 4
							   WHEN :NEW.SO_TYPE =''BILDE''    THEN 5
							   WHEN :NEW.SO_TYPE =''KOLLI''    THEN 6
							   END INTO :NEW.SUPEROBJEKTTYPEID FROM DUAL;			    
				  END IF;
		END;';
  End if;		
		
  jobbnummer:=2;  
	if jobnummer_in <= jobbnummer then
    	EXECUTE IMMEDIATE 'ALTER TRIGGER SUPEROBJEKTTYPE_SO_TYPE ENABLE';
	End if;		
	jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus36 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(36, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	
	