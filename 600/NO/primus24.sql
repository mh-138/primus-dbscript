CREATE OR REPLACE FUNCTION primus24(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
    Forklaring!!!
       2013.09.27:
      Table and sequence necessary to make task tracking possible.
    */

    BEGIN

      jobbnummer := 1;
      executeScript(jobbnummer, jobnummer_in,
                    'ALTER TABLE TILSTAND ADD(BESKRIVELSE VARCHAR(2000 CHAR))');
      jobbnummer := 2;
      executeScript(jobbnummer, jobnummer_in,
                    'UPDATE adm_h_formaaltype_l set filterfeltid = NULL');

      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'ALTER TABLE tilstand_l ADD(filterfeltid number(3,0),
	  constraint fk_tilstand_l_filterfeltid FOREIGN KEY (filterfeltid) references OBJEKTTYPER (TYPEID))';
      END IF;
      jobbnummer := 8;
      IF jobnummer_in <= jobbnummer
      THEN -- Add UUID field to bilde table
        EXECUTE IMMEDIATE 'ALTER TABLE bilde ADD(uuid VARCHAR2(160 CHAR))';

      END IF;
      jobbnummer := 9;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (18, ''Observasjon'') ');
      ELSE
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (18, ''Observation'') ');
      END IF;
      jobbnummer := 10;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (19, ''Oppgave'') ');
      ELSE
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (19, ''Uppgift'') ');
      END IF;
      jobbnummer := 11;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (20, ''Kommentar'') ');
      ELSE
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (20, ''Kommentar'') ');
      END IF;
      jobbnummer := 12;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (21, ''Annotering'') ');
      ELSE
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (21, ''Notering'') ');
      END IF;
      jobbnummer := 13;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (22, ''Analyse'') ');
      ELSE
        executeScript(jobbnummer, jobnummer_in,
                      'INSERT INTO ADM_HENDELSETYPE_L (TYPEID, TEKST) VALUES (22, ''Analys'') ');
      END IF;

      jobbnummer := 14;
      executeScript(jobbnummer, jobnummer_in,
                    'alter table adm_hendelse_person modify (jpnr null)');
      jobbnummer := 19;
      executeScript(jobbnummer, jobnummer_in,
                    'update superobjekt set superobjekttypeid = 1 where so_type = ''Objekt'' and superobjekttypeid is null ');
      jobbnummer := 20;
      executeScript(jobbnummer, jobnummer_in,
                    'update superobjekt set superobjekttypeid = 2 where so_type = ''ACTOR'' and superobjekttypeid is null ');
      jobbnummer := 21;
      executeScript(jobbnummer, jobnummer_in,
                    'update superobjekt set superobjekttypeid = 3 where so_type = ''ADM_HEND'' and superobjekttypeid is null ');
      jobbnummer := 22;
      executeScript(jobbnummer, jobnummer_in,
                    'update superobjekt set superobjekttypeid = 4 where so_type = ''DAMAGE'' and superobjekttypeid is null ');
      jobbnummer := 23;
      COMMIT;
      jobbnummer := 26;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus24 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(24, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/