create or replace function primus68(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  declare
    nnewHID number;
  begin
    jobbnummer := 1;

    select hidseq.nextval into nnewHID from dual;
    insert into hendelse (hid, signid, dato) values (nnewHID, 1, sysdate);

    -----------------------------------------------------
    --- brukes for oppgradering av tabellen innsprift ---
    --- slik at denne dekker SPECTRUM kravene         ---
    -----------------------------------------------------
    
    --- nye tabeller:
    --- -- innskriftposisjon_l
    --- -- innskriftmetode_l
    --- -- innskriftskriftsystem_l
    --- -- innskrifthovedtype_l
    
    jobbnummer := 2;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      execute immediate 'create table innskriftposisjon_l ( posisjonid NUMBER( 4 )
                                         , posisjon VARCHAR2(40 CHAR)
                                         , merke VARCHAR2(1 CHAR) default (''Y'')
                                         , UUID VARCHAR2(40 CHAR)
                                         , HID_OPPRETTET NUMBER( 8 )
                                         , HID_SLETTET NUMBER( 8 )
                                         , AUTORITET VARCHAR2(10 CHAR)
                                         , AUTORITET_STATUS VARCHAR2(1 CHAR)
                                         , AUTORITET_DATASET VARCHAR2(100 CHAR)
                                         , AUTORITET_KILDE VARCHAR2(100 CHAR)
                                         , FILTERFELT VARCHAR2(5 CHAR)
                                         , CONSTRAINT PK_innskriftposisjon_TYPEID PRIMARY KEY (posisjonid) ENABLE
                                         , CONSTRAINT UK_innskriftposisjon_UUID UNIQUE (UUID)
                                         , CONSTRAINT FK_innskriftposisjon_HIDOPPR FOREIGN KEY (HID_OPPRETTET)
                                              REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                         , CONSTRAINT FK_innskriftposisjon_HIDSLET FOREIGN KEY (HID_SLETTET)
                                              REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                         ) TABLESPACE PRIMUSDT 
        ';
    end if;

    jobbnummer := 3;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 1
                                        , ''bakside''
                                        , ''fdeaf72c-9d1d-40a7-8647-4530fb20a277''
                                        , :newhid)' using nnewHID;
        
        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 2
                                        , ''venstre''
                                        , ''71869de9-bead-449b-93a2-611d1fd80441''
                                        , :newhid)' using nnewHID;

        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 3
                                        , ''høyre''
                                        , ''dc0166b5-1a0d-48fd-ab72-15cf607e1913''
                                        , :newhid)' using nnewHID;
      else
        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 1
                                        , ''baksida''
                                        , ''fdeaf72c-9d1d-40a7-8647-4530fb20a277''
                                        , :newhid)' using nnewHID;

        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 2
                                        , ''vänster''
                                        , ''71869de9-bead-449b-93a2-611d1fd80441''
                                        , :newhid)' using nnewHID;

        execute immediate ' insert into innskriftposisjon_l 
                                        ( posisjonid
                                        , posisjon
                                        , uuid
                                        , hid_opprettet
                                        ) values
                                        ( 3
                                        , ''höger''
                                        , ''dc0166b5-1a0d-48fd-ab72-15cf607e1913''
                                        , :newhid)' using nnewHID;                                        
      end if;
    end if;

    jobbnummer := 4;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      execute immediate '
        create table innskriftmetode_l ( metodeid NUMBER( 4 )
                                       , metode VARCHAR2(40 CHAR)
                                       , merke VARCHAR2(1 CHAR) default (''Y'')
                                       , UUID VARCHAR2(40 CHAR)
                                       , HID_OPPRETTET NUMBER( 8 )
                                       , HID_SLETTET NUMBER( 8 )
                                       , AUTORITET VARCHAR2(10 CHAR)
                                       , AUTORITET_STATUS VARCHAR2(1 CHAR)
                                       , AUTORITET_DATASET VARCHAR2(100 CHAR)
                                       , AUTORITET_KILDE VARCHAR2(100 CHAR)
                                       , FILTERFELT VARCHAR2(5 CHAR)
                                       , CONSTRAINT PK_innskriftmetode_TYPEID PRIMARY KEY (metodeid) ENABLE
                                       , CONSTRAINT UK_innskriftmetode_UUID UNIQUE (UUID)
                                       , CONSTRAINT FK_innskriftmetode_HIDOPPR FOREIGN KEY (HID_OPPRETTET)
                                            REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                       , CONSTRAINT FK_innskriftmetode_HIDSLET FOREIGN KEY (HID_SLETTET)
                                            REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                       ) TABLESPACE PRIMUSDT
        ';
    end if;

    jobbnummer := 5;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'insert into innskriftmetode_l ( metodeid
                                      , metode
                                      , uuid
                                      , hid_opprettet
                                      ) values 
                                      ( 1
                                      , ''malt''
                                      , ''93ed3f5d-3e11-45fe-9c77-19e7d0fcec8a''
                                      , :newHID) ' using nnewHID;

        execute immediate 'insert into innskriftmetode_l ( metodeid
                                      , metode
                                      , uuid
                                      , hid_opprettet
                                      ) values 
                                      ( 2
                                      , ''trykk''
                                      , ''1100e54f-5075-458c-8f9c-d225c2fcbae0''
                                      , :newHID) ' using nnewHID;      
      else
        execute immediate 'insert into innskriftmetode_l ( metodeid
                                      , metode
                                      , uuid
                                      , hid_opprettet
                                      ) values 
                                      ( 1
                                      , ''målad''
                                      , ''93ed3f5d-3e11-45fe-9c77-19e7d0fcec8a''
                                      , :newHID) ' using nnewHID; 
        
        execute immediate 'insert into innskriftmetode_l ( metodeid
                                      , metode
                                      , uuid
                                      , hid_opprettet
                                      ) values 
                                      ( 2
                                      , ''tryck''
                                      , ''1100e54f-5075-458c-8f9c-d225c2fcbae0''
                                      , :newHID) ' using nnewHID;                                       
      end if;
    end if;
    
    jobbnummer := 6;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      ---ISO 15924
      execute immediate '
        create table innskriftskriftsystem_l ( skriftsystemid NUMBER( 4 )
                                             , skriftsystemkode varchar2( 5 char )
                                             , skriftsystem VARCHAR2(40 CHAR)
                                             , merke VARCHAR2(1 CHAR) default (''Y'')
                                             , UUID VARCHAR2(40 CHAR)
                                             , HID_OPPRETTET NUMBER( 8 )
                                             , HID_SLETTET NUMBER( 8 )
                                             , AUTORITET VARCHAR2(10 CHAR)
                                             , AUTORITET_STATUS VARCHAR2(1 CHAR)
                                             , AUTORITET_DATASET VARCHAR2(100 CHAR)
                                             , AUTORITET_KILDE VARCHAR2(100 CHAR)
                                             , FILTERFELT VARCHAR2(5 CHAR)
                                             , CONSTRAINT PK_innskriftskriftsystem_ID PRIMARY KEY (skriftsystemid) ENABLE
                                             , CONSTRAINT UK_innskriftskriftsystem_UUID UNIQUE (UUID)
                                             , CONSTRAINT FK_innskriftskriftsystem_HIDOP FOREIGN KEY (HID_OPPRETTET)
                                                  REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                             , CONSTRAINT FK_innskriftskriftsystem_HIDSL FOREIGN KEY (HID_SLETTET)
                                                  REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                             ) TABLESPACE PRIMUSDT    
        ';
    end if;

    jobbnummer := 7;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then

        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 220
                                                               , ''Cyrl''
                                                               , ''Det kyrilliske alfabetet''
                                                               , ''b81a61c4-e72c-4e45-8b25-de534866e532''
                                                               , :newHID ) ' using nnewHID;

        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 200
                                                               , ''Grek''
                                                               , ''Det greske alfabetet''
                                                               , ''1209360c-9571-4e4c-8917-bdbff1834c7e''
                                                               , :newHID )' using nnewHID;
        
        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 215
                                                               , ''Latn''
                                                               , ''Det latinske alfabetet''
                                                               , ''ca9d649f-a131-43fc-b3c8-2d6075ab02b1''
                                                               , :newHID ) ' using nnewHID;                                                                
                                                                 
      else
        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 220
                                                               , ''Cyrl''
                                                               , ''Kyrilliska alfabetet''
                                                               , ''b81a61c4-e72c-4e45-8b25-de534866e532''
                                                               , :newHID ) ' using nnewHID;
      
      
        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 200
                                                               , ''Grek''
                                                               , ''Grekiska alfabetet''
                                                               , ''1209360c-9571-4e4c-8917-bdbff1834c7e''
                                                               , :newHID ) ' using nnewHID;      

        execute immediate 'insert into innskriftskriftsystem_l ( skriftsystemid
                                                               , skriftsystemkode
                                                               , skriftsystem
                                                               , uuid
                                                               , hid_opprettet 
                                                               ) values 
                                                               ( 215
                                                               , ''Latn''
                                                               , ''Latinska alfabetet''
                                                               , ''ca9d649f-a131-43fc-b3c8-2d6075ab02b1''
                                                               , :newHID ) ' using nnewHID;      

      end if;
    
    end if;
    
    jobbnummer := 8;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      execute immediate '
        create table innskrifthovedtype_l ( hovedtypeid NUMBER( 4 )
                                          , hovedtype VARCHAR2(40 CHAR)
                                          , merke VARCHAR2(1 CHAR) default (''Y'')
                                          , UUID VARCHAR2(40 CHAR)
                                          , HID_OPPRETTET NUMBER( 8 )
                                          , HID_SLETTET NUMBER( 8 )
                                          , AUTORITET VARCHAR2(10 CHAR)
                                          , AUTORITET_STATUS VARCHAR2(1 CHAR)
                                          , AUTORITET_DATASET VARCHAR2(100 CHAR)
                                          , AUTORITET_KILDE VARCHAR2(100 CHAR)
                                          , FILTERFELT VARCHAR2(5 CHAR)
                                          , CONSTRAINT PK_innskrifthovedtype_TYPEID PRIMARY KEY (hovedtypeid) ENABLE
                                          , CONSTRAINT UK_innskrifthovedtype_UUID UNIQUE (UUID)
                                          , CONSTRAINT FK_innskrifthovedtype_HIDOPPR FOREIGN KEY (HID_OPPRETTET)
                                              REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                          , CONSTRAINT FK_innskrifthovedtype_HIDSLET FOREIGN KEY (HID_SLETTET)
                                              REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                          ) TABLESPACE PRIMUSDT 
        ';
    
    end if;
    
    jobbnummer := 9;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      if svenskEllerNorskDatabase = 'NOR' then

        execute immediate 'insert into innskrifthovedtype_l ( hovedtypeid
                                                            , hovedtype
                                                            , uuid
                                                            , hid_opprettet 
                                                            ) values 
                                                            ( 1
                                                            , ''tekst''
                                                            , ''769905a5-d338-426d-a943-19c9ed1870ce''
                                                            , :newhid ) ' 
                                                        using nnewHID;

        execute immediate 'insert into innskrifthovedtype_l ( hovedtypeid
                                                            , hovedtype
                                                            , uuid
                                                            , hid_opprettet 
                                                            ) values 
                                                            ( 2
                                                            , ''merke/mønster''
                                                            , ''45a64e06-47fa-430e-835c-7868f4c26034''
                                                            , :newhid ) ' 
                                                        using nnewHID;
      else
        execute immediate 'insert into innskrifthovedtype_l ( hovedtypeid
                                                            , hovedtype
                                                            , uuid
                                                            , hid_opprettet 
                                                            ) values 
                                                            ( 1
                                                            , ''text''
                                                            , ''769905a5-d338-426d-a943-19c9ed1870ce''
                                                            , :newhid ) ' 
                                                        using nnewHID;

        execute immediate 'insert into innskrifthovedtype_l ( hovedtypeid
                                                            , hovedtype
                                                            , uuid
                                                            , hid_opprettet 
                                                            ) values 
                                                            ( 2
                                                            , ''märke/mönster''
                                                            , ''45a64e06-47fa-430e-835c-7868f4c26034''
                                                            , :newhid ) ' 
                                                        using nnewHID;
      end if;
    
    end if;
    
    jobbnummer := 10;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add 
                                ( jpnr number( 8 ) ) ' );

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add 
                                ( constraint fk_innskrifth_jurperson 
                                             foreign key ( jpnr ) 
                                             references jurperson ( jpnr ) ) ' );
      
    end if;
    
    jobbnummer := 11;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add( datofra date )');

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add( datotil date )');

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add( datokommentar varchar2( 100 char ) )');
    
    end if;
    
    jobbnummer := 12;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
    
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( tolkning varchar2( 4000 char ) )');

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( oversettelse varchar2( 4000 char ) )');
                       
    end if;
    
    jobbnummer := 13;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( spraak number( 10 ) ) ');     

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( constraint fk_innskrifth_spraak foreign key (spraak) references lister ( kode ) ) ');     

    end if;

    jobbnummer := 14;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( metodeid number( 4 ) ) ');     

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( constraint fk_innskrifth_metode foreign key ( metodeid ) references innskriftmetode_l ( metodeid ) ) ');     

    end if;
    
    jobbnummer := 15;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 68, jobbnummer );
      
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( hovedtypeid number( 4 ) ) ');     

      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table innskrift_h add ( constraint fk_innskrifth_hovedtype foreign key ( hovedtypeid ) references innskrifthovedtype_l ( hovedtypeid ) ) ');     

    end if;

    jobbnummer := 100;
    
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus68 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(68, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      