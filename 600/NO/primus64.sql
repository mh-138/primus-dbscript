create or replace function primus64(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table varemerke_h add ( jpnr number( 8 ) )');
    end if;

    jobbnummer := 2;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table varemerke_h add ( constraint fk_varemerkeh_jurperson foreign key ( jpnr ) references jurperson ( jpnr ) )' );
    end if;

    jobbnummer := 3;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table emneord add ( parent_id number( 8 ) default -1 )');
    end if;
    
    jobbnummer := 4;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table emneord add ( m_path varchar2( 100 char ) )');
    end if;
    
    jobbnummer := 5;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table motivord_l add ( parent_id number( 8 ) default -1 )');
    end if;

    jobbnummer := 6;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 64, jobbnummer );
      executeAlterTable( jobbnummer 
                       , jobbnummer_inn
                       , 'alter table motivord_l add ( m_path varchar2( 100 char ) )');
    end if;

    jobbnummer := 7;
    
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus64 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(64, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      