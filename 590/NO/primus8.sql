create or replace function primus8(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
		Overf�rer alle teknikk verdier fra teknikk_h til objekt_teknikk.
	*/
	
	begin
		jobbnummer:=1;
        if jobnummer_in <= jobbnummer then
		  For TH in (Select OBJID
							, HID
							, NR
							, HIERARKISKEID
							, OBJID_SISTE
							, KOMMENTAR 
							From PRIMUS.TEKNIKK_H)
			Loop
				execute immediate '
				Insert into PRIMUS.OBJEKT_TEKNIKK
					( OBJID
					, HID
					, NR
					, OBJID_SISTE
					, HIERARKISKEID
					, KOMMENTAR
					, TYPEID) 
				values
					( :a
					, :b
					, :c
					, :d
					, :e
					, :f
					, 2 ) ' 
					using TH.objid
					    , TH.hid
						, TH.nr
						, TH.OBJID_SISTE
						, TH.HIERARKISKEID
						, TH.KOMMENTAR;
			End Loop;
			commit;
		End if;			
			
		jobbnummer:=2;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus8 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(8, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;		
/
