create or replace function primus10(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is	
 cnt NUMBER;	
begin
	dbms_output.enable;	
	/*
		Oppdaterer dekorteknikk_l med nye mpath verdier fra 
	*/
	
	begin
	  jobbnummer:=1;
      if jobnummer_in <= jobbnummer then                		 
		cnt := 1;
		While cnt >0 
		LOOP   		   
			execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L DL 
							   SET DL.TM_PATH=(SELECT DLL.TM_PATH||DL.NEW_ID||''/''  
							   FROM PRIMUS.DEKORTEKNIKK_L DLL 
								WHERE DLL.ID=DL.PARENT_ID AND DLL.TM_PATH<>''TPATH'') 
								WHERE DL.PARENT_ID<>-1 AND DL.TM_PATH=''TPATH''';
					
			commit;
			execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L DL SET DL.TM_PATH=''TPATH'' WHERE DL.PARENT_ID<>-1 AND DL.TM_PATH IS NULL';          				
			
			commit;			
			EXECUTE IMMEDIATE 'SELECT Count(*) FROM PRIMUS.DEKORTEKNIKK_L WHERE TM_PATH=''TPATH''' INTO cnt;    		    
		
			END LOOP;		 							
	  End if;	
			
	  jobbnummer := 100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus10 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(10, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;		
/
