create or replace function primus6(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
	Funksjonen legger inn den nye tabellen motiv_datering_h.
	Legger inn kolonne hid_motiv_datering i tabellen OBJEKT.
	*/
	
	begin
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then	
		      EXECUTE IMMEDIATE('CREATE TABLE PRIMUS.MOTIV_DATERING_H 
								( OBJID NUMBER (8,0)
								, HID NUMBER (8,0)
								, DATO_FRA DATE
								, DATO_TIL DATE
								, KOMMENTAR VARCHAR2 (500 CHAR)
								, CONSTRAINT PK_MOTIV_DATERING_H_H PRIMARY KEY (OBJID, HID)
								, CONSTRAINT FK_MOTIV_DATERING_H_OBJID FOREIGN KEY (OBJID) REFERENCES PRIMUS.OBJEKT (OBJID)
								, CONSTRAINT FK_MOTIV_DATERING_H_HID FOREIGN KEY (HID) REFERENCES PRIMUS.HENDELSE (HID)
								) tablespace primusdt');
        End if; 								 
		
		jobbnummer := 2;	      
		if jobnummer_in <= jobbnummer then
			execute immediate('grant insert, update on primus.motiv_datering_h to pri_reg');
		end if;
		
		jobbnummer := 3;
		if jobnummer_in <= jobbnummer then		
		EXECUTE IMMEDIATE ('ALTER TABLE PRIMUS.OBJEKT ADD( HID_MOTIV_DATERING NUMBER(8,0),
						    CONSTRAINT FK_OBJEKT_MOTIV_DATERING_HID FOREIGN KEY 
							(OBJID, HID_MOTIV_DATERING) REFERENCES PRIMUS.MOTIV_DATERING_H(OBJID, HID))');  
		End if; 								 
		
		jobbnummer:= 4;	     
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus6 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(6, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/
