create or replace function primus9(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
	Funksjon som gj�r klar dekorteknikk_l tabellen for sammesl�ing med teknikk_l tabellen.
	*/
	
	begin
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then
		 execute immediate 'Update PRIMUS.DEKORTEKNIKK_L Set PARENT_ID=-1 Where PARENT_ID is NULL';
		 commit;
		End if;		
		jobbnummer:=2;
		if jobnummer_in <= jobbnummer then
		 execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L SET NEW_ID=TEKNIKK_LIDSEQ.NEXTVAL WHERE parent_ID=-1';
		 commit;
		end if; 
		jobbnummer:=3;
		if jobnummer_in <= jobbnummer then
		 execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L SET NEW_ID=TEKNIKK_LIDSEQ.NEXTVAL WHERE parent_ID<>-1';
		 commit;
		End if; 
		jobbnummer:=4;
		if jobnummer_in <= jobbnummer then
		 execute immediate 'Update PRIMUS.DEKORTEKNIKK_L Set NEW_PID=-1 , TM_PATH=NEW_ID||''/'' Where PARENT_ID=-1';
		 COMMIT;
		End if; 
		jobbnummer:=5;
		if jobnummer_in <= jobbnummer then
		 execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L DL SET DL.NEW_PID=(SELECT NEW_ID FROM PRIMUS.DEKORTEKNIKK_L DLL WHERE DLL.ID=DL.PARENT_ID AND dll.parent_id=-1) WHERE PARENT_ID<>-1';
		 COMMIT;
		End if; 
		jobbnummer:=6;
		if jobnummer_in <= jobbnummer then 
		 execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L DL SET DL.TM_PATH=(SELECT DLL.TM_PATH||DL.NEW_ID||''/''  FROM PRIMUS.DEKORTEKNIKK_L DLL WHERE DLL.ID=DL.PARENT_ID AND dll.parent_id=-1) WHERE DL.PARENT_ID<>-1 AND DL.NEW_PID IS NOT NULL';
		 COMMIT;
		End if; 
		jobbnummer:=7;
	    if jobnummer_in <= jobbnummer then	
		 execute immediate 'UPDATE PRIMUS.DEKORTEKNIKK_L DL SET DL.TM_PATH=''TPATH'', DL.NEW_PID=(SELECT NEW_ID FROM PRIMUS.DEKORTEKNIKK_L DLL WHERE DLL.ID=DL.PARENT_ID ) WHERE PARENT_ID<>-1 AND DL.NEW_PID IS NULL';
		 COMMIT;
		End if; 
		jobbnummer:=8;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus9 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(9, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;	
/
	