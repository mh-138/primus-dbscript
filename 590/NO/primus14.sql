create or replace function primus14(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
		Legger inn ny XML funksjon for personer.
	*/
	
	begin
		jobbnummer:=1;		
	    if jobnummer_in <= jobbnummer then 
			EXECUTE IMMEDIATE ('create or replace FUNCTION  XML_PERSON(JPNR_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
				BEGIN
				  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(''JP'' AS OBJTYPEID)
				  , XMLFOREST(UT.JPNAVN AS JPNAVN)   
				  , XMLFOREST(UT.NASJONALITET AS NASJONALITET)     
				  , XMLFOREST(UT.KJONN AS KJONN)
				  , XMLFOREST(UT.JPIDENTITY AS JPIDENTITET)
				  , XMLFOREST(UT.AUTORITET AS AUTORITET)  
				  , XMLFOREST(UT.AUTORITET_DATASET AS AUTORITET_DATASET)  
				  , XMLFOREST(UT.AUTORITET_KILDE AS AUTORITET_KILDE)  
				  , XMLFOREST(UT.AUTORITET_STATUS AS AUTORITET_STATUS)  
				  , XMLFOREST(UT.STATUS AS STATUS)    
				  , XMLELEMENT(JPDATO, XMLFOREST(UT.FOEDT_ETABL_AAR as JPFRADATO), 
									 XMLFOREST(UT.DOED_NEDL_AAR AS JPTILDATO))  
				  ,(
					SELECT XMLFOREST(Lst.JURPERSONTYPE AS JPTYPE)
					FROM PRIMUS.JURPERSONTYPE_L LST where LST.typeid=ut.jptypeid
				   )   
				  ,(
					SELECT XMLAGG(XMLELEMENT(JPALTNAVN, XMLFOREST(alt.ALTNAVN AS ALTNAVN), 
														XMLFOREST(alt.KOMMENTAR AS JPKOMM))) 
														FROM PRIMUS.JP_ALTNAVN alt where alt.JPNR=UT.JPNR
				   )
				)
				INTO OBJECTXML FROM PRIMUS.JURPERSON UT WHERE UT.JPNR=JPNR_IN;
				RETURN OBJECTXML;
				End Xml_Person;');
	  End if;		
      jobbnummer:=2;	 
	    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus14 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(14, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/
		