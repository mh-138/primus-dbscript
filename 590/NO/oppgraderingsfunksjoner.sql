CREATE TABLE PRIMUSVERSJON 
   ( VERSJONSNR NUMBER(6,0)
   , STATUS VARCHAR2(10 CHAR)
   , JOBBNR NUMBER(6,0)
   , MELDING VARCHAR2(1000 CHAR)
   , STARTTID TIMESTAMP (6)
   , SLUTTID TIMESTAMP (6)
   , CONSTRAINT PK_PRIMUSOPPGRADERING PRIMARY KEY (VERSJONSNR)
   ) TABLESPACE PRIMUSDT ;

create or replace procedure primuskompilerFunkOgProc
  AUTHID CURRENT_USER
  is
begin
  for ro in (select object_type, object_name from user_objects where status = 'INVALID')
  loop
    begin
      execute immediate 'alter ' || ro.object_type || ' ' || ro.object_name || ' compile';
    exception
      when others then
        SYS.DBMS_OUTPUT.PUT_LINE('Rekopilering feilet for: ' || ro.object_type || ' ' || ro.object_name || ' Feilmelding: ' || SQLERRM);
    end;
  end loop;
end;
/   
   
create or replace procedure PrimusOppgraderingStart(in_versjonsnr in number, in_status in varchar2, in_jobbnr in number, in_melding in varchar2) is
begin
  insert into PRIMUSVERSJON
    (versjonsnr, status, jobbnr, melding, starttid)
  values 
    (in_versjonsnr, in_status, in_jobbnr, in_melding, CURRENT_TIMESTAMP);
    
  commit;
end;
/

create or replace procedure PrimusOppgraderingStopp(in_versjonsnr in number, in_status in varchar2, in_jobbnr in number, in_melding in varchar2) is
begin
  update PRIMUSVERSJON
    set status = in_status
    , jobbnr = in_jobbnr
    , melding = in_melding
    , sluttid = CURRENT_TIMESTAMP
  where versjonsnr = in_versjonsnr;
  
  commit;
end;
/


Create Or Replace 
function SjekkOmOppgraderingSkalKjores(sversjonnr in number, jobbnummer out Number) return number is
  sisteVersjon PRIMUSVERSJON.versjonsnr%type;
  sisteStatus PRIMUSVERSJON.status%type;
  sistejobbnr PRIMUSVERSJON.jobbnr%type;  
begin
  begin
    jobbnummer := 0;
    Select Versjonsnr, Status, Jobbnr Into Sisteversjon, Sistestatus, Sistejobbnr 
	From Primusversjon Where Versjonsnr = (Select Max(Versjonsnr) From Primusversjon);    
  
  if ((sisteVersjon=sversjonnr) and (sisteStatus = 'FEIL')) then 
      Jobbnummer := Sistejobbnr;
	   Return 1;	
  else if ((sisteVersjon=sversjonnr) and (sisteStatus = 'Start')) then      
	  return 1;
  else if (((sisteVersjon + 1)=sversjonnr) and (sisteStatus = 'OK')) then      
	  return 1;	  
  else	  
      return 0;
  end if;
  end if;
  end if;  
  exception
    When No_Data_Found Then
      if sversjonnr = 1 then      
	    return 1;
      end if;
    when others then
      return 0;
  End;
End;
/

create or replace function oppgraderPrimus(fraVersjon in number, tilversjon in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
funknavn varchar2(500 char);
retval varchar2(500 char);
jobbnr number;
currentVnr number;
oppgradering_feilet EXCEPTION;
begin
  begin
    for i in fraVersjon..tilversjon loop 
      currentVnr := i;
      jobbnr := 0;
      
      if SjekkOmOppgraderingSkalKjores(i, jobbnr) = 1 then
         If Jobbnr =0 Then
          Primusoppgraderingstart(I,'Start',Jobbnr,'');
         end if;
        
        funknavn := 'begin :result := primus' || to_char(i) || '(:p1Out, :jobbnr_in); end;';
        sys.dbms_output.put_line(funknavn);
        execute immediate funknavn using out retval, out jobbnr, in jobbnr ;
        
        if retval <> 'OK' then
          raise oppgradering_feilet;
--          sys.dbms_output.put_line('feil - oppgradering stopper');
--          PRIMUSOPPGRADERINGSTOPP(i, retval, jobbnr, 'Oppgradering feilet.');
        else
          PrimusOppgraderingStopp(i, retval, jobbnr, '');
          sys.dbms_output.put_line('Funksjon "Primus' || to_char(i) || ' fullf�rt!');
        end if;
      end if;
      
    end loop;
	
	primuskompilerFunkOgProc;
	primuskompilerFunkOgProc;
	
    return 'OK';
    
  exception
    when others then     
      return 'FEIL!!!';
  end;
end;
/

create or replace procedure executeScript( jobbnummer number, jobnummer_in number, sScript varchar2) is
begin

    if jobnummer_in <= jobbnummer then	
		execute immediate (sScript);
	 End if;    

end;
/


create or replace function svenskEllerNorskDatabase 
	return varchar2 
	AUTHID CURRENT_USER
	IS
	database varchar2(10 char);
begin
	begin
		select verdi into database from environment where upper(variabel) = 'UID';
	
		if substr(lower(database), 0, 2) = 'se' then
			return 'SWE';
		else
			return 'NOR';
		end if;
	exception 	
		when no_data_found then
			return 'NOR';
	end;
end;
/


