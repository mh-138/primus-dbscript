create or replace function primus4(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER 
	is
begin
	dbms_output.enable;
	
	/*
	Funksjonen setter inn nye kolonner i tabellene jurperson, sted, gruppe og dekorteknikk
	*/
	
	begin
		jobbnummer := 1;
		if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE ('alter table PRIMUS.JURPERSON add (STATUS VARCHAR2(50 char), Gruppe NUMBER(6,0), 
	                    constraint FK_JURPERSON_GRUPPE foreign key(GRUPPE) references PRIMUS.GRUPPE(GRUPPEID))');
		End if;
		
		jobbnummer:= 2;	      
		if jobnummer_in <= jobbnummer then		
		EXECUTE IMMEDIATE ('alter table PRIMUS.STED   add (Gruppe NUMBER(6,0), constraint FK_STED_GRUPPE foreign key(GRUPPE) references GRUPPE(GRUPPEID))');
		End if;				
		jobbnummer:= 3;	      
		if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE ('alter table PRIMUS.GRUPPE add (OPT_OBJEKT VARCHAR2(1 char), OPT_AKTOR  VARCHAR2(1 char), OPT_STED   VARCHAR2(1 char))');
		End if;
		
		jobbnummer:= 4;	      
		if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE ('alter table PRIMUS.DEKORTEKNIKK_L add (NEW_ID NUMBER(6,0), NEW_PID NUMBER(6,0), TM_PATH VARCHAR2(100 CHAR) )');  
		End if;
		
		jobbnummer:= 5;
		if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE ('alter table PRIMUS.DEKORTEKNIKK_H add (NEW_ID NUMBER(6,0), NEW_NR NUMBER(3,0))');  
		End if;
		
		jobbnummer:= 6;	      
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus4 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(4, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/
