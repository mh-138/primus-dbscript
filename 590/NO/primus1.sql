create or replace function primus1(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
	
	dbspraak varchar2(3 char);
begin
	dbms_output.enable;
	
	/*
	Forklaring!!!
	*/
	
	begin
	 jobbnummer:=1;
		  if jobnummer_in <= jobbnummer then
		  
			select svenskellernorskdatabase into dbspraak from dual;
			
			if dbspraak = 'NOR' then
			  execute immediate 'create or replace 
				FUNCTION HENTOBJEKTTITTEL (  OBJID IN NUMBER DEFAULT -1 ) RETURN XMLTYPE AS tittelXML XMLTYPE;
				 CURSOR HENT_TITTEL(OBJEKTID NUMBER) IS
				  SELECT TS.nxml FROM ( 
				  select XMLAGG(XMLELEMENT(TITTEL, XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''01'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''NOR''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''02'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''NOB''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''03'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''NNO''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''04'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''ENG''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''05'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''GER''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''06'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''FRE''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''07'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''ITA''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''08'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode = ''LAT''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''09'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode not in (''NOR'',''NOB'', ''NNO'',''ENG'',''GER'',''FRE'',''ITA'',''LAT'') 
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''10'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 1 
				  and ona.kode is null 
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''11'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''NOR''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''12'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''NOB''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''13'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''NNO''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''14'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''ENG''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''15'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''GER''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''16'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''FRE''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''17'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''ITA''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''18'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode = ''LAT''  
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''19'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode not in (''NOR'',''NOB'', ''NNO'',''ENG'',''GER'',''FRE'',''ITA'',''LAT'') 
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''20'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid  
				  and ona.navn is not null and ona.statusid = 7 
				  and ona.kode is null 
				  and ona.statusid =l.statusid
				UNION ALL
				  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''21'' as rekkefolge	
				  from primus.objekt_navn ona, primus.objektnavnstatus_l l
				  where ona.objid_siste = objektid 
				  AND ona.navn is not null 
				  AND ona.kode is null 
				  AND ona.statusid =l.statusid (+) 
				ORDER BY REKKEFOLGE
				) TS where nxml is not NULL AND ROWNUM=1;
				
				BEGIN
				   OPEN HENT_TITTEL(OBJID);  
					FETCH HENT_TITTEL INTO  tittelXML;
				   Close HENT_TITTEL;      
					
				  RETURN tittelXML;
				END HENTOBJEKTTITTEL;';
			elsif dbspraak = 'SWE' then
				execute immediate 'create or replace 
					FUNCTION HENTOBJEKTTITTEL (  OBJID IN NUMBER DEFAULT -1 ) RETURN XMLTYPE AS tittelXML XMLTYPE;
					 CURSOR HENT_TITTEL(OBJEKTID NUMBER) IS
					  SELECT TS.nxml FROM ( 
					  select XMLAGG(XMLELEMENT(TITTEL, XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''01'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''SWE''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''04'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''ENG''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''05'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''GER''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''06'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''FRE''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''07'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''ITA''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''08'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode = ''LAT''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''09'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode not in (''SWE'',''ENG'',''GER'',''FRE'',''ITA'',''LAT'') 
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''10'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 1 
					  and ona.kode is null 
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''11'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''SWE''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''14'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''ENG''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''15'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''GER''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''16'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''FRE''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''17'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''ITA''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''18'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode = ''LAT''  
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''19'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode not in (''SWE'',''ENG'',''GER'',''FRE'',''ITA'',''LAT'') 
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''20'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid  
					  and ona.navn is not null and ona.statusid = 7 
					  and ona.kode is null 
					  and ona.statusid =l.statusid
					UNION ALL
					  select XMLAGG(XMLELEMENT(TITTEL,  XMLFOREST(ona.navn as TITTELNAVN, l.navnstatus as status, ona.kode as kode))) as nxml, ''21'' as rekkefolge	
					  from primus.objekt_navn ona, primus.objektnavnstatus_l l
					  where ona.objid_siste = objektid 
					  AND ona.navn is not null 
					  AND ona.kode is null 
					  AND ona.statusid =l.statusid (+) 
					ORDER BY REKKEFOLGE
					) TS where nxml is not NULL AND ROWNUM=1;
					
					BEGIN
					   OPEN HENT_TITTEL(OBJID);  
						FETCH HENT_TITTEL INTO  tittelXML;
					   Close HENT_TITTEL;      
						
					  RETURN tittelXML;
					END HENTOBJEKTTITTEL;';
					
			end if;
		End if;	
   jobbnummer:= 2;	 
		 if jobnummer_in <= jobbnummer then
		  commit;
		 End if;
   jobbnummer:= 3;	 	 
      return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus1 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(1, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/		