CREATE OR REPLACE FUNCTION primus1(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    BEGIN
      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'INSERT INTO SUPEROBJEKTTYPE_L(SUPEROBJEKTTYPEID, BESKRIVELSE) VALUES(8, ''OPPGAVE'')';
        EXECUTE IMMEDIATE 'INSERT INTO SUPEROBJEKTTYPE_L(SUPEROBJEKTTYPEID, BESKRIVELSE) VALUES(9, ''PROSEDYRE'')';
        COMMIT;
      END IF;
      jobbnummer := 2;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE OR REPLACE TRIGGER SUPEROBJEKTTYPE_SO_TYPE
          BEFORE INSERT ON SUPEROBJEKT   FOR EACH ROW
          DECLARE
             BEGIN
              IF (:NEW.SO_TYPE is NULL) AND (:NEW.SUPEROBJEKTTYPEID is not NULL) THEN
               SELECT CASE
                     WHEN :NEW.SUPEROBJEKTTYPEID = 1 THEN ''Objekt''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 2 THEN ''Actor''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 3 THEN ''ADM_HEND''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 4 THEN ''DAMAGE''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 5 THEN ''BILDE''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 6 THEN ''KOLLI''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 7 THEN ''VIDEO''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 8 THEN ''OPPGAVE''
                     WHEN :NEW.SUPEROBJEKTTYPEID = 9 THEN ''PROSEDYRE''
                     END INTO :NEW.SO_TYPE FROM DUAL;
              ELSIF (:NEW.SO_TYPE IS NOT NULL) AND (:NEW.SUPEROBJEKTTYPEID IS NULL) THEN
               SELECT CASE
                     WHEN :NEW.SO_TYPE =''Objekt''    THEN 1
                     WHEN :NEW.SO_TYPE =''Actor''     THEN 2
                     WHEN :NEW.SO_TYPE =''ADM_HEND''  THEN 3
                     WHEN :NEW.SO_TYPE =''DAMAGE''    THEN 4
                     WHEN :NEW.SO_TYPE =''BILDE''     THEN 5
                     WHEN :NEW.SO_TYPE =''KOLLI''     THEN 6
                     WHEN :NEW.SO_TYPE =''VIDEO''     THEN 7
                     WHEN :NEW.SO_TYPE =''TASK''      THEN 8
                     WHEN :NEW.SO_TYPE =''PROSEDYRE'' THEN 9
                     END INTO :NEW.SUPEROBJEKTTYPEID FROM DUAL;
              END IF;
          END;';
        COMMIT;
      END IF;
      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE PROSEDYRE
        ( OBJID NUMBER(8,0)
        , UUID VARCHAR2(40 CHAR)
        , HID_OPPRETTET NUMBER(8,0)
        , HID_OPPDATERT NUMBER(8,0)
	      , HID_SLETTET NUMBER(8,0)
        , TITTEL VARCHAR2(250 CHAR)
        , BESKRIVELSE VARCHAR(4000 CHAR)
        , HID_INTERVALL NUMBER(8,0)
        , GRUPPE NUMBER(6,0)
        , CONSTRAINT PK_PROSEDYRE PRIMARY KEY (OBJID)
        , CONSTRAINT UK_PROSEDYRE_UUID UNIQUE (UUID)
	      , CONSTRAINT FK_PROSEDYRE_OBJID FOREIGN KEY (OBJID)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_HID_OPPRETTET FOREIGN KEY (HID_OPPRETTET)
	          REFERENCES HENDELSE (HID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_HID_OPPDATERT FOREIGN KEY (HID_OPPDATERT)
	          REFERENCES HENDELSE (HID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_HID_SLETTET FOREIGN KEY (HID_SLETTET)
	          REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_PROSEDYRE_GRUPPE FOREIGN KEY (GRUPPE)
            REFERENCES GRUPPE (GRUPPEID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 4;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE PROSEDYRE_SJEKKLISTE
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
	      , HID NUMBER(8,0) NOT NULL ENABLE
	      , NR NUMBER(7,0) NOT NULL ENABLE
	      , OBJID_SISTE NUMBER(8,0)
	      , BESKRIVELSE VARCHAR2(1000 CHAR)
	      , REKKEFOLGE NUMBER(3,0)
	      , CONSTRAINT PK_PROSEDYRE_SJEKKLISTE PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_PROSEDYRE_SL_OBJID FOREIGN KEY (OBJID)
	          REFERENCES PROSEDYRE (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_SL_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES PROSEDYRE (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_SL_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 5;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE PROSEDYRE_PERSON
        (	OBJID NUMBER(8,0) NOT NULL ENABLE
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(3,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
      	, JP_OBJID NUMBER(8,0)
	      , ROLLEKODE VARCHAR2(5 CHAR) NOT NULL ENABLE
        , ROLLEID NUMBER(8,0) NOT NULL ENABLE
	      , REKKEFOLGE NUMBER(3,0)
        , CONSTRAINT PK_PROSEDYRE_P PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_PROSEDYRE_P_OBJID FOREIGN KEY (OBJID)
	          REFERENCES PROSEDYRE (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_P_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES PROSEDYRE (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_P_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_PROSEDYRE_P_JP_OBJID FOREIGN KEY (JP_OBJID)
            REFERENCES JURPERSON (OBJID) ENABLE
        , CONSTRAINT FK_PROSEDYRE_P_ROLLEID FOREIGN KEY (ROLLEID)
	          REFERENCES ROLLEJP_L (ROLLEID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 6;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE SUPEROBJEKT_PROSEDYRE
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
        , HID NUMBER(8,0)
        , NR NUMBER(3,0)
        , PROSEDYRE_OBJID NUMBER(8,0)
        , CONSTRAINT PK_SO_PROSEDYRE PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_SO_PROSEDYRE_OBJID FOREIGN KEY (OBJID)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_PROSEDYRE_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_PROSEDYRE_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
	      , CONSTRAINT FK_SO_PROSEDYRE_I_OBJID FOREIGN KEY (PROSEDYRE_OBJID)
	          REFERENCES PROSEDYRE (OBJID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 7;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE PROSEDYRE_INTERVALL
        (	OBJID NUMBER(8,0) NOT NULL ENABLE
        , HID NUMBER(8,0) NOT NULL ENABLE
        , INTERVALLTYPEID NUMBER(8,0)
        , INTERVALL NUMBER(2,0)
        , DAGER NUMBER(3,0)
        , MAANEDTYPEID NUMBER(8,0)
        , STARTDATO DATE
        , STOPTYPEID NUMBER(8,0)
        , ANTALL NUMBER(5,0)
        , TELLER NUMBER(5,0)
        , STOPDATO DATE
        , FRIST_DAGER NUMBER(3,0)
        , CONSTRAINT PK_PROSEDYRE_INTERVALL PRIMARY KEY (OBJID, HID)
	      , CONSTRAINT FK_PROSEDYRE_I_OBJID FOREIGN KEY (OBJID)
	          REFERENCES PROSEDYRE (OBJID) ENABLE
	      , CONSTRAINT FK_PROSEDYRE_I_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_PROSEDYRE_I_TYPE_ID FOREIGN KEY (INTERVALLTYPEID)
            REFERENCES INTERVALL_L (TYPEID) ENABLE
        , CONSTRAINT FK_PROSEDYRE_I_STOP_TYPE_ID FOREIGN KEY (STOPTYPEID)
            REFERENCES INTERVALL_L (TYPEID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 8;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'ALTER TABLE PROSEDYRE ADD
        ( CONSTRAINT FK_PROSEDYRE_HID_INTERVALL FOREIGN KEY (OBJID, HID_INTERVALL)
	          REFERENCES PROSEDYRE_INTERVALL (OBJID, HID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 9;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE OPPGAVE2
        ( OBJID NUMBER(8,0)
        , UUID VARCHAR2(40 CHAR)
        , HID_OPPRETTET NUMBER(8,0)
	      , HID_SLETTET NUMBER(8,0)
        , TITTEL VARCHAR2(250 CHAR)
        , BESKRIVELSE VARCHAR2(4000 CHAR)
        , START_DATO DATE
        , FRIST_DATO DATE
        , UTFORT_FRA_DATO DATE
        , UTFORT_TIL_DATO DATE
        , CONSTRAINT PK_OPPGAVE2 PRIMARY KEY (OBJID)
        , CONSTRAINT UK_OPPGAVE2_UUID UNIQUE (UUID)
	      , CONSTRAINT FK_OPPGAVE2_OBJID FOREIGN KEY (OBJID)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_HID_OPPRETTET FOREIGN KEY (HID_OPPRETTET)
	          REFERENCES HENDELSE (HID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_HID_SLETTET FOREIGN KEY (HID_SLETTET)
	          REFERENCES HENDELSE (HID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 10;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE OPPGAVE2_SJEKKLISTE
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
	      , HID NUMBER(8,0) NOT NULL ENABLE
	      , NR NUMBER(7,0) NOT NULL ENABLE
	      , OBJID_SISTE NUMBER(8,0)
        , UTFORT CHAR(1 CHAR) DEFAULT ''X''
	      , BESKRIVELSE VARCHAR2(1000 CHAR)
	      , REKKEFOLGE NUMBER(3,0)
	      , CONSTRAINT PK_OPPGAVE2_S_SJEKKLISTE PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_OPPGAVE2_S_OBJID FOREIGN KEY (OBJID)
	          REFERENCES OPPGAVE2 (OBJID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_S_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES OPPGAVE2 (OBJID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_S_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 11;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE OPPGAVE2_PERSON
        (	OBJID NUMBER(8,0)
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(3,0)
        , OBJID_SISTE NUMBER(8,0)
      	, JP_OBJID NUMBER(8,0)
	      , ROLLEID NUMBER(8,0) NOT NULL ENABLE
	      , REKKEFOLGE NUMBER(3,0)
        , CONSTRAINT PK_OPPGAVE2_P PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_OPPGAVE2_P_OBJID FOREIGN KEY (OBJID)
	          REFERENCES OPPGAVE2 (OBJID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_P_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES OPPGAVE2 (OBJID) ENABLE
	      , CONSTRAINT FK_OPPGAVE2_P_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_OPPGAVE2_P_JP_OBJID FOREIGN KEY (JP_OBJID)
            REFERENCES JURPERSON (OBJID) ENABLE
        , CONSTRAINT FK_OPPGAVE2_P_ROLLEID FOREIGN KEY (ROLLEID)
	          REFERENCES ROLLEJP_L (ROLLEID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 12;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE SUPEROBJEKT_OPPGAVE2
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(3,0) NOT NULL ENABLE
        , OPPGAVE2_OBJID NUMBER(8,0)
        , CONSTRAINT PK_SO_OPPGAVE2 PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_SO_OPPGAVE2_OBJID FOREIGN KEY (OBJID)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_OPPGAVE2_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_OPPGAVE2_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
	      , CONSTRAINT FK_SO_OPPGAVE2_O_OBJID FOREIGN KEY (OPPGAVE2_OBJID)
	          REFERENCES OPPGAVE2 (OBJID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 13;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE SUPEROBJEKT_KOMMENTAR
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(3,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
        , BESKRIVELSE VARCHAR2(4000 CHAR)
        , CONSTRAINT PK_SO_KOMMENTAR PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_SO_KOMMENTAR_OBJID FOREIGN KEY (OBJID)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_KOMMENTAR_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
	          REFERENCES SUPEROBJEKT (OBJID) ENABLE
	      , CONSTRAINT FK_SO_KOMMENTAR_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 14;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE BYGNING_BEHANDLING
        (	OBJID NUMBER(8,0)
	      , CONSTRAINT PK_BYGNING_BEHANDLING PRIMARY KEY (OBJID)
	      , CONSTRAINT FK_BYGNING_BEH_OBJID FOREIGN KEY (OBJID)
	          REFERENCES ADM_HENDELSE (OBJID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 15;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'CREATE TABLE BYGNING_BEHANDLING_DEL
        (	OBJID NUMBER(8,0) NOT NULL ENABLE
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(3,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
        , BYGNINGSDELSTYPEID NUMBER(6,0)
        , BESKRIVELSE VARCHAR2(4000 CHAR)
        , MATERIALE_BESKRIVELSE VARCHAR2(4000 CHAR)
        , KOSTNAD NUMBER(10,2)
        , KOSTNAD_VALUTA_ID NUMBER(8,0)
        , KOSTNAD_BESKRIVELSE VARCHAR2(4000 CHAR)
        , TIDSBRUK_TIMER NUMBER(10,2)
        , REKKEFOLGE NUMBER(3,0)
	      , CONSTRAINT PK_BYGNING_BEHANDLING_DEL PRIMARY KEY (OBJID, HID, NR)
	      , CONSTRAINT FK_BYGNING_BEH_DEL_OBJID FOREIGN KEY (OBJID)
	          REFERENCES BYGNING_BEHANDLING (OBJID) ENABLE
	      , CONSTRAINT FK_BYGNING_BEH_DEL_OBJID_S FOREIGN KEY (OBJID_SISTE)
	          REFERENCES BYGNING_BEHANDLING (OBJID) ENABLE
	      , CONSTRAINT FK_BYGNING_BEH_DEL_HID FOREIGN KEY (HID)
	          REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_BYGNING_BEH_DEL_BDTYPEID FOREIGN KEY (BYGNINGSDELSTYPEID)
	          REFERENCES BYGNINGSDELSTYPE_L (TYPEID) ENABLE
	      , CONSTRAINT FK_BYGNING_BEH_DEL_VALUTAID FOREIGN KEY (KOSTNAD_VALUTA_ID)
	          REFERENCES VALUTA_L (VALUTA_ID) ENABLE)';
        COMMIT;
      END IF;
      jobbnummer := 16;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Ny hendelsestype for bygningsbehandling (Utført bygningsarbeid)
        EXECUTE IMMEDIATE 'INSERT INTO ADM_HENDELSETYPE_L(TYPEID, TEKST) VALUES(24, ''Bygningsbehandling'')';
        COMMIT;
      END IF;
      jobbnummer := 17;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Kopier bygningsoppgave til bygning_behandling
        EXECUTE IMMEDIATE
        'INSERT INTO BYGNING_BEHANDLING(OBJID)
         SELECT AH.OBJID
         FROM ADM_HENDELSE AH
          INNER JOIN OPPGAVE OG ON AH.OBJID = OG.OBJID
          INNER JOIN SUPEROBJEKT SO ON AH.OBJID = SO.OBJID
          INNER JOIN SUPEROBJEKT_status_h SOS ON SO.OBJID = SOS.OBJID AND SO.HID_STATUS = SOS.HID AND SOS.STATUSID = 3
          INNER JOIN OBJECT_ADM_HENDELSE OAH ON AH.OBJID = OAH.ADMH_OBJID
          INNER JOIN OBJECT O ON OAH.OBJID = O.OBJID AND O.OBJTYPEID = 3
        WHERE AH.HOVEDTYPE = 19';
        COMMIT;
      END IF;
      jobbnummer := 18;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Kopiere bygningsoppgave kommentarhendelse til bygning_behandling_del
        EXECUTE IMMEDIATE
        'DECLARE
          PREV_OBJID NUMBER := 0;
          NR NUMBER := 1;
          REKKEFOLGE NUMBER:= 0;
        BEGIN
          FOR REC IN (SELECT AHK.PARENT_ID AS OBJID, AHK.HID_OPPRETTET AS HID, 1 AS NR, AHK.PARENT_ID AS OBJID_SISTE, OG.BYGNINGSDELSTYPEID AS BYGNINGSDELSTYPEID,
              OB.BESKRIVELSE AS BESKRIVELSE, K.MATERIALBRUK AS MATERIALE_BESKRIVELSE,
              AHKOST.KOSTNAD AS KOSTNAD , AHKOST.VALUTA_ID AS KOSTNAD_VALUTA_ID, AHKOST.KOMMENTAR AS KOSTNAD_BESKRIVELSE, AHTID.TIMER AS TIDSBRUK_TIMER
            FROM ADM_HENDELSE AHK
            INNER JOIN KOMMENTAR K ON AHK.OBJID = K.OBJID
            LEFT JOIN OBJ_BESKRIVELSE_H OB ON AHK.OBJID = OB.OBJID AND AHK.HID_BESKRIVELSE = OB.HID
            LEFT JOIN ADM_H_TIDSBRUK AHTID ON AHK.OBJID = AHTID.OBJID AND AHK.HID_TIDSBRUK = AHTID.HID
            LEFT JOIN ADM_H_KOSTNAD AHKOST ON AHK.OBJID = AHKOST.OBJID AND AHK.HID_KOSTNAD = AHKOST.HID
            INNER JOIN OPPGAVE OG ON AHK.PARENT_ID = OG.OBJID
            WHERE AHK.hovedtype = 20 AND AHK.PARENT_ID in
              (SELECT AH.OBJID
               FROM ADM_HENDELSE AH
               INNER JOIN OPPGAVE OG ON AH.OBJID = OG.OBJID
               INNER JOIN SUPEROBJEKT SO ON AH.OBJID = SO.OBJID
               INNER JOIN SUPEROBJEKT_STATUS_H SOS ON SO.OBJID = SOS.OBJID AND SO.HID_STATUS = SOS.HID AND SOS.STATUSID = 3
               INNER JOIN OBJECT_ADM_HENDELSE OAH ON AH.OBJID = OAH.ADMH_OBJID
               INNER JOIN OBJECT O ON OAH.OBJID = O.OBJID AND O.OBJTYPEID = 3
               WHERE AH.HOVEDTYPE = 19)
            ORDER BY AHK.PARENT_ID, AHK.HID_OPPRETTET)
          LOOP
            IF REC.OBJID = PREV_OBJID THEN
              NR := NR + 1;
              REKKEFOLGE := REKKEFOLGE + 1;
            ELSE
              NR := 1;
              REKKEFOLGE := 0;
            END IF;
            PREV_OBJID := REC.OBJID;
            INSERT INTO BYGNING_BEHANDLING_DEL(OBJID, HID, NR, OBJID_SISTE, BYGNINGSDELSTYPEID, BESKRIVELSE, MATERIALE_BESKRIVELSE, KOSTNAD, KOSTNAD_VALUTA_ID, KOSTNAD_BESKRIVELSE, TIDSBRUK_TIMER, REKKEFOLGE)
            VALUES (REC.OBJID, REC.HID, NR, REC.OBJID, REC.BYGNINGSDELSTYPEID, REC.BESKRIVELSE, REC.MATERIALE_BESKRIVELSE, REC.KOSTNAD, REC.KOSTNAD_VALUTA_ID, REC.KOSTNAD_BESKRIVELSE, REC.TIDSBRUK_TIMER, REKKEFOLGE);
           END LOOP;
        END;';
        COMMIT;
      END IF;
      jobbnummer := 19;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Endre hendelsetype for bygningsbehandling fra oppgave
        EXECUTE IMMEDIATE 'UPDATE ADM_HENDELSE SET HOVEDTYPE = 24 WHERE EXISTS (SELECT * FROM BYGNING_BEHANDLING ADM_HENDELSE.OBJID = BYGNING_BEHANDLING.OBJID)';
      COMMIT;
      END IF;
      jobbnummer := 20;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Kopier resterende oppgaver til oppgave2
        EXECUTE IMMEDIATE
        'INSERT INTO OPPGAVE2(OBJID, UUID, HID_OPPRETTET, HID_SLETTET, HID_BESKRIVELSE, START_DATO, FRIST_DATO, UTFORT_FRA_DATO, UTFORT_TIL_DATO)
         SELECT AH.OBJID, AH.UUID, AH.HID_OPPRETTET, AH.HID_SLETTET, AH.HID_BESKRIVELSE, SOD.FRADATO, OG.FRIST, SOD.FRADATO, SOD.TILDATO
         FROM ADM_HENDELSE AH
          INNER JOIN OPPGAVE OG ON AH.OBJID = OG.OBJID
          LEFT JOIN SUPEROBJEKT_DATERING_H SOD ON AH.OBJID = SOD.OBJID AND AH.HID_DATERING = SOD.HID
         WHERE AH.HOVEDTYPE = 19';
        COMMIT;
      END IF;
      jobbnummer := 21;
      IF jobnummer_in <= jobbnummer
      THEN
        -- Kopier oppgavehendelse personer til oppgave2_person
        EXECUTE IMMEDIATE
        'INSERT INTO OPPGAVE2_PERSON(OBJID, HID, NR, OBJID_SISTE, JP_OBJID, ROLLEKODE, REKKEFOLGE)
         SELECT AHP.OBJID, AHP.HID, AHP.NR, AHP.OBJID_SISTE, JP.OBJID, AHP.ROLLEKODE, AHP.REKKEFOLGE
         FROM ADM_HENDELSE_PERSON AHP
          INNER JOIN ADM_HENDELSE AH ON AHP.OBJID = AH.OBJID AND AH.HOVEDTYPE = 19
          INNER JOIN JURPERSON JP ON AHP.JPNR = JP.JPNR';
        COMMIT;
      END IF;
      jobbnummer := 22;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
          -- Kopier admhendelse kommentar til superobjekt_kommentar
        'DECLARE
         PREV_OBJID NUMBER := 0;
         NR NUMBER := 1;
         BEGIN
           FOR REC IN (SELECT AH.PARENT_ID AS OBJID, OB.HID AS HID, AH.PARENT_ID AS OBJID_SISTE, OB.BESKRIVELSE AS BESKRIVELSE
                       FROM ADM_HENDELSE AH
                        LEFT JOIN OBJ_BESKRIVELSE_H OB ON AH.OBJID = OB.OBJID AND AH.HID_BESKRIVELSE = OB.HID
                       WHERE HOVEDTYPE = 20 AND HID_SLETTET IS NULL AND OB.HID IS NOT NULL
                       ORDER BY AH.PARENT_ID, OB.HID)
           LOOP
             IF REC.OBJID = PREV_OBJID THEN
               NR := NR + 1;
             ELSE
               NR := 1;
             END IF;
             PREV_OBJID := REC.OBJID;
             DBMS_OUTPUT.put_line (''OBJID: '' || REC.OBJID || '' HID: '' || REC.HID || '' NR: '' || NR);
             INSERT INTO SUPEROBJEKT_KOMMENTAR(OBJID, HID, NR, OBJID_SISTE, BESKRIVELSE) VALUES(REC.OBJID, REC.HID, NR, REC.OBJID_SISTE, REC.BESKRIVELSE);
           END LOOP;
        END';
        COMMIT;
      END IF;
      jobbnummer := 23;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        -- OPPRETT PROSEDYRE FRA INTERVALL OG OPPGAVE
        'DECLARE
          SEQ NUMBER := 0;
          O_OBJID NUMBER := 0;
          NR NUMBER := 1;
        BEGIN
          FOR REC IN
          (SELECT AH.OBJID AS OBJID, AH.HID_OPPRETTET AS HID, AH.HID_BESKRIVELSE AS HID_BESKRIVELSE, IV.HID AS I_HID, IV.INTERVALLTYPEID AS I_INTERVALLTYPEID, IV.INTERVALL AS I_INTERVALL, IV.DAGER AS I_DAGER, IV.MAANEDTYPEID AS I_MAANEDTYPEID, IV.STARTDATO AS I_STARTDATO, IV.STOPTYPEID AS I_STOPTYPEID, IV.ANTALL AS I_ANTALL, IV.TELLER AS I_TELLER, IV.STOPDATO AS I_STOPDATO
           FROM ADM_HEND_INTERVALL AHI
             INNER JOIN ADM_HENDELSE AH ON AHI.OBJID = AH.OBJID AND AH.HID_SLETTET IS NULL
             INNER JOIN INTERVALL IV ON AHI.INTERVALLID = IV.INTERVALLID AND IV.HID_SLETTET IS NULL
           ORDER BY AH.OBJID)
          LOOP
            IF O_OBJID != REC.OBJID THEN
              -- PROSEDYRE SUPEROBJEKTID
              SELECT OBJIDSEQ.NEXTVAL INTO SEQ FROM DUAL;
              INSERT INTO SUPEROBJEKT(OBJID, SUPEROBJEKTTYPEID) values(SEQ, 9);
              -- PROSEDYRE
              INSERT INTO PROSEDYRE(OBJID, UUID, HID_OPPRETTET, HID_BESKRIVELSE, HID_INTERVAL) VALUES(SEQ, GETORACLEUUID(), REC.HID_OPPRETTET, REC.HID_BESKRIVELSE)
              COMMIT;
              -- KOPIERE ADM_HENDELSE_PERSON TIL PROSEDYRE_PERSON
              INSERT INTO PROSEDYRE_PERSON(OBJID, HID, NR, OBJID_SISTE, JP_OBJID, ROLLEKODE, REKKEFOLGE)
                SELECT AHP.OBJID, AHP.HID, AHP.NR, AHP.OBJID_SISTE, JP.OBJID, AHP.ROLLEKODE, AHP.REKKEFOLGE
                FROM ADM_HENDELSE_PERSON AHP
                  INNER JOIN ADM_HENDELSE AH ON AHP.OBJID = AH.OBJID AND AH.OBJID = REC.OBJID
                  INNER JOIN JURPERSON JP ON AHP.JPNR = JP.JPNR;
              -- PROSEDYRE INTERVALL
              INSERT INTO PROSEDYRE_INTERVALL(OBJID, HID, INTERVALLTYPEID, INTERVALL, DAGER, MAANEDTYPEID, STARTDATO, STOPTYPEID, ANTALL, TELLER, STOPDATO) VALUES(SEQ, REC.I_HID, REC.I_INTERVALLTYPEID, REC.I_INTERVALL, REC.I_DAGER, REC.I_MAANEDTYPEID, REC.I_STARTDATO, REC.I_STOPTYPEID, REC.I_ANTALL, REC.I_TELLER, REC.I_STOPDATO);
              COMMIT;
              -- PROSEDYRE -> PROSEDYRE_INTERVALL RELASJON
              UPDATE PROSEDYRE SET HID_INTERVAL = HID WHERE OBJID = SEQ;
              O_OBJID := REC.OBJID
              NR := 1;
              -- SUPEROBJEKT -> PROSEDYRE RELASJON TODO
              -- INSERT INTO SUPEROBJEKT_PROSEDYRE(OBJID, HID, NR, OBJID_SISTE, INTERVALL_OBJID) VALUES();
            ELSE
              NR := NR + 1;
            END;
            -- PROSEDYRE -> OPPGAVE RELASJON
            INSERT INTO SUPEROBJEKT_OPPGAVE(OBJID, HID, NR, OBJID_SISTE, OPPGAVE_OBJID) VALUES(REC.OBJID, REC.I_HID, NR, REC.OBJID, SEQ);
            -- SUPEROBJEKT -> OPPGAVE RELASJON TODO
            -- INSERT INTO SUPEROBJEKT_OPPGAVE(OBJID, HID, NR, OBJID_SISTE, OPPGAVE_OBJID) VALUES();
          END LOOP;
        END;';
        COMMIT;
      END IF;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      SYS.DBMS_OUTPUT.PUT_LINE('Primus1 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(1, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;

/
